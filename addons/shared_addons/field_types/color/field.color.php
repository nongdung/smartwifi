<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * PyroStreams WYSIWYG Field Type
 *
 * @package		PyroStreams
 * @author		PyroCMS Dev Team
 * @copyright	Copyright (c) 2011 - 2013, PyroCMS
 */
class Field_color
{
	public $field_type_slug			= 'color';
	
	public $db_col_type				= 'text';

	public $admin_display			= 'full';

	public $version					= '1.0';
	
	public $custom_parameters		= array('default_value');

	public $author					= array('name' => 'SmartWifi', 'url'=>'http://smartwifi.vn');
	
	// --------------------------------------------------------------------------

	/**
	 * Event
	 *
	 * Called before the form is built.
	 *
	 * @access	public
	 * @return	void
	 */
	public function event()
	{
		//$this->CI->type->add_misc($this->CI->type->load_view('color', 'color_admin', null));
		$this->CI->type->add_js('color', 'jscolor.js');
	}

	// --------------------------------------------------------------------------

	/**
	 * Output form input
	 *
	 * @param	array
	 * @param	array
	 * @return	string
	 */
	public function form_output($data)
	{

		$options['name'] 	= $data['form_slug'];
		$options['id']		= $data['form_slug'];
		$options['value']	= $data['value'];
		$options['class']   = "color";

		return form_input($options);
	}
	
	public function pre_save($input)
	{
		if (substr($input, 0, 1) != "#") {
			return "#".$input;
		} else {
			$input;
		}
		
	    
	}

}