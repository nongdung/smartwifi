<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="container width-960" id="facebook-callback">
	<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="panel panel-primary">
					<div class="panel-body">			
							<input type="hidden" id="fb-username" name="fb-username" value="<?php echo $settings['wifi_login_account']['login_username'];?>"/>
							<input type="hidden" id="fb-password" name="fb-password" value="<?php echo $settings['wifi_login_account']['login_password'];?>"/>
							<input type="hidden" id="sharing" name="sharing" value="<?php echo $settings['sharing']['val'];?>"/>
							<input type="hidden" id="wait_time" name="wait_time" value="<?php echo $settings['wait_time'];?>"/>
							<input type="hidden" id="location_id" name="location_id" value="<?php echo $location_detail['id'];?>"/>
							<input type="hidden" id="userurl" name="userurl" value="<?php echo $this->session->userdata('userurl');?>"/>
							<input type="hidden" id="login_success_redirect" name="login_success_redirect" value="<?php echo $location_detail['redirect_url'];?>"/>
							<input type="hidden" id="gw_mac" name="gw_mac" value="<?php echo $this->session->userdata('called');?>"/>
							<input type="hidden" id="client_mac" name="client_mac" value="<?php echo $this->session->userdata('mac');?>"/>
							<input type="hidden" id="merchant_id" name="merchant_id" value="<?php echo $location_detail['created_by']['user_id'];?>"/>
							
							<div id="fb-root"></div>			
							
							<div id="fb-step-0" style="display:inline; margin:0 auto;" class="clearfix">
								{{ theme:image file="wait.gif" width="30" height="30" alt="Please wait" style="margin: 0 auto;" class="img-rounded center-block" }}
							</div>
							
							<div class="form-group" id="fb-step-1" style="display:none; margin-top:10px;">
								<div class="text-center text-info">
									<div id="fb-login-msg"></div>
									<div id="clock"></div>
									
									{{ if settings:sharing:val == "Enable" }}
									<div id="btn-connect-fb" style="margin-top:5px;margin-bottom:10px;display:none;">
										<a class="btn btn-primary btn-responsive" onClick="swFacebook.Share(914114941962202);">Checkin & Connect</a>
										<br/>
										<label style="margin-top:0px;"><input id="checkin" type="checkbox" name="checkin" value="1" checked><i>Check-in</i></label>
										<br/>
									</div>
									{{ endif }}
														
									<div class="fb-page" data-width="360" data-href="https://www.facebook.com/<?php echo $settings['page_id'];?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"></div>			
									<br/>
									<div id="statusPage" style="display:none;">			
										<div class="text-center">
											Well done, you are now connected to Internet.
												
											<div id="originalURL"></div><br/>
											<span id="statusMessage"></span>
											<div class="text-center" style="padding-bottom:5px;">				
												<a href="http://1.0.0.0" class="btn btn-xs btn-warning btn-responsive">Wifi Logout</a>
											</div>
										</div>	
									</div>
								</div>
							</div>				
							<div id="waitPage-FB" style="display:none; margin:auto;" class="clearfix">
								{{ theme:image file="wait.gif" width="30" height="30" alt="Please wait" style="margin: 0 auto;" class="img-rounded center-block" }}
							</div>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
	</div>
</div>
<script type="text/javascript">
	var swUser = {};
	//swController.onUpdate = SmartWifi.updateUI;
	swController.onError  = SmartWifi.handleError;
	
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '<?php echo $fb_settings['app_id'];?>',
			cookie     : true,
			xfbml      : true,
			version    : 'v2.4'
		});
		FB.getLoginStatus(swFacebook.updateButton);
		FB.Event.subscribe('edge.create', swFacebook.LikeConnect);
		FB.Event.subscribe('comment.create', swFacebook.CommentConnect);		
	};

	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	var postObj = {
		"enable":"<?php echo $settings['sharing']['val'];?>",
		"pageid":"<?php echo $settings['page_id'];?>",
		"name":"<?php echo $settings['post_name'];?>",
		"link":"<?php echo $settings['post_link'];?>",
		"caption":"<?php echo $settings['post_caption'];?>",
		"desc":"<?php echo $settings['post_desc'];?>",
		"pic":"<?php echo $settings['post_picture'];?>",
		"place":"<?php echo $settings['post_location'];?>"
	};

	var $clock = $('#clock')
		.on('update.countdown', function(event) {
			var format = '%H:%M:%S';
			var time, tt, sec;
			time=event.strftime(format);
			tt=time.split(":");
			sec=tt[0]*3600+tt[1]*60+tt[2]*1;
			$(this).html("(Please wait) Bạn sẽ được đăng nhập Wifi trong: " + sec +"s");      
		})
		.on('finish.countdown', function(event) {
			if (swController.clientState == 1) {
				swController.onUpdate = function(cmd){
					swFacebook.Connect();
				};
				swController.logoff();
			} else {
				swFacebook.Connect();
			}			
    		
    		setTimeout(function() {
    			if (swController.clientState != 1) {
    				$('#fb-login-msg').html('<p class="text-danger">Không thể đăng nhập wifi. Thử lại tại <a href="http://1.0.0.0">đây</a></p>');
    			}
			},5000);   		
			$(this).html('');
		});
	
	$(document).ready(function(){
		$.ajaxSetup({
        	data: {
            	csrf_hash_name: "<?php echo $this->security->get_csrf_hash(); ?>"
        	}
    	});
    	
		$.post( "husers/apis/get_client", {client_mac : "<?php echo $this->session->userdata('mac');?>"}, function( jsonp ) {
			if (jsonp && !jsonp.error) {
				swUser = jsonp;
				if (jsonp.name) {
					$('#c-name').html(jsonp.name);
				} else {
					$('#c-name').html("bạn" + "<?php echo $this->security->get_csrf_hash(); ?> ")
				}
				if (!jsonp.profile_id) {
					//Create anonymous profile
					userdata = {
						'client_mac' : "<?php echo $this->session->userdata('mac')?>",										
					};
								
					$.post( "husers/apis/create_anonymous_profile", userdata, function( response ) {
						swUser = response;
						console.log(JSON.stringify(response));
					});
				}
				
			} else {
				$('#c-name').html("Error:" + jsonp.error.message);
			}
		});
	}); //end of document ready
</script>
