<section id="login">
	<div class="row-fluid">
		<h2><?php echo lang('user_reset_password_title');?></h2>
		
		<div class="row-fluid">
			<?php echo form_open('users/reset_pass', array('id'=>'reset-pass', 'class' => 'crud_form')); ?>
			<div class="span10 offset1 form-horizontal well">
				<fieldset>
	        		<legend><?php echo lang('user:reset_instructions'); ?></legend>
	        		
	        		<?php if (validation_errors()): ?>		      
					<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
						&times;
					</button>
					<?php echo validation_errors(); ?>
					</div>
					<?php endif; ?>
	        		
	        		<div class="control-group">
						<label class="control-label" for="email"><?php echo lang('global:email') ?></label>
					    <div class="controls">
					      	<input type="text" name="email" maxlength="100" value="<?php echo set_value('email'); ?>" />
					    </div>
					</div>
					<div class="control-group">
						<label class="control-label" for="username"><?php echo lang('user:username') ?></label>
					    <div class="controls">
					      	<input type="text" name="user_name" maxlength="40" value="<?php echo set_value('user_name'); ?>" />
					    </div>
					</div>
					
					<div class="control-group">
						<label class="control-label" for="username"></label>
						<?php echo $recaptcha_html; ?>
					</div>
					
	        		<div class="form-actions" style="margin-top: 10px;">
	            		<?php echo form_submit('btnSubmit', lang('user:reset_pass_btn'), 'class="btn btn-primary"') ?>
	          		</div>
	        	</fieldset>
			</div>
		<?php echo form_close(); ?>
		</div>
	</div>
</section>