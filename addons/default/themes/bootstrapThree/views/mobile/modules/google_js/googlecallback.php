<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="container" id="google-callback">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-body">		
						<input type="hidden" id="gg-username" name="gg-username" value="<?php echo $settings['wifi_login_account']['login_username'];?>"/>
						<input type="hidden" id="gg-password" name="gg-password" value="<?php echo $settings['wifi_login_account']['login_password'];?>"/>
						<input type="hidden" id="location_id" name="location_id" value="<?php echo $location_detail['id'];?>"/>
						<input type="hidden" id="userurl" name="userurl" value="<?php echo $this->session->userdata('userurl');?>"/>
						<input type="hidden" id="login_success_redirect" name="login_success_redirect" value="<?php echo $location_detail['redirect_url'];?>"/>
						<input type="hidden" id="wait_time" name="wait_time" value="<?php echo $settings['wait_time'];?>"/>
						<input type="hidden" id="gw_mac" name="gw_mac" value="<?php echo $this->session->userdata('called');?>"/>
						<input type="hidden" id="client_mac" name="client_mac" value="<?php echo $this->session->userdata('mac');?>"/>
						<input type="hidden" id="merchant_id" name="merchant_id" value="<?php echo $location_detail['created_by']['user_id'];?>"/>
						
						<div class="text-center">
							<div id="content"></div>
							<span id="clock"></span>
						</div>
						<br/>
						<div id="statusPage" style="display:none;">			
									<div class="text-center">
										Well done, you are now connected to Internet.
											
										<div id="originalURL"></div><br/>
										<span id="statusMessage"></span>
										<div class="text-center" style="padding-bottom:5px;">				
											<a href="http://1.0.0.0" class="btn btn-xs btn-warning btn-responsive">Wifi Logout</a>
										</div>
									</div>	
						</div>
						<div id="sw-gg-wait" style="display:inline; margin:0 auto;" class="clearfix">
							{{ theme:image file="wait.gif" width="30" height="30" alt="Please wait" style="margin: 0 auto;" class="img-rounded center-block" }}
						</div>
				</div>
			</div>
		</div>
	</div>
</div>	
<script type="text/javascript">
	var swUser = {};
	//swController.onUpdate = SmartWifi.updateUI;
	swController.onError  = SmartWifi.handleError;
	swGoogle.clock = $('#clock')
		.on('update.countdown', function(event) {
			var format = '%H:%M:%S';
			var time, tt, sec;
			time=event.strftime(format);
			tt=time.split(":");
			sec=tt[0]*3600+tt[1]*60+tt[2]*1;
			$(this).html("(Please wait) Bạn sẽ được đăng nhập Wifi trong: " + sec +"s");      
		})
		.on('finish.countdown', function(event) {
			if (swController.clientState == 1) {
				swController.onUpdate = function(cmd){
					swGoogle.ggConnect(); 
				};
				swController.logoff();
			} else {
				swGoogle.ggConnect(); 
			}			
    		  		
    		$('#sw-gg-wait').hide();
			$(this).html('');
			setTimeout(function() {
    			if (swController.clientState != 1) {
    				$('#content').html('<p class="text-danger">Không thể đăng nhập wifi. Thử lại tại <a href="http://1.0.0.0">đây</a></p>');
    			}
			},5000);
		});

	swGoogle.clientId = '<?php echo $gg_settings['client_id'];?>';
	swGoogle.apiKey = '<?php echo $gg_settings['api_key'];?>';
	<?php if ($gg_settings['plus_login']):?>
	swGoogle.scopes = 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/plus.login';
	<?php else:?>
	swGoogle.scopes = 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile';
	<?php endif;?>
	
	
	$(document).ready(function(){
		$.ajaxSetup({
        	data: {
            	csrf_hash_name: "<?php echo $this->security->get_csrf_hash(); ?>"
        	}
    	});
    	
    	$.post( "husers/apis/get_client", {client_mac : "<?php echo $this->session->userdata('mac');?>"}, function( jsonp ) {
			if (jsonp && !jsonp.error) {
				swUser = jsonp;
				if (jsonp.name) {
					$('#c-name').html(jsonp.name);
				} else {
					$('#c-name').html("bạn" + "<?php echo $this->security->get_csrf_hash(); ?> ")
				}
				if (!jsonp.profile_id) {
					//Create anonymous profile
					userdata = {
						'client_mac' : "<?php echo $this->session->userdata('mac')?>",										
					};
								
					$.post( "husers/apis/create_profile", userdata, function( response ) {
						swUser = response;
						console.log(JSON.stringify(response));
					});
				}
				
			} else {
				$('#c-name').html("Error:" + jsonp.error.message);
			}
		});
	}); //end of document ready
	
</script>
<script src="https://apis.google.com/js/client.js?onload=GoogleClientLoad" async defer></script>
