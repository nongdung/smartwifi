<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_fields extends Admin_Controller
{
	protected $section = 'click_fields';
	protected $_stream = 'settings';
	protected $_namespace = 'click';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->driver('Streams');
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

	public function index()
	{
		//echo $this->module;exit;
		$buttons = array(
			array(
				'url'		=> 'admin/' . $this->module . '/fields/edit/-assign_id-', 
				'label'		=> $this->lang->line('global:edit')
			),
			array(
				'url'		=> 'admin/' . $this->module . '/fields/delete/-assign_id-',
				'label'		=> $this->lang->line('global:delete'),
				'confirm'	=> true,
				'locked'	=> TRUE
			)
		);
		$this->streams->cp->assignments_table(
								$this->_stream,
								$this->_namespace,
								25,
								'admin/' . $this->module . '/fields',
								TRUE,
								array('buttons' => $buttons));
	}

	function edit($assign_id)
	{
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
		if ( ! $assign_id )
		{
			show_error(lang('streams:cannot_find_assign'));
		}

		$extra = array(
			'title'			=> lang('streams:edit_field'),
			'show_cancel'	=> true,
			'cancel_uri'	=> 'admin/' . $this->module . '/fields'
		);

		$this->streams->cp->field_form($this->_stream, $this->_namespace, 'edit', 'admin/' . $this->module . '/fields', $assign_id, array(), true, $extra);
	}
	
	public function delete($assign_id = null)
	{
		if ( ! $assign_id)
		{
			show_error(lang('streams:cannot_find_assign'));
		}
	
		// Tear down the assignment
		if ( ! $this->streams->cp->teardown_assignment_field($assign_id))
		{
		    $this->session->set_flashdata('notice', lang('streams:field_delete_error'));
		}
		else
		{
		    $this->session->set_flashdata('success', lang('streams:field_delete_success'));			
		}
	
		redirect('admin/' . $this->module . '/fields');
	}
	
	public function create()
	{
		$extra['title'] 		= lang('streams:add_field');
		$extra['show_cancel'] 	= true;
		$extra['cancel_uri'] 	= 'admin/' . $this->module . '/fields';

		$this->streams->cp->field_form($this->_stream, $this->_namespace, 'new', 'admin/' . $this->module . '/fields', null, array(), true, $extra);
	}

}