<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin extends Admin_Controller
{
	protected $section = 'wificlick';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('wificlick');
		$this->load->driver('Streams');
		role_or_die('hotspot', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all billing plans
     */
    public function index()
    {		
		redirect('admin/splash/methods');
    }
    

	public function settings($loc_id ='')
	{
		//Check loc_id
		$loc_detail = $this->streams->entries->get_entry($loc_id,'list','location');
		if (!$loc_detail && ($loc_detail->created_by_user_id <> $this->current_user->id)) {
			$this->session->set_flashdata('error', "This location is not yours");
			redirect('admin/location');
		}
		$this->load->model('click_m');
		
		$settings = $this->click_m->get_settings($loc_id);
		//print_r($settings);exit;
		$extra = array(
		    'return'            => 'admin/splash/methods',
		    'success_message'   => "Updated",
		    'failure_message'   => "Error...!",
		    'title'             => lang('wificlick:settings')
		);
		$hidden = array('loc_id');
		$defaults = array('loc_id' => $loc_id);
		
		if (! empty($settings)) {
			//delete cache
			$this->pyrocache->delete_cache("click_m", "get_settings", array($loc_id));
			
			//Update
			$this->streams->cp->entry_form('settings', 'click', 'edit', $settings['id'], true, $extra, $skips = array('loc_id'));
		} else {
			//Add new
			$this->streams->cp->entry_form('settings', 'click', 'new',null, true, $extra, array(),false,$hidden, $defaults);
		}

	}

}