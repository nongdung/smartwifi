<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Click_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		
	}
	
	public function get_settings($location_id)
	{
		$this->load->driver('Streams');
		
		$params = array(
		    'stream'    => 'settings',
		    'namespace' => 'click',
		    'limit'	=> 1,
		    'where'		=> "`loc_id` = '".$location_id."'"
		);
		
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			return $entries['entries'][0];
		} else {
			return array();
		}
		
		
	}
	
	public function get_html($location_id, $device_type, $settings = null, $userdata_key = null)
	{
		if ($userdata_key) {
			$state = $userdata_key;
		} else {
			$state = 0;
		}
		
		//$settings = $this->pyrocache->model('click_m', 'get_settings', array($location_id), 30);
		$link_tmp = "click/login/".$location_id;
		if ($device_type) {
			$link_tmp .= "/".$device_type;
		}
		if ($state) $link_tmp .= "?state=".$state;
		$login_url = "<a class='wifi-login-link' href='".$link_tmp."'>". $settings['caption']."</a>";
		$data = array(
			'html' => $login_url,
			'link' => $link_tmp,
			'heading' => $settings['heading'],
			'caption' => $settings['caption'],
			'settings' => $settings,
		);		
		return $data;
	}
	
	public function check_status($location_id)
	{
		$this->load->driver('Streams');
		
		$params = array(
		    'stream'    => 'settings',
		    'namespace' => 'click',
		    'where'		=> "`loc_id` = '".$location_id."' AND `status` = 'Enable'"
		);
		
		$entries = $this->streams->entries->get_entries($params);
		return $entries['total'];
		
	}

}
