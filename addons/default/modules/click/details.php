<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Click extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Wifi Login by Click'
			),
			'description' => array(
				'en' => 'Wifi Click Login Module'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',		
		);
		
		if (group_has_role('hotspot', 'admin')) {
			$info['sections']['click_fields'] = array(
					'name' 	=> 'Settings Fields', // These are translated from your language file
					'uri' 	=> 'admin/click/fields',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'global:add',
							'uri' 	=> 'admin/click/fields/create',
							'class' => 'add'
							)
					)
			);			
		}
									
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		
		$this->db->delete('settings', array('module' => 'click'));
		//add stream
		if ( ! $this->streams->streams->add_stream('Click Login Settings', 'settings', 'click', 'click_', null)) return false;
		
		
		//Get account stream ID	
		$account_stream = $this->streams->streams->get_stream('accounts', 'wifi_accounts');
		$loc_stream = $this->streams->streams->get_stream('list', 'location');

		$choice = 'Enable
		Disable';
		
		$fields = array(
			array(
                'name' => 'Status',
                'slug' => 'status',
                'namespace' => 'click',
                'type' => 'choice',
                'extra'         => array('default_value' => 'Enable','choice_data' => $choice, 'choice_type' => 'dropdown'),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Wifi Login Account',
                'slug' => 'wifi_login_account',
                'namespace' => 'click',
                'type' => 'relationship',
                'extra' => array('choose_stream' => $account_stream->id),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Heading',
                'slug' => 'heading',
                'namespace' => 'click',
                'type' => 'text',
                'extra' => array('max_length' => 200,'default_value' => "-- Or --"),
                'assign' => 'settings'
            ),
            array(
                'name' => 'Caption',
                'slug' => 'caption',
                'namespace' => 'click',
                'type' => 'text',
                'extra' => array('max_length' => 64,'default_value' => "Wifi Login"),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Position',
                'slug' => 'position',
                'namespace' => 'click',
                'type' => 'integer',
                'extra' => array('max_length' => 1,'default_value' => 1),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Wait Time',
                'slug' => 'wait_time',
                'namespace' => 'click',
                'type' => 'integer',
                'extra' => array('max_length' => 2,'default_value' => 5),
                'assign' => 'settings',
            ),
            array(
                'name' => 'Button Color',
                'slug' => 'btn_color',
                'namespace' => 'click',
                'type' => 'color',
                'extra' => array('default_value' => "#4862a3"),
                'assign' => 'settings',
            ),
            array(
                'name' => 'Location ID',
                'slug' => 'loc_id',
                'namespace' => 'click',
                'type' => 'relationship',
                'extra' => array('choose_stream' => $loc_stream->id),
                'assign' => 'settings',
                'unique' => TRUE,
                'required' => true
            ),

		);

        $this->streams->fields->add_fields($fields);		

        $this->streams->streams->update_stream('settings', 'click', array(
            'view_options' => array(
            	'status',
                'wifi_login_account',
                'position',
                'wait_time',
                'css_class',
                'loc_id'
            )
        ));
		
		$entry_data = array(
		    'mod_name' => "Click Login",
		    'mod_slug'   => "click",
		    'mod_desc'   => "Wifi Login by Click",		    
		);
		$this->streams->entries->insert_entry($entry_data, 'modules', 'login_modules');
		
		return TRUE;
		
	}

	public function uninstall()
	{
		$this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
		$this->streams->utilities->remove_namespace('click');
		$this->db->delete('settings', array('module' => 'click'));
		
		$params = array(
		    'stream'    => 'modules',
		    'namespace' => 'login_modules',
		    'where' 	=> "mod_slug = 'click'"
		);		
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			foreach ($entries['entries'] as $entry) {
				$this->streams->entries->delete_entry($entry["id"], "modules", "login_modules");
			}
		}		
		
		return TRUE;
	}


	public function upgrade($old_version)
	{
		$fields = array(
            array(
                'name' => 'Button Color',
                'slug' => 'btn_color',
                'namespace' => 'click',
                'type' => 'color',
                'extra' => array('default_value' => "#4862a3"),
                'assign' => 'settings',
            ),
		);

        $this->streams->fields->add_fields($fields);
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
    
    
    public function admin_menu(&$menu)
    {	
		//if (group_has_role('hotspot', 'merchant')) {
            
		//$menu['Methods']['Login with Facebook']   = 'admin/facebook';
        //}		
		//add_admin_menu_place('Methods', 2);
		//$menu['Methods']['Login by Click'] = 'admin/click';
		//add_admin_menu_place('Methods', 2);
	}
}
/* End of file details.php */
