<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	www.your-site.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://www.codeigniter.com/user_guide/general/routing.html
*/

$route['theme_default/admin/fields(/:num)?']			= 'admin_fields/index$1';
$route['theme_default/admin/fields(/:any)?']			= 'admin_fields$1';

$route['theme_default/admin/slider(/:num)?']			= 'admin_slider/index$1';
$route['theme_default/admin/slider(/:any)?']			= 'admin_slider$1';
/*
$route['theme_default/admin/desktop(/:num)?']			= 'admin_desktop/index$1';
$route['theme_default/admin/desktop(/:any)?']			= 'admin_desktop$1';

$route['theme_default/admin/mobile(/:num)?']			= 'admin_mobile/index$1';
$route['theme_default/admin/mobile(/:any)?']			= 'admin_mobile$1';

*/

$route['theme_default/admin(/:num)?']			= 'admin/index$1';
// front-end
//$route['facebook(/:num)?']			= 'landing/index$1';
//$route['facebook(/:any)?']			= 'landing$1';