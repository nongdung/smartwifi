<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_slider extends Admin_Controller
{
	protected $section = 'Slider';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('theme_default');
		$this->load->driver('Streams');
		role_or_die('hotspot', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all billing plans
     */
    public function index($location_id = null)
    {
    	$this->load->model('location/location_m');
    	if (! $this->location_m->checkLocationOwner($location_id,$this->current_user->id)) {
			$this->session->set_flashdata('notice', "You don't have right to edit that location");
			redirect('admin/location');
		}
		
    	$params = array(
		    'stream'    => 'theme_default',
		    'namespace' => 'theme_default',
		    'where'		=> "`created_by` ='".$this->current_user->id."' AND `location_id` = '".$location_id."'"
		);
		
		$entries = $this->streams->entries->get_entries($params);
		
		$location_detail = $this->location_m->get_detail($location_id);
    	$this->template
    				->set('title', lang('theme_default:overview'))
					->set('entries', $entries['entries'])
					->set('location_detail',$location_detail)
    				->build('admin/overview');
    }
	
	function edit($location_id = null)
    {
    	$this->load->model('location/location_m');
		$this->load->model('theme_default_m');
    	$location_id OR redirect('admin');
		
    	if (! $this->location_m->checkLocationOwner($location_id, $this->current_user->id)) {
			redirect('admin');
		}
		
		//$theme_detail = $theme = $this->theme_default_m->get_theme_detail($this->current_user->id, $location_id);
		$theme_detail = $this->theme_default_m->get($location_id);
		//print_r($theme_detail);exit;
		
    	$stream = $this->streams->streams->get_stream('theme_default', 'theme_default');
		$stream_fields = $this->streams_m->get_stream_fields($stream->id, $stream->stream_namespace);
		
		$values = $this->fields->set_values($stream_fields, $theme_detail, 'edit');
		$fields = $this->streams->fields->get_stream_fields($stream->stream_slug, $stream->stream_namespace, $values, $theme_detail->id);
		//print_r($values);exit;
		foreach ($fields as $key => $field) {
			if (substr($field['field_slug'], 0,5) != 'slide') {
				unset($fields[$key]);
			}
		}
		
			
		//Validation
		$field_validations = $this->streams->streams->validation_array($stream->stream_slug, $stream->stream_namespace, 'edit');
		
		foreach ($field_validations as $key => $rule) {
			if (substr($rule['field'], 0,5) != 'slide') {
				unset($field_validations[$key]);
			}
		}
		
		// Run stream field events
		$this->fields->run_field_events($stream_fields, array(), $values);
		
		
		$this->form_validation->set_rules($field_validations);
		
		if ($this->form_validation->run()){
			
			$this->streams->entries->update_entry($theme_detail->id, $_POST, $stream->id, $stream->stream_namespace);
			redirect('admin/theme_default/'.$location_id);
		}
		
		if (isset($theme_detail) && $theme_detail) {
			//edit
			$location_detail = $this->location_m->get_detail($location_id);
			$this->template
					->set('location_detail', $location_detail)
					->set('fields', $fields)
					->build("admin/slider/form");
		} else {
			//add new
			$this->session->set_flashdata('error', lang('proser:add_general_fields_first'));
			redirect('admin/theme_default/edit/'. $location_id);
		}
		
		
		
		/*
		print_r($fields);exit;
    	//$this->load->model('theme_fields_m');
    	//$skip_fields = $this->theme_fields_m->get_skip_fields('general_fields');
		//print_r($skip_fields);exit;
		//Get theme id
		$params = array(
		    'stream'    => 'themes',
		    'namespace' => 'splash',
		    'where'		=> "`stream_slug` ='theme_default' AND `namespace_slug` = 'theme_default'"
		);		
		$theme = $this->streams->entries->get_entries($params);
		if (! $theme['total']) {
			redirect("admin");
		}

		$skip_fields=array();
    	$params = array(
		    'stream'    => 'theme_default',
		    'namespace' => 'theme_default',
		    'where'		=> "`created_by` ='".$this->current_user->id."' AND `location_id` = '".$location_id."'"
		);		
		$entries = $this->streams->entries->get_entries($params);
		//print_r($entries);exit;
		$extra = array(
			    'return'            => 'admin/theme_default/'.$location_id,
			    'success_message'   => lang('hotspot:submit_success'),
			    'failure_message'   => lang('hotspot:submit_error'),
			    'title'             => 'Edit Fields'
		);
		
		$tabs = array(
		    array(
		        'title'     => "General fields",
		        'id'        => 'general-tab',
		        'fields'    => explode(",","location_id,".Settings::get('general_fields'))
		    ),
		    array(
		        'title'     => "Desktop fields",
		        'id'        => 'desktop-tab',
		        'fields'    => explode(",",Settings::get('desktop_fields'))
		    ),
		    array(
		        'title'     => "Mobile fields",
		        'id'        => 'mobile-tab',
		        'fields'    => explode(",",Settings::get('mobile_fields'))
		    ),
		    array(
		        'title'     => "Custom CSS",
		        'id'        => 'css-tab',
		        'fields'    => array("custom_css")
		    )
		);
		$hiddens = array('location_id');
		$defaults = array('location_id' => $location_id);
		
		if ($entries['total']) {
			//delete cached
			$this->pyrocache->delete_cache("hotspot_theme_m", "getThemeDetail", array($theme['entries'][0]['id'], $location_id));
			//Edit
			$this->streams->cp->entry_form('theme_default', 'theme_default', 'edit', $entries['entries'][0]['id'], true, $extra, $skip_fields, $tabs,$hiddens, $defaults);
			
			
		} else {
			//add new
			$this->streams->cp->entry_form('theme_default', 'theme_default', 'new', null, true, $extra, $skip_fields, $tabs,$hiddens, $defaults);
		}
		
		*/		
		//$this->template->build('admin/general');
    }  

}