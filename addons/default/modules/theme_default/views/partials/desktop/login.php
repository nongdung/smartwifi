<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<!-- Header -->
    <a name="about"></a>
    <div class="intro-header" style="background:linear-gradient(rgba(0,0,0,.5), rgba(0,0,0,.5)), url({{ theme_detail:fullscreen_desktop_image:image }}) no-repeat center center; background-size: cover;">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                    	{{ if exists theme_detail:header_text and theme_detail:header_text != "" }}
							<span id="c-name">
								<?php if (!empty($client_info)) echo "Hi ". $client_info['name']; ?>
							</span>		
							{{ theme_detail:header_text }}	
						{{ endif }}
                        
                        <hr class="intro-divider">

						<div class="row wifi-login-area">
							<?php foreach ($wifi_method_position as $method => $value) :?>
									<?php if ($method == 'youtube'):?>
										<div class="col-xs-12 col-sm-12 col-md-12">
										<?php echo $wifi_login_html[$method]['html']; ?>
										</div>
									<?php endif;?>
							<?php endforeach;?>
									<div class="col-xs-2 col-sm-3 col-md-4">&nbsp;</div>
									<div class="col-xs-8 col-sm-6 col-md-4">				
										<?php foreach ($wifi_method_position as $method => $value) :?>
											<?php if ($method == 'modpro'):?>
												<?php echo $wifi_login_html[$method]['html']; ?>
											<?php endif;?>
											
											<?php if ($method == 'facebook_js'):?>
												<a href="<?php echo $wifi_login_html[$method]['link']; ?>" class="login-link btn btn-block btn-facebook azm-social azm-btn azm-facebook">
													<i class="fa fa-facebook fa-55 pull-left"></i>
													<?php echo $wifi_login_html[$method]['caption']; ?>
												</a>
											<?php endif;?>
											<?php if ($method == 'google_js'):?>
												<a href="<?php echo $wifi_login_html[$method]['link']; ?>" class="login-link btn btn-block btn-google azm-social azm-btn azm-google-plus">
													<i class="fa fa-google fa-55 pull-left"></i>
													<?php echo $wifi_login_html[$method]['caption']; ?>
												</a>
											<?php endif;?>
											<?php if ($method == 'click'):?>
												<a href="<?php echo $wifi_login_html[$method]['link']; ?>" class="login-link btn btn-block btn-click azm-social azm-btn azm-twitter">
													<i class="fa fa-wifi fa-55 pull-left"></i>
													<?php echo $wifi_login_html[$method]['caption']; ?>
												</a>
											<?php endif;?>
											
											<?php //echo $wifi_login_html[$method]['html'];?>
										<?php endforeach;?>
									
									</div>
									<div class="col-xs-2 col-sm-3 col-md-4">&nbsp;</div>
							</div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
<!-- /.intro-header -->

<!-- Page Content -->

	<a  name="services"></a>
    <div class="content-section-a">

        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Death to the Stock Photo:<br>Special Thanks</h2>
                    <p class="lead">A special thanks to <a target="_blank" href="http://join.deathtothestockphoto.com/">Death to the Stock Photo</a> for providing the photographs that you see in this template. Visit their website to become a member.</p>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="{{ theme:image_url file="theme-default/ipad.png" }}" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

    <div class="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">3D Device Mockups<br>by PSDCovers</h2>
                    <p class="lead">Turn your 2D designs into high quality, 3D product shots in seconds using free Photoshop actions by <a target="_blank" href="http://www.psdcovers.com/">PSDCovers</a>! Visit their website to download some of their awesome, free photoshop actions!</p>
                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <img class="img-responsive" src="{{ theme:image_url file="theme-default/dog.png" }}" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-b -->

    <div class="content-section-a">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Google Web Fonts and<br>Font Awesome Icons</h2>
                    <p class="lead">This template features the 'Lato' font, part of the <a target="_blank" href="http://www.google.com/fonts">Google Web Font library</a>, as well as <a target="_blank" href="http://fontawesome.io">icons from Font Awesome</a>.</p>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="{{ theme:image_url file="theme-default/phones.png" }}" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->


{{ if exists advert }}
				<div class="container width-600" style="padding-bottom: 15px;padding-top: 15px;">
					<div class="row">
						
						<div class="col-xs-12 col-sm-12">
							
							<div class="advert-container">
								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3 col-sm-2"><img alt="logo" class="advert-brand-logo img-responsive" src="{{ advert:brand_logo:image }}"> </div>
											<div class="col-xs-9 col-sm-10 heading">{{ advert:brand_name }}</div>
										</div>
										
									</div>
									<div class="panel-body">
								    	<div class="advert-text">{{ advert:advert_text }}</div>
								    	<a href="{{ advert:website_url }}/home" class="piwikContentTarget" target="_blank" data-track-content data-content-name="{{ advert:advert_campaign:id }}" data-content-piece="{{ advert:id }}">
								    		<div id="advert-image" class="img-responsive">{{ advert:advert_image:img }}</div>
								    		<div id="advert-headline"><h3>{{ advert:advert_headline }}</h3></div>
											<div id="advert-link-desc">{{ advert:advert_link_desc }}</div>
											<span id="display-link">{{ advert:advert_display_link }}</span>
											<span id="cta-btn" class="pull-right btn btn-default">{{ advert:cta_button }}</span>
										</a>
									</div>
				
								</div>
							</div>
						</div>
						
					</div>
				</div>
{{ endif }}


<script>
	$('.wifi-login-link').click(function(e) {
		var my = $(e.target);
		my.html('Redirecting...');
		//$('#sw-status-wait').fadeIn();
	});
	
	var swUser = {};
	$(document).ready(function(){
		$.ajaxSetup({
        	data: {
            	csrf_hash_name: "<?php echo $this->security->get_csrf_hash(); ?>"
        	}
    	});
    	
    	if (!client_info){
    		//Create anonymous profile
			var userdata = {
				'client_mac' : "<?php echo $this->session->userdata('mac')?>",																
			};
								
			$.post( "husers/apis/create_anonymous_profile", userdata, function( response ) {
				swUser = response;
				console.log(JSON.stringify(response));
			});
    	}
    	
    	/*   		
		$.post( "husers/apis/get_client", {client_mac : "<?php echo $this->session->userdata('mac');?>"}, function( jsonp ) {
			if (!jsonp.error) {
				swUser = jsonp;
				if (jsonp.name) {
					//$('#c-name').html("Hi " + jsonp.name + "!<br/>");
				} else {
					//$('#c-name').html("");
				}
				if (!jsonp || !jsonp.profile_id) {
					//Create anonymous profile
					userdata = {
						'client_mac' : "<?php echo $this->session->userdata('mac')?>",																
					};
								
					$.post( "husers/apis/create_anonymous_profile", userdata, function( response ) {
						swUser = response;
						console.log(JSON.stringify(response));
					});
				}
			} else {
				$('#c-name').html();
				$('#error-msg').html("Error:" + jsonp.error.message);
				console.log(JSON.stringify(jsonp));
			}
		});

		*/
		
    });
   
    $('#agreement').bind("change",function(){
    	if (document.getElementById('agreement').checked) {
            $('#error-msg').html("");
        }
    });

</script>
