<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="#"><img class="img-responsive" src="{{ url:site }}files/thumb/{{ theme_detail:logo:id }}/32/auto" alt="Logo"/></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#about">About</a>
                    </li>
                    <li>
                        <a href="#services">Services</a>
                    </li>
                    <li>
                        <a href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


    <!-- Header -->
    <a name="about"></a>
    <div class="intro-header">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        {{ if exists theme_detail:header_text and theme_detail:header_text != "" }}
							<div class="container width-800">
								<div class="row">
										<div class="col-sm-12 text-center">
											<span id="c-name">
												<?php if (!empty($client_info)) echo "Hi ". $client_info['name']; ?>
											</span>		
											{{ theme_detail:header_text }}
											
										</div>
								</div>	
							</div>		
						{{ endif }}
                        <hr class="intro-divider">
              
                        <div class="wifi-login-area">
							<div class="row">
								<?php foreach ($wifi_method_position as $method => $value) :?>
									<?php if ($method == 'youtube'):?>
										<div class="col-xs-12 col-sm-12 col-md-12">
										<?php echo $wifi_login_html[$method]['html']; ?>
										</div>
									<?php endif;?>
								<?php endforeach;?>
									<div class="col-xs-2 col-md-4"></div>
									<div class="col-xs-8 col-md-4">				
										<?php foreach ($wifi_method_position as $method => $value) :?>
											<?php if ($method == 'modpro'):?>
												<?php echo $wifi_login_html[$method]['html']; ?>
											<?php endif;?>
											
											<?php if ($method == 'facebook_js'):?>
												<a href="<?php echo $wifi_login_html[$method]['link']; ?>" class="login-button login-link btn btn-block btn-facebook azm-social azm-btn azm-facebook">
													<i class="fa fa-facebook fa-55 pull-left"></i>
													<?php echo $wifi_login_html[$method]['caption']; ?>
												</a>
											<?php endif;?>
											<?php if ($method == 'google_js'):?>
												<a href="<?php echo $wifi_login_html[$method]['link']; ?>" class="login-button login-link btn btn-block btn-google azm-social azm-btn azm-google-plus">
													<i class="fa fa-google fa-55 pull-left"></i>
													<?php echo $wifi_login_html[$method]['caption']; ?>
												</a>
											<?php endif;?>
											<?php if ($method == 'click'):?>
												- OR -
												<a href="<?php echo $wifi_login_html[$method]['link']; ?>" class="login-button login-link btn btn-block btn-click azm-social azm-btn azm-twitter">
													<i class="fa fa-wifi fa-55 pull-left"></i>
													<?php echo $wifi_login_html[$method]['caption']; ?>
												</a>
											<?php endif;?>
											<?php //echo $wifi_login_html[$method]['html'];?>
										<?php endforeach;?>				
									</div>
									<div class="col-xs-2 col-md-4"></div>
							</div>
						</div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->