<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

{{ if exists theme_detail:success_message and theme_detail:success_message != "" }}
	<div class="container width-800" style="margin-top:10px;">
		<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-primary">
					  <div class="panel-body">
					  	{{ theme_detail:success_message }}
					  	<div class="fb-page" data-width="460" data-href="https://www.facebook.com/<?php echo $merchant_fb_settings['page_id'];?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"></div>
					  </div>
					</div>
				</div>
		</div>
	</div>	
{{ endif }}