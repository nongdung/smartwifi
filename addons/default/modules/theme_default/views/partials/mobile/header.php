<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="sw-wrapper">
	<div class="masthead clearfix">
		<div class="container">
			<div class="row">
					<div class="center-block pagination-centered text-center">
						<div id="carousel-generic" class="carousel slide" data-ride="carousel" data-interval="5000">

							<!-- Wrapper for slides -->
							<div class="carousel-inner">
								<div class="active item">			
									<img class="img-responsive center-block" src="{{ url:site }}files/thumb/{{ theme_detail:mobile_header_image:id }}/1024/auto" alt="Header1"/>
								</div>
								
								{{ if exists theme_detail:mobile_header_image_2:id }}
								<div class="item">			
									<img class="img-responsive center-block" src="{{ url:site }}files/thumb/{{ theme_detail:mobile_header_image_2:id }}/1024/auto" alt="Header1"/>
								</div>
								{{ endif }}
								
								{{ if exists theme_detail:mobile_header_image_3:id }}
								<div class="item">			
									<img class="img-responsive center-block" src="{{ url:site }}files/thumb/{{ theme_detail:mobile_header_image_3:id }}/1024/auto" alt="Header1"/>
								</div>
								{{ endif }}
						
							</div>
						
						</div>
					</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-xs-12" style="clear:both;padding-top:10px;padding-bottom:10px;">
				<script type='text/javascript'><!--//<![CDATA[
				   var m3_u = (location.protocol=='https:'?'https://ad.smartwifi.vn/www/delivery/ajs.php':'http://ad.smartwifi.vn/www/delivery/ajs.php');
				   var m3_r = Math.floor(Math.random()*99999999999);
				   if (!document.MAX_used) document.MAX_used = ',';
				   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
				   document.write ("?zoneid=5");
				   document.write ('&cb=' + m3_r);
				   if (document.MAX_used != ',') document.write ("&exclude=" + document.MAX_used);
				   document.write (document.charset ? '&charset='+document.charset : (document.characterSet ? '&charset='+document.characterSet : ''));
				   document.write ("&loc=" + escape(window.location));
				   if (document.referrer) document.write ("&referer=" + escape(document.referrer));
				   if (document.context) document.write ("&context=" + escape(document.context));
				   if (document.mmm_fo) document.write ("&mmm_fo=1");
				   document.write ("'><\/scr"+"ipt>");
				//]]>--></script><noscript><a href='http://ad.smartwifi.vn/www/delivery/ck.php?n=a073be16&cb=<?php echo mt_rand();?>' target='_blank'><img src='http://ad.smartwifi.vn/www/delivery/avw.php?zoneid=5&cb=<?php echo mt_rand();?>=a073be16' border='0' alt='' /></a></noscript>		
			</div>	
		</div>
	</div>	
	
	{{ if theme_detail:text_ads_position:val == 'Top' and {url:segments segment="2"} != "success" }}
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-ads" style="text-align: left">
				{{ theme_detail:text_ads }}
			</div>		
		</div>	
	</div>
	{{ endif }}
