<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

<div class="wifi-login-section" style="{{ theme_detail:body_style }}">
	<div class="container">
		<div class="row">
				{{ if exists theme_detail:header_text and theme_detail:header_text != "" }}
					<div class="col-xs-12 text-center header">
						<span id="c-name">
							<?php if (!empty($client_info)) echo "Hi ". $client_info['name']; ?>
						</span>
						{{ theme_detail:header_text }}
					</div>		
				{{ endif }}
				
				<?php foreach ($wifi_method_position as $method => $value) :?>
					<?php if ($method == 'youtube'):?>
						<div class="col-xs-12">
							<?php echo $wifi_login_html[$method]['html']; ?>
						</div>
					<?php endif;?>
				<?php endforeach;?>
				
				<div class="hidden-xs hidden-sm col-md-3">&nbsp;</div>
				<div class="col-xs-12 col-sm-12 col-md-6">				
					<?php foreach ($wifi_method_position as $method => $value) :?>
						<?php if ($method == 'facebook_js'):?>
							<a href="<?php echo $wifi_login_html[$method]['link']; ?>" class="login-link btn btn-block azm-social azm-btn-mobile azm-facebook">
								<i class="fa fa-facebook fa-55 pull-left"></i>
								<?php echo $wifi_login_html[$method]['caption']; ?>
							</a>
						<?php endif;?>
						<?php if ($method == 'google_js'):?>
							<a href="<?php echo $wifi_login_html[$method]['link']; ?>" class="login-link btn btn-block azm-social azm-btn-mobile azm-google-plus">
								<i class="fa fa-google fa-55 pull-left"></i>
								<?php echo $wifi_login_html[$method]['caption']; ?>
							</a>
						<?php endif;?>
						<?php if ($method == 'click'):?>
							<a href="<?php echo $wifi_login_html[$method]['link']; ?>" class="login-link btn btn-block azm-social azm-btn-mobile azm-twitter">
								<i class="fa fa-wifi fa-55 pull-left"></i>
								<?php echo $wifi_login_html[$method]['caption']; ?>
							</a>
	
						<?php endif;?>
						<?php //echo $wifi_login_html[$method]['html'];?>
					<?php endforeach;?>
					
					<div class="text-center agreement">
						<input id="agreement" type="checkbox" name="agreement" value="1"/>
						&nbsp; Tôi đồng ý với <u><a href="#" data-toggle="modal" data-target="#AgreementModal">điều khoản sử dụng</a></u> Wifi
						<div id="error-msg" class="text-center text-danger"></div>
						<!--Agreement Modal -->
						<div class="modal fade" id="AgreementModal" tabindex="-1" role="dialog" aria-labelledby="AgreementModalLabel">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="AgreementModalLabel">Điều khoản sử dụng/Agreement</h4>
						      </div>
						      <div class="modal-body" style="text-align: left;">
						      		{{ theme_detail:agreement_terms }}
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Close</button>
						      </div>
						    </div>
						  </div>
						</div>
					</div>
					{{ if theme_detail:userguide != "" }}
					<div class="text-center agreement">
						<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<a data-toggle="modal" data-target="#GuideModal" href="#">Hướng dẫn sử dụng/User guide</a>
						<!-- Guide Modal -->
						<div class="modal fade" id="GuideModal" tabindex="-1" role="dialog" aria-labelledby="GuideModalLabel">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="GuideModalLabel">Hướng dẫn sử dụng/User guide</h4>
						      </div>
						      <div class="modal-body" style="text-align: left;">
						      		{{ theme_detail:userguide }}
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default btn-primary" data-dismiss="modal">Close</button>
						      </div>
						    </div>
						  </div>
						</div>
					</div>
					{{ endif }}
				</div>
				<div class="hidden-xs hidden-sm col-md-3">&nbsp;</div>
		</div>
	</div>
</div>


<!-- prosers Heading-->
<?php if ($all_loc_prosers || $this_loc_prosers): ?>
	<div class="proser-section">
		        <div class="container">
		            <div class="row">
		                <div class="col-xs-7">
		                	<?php echo $location_detail['products_and_services_heading'];?>
		         		</div>
		                <div class="col-xs-5">
		                	<div class="btn-group pull-right">
							  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							    Filter <span class="caret"></span>
							  </button>
							  <ul class="dropdown-menu">
							  	<li><a href="#"><?php echo lang('global:select-all');?></a></li>
							  	<?php foreach($sections as $key => $title):?>
							  		<li><a href="#"><?php echo $title;?></a></li>
							  	<?php endforeach;?>
							   
							  </ul>
							  
							</div>
		                </div>
		            </div>
		
		        </div>
		        <!-- /.container -->
	</div>
	
	<div id="prosers">
	<!--featured prosers - all location -->
	<?php foreach ($all_loc_prosers AS $proser): ?>
		<?php if ($proser->featured == 'yes'):?>
			<div class="featured-content-section">
		        <div class="container">
		            <div class="row">
		                <div class="col-xs-12">
		                    <div class="item-heading"><?php echo $proser->title;?>
		                    	<?php if(isset($proser->price) && $proser->price != ''):?>
		                    	<span class="item-price pull-right"><?php echo number_format($proser->price).' '.$proser->currency;?></span>
		                    	<?php endif;?>
		                    </div>
		                    
		                </div>
		
		                <div class="col-xs-12">
		                    <img class="img-responsive img-rounded" src="<?php echo $proser->image;?>" />
		                    <div class="intro"><?php echo $proser->intro;?></div>
		                </div>
		            </div>
		
		        </div>
		        <!-- /.container -->
				</div>
		    </div>
		    
		<?php endif;?>	
	<?php endforeach;?>
	
	<!--prosers - all location -->
	<?php foreach ($all_loc_prosers AS $proser): ?>
		<?php if ($proser->featured == 'no'):?>
			<div class="content-section">
		        <div class="container">
		            <div class="row">
		                <div class="col-xs-7">		                    
		                    <img class="img-responsive img-rounded" src="<?php echo $proser->image;?>" />
		                </div>
		
		                <div class="col-xs-5">
		                    <div class="item-heading">
		                    	<?php echo $proser->title;?>
		                    	<?php if(isset($proser->price) && $proser->price != ''):?>
		                    	<br />
		                    	<span class="item-price"><?php echo number_format($proser->price).' '.$proser->currency;?></span>
		                    	<hr />
		                    	<?php endif;?>
		                    </div>
		                    
		                </div>
		                <div class="col-xs-12">
		                	<div class="intro">
		                	<?php echo $proser->intro;?>
		                	</div>
		                </div>
		                
		            </div>
		
		        </div>
		        <!-- /.container -->
		
		    </div>
		<?php endif;?>	
	<?php endforeach;?>
	
	<!-- featured prosers - this location -->
	<?php foreach ($this_loc_prosers AS $proser): ?>
		<?php if ($proser->featured == 'yes'):?>
			<div class="featured-content-section">
		        <div class="container">
		            <div class="row">
		                <div class="col-xs-12">
		                    <div class="item-heading"><?php echo $proser->title;?>
		                    	<?php if(isset($proser->price) && $proser->price != ''):?>
		                    	<span class="item-price pull-right"><?php echo number_format($proser->price).' '.$proser->currency;?></span>
		                    	<?php endif;?>
		                    </div>
		                    
		                </div>
		
		                <div class="col-xs-12">
		                    <img class="img-responsive img-rounded" src="<?php echo $proser->image;?>" />
		                    <div class="intro"><?php echo $proser->intro;?></div>
		                </div>
		            </div>
		
		        </div>
		        <!-- /.container -->
				</div>
		    </div>
		<?php endif;?>	
	<?php endforeach;?>
	
	<!--prosers - this location -->
	<?php foreach ($this_loc_prosers AS $proser): ?>
		<?php if ($proser->featured == 'no'):?>
			<div class="content-section">
		        <div class="container">
		            <div class="row">
		                <div class="col-xs-4">		                    
		                    <img class="img-responsive img-rounded" src="<?php echo $proser->image;?>" />
		                </div>
		
		                <div class="col-xs-8">
		                    <div class="item-heading">
		                    	<?php echo $proser->title;?>
		                    	<?php if(isset($proser->price) && $proser->price != ''):?>
			                	<div class="item-price pull-right">
			                		<?php echo number_format($proser->price).' '.$proser->currency;?>
			                	</div>
		                    <?php endif;?>
		                    </div>
		                    
		                    <div class="intro">
		                	<?php echo $proser->intro;?>
		                	</div>
		                	
		                </div>
		                
		                
		            </div>
		
		        </div>
		        <!-- /.container -->
		
		    </div>
		<?php endif;?>	
	<?php endforeach;?>
	
<?php endif;?>

<script>
	$('.wifi-login-link').click(function(e) {
		var my = $(e.target);
		my.html('Redirecting...');
		//$('#sw-status-wait').fadeIn();
	});
	
	
	var swUser = {};
	$(document).ready(function(){
		$.ajaxSetup({
        	data: {
            	csrf_hash_name: "<?php echo $this->security->get_csrf_hash(); ?>"
        	}
    	});
    	if (!client_info){
    		//Create anonymous profile
			var userdata = {
				'client_mac' : "<?php echo $this->session->userdata('mac')?>",																
			};
								
			$.post( "husers/apis/create_anonymous_profile", userdata, function( response ) {
				swUser = response;
				console.log(JSON.stringify(response));
			});
    	}
    	/*  		
		$.post( "husers/apis/get_client", {client_mac : "<?php echo $this->session->userdata('mac');?>"}, function( jsonp ) {
			if (!jsonp.error) {
				swUser = jsonp;
				if (jsonp.name) {
					$('#c-name').html("Hi " + jsonp.name + "! Welcome to {{ location_detail:loc_name }}");
				} else {
					$('#c-name').html("Welcome to {{ location_detail:loc_name }}");
				}
				if (!jsonp || !jsonp.profile_id) {
					//Create anonymous profile
					userdata = {
						'client_mac' : "<?php echo $this->session->userdata('mac')?>",																
					};
								
					$.post( "husers/apis/create_anonymous_profile", userdata, function( response ) {
						swUser = response;
						console.log(JSON.stringify(response));
					});
				}
			} else {
				$('#c-name').html("");
				$('#error-msg').html("Error:" + jsonp.error.message);
				console.log(JSON.stringify(jsonp));
			}
		});
		*/	
    });
    
    $('.login-link').bind("click",function(){
    	if (document.getElementById('agreement').checked) {
            $('#error-msg').html("");
            return true;
            
        } else {
            $('#error-msg').html("Bạn cần đồng ý với điều khoản sử dụng của chúng tôi!");
            //$('#error-msg')..blink();
            setInterval(function(){
	            $("#error-msg").fadeOut(function () {
	                $(this).fadeIn();
	            });
	        } ,2000);
            return false;
        }
        
    });
    $('#agreement').bind("change",function(){
    	if (document.getElementById('agreement').checked) {
            $('#error-msg').html("");
        }
    });
</script>
<?php
//print_r($login_template_details);
?>