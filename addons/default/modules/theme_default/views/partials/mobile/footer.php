<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

	{{ if exists theme_detail:text_ads and theme_detail:text_ads_position:val == 'Bottom' and {url:segments segment="2"} != "success" }}
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-ads" style="text-align: left">
					{{ theme_detail:text_ads }}
			</div>						
		</div>	
	</div>
	{{ endif }}
	<div class="push"></div>
	
</div>

<div class="mastfoot">
	<div class="container">
		<div class="row">
			<div class="col-xs-3 center-block pagination-centered text-center logo">
				<img id="footer-img" alt="Footer" class="img-responsive center-block" src="{{ url:site }}files/thumb/{{ theme_detail:logo:id }}/auto/40" />
			</div>
			<div class="col-xs-9">
				<div id="info">
					<h6>{{ location_detail:loc_name }}</h6>
					<span>Address: {{ location_detail:address }}</span>
					<br/>
					{{ if exists location_detail:info and location_detail:info != "" }}
						<span>{{ location_detail:info }}</span>
					{{ endif }}
					
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('.text-ads').readmore({
		speed: 500,
		collapsedHeight:95,
		moreLink: '<a href="#" class="readmore" style="text-align:right;padding-right:10px;">... Đọc thêm</a>',
		lessLink: '<a href="#" class="readmore" style="text-align:right;padding-right:10px;">Close</a>'
	});
</script>