<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('theme_default:overview'); ?> <i>(Location: <?php echo $location_detail['loc_name'];?>)</i></h4>
</section>

<section class="item">
	<div class="content">
		<?php if (! empty($entries)):?>
			<table class="table-list" border="0" cellspacing="0">
				<thead>
					<tr>
						<th style="width: 10%;">No.</th>					
						<th style="width: 30%;">Field</th>
						<th style="width: 60%;">Content</th>
					</tr>
				</thead>
				<tbody>
					<tr><td colspan="3" style="padding-top: 20px;padding-bottom: 5px;"><strong>General Fields</strong></td></tr>
					<tr>
						<td>1</td>
						<td>Header Text</td>
						<td><?php echo $entries[0]['header_text']; ?></td>
					</tr>
					
					<tr><td colspan="3" style="padding-top: 20px;padding-bottom: 5px;"><strong>Desktop Fields</strong></td></tr>
					<tr>
						<td>2</td>
						<td>Desktop Header Image</td>
						<td><?php echo $entries[0]['desktop_header_image']['thumb_img']; ?></td>
					</tr>
					<tr>
						<td>3</td>
						<td>Desktop Footer Image</td>
						<td><?php echo $entries[0]['desktop_footer_image']['thumb_img']; ?></td>
					</tr>
					
					<tr><td colspan="3" style="padding-top: 20px;padding-bottom: 5px;"><strong>Mobile Fields</strong></td></tr>
					<tr>
						<td>4</td>
						<td>Mobile Header Image</td>
						<td><?php echo $entries[0]['mobile_header_image']['thumb_img']; ?></td>
					</tr>
					<tr>
						<td>5</td>
						<td>Mobile Footer Image</td>
						<td><?php echo $entries[0]['mobile_footer_image']['thumb_img']; ?></td>
					</tr>
				</tbody>
			</table>
		<?php else:?>
	    		<div class="no_data">No record</div>    		
	    <?php endif;?>
	    <div class="float-right buttons">
	    <?php
	    	echo anchor('admin/theme_default/edit/'.$location_detail['id'], "Edit Fields", 'class="btn blue"')." ";
			echo anchor('admin/theme_default/slider/edit/'.$location_detail['id'], "Slider", 'class="btn blue"')." ";
			echo anchor('admin/splash/landing', "Back", 'class="btn gray"')." ";
	    	//print_r($location_detail);    	
	    ?>
	    </div>
    </div>	
</section>