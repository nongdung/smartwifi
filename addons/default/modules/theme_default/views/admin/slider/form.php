<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('theme_default:slider'); ?> <i>(Location: <?php echo $location_detail['loc_name'];?>)</i></h4>
</section>

<section class="item">
	<div class="content">
		<?php echo form_open_multipart() ?>
		<div class="form_inputs" id="proser-custom-fields">
			<fieldset>
				<ul>
				<?php
				foreach ($fields as $field) {
					echo $this->load->view('admin/partials/streams/form_single_display', array('field' => $field), true);
				}
				?>
				</ul>
			</fieldset>
		</div>
		<div class="buttons">
			<?php 
			$this->load->view('admin/partials/buttons', array('buttons' => array('save'))); 
			echo anchor('admin/theme_default/'.$location_detail['id'], "Back", 'class="btn gray"')." ";
			?>
		</div>
		<?php echo form_close() ?>
    </div>	
</section>