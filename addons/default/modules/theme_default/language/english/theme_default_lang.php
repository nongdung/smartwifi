<?php
//messages
$lang['theme_default:overview']			=	'Overview';
$lang['theme_default:general_fields']			=	'General Fields';
$lang['theme_default:desktop_fields']			=	'Desktop Fields';
$lang['theme_default:mobile_fields']			=	'Mobile Fields';

//Slide
$lang['theme_default:slider']			=	'Slider';
$lang['theme_default:add_general_fields_first']			=	'Please complete the following fields first';
?>