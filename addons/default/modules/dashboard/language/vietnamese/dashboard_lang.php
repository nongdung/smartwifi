<?php
//messages
$lang['dashboard:filters']			=	'Bộ lọc';
$lang['dashboard:dashboard']			=	'Bảng điều khiển';
$lang['dashboard:overview']			=	'Tổng quan';
$lang['dashboard:location']			=	'Vị trí';
$lang['dashboard:devices']			=	'Thiết bị';
$lang['dashboard:total_users']			=	'Số người dùng';
$lang['dashboard:social_accounts']			=	'Mạng xã hội';
?>