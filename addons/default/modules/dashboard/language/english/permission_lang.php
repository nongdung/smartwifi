<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Blog Permissions
$lang['dashboard:role_merchant']	= 'Merchant';
$lang['dashboard:role_manager']     = 'Manager';
$lang['dashboard:role_admin']     = 'Admin';