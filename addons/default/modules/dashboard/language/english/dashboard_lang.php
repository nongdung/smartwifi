<?php
//messages
$lang['dashboard:filters']			=	'Filters';
$lang['dashboard:dashboard']			=	'Dashboard';
$lang['dashboard:overview']			=	'Overview';
$lang['dashboard:location']			=	'Location';
$lang['dashboard:devices']			=	'Devices';
$lang['dashboard:total_users']			=	'Total Users';
$lang['dashboard:social_accounts']			=	'Social Accounts';
?>