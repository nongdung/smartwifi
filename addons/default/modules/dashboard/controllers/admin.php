<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin extends Admin_Controller
{
	protected $section = 'dashboard';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('dashboard');
		$this->load->driver('Streams');
		$this->load->model('dashboard_m');
		role_or_die('husers', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all billing plans
     */
    public function index()
    {    	
    	$this->template
			->enable_parser(true)
			->title(lang('global:dashboard'));

		$this->load->language('dashboard');	
		$this->load->language('husers/husers');
		
		//$overview_data = $this->dashboard_m->get_overview();
		$overview_data = $this->pyrocache->model('dashboard_m', 'get_overview', array($this->current_user->id), 7200);
		$this->template
			->append_css('module::dashboard.css')
			
			//->append_js('module::excanvas.min.js')
			//->append_js('module::jquery.flot.min.js')
			//->append_js('module::jquery.flot.time.min.js')
			//->append_js('module::jquery.flot.pie.min.js')
			->append_js('module::date.js')
			->append_js('admin/filter.js')
			->append_js('module::sw-dashboard.js')
			->append_metadata('<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">')
			->append_metadata('<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>')
			->append_metadata('<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>')
			->set("overview_data", $overview_data)
			->build('admin/dashboard');
    }
}