<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Dashboard extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Wifi Dashboard',
				'vi' => 'Tổng quan wifi'
			),
			'description' => array(
				'en' => 'Dashboard for wifi marketing analytics',
				'vi' => 'Trang tổng quan về wifi marketing'
			),
			'frontend' => FALSE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',
			'roles' => array(
                'merchant','manager', 'admin'
            )		
		);
		
		if (!group_has_role('dashboard', 'admin')) {
			$info['sections']['fields'] = array(
					'name' 	=> 'dashboard:dashboard', // These are translated from your language file
					'uri' 	=> 'admin/dashboard',
			);			
		}
								
		return $info;
	}

	public function install()
	{
		
		// Install the settings  c975cf8316edde7ac3106d0ad031099c
		$settings = array(
			array(
				'slug' => 'piwik_host',
				'title' => 'Piwik Host',
				'description' => '',
				'type' => 'text',
				'default' => "",
				'value' => '',
				'options' => '',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => 'dashboard',
				'order' => 11,
			),
			array(
				'slug' => 'piwik_idSite',
				'title' => 'Piwik Site ID',
				'description' => '',
				'type' => 'text',
				'default' => "",
				'value' => '',
				'options' => '',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => 'dashboard',
				'order' => 10,
			),
			array(
				'slug' => 'piwik_token_auth',
				'title' => 'Piwik Token Auth',
				'description' => '',
				'type' => 'text',
				'default' => "",
				'value' => '',
				'options' => '',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => 'dashboard',
				'order' => 9,
			),
		);

		foreach ($settings as $setting)
		{
			if ( ! $this->db->insert('settings', $setting))
			{
				return false;
			}
		}

		return TRUE;
		
	}

	public function uninstall()
	{

		$this->db->delete('settings', array('module' => 'dashboard'));

		return TRUE;
	}


	public function upgrade($old_version)
	{
		
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
	
	public function admin_menu(&$menu)
    {
	}

}
/* End of file details.php */
