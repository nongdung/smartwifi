<!-- Add an extra div to allow the elements within it to be sortable! -->
<div id="sortable">
	
	<div class="one_full">		
		<section class="draggable title">
			<h4><?php echo lang('dashboard:overview'); ?></h4>
			<a class="tooltip-s toggle" title="Toggle this element"></a>
		</section>
		
		<section class="item">			
			<div class="section group">
				<div class="col span_1_of_4">
					<div id="sw-location" class="overview-block">
						<i class="fa fa-35 fa-location"></i>
						<h2><?php echo lang('dashboard:location'); ?></h2>
						<?php echo $overview_data['online_locations']."/".$overview_data['total_locations'];?>
					</div>
				</div>
				<div class="col span_1_of_4">
					<div id="sw-devices" class="overview-block">
						<i class="fa fa-35 fa-devices"></i>
						<h2><?php echo lang('dashboard:devices'); ?></h2>
						<?php echo $overview_data['total_devices'];?>
					</div>
				</div>
				<div class="col span_1_of_4">
					<div id="sw-total-users" class="overview-block">
						<i class="fa fa-35 fa-visitors"></i>
						<h2><?php echo lang('dashboard:total_users'); ?></h2>
						<?php echo $overview_data['total_accounts'];?>
					</div>
				</div>
				<div class="col span_1_of_4">
					<div id="sw-social-accounts" class="overview-block">
						<i class="fa fa-35 fa-social-account"></i>
						<h2><?php echo lang('dashboard:social_accounts'); ?></h2>
						<div class="content">
						Facebook:<?php echo $overview_data['fb_accounts'];?><br/>  Google: <?php echo $overview_data['gg_accounts'];?>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

	<div class="one_full">		
		<section class="draggable title">
			<h4>Visits/Pageviews <i><span id="current-filter-name"><?php echo lang('husers:last-7-days');?></span></i></h4>
			<a id="wifi-login-chart" class="tooltip-s show-filter" title="<?php echo lang('global:filters');?>"></a>		
		</section>
		
		<section class="item">
			<div class="content">
				<?php echo form_open('', 'class="crud filters" id="wifi-login-chart-filters_form"') ?>			
					<fieldset id="wifi-login-chart-filters" class="filters chart-filters" style="display: none">					
						<legend><?php echo lang('global:filters');?></legend>
						<ul>
							<li>											
							<?php
								$f_time = array(
									'last-7-days'	=> lang('husers:last-7-days'),
									'last-30-days'	=> lang('husers:last-30-days'),
									'this-week'	=> lang('husers:this-week'),
									'last-week'	=> lang('husers:last-week'),
									'this-month'	=> lang('husers:this-month'),
									'last-month'	=> lang('husers:last-month'),
									'last-3-months'	=> lang('husers:last-3-months'),									
									'custom'	=> "Custom...",
								);
							?>
							<?php echo form_dropdown('f_time', $f_time, 'last-7-days','id="f_time"') ?>
							</li>
							<li id="custom-datetime" style="display:none">
                            	<input type="text" name="date_from" placeholder="From..." class="datepicker" id="date_from" />
                            	<input type="text" name="date_to" placeholder="To..." class="datepicker" id="date_to" />
                    		</li>
                    		
						</ul>
						
					</fieldset>
				<?php echo form_close(); ?>
				<div id="wifi-login-stage">
					<div id="visits-chart" class="center-block" style="height: 250px;">
						<?php echo Asset::img('module::loading.gif', 'Loading') ?>
					</div>
					<div id="visits-chart-legend" class="morris-legend"></div>
					
				</div>
			</div>

			
		</section>
		
	</div>
	
	<div class="one_full">
		<section class="draggable title">
			<h4>Device Statistics <i><span id="current-device-filter"><?php echo lang('husers:last-7-days');?></span></i></h4>
			<a id="device-statistics" class="tooltip-s show-filter" title="<?php echo lang('global:filters');?>"></a>			
		</section>
		<section class="item">
			<?php echo form_open('', 'class="crud filters" id="device-statistics-filters_form"') ?>			
					<fieldset id="device-statistics-filters" class="filters chart-filters" style="display: none">					
						<legend><?php echo lang('global:filters');?></legend>
						<ul>
							<li>											
							<?php
								$f_time = array(
									'today'	=> "Today",
									'yesterday'	=> "Yesterday",
									'last-7-days'	=> lang('husers:last-7-days'),
									'last-30-days'	=> lang('husers:last-30-days'),
									'this-week'	=> lang('husers:this-week'),
									'last-week'	=> lang('husers:last-week'),
									'this-month'	=> lang('husers:this-month'),
									'last-month'	=> lang('husers:last-month'),
									'last-3-months'	=> lang('husers:last-3-months'),									
									'custom'	=> "Custom...",
								);
							?>
							<?php echo form_dropdown('f_device_time', $f_time, 'last-7-days','id="f_device_time"') ?>
							</li>
							<li id="device-custom-datetime" style="display:none">
                            	<input type="text" name="device_date_from" placeholder="From..." class="datepicker" id="device_date_from" />
                            	<input type="text" name="device_date_to" placeholder="To..." class="datepicker" id="device_date_to" />
                    		</li>                    		
						</ul>
						
					</fieldset>
			<?php echo form_close(); ?>
			<div class="section group">
				<div class="col span_1_of_2">
					<h4 style="text-align: center;">Device Type (visits)</h4>					
					<div id="device-type" class="center-block" style="height: 250px;">
						<?php echo Asset::img('module::loading.gif', 'Loading') ?>
					</div>
				</div>
				<div class="col span_1_of_2">
					<h4 style="text-align: center;">Top 10 device brand (visits)</h4>					
					<div id="device-brand" class="center-block" style="height: 300px;">
						<?php echo Asset::img('module::loading.gif', 'Loading') ?>
					</div>
				</div>
								
			</div>
		</section>
	</div>
	
	
	
</div>
<!-- End sortable div -->

<script type="text/javascript">
	var piwik = {
		host	: '<?php echo Settings::get('piwik_host');?>',
		token	: '<?php echo Settings::get('piwik_token_auth');?>',
		idSite	: '<?php echo Settings::get('piwik_idSite');?>',
		defaultDate	: '<?php echo date("Y-m-d",mktime(0, 0, 0, date("m")  , date("d")-7, date("Y")));?>'
	};
	var merchantId = '<?php echo $this->current_user->id; ?>';
	var loading_img = '<?php echo Asset::img('module::loading.gif', 'Loading') ?>';
</script>