<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Husers_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		
	}
	
	function get_profile($data = null, $type = null)
	{
		switch ($type) {
			case 'facebook':
				$field = "social_network_id";
				$this->db->where("profile_type", "facebook");
				break;
			case 'google':
				$field = "social_network_id";
				$this->db->where("profile_type", "google");
				break;
			case 'email':
				$field = "email";
				break;
			case 'mobile':
				$field = "mobile";
				break;
			default:
				return array();
				break;
		}
		
		$query = $this->db
						->where($field, $data)
						->get("husers_profiles");
		if ($query->num_rows() > 0) {
			$data = $query->result_array();
			return $data[0];
		} else {
			return array();
		}
		
	}

}
