<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		$this->load->driver('Streams');
	}
	
	function get_overview($merchant_id)
	{
		$data = array();
		
		//Get number of location
		$params = array(
		    'stream'    => 'list',
		    'namespace' => 'location',
		    'where'		=> "`created_by` = '".$merchant_id."'"
		);
		$total_locations = $this->streams->entries->get_entries($params);
		$data['total_locations'] = $total_locations['total'];
		
		//Get online locations
		$params['where'] .= " AND `status` = 1";
		$online_locations = $this->streams->entries->get_entries($params);
		$data['online_locations'] = $online_locations['total'];
		
		//Get total number of devices
		$params = array(
		    'stream'    => 'list',
		    'namespace' => 'devices',
		    'where'		=> "`created_by` = '".$merchant_id."'"
		);
		$total_devices = $this->streams->entries->get_entries($params);
		$data['total_devices'] = $total_devices['total'];
		
		//Get number of online devices
		
		//Get total accounts
		$data['total_accounts'] = $this->db
										->select("DISTINCT(`profile_id`)")
										->where("merchant_id", $merchant_id)
										->get("default_husers_raw_logs")
										->num_rows();
		
		//Get Facebook Accounts
		/*
		$data['fb_accounts'] = $this->db
										->select("DISTINCT(`profile_id`)")
										->where("merchant_id", $merchant_id)
										->where("action", "facebook")
										->get("default_husers_raw_logs")
										->num_rows();
		*/								
		$fb_query = $this->db
							->select("COUNT( DISTINCT(`default_husers_raw_logs`.`profile_id`)) AS total")
							->from("default_husers_raw_logs")
							->join('husers_profiles', 'default_husers_raw_logs.profile_id = husers_profiles.id')
							->where("default_husers_raw_logs.merchant_id", $merchant_id)
							->where("husers_profiles.profile_type", 'facebook')
							->get()
							->result_array();
		$data['fb_accounts'] = $fb_query[0]['total'];
		
		//Get Google Accounts
		/*
		$data['gg_accounts'] = $this->db
										->select("DISTINCT(`profile_id`)")
										->where("merchant_id", $merchant_id)
										->where("action", "google")
										->get("default_husers_raw_logs")
										->num_rows();
		*/
										
		$gg_query = $this->db
							->select("COUNT( DISTINCT(`default_husers_raw_logs`.`profile_id`)) AS total")
							->from("default_husers_raw_logs")
							->join('husers_profiles', 'default_husers_raw_logs.profile_id = husers_profiles.id')
							->where("default_husers_raw_logs.merchant_id", $merchant_id)
							->where("husers_profiles.profile_type", 'google')
							->get()
							->result_array();
		$data['gg_accounts'] = $gg_query[0]['total'];
		
		return $data;
	}

}
