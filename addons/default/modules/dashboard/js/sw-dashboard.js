
$(function() {
	var segment_string = "";
	if (merchantId) segment_string = "&segment=customVariablePageName1==merchantId;customVariablePageValue1==" + merchantId;
	//Loading at first
	var req1 = encodeURIComponent("method=VisitsSummary.getVisits&idSite=" + piwik.idSite + "&period=day&date=" + piwik.defaultDate + ",today" + segment_string);
	var req2 = encodeURIComponent("method=Actions.get&idSite=" + piwik.idSite + "&period=day&date=" + piwik.defaultDate + ",today" + segment_string);
	var deviceType = encodeURIComponent("method=DevicesDetection.getType&idSite=" + piwik.idSite + "&period=range&date=" + piwik.defaultDate + ",today" + segment_string);
	var deviceBrand = encodeURIComponent("method=DevicesDetection.getBrand&idSite=" + piwik.idSite + "&period=range&date=" + piwik.defaultDate + ",today" + segment_string);
	var req_url = piwik.host + "index.php?module=API&method=API.getBulkRequest&format=json&urls[0]=" + req1 + "&urls[1]=" + req2 + "&urls[2]=" + deviceType + "&urls[3]=" + deviceBrand + "&token_auth=" + piwik.token;
	$.ajax({
		type: 'GET',
		contentType: "application/json",
		url:req_url,
		crossDomain:true,
		dataType : 'jsonp',
		success : function(json_data){
			//console.log(JSON.stringify(json_data[2]));
			$('#device-type').empty();
			$('#device-brand').empty();
			$('#visits-chart').empty();
			var devTypeData = [];
			jQuery.each(json_data[2], function(key, obj) {
					if (obj.nb_visits) {
						var tmp = {label: obj.label, value: obj.nb_visits};
						devTypeData.push(tmp);
					};
			});

			if(devTypeData.length > 0){ 
				Morris.Donut({
				  element: 'device-type',
				  data: devTypeData,
				  colors: ['#02adea', '#52cf2b', '#ccc'],
				  resize: true
				});
			}else {
				$('#device-type').html('<span>No data</span>');
			}
			//Device brand
			var devBrand = [];
			jQuery.each(json_data[3], function(key, obj) {
				if (key < 10) devBrand.push({brand: obj.label, visits: obj.nb_visits});	
			});
			if(devBrand.length > 0){
				Morris.Bar({
				  element: 'device-brand',
				  data: devBrand,
				  xkey: 'brand',
				  ykeys: ['visits'],
				  labels: ['Visits'],
				  barColors: ['#02adea'],
				  xLabelAngle: 60,
				  resize: true
				});
			}else {
				$('#device-brand').html('<span>No data</span>');
			}
			
			var morrisVisits = [];
			jQuery.each(json_data[0], function(date, val) {
				if (! val) { val = 0;};
				var subdata = {date: date, visits: val};
				morrisVisits.push(subdata);
			});
			jQuery.each(json_data[1], function(date, val) {
				jQuery.each(morrisVisits, function(key, subdata) {
					if (subdata.date == date) {
						if (val.nb_pageviews) {
							morrisVisits[key].pageviews = val.nb_pageviews;
						} else {
							morrisVisits[key].pageviews = 0;
						};
					};
				});
			});
			
			MorrisBuildVisits(morrisVisits, 'visits-chart', '#visits-chart-legend');
											
		} //end of ajax success func
	});
	
	
	
	$('#wifi-login-chart').click(function() {$('#wifi-login-chart-filters').slideToggle(500); });
	
	if ($( "#f_time" ).val() === 'custom') $('#custom-datetime').show();
	
	$('#wifi-login-chart-filters select, #wifi-login-chart-filters input').change(function(e) {
		
		if ($( this ).val() === 'custom') {			
			$('#custom-datetime').show();
			$('#current-filter-name').html("(Custom date range)");
		} else {
			
			if ($( this ).attr("name") === 'f_time') {
				$('#custom-datetime').hide();
				$('#current-filter-name').html('(' + $(this).find('option:selected').text() + ')');
			}
			update_wifi_login_chart();
		}
	});

	function update_wifi_login_chart() {
		var inputs = $('#wifi-login-chart-filters_form').serializeArray();
		var custom_date = false;
		var custom_date_from = "";
		var custom_date_to = "";
		var date_range = "&period=day&date=" + Date.today().add(-6).days().toString('yyyy-MM-dd') + ",today";
		var this_monday = Date.today().moveToDayOfWeek(1,-1);
		var this_sunday = Date.today().moveToDayOfWeek(0);
		$('#visits-chart').html(loading_img);
		$.each(inputs, function() {
			if (this.name === 'f_time') {
				switch(this.value) {
				    case "custom":
				        custom_date = true;				        
				        break;
				    
				    case "last-7-days":
				        date_range = "&period=day&date=" + Date.today().add(-6).days().toString('yyyy-MM-dd') + ",today";
				        break;
				    case "last-30-days":
				        date_range = "&period=day&date=" + Date.today().add(-29).days().toString('yyyy-MM-dd') + ",yesterday";
				        break;
				    case "this-week":				    	
						date_range = "&period=day&date=" + this_monday.toString('yyyy-MM-dd') + "," + this_sunday.toString('yyyy-MM-dd');
				    	break;
				    case "last-week":				    				    	
				    	var monday = this_monday.addDays(-7);	
						var sunday = this_sunday.addDays(-7);	
						date_range = "&period=day&date=" + monday.toString('yyyy-MM-dd') + "," + sunday.toString('yyyy-MM-dd');
				    	break;
				    case "this-month":
				    	var firstday = new Date().moveToFirstDayOfMonth();
						var lastday = new Date().moveToLastDayOfMonth();
						date_range = "&period=day&date=" + firstday.toString('yyyy-MM-dd') + "," + lastday.toString('yyyy-MM-dd');
				    	break;
				    case "last-month":
				    	var firstday = new Date().addMonths(-1).moveToFirstDayOfMonth();
						var lastday = new Date().addMonths(-1).moveToLastDayOfMonth();
						date_range = "&period=day&date=" + firstday.toString('yyyy-MM-dd') + "," + lastday.toString('yyyy-MM-dd');
				    	break;
				    case "last-3-months":
				    	var firstday = new Date().addMonths(-3).moveToFirstDayOfMonth();
						var lastday = new Date().addMonths(-1).moveToLastDayOfMonth();
						date_range = "&period=day&date=" + firstday.toString('yyyy-MM-dd') + "," + lastday.toString('yyyy-MM-dd');
				    	break;
				    default:
				        break;
				}
			} 			
			if (this.name === 'date_from') {
				custom_date_from = this.value;
			}
			if (this.name === 'date_to') {
				custom_date_to = this.value;
			}
			
		});
		
		if (custom_date && custom_date_from) {
			date_range = "&period=day&date=" + custom_date_from + ",";
			if (custom_date_to) {
				date_range += custom_date_to;
			} else{
				date_range += "yesterday";
			};
			
		};
		//console.log(date_range);
		var segment_string = "";
		if (merchantId) segment_string = "&segment=customVariablePageName1==merchantId;customVariablePageValue1==" + merchantId;
	
		var req1 = encodeURIComponent("method=VisitsSummary.getVisits&idSite=" + piwik.idSite + date_range + segment_string);
		var req2 = encodeURIComponent("method=Actions.get&idSite=" + piwik.idSite + date_range + segment_string);
		var req_url = piwik.host + "index.php?module=API&method=API.getBulkRequest&format=json&urls[0]=" + req1 + "&urls[1]=" + req2 + "&token_auth=" + piwik.token;
		
		$.ajax({
			type: 'GET',
			contentType: "application/json",
			url:req_url,
			crossDomain:true,
			dataType : 'jsonp',
			success : function(json_data){
				//console.log(JSON.stringify(json_data[0]));
				$('#visits-chart').empty();
				var today = Date.today().setHours(0,0,0,0);
				morrisVisits = [];
				jQuery.each(json_data[0], function(date, val) {
					if (! val) { val = 0;};
					var thisday = new Date(date).setHours(0,0,0,0);
					//console.log(thisday);
					if (today < thisday) {
						var subdata = {date: date, visits: null};
					} else {
						var subdata = {date: date, visits: val};
					};
					
					
					morrisVisits.push(subdata);
				});
				jQuery.each(json_data[1], function(date, val) {
					jQuery.each(morrisVisits, function(key, subdata) {
						if (subdata.date == date) {
							var thisday = new Date(date).setHours(0,0,0,0);
							if (today < thisday) {
								morrisVisits[key].pageviews = null;
							} else {
								if (val.nb_pageviews) morrisVisits[key].pageviews = val.nb_pageviews; else morrisVisits[key].pageviews = 0;
							};
							
						};
					});
				});
				//console.log(JSON.stringify(morrisVisits));
				MorrisBuildVisits(morrisVisits, 'visits-chart', '#visits-chart-legend');					
			}
		});
	}
	
	//Device Statistics
	$('#device-statistics').click(function() {$('#device-statistics-filters').slideToggle(500); });
	
	if ($( "#f_device_time" ).val() === 'custom') $('#device-custom-datetime').show(); //show if custom onload
	
	$('#device-statistics-filters select, #device-statistics-filters input').change(function(e) {
		
		if ($( this ).val() === 'custom') {			
			$('#device-custom-datetime').show();
			$('#current-device-filter').html("(Custom date range)");
		} else {
			
			if ($( this ).attr("name") === 'f_device_time') {
				$('#device-custom-datetime').hide();
				$('#current-device-filter').html('(' + $(this).find('option:selected').text() + ')');
			}
			update_device_chart();
		}
	});
	
	
	//Device overview

});

/**
 * 
 * @param {Object} data: array of object
 * @param {Object} container: id of container element without #
 * @param {Object} legend_container: id of legend container element WITH #
 */

function MorrisBuildVisits(data, container, legend_container){
	$('#' + container).empty();
	var chart =  new Morris.Line({
		element: container,
		data: data,
		xkey: 'date',
		ykeys: ['visits', 'pageviews'],
		labels: ['Visits', 'Pageview'],
		lineColors: ['#52cf2b', '#02adea'], //gray: #b7c0c7 blue: #02ades green: #52cf2b
		xLabelFormat : function (x) { return x.toString("MMM-d"); },
		resize: true,
		redraw: true
	});
	$(legend_container).empty();  //reset
	chart.options.labels.forEach(function(label, i){
		var legendItem = $('<span></span>').text(label).css('color', chart.options.lineColors[i]);
		$(legend_container).append(legendItem);
	});
};

function CustomBuildGraph(placeholder, data, legend_container) {
	var labels = [], lineColors = [], morris_data = [];
	$.each(data, function(key,val) {
		labels.push(val.label);
		lineColors.push(val.color);
		$.each(val.data, function(key, p) {
			var d = new Date(p[0]).toString("yyyy-M-d");
			var sub_data = {};
			sub_data.date = d;
			morris_data.push({'date': d, vallabel: p[1]});
		});	
		//console.log(JSON.stringify(val));
	});	
	//console.log(JSON.stringify(morris_data));
	$.plot($(placeholder), data, {
		lines: { show: true },
		points: { show: true },
		grid: { hoverable: true, backgroundColor: '#fefefe' },
		series: {
			lines: { show: true, lineWidth: 2 },
			shadowSize: 0
		},
		legend: {
			show: true,
			noColumns: 1, // number of colums in legend table
			labelFormatter: null, // fn: string -> string
			labelBoxBorderColor: "#ccc", // border color for the little label boxes
			container: $(legend_container), // container (as jQuery object) to put legend in, null means default on top of graph
			position: "ne", // position of default legend container within plot
			margin: 0, // distance from grid edge to default legend container within plot
			backgroundColor: "#dea", // null means auto-detect
			backgroundOpacity: 1, // set to 0 to avoid background
			sorted: null    // default to no legend sorting
		},
		xaxis: { mode: 'time', timeformat: "%d/%m/%y", minTickSize: [1, "day"]},
		selection: { mode: "x" }
	});
}

function showTooltip(x, y, contents) {
								$('<div id="tooltip">' + contents + '</div>').css( {
									position: 'absolute',
									display: 'none',
									top: y + 5,
									left: x + 5,
									'border-radius': '3px',
									'-moz-border-radius': '3px',
									'-webkit-border-radius': '3px',
									padding: '3px 8px 3px 8px',
									color: '#ffffff',
									background: '#000000',
									opacity: 0.80
								}).appendTo("body").fadeIn(500);
}

function update_device_chart(){
	var inputs = $('#device-statistics-filters_form').serializeArray();
	var custom_date = false;
	var custom_date_from = "";
	var custom_date_to = "";
	var date_range = "&period=day&date=yesterday"; //default is yesterday
	var this_monday = Date.today().moveToDayOfWeek(1,-1);
	var this_sunday = Date.today().moveToDayOfWeek(0);
	
	$('#device-type').html(loading_img);
	$('#device-brand').html(loading_img);
		
	$.each(inputs, function() {
		if (this.name === 'f_device_time') {
			switch(this.value) {
			    case "custom":
			        custom_date = true;				        
			        break;
			    case "today":
			        date_range = "&period=day&date=today";
			        break;
			    case "yesterday":
			        date_range = "&period=day&date=yesterday";
			        break;
			    case "last-7-days":
			        date_range = "&period=range&date=" + Date.today().add(-6).days().toString('yyyy-MM-dd') + ",yesterday";
			        break;
			    case "last-30-days":
			        date_range = "&period=range&date=" + Date.today().add(-29).days().toString('yyyy-MM-dd') + ",yesterday";
			        break;
			    case "this-week":				    	
					date_range = "&period=range&date=" + this_monday.toString('yyyy-MM-dd') + "," + this_sunday.toString('yyyy-MM-dd');
			    	break;
			    case "last-week":				    				    	
			    	var monday = this_monday.addDays(-7);	
					var sunday = this_sunday.addDays(-7);	
					date_range = "&period=range&date=" + monday.toString('yyyy-MM-dd') + "," + sunday.toString('yyyy-MM-dd');
			    	break;
			    case "this-month":
			    	var firstday = new Date().moveToFirstDayOfMonth();
					var lastday = new Date().moveToLastDayOfMonth();
					date_range = "&period=range&date=" + firstday.toString('yyyy-MM-dd') + "," + lastday.toString('yyyy-MM-dd');
			    	break;
			    case "last-month":
			    	var firstday = new Date().addMonths(-1).moveToFirstDayOfMonth();
					var lastday = new Date().addMonths(-1).moveToLastDayOfMonth();
					date_range = "&period=range&date=" + firstday.toString('yyyy-MM-dd') + "," + lastday.toString('yyyy-MM-dd');
			    	break;
			    case "last-3-months":
			    	var firstday = new Date().addMonths(-3).moveToFirstDayOfMonth();
					var lastday = new Date().addMonths(-1).moveToLastDayOfMonth();
					date_range = "&period=range&date=" + firstday.toString('yyyy-MM-dd') + "," + lastday.toString('yyyy-MM-dd');
			    	break;
			    default:
			        break;
			}
		} 			
		if (this.name === 'date_from') {
			custom_date_from = this.value;
		}
		if (this.name === 'date_to') {
			custom_date_to = this.value;
		}
			
	});
		
	if (custom_date && custom_date_from) {
		date_range = "&period=range&date=" + custom_date_from + ",";
		if (custom_date_to) {
			date_range += custom_date_to;
		} else{
			date_range += "yesterday";
		};
			
	};
	
	var segment_string = "";
	if (merchantId) segment_string = "&segment=customVariablePageName1==merchantId;customVariablePageValue1==" + merchantId;
	
	var deviceType = encodeURIComponent("method=DevicesDetection.getType&idSite=" + piwik.idSite + "&period=range&date=" + date_range + segment_string);
	var deviceBrand = encodeURIComponent("method=DevicesDetection.getBrand&idSite=" + piwik.idSite + "&period=range&date=" + date_range + segment_string);
	var req_url = piwik.host + "index.php?module=API&method=API.getBulkRequest&format=json&urls[0]=" + deviceType + "&urls[1]=" + deviceBrand + "&token_auth=" + piwik.token;
	$.ajax({
		type: 'GET',
		contentType: "application/json",
		url:req_url,
		crossDomain:true,
		dataType : 'jsonp',
		success : function(json_data){
			//console.log(JSON.stringify(json_data));
			var devTypeData = [];
			jQuery.each(json_data[0], function(key, obj) {
					if (obj.nb_visits) {
						var tmp = {label: obj.label, value: obj.nb_visits};
						devTypeData.push(tmp);
					};
			});
			
			//reset
			$('#device-type').empty();
			$('#device-brand').empty();
			if(devTypeData.length > 0){
				Morris.Donut({
				  element: 'device-type',
				  data: devTypeData,
				  colors: ['#02adea', '#52cf2b', '#ccc'],
				  resize: true
				});
			} else {
				$('#device-type').html('<span>No data</span>');
			}
			//Device brand
			var devBrand = [];
			jQuery.each(json_data[1], function(key, obj) {
				if (key < 10) devBrand.push({brand: obj.label, visits: obj.nb_visits});	
			});
			
			if(devBrand.length > 0){
				Morris.Bar({
				  element: 'device-brand',
				  data: devBrand,
				  xkey: 'brand',
				  ykeys: ['visits'],
				  labels: ['Visits'],
				  barColors: ['#02adea'],
				  xLabelAngle: 60,
				  resize: true
				});
			} else {
				$('#device-brand').html('<span>No data</span>');
			}
											
		} //end of ajax success func
	});
}
