<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Facebook_js extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Facebook JS'
			),
			'description' => array(
				'en' => 'Wifi Login with Facebook Javascript'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',		
		);
		
		if (group_has_role('hotspot', 'admin')) {
			$info['sections']['fb_fields'] = array(
					'name' 	=> 'Settings Fields', // These are translated from your language file
					'uri' 	=> 'admin/facebook_js/fields',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'global:add',
							'uri' 	=> 'admin/facebook_js/fields/create',
							'class' => 'add'
							)
					)
			);			
		}							
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		$this->load->language('facebook_js/facebook_js');
		$this->db->delete('settings', array('module' => 'facebook_js'));
		//add stream
		if ( ! $this->streams->streams->add_stream('Facebook JS Settings', 'settings', 'facebook_js', 'facebook_js_', null)) return false;
		
		//Get account stream ID	
		$account_stream = $this->streams->streams->get_stream('accounts', 'wifi_accounts');
		$loc_stream = $this->streams->streams->get_stream('list', 'location');
			
		$choice = 'Enable
		Disable';
		$fields = array(
			array(
                'name' => 'Status',
                'slug' => 'status',
                'namespace' => 'facebook_js',
                'type' => 'choice',
                'extra'         => array('default_value' => 'Enable','choice_data' => $choice, 'choice_type' => 'dropdown'),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Page ID',
                'slug' => 'page_id',
                'namespace' => 'facebook_js',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'settings',              
            ),
            array(
                'name' => 'Wifi Login Account',
                'slug' => 'wifi_login_account',
                'namespace' => 'facebook_js',
                'type' => 'relationship',
                'extra' => array('choose_stream' => $account_stream->id),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Position',
                'slug' => 'position',
                'namespace' => 'facebook_js',
                'type' => 'integer',
                'extra' => array('max_length' => 1,'default_value' => 1),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Caption',
                'slug' => 'caption',
                'namespace' => 'facebook_js',
                'type' => 'text',
                'extra' => array('max_length' => 64,'default_value' => "Login with Facebook"),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Wait Time',
                'slug' => 'wait_time',
                'namespace' => 'facebook_js',
                'type' => 'integer',
                'extra' => array('max_length' => 2,'default_value' => 5),
                'assign' => 'settings',
            ),
            array(
                'name' => 'CSS Class',
                'slug' => 'css_class',
                'namespace' => 'facebook_js',
                'type' => 'text',
                'extra' => array('max_length' => 64,'default_value' => "btn btn-primary btn-block"),
                'assign' => 'settings',
            ),
            array(
                'name' => 'Enable Sharing',
                'slug' => 'sharing',
                'namespace' => 'facebook_js',
                'type' => 'choice',
                'extra'         => array('default_value' => 'Disable','choice_data' => $choice, 'choice_type' => 'dropdown'),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Post Name',
                'slug' => 'post_name',
                'namespace' => 'facebook_js',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'settings',
            ),
            array(
                'name' => 'Post Link',
                'slug' => 'post_link',
                'namespace' => 'facebook_js',
                'type' => 'text',
                'assign' => 'settings',
            ),
            array(
                'name' => 'Post Caption',
                'slug' => 'post_caption',
                'namespace' => 'facebook_js',
                'extra' => array('max_length' => 64),
                'type' => 'text',
                'assign' => 'settings',
            ),
            array(
                'name' => 'Post Description',
                'slug' => 'post_desc',
                'namespace' => 'facebook_js',
                'type' => 'text',
                'assign' => 'settings',
            ),
            array(
                'name' => 'Post Picture Link',
                'slug' => 'post_picture',
                'namespace' => 'facebook_js',
                'type' => 'text',
                'assign' => 'settings',
            ),
            array(
                'name' => 'Checkin Location',
                'slug' => 'post_location',
                'namespace' => 'facebook_js',
                'extra' => array('max_length' => 64),
                'type' => 'text',
                'assign' => 'settings',
            ),
            array(
                'name' => 'Location ID',
                'slug' => 'loc_id',
                'namespace' => 'facebook_js',
                'type' => 'relationship',
                'extra' => array('choose_stream' => $loc_stream->id),
                'assign' => 'settings',
                'unique' => TRUE,
                'required' => true
            ),
		);

        $this->streams->fields->add_fields($fields);
		
		

        $this->streams->streams->update_stream('settings', 'facebook_js', array(
            'view_options' => array(
            	'status',
                'page_id',
                'wifi_login_account',
                'position',
                'sharing',
                'post_name',
                'post_link',
                'post_caption',
                'post_desc',
                'post_picture',
                'post_location'
            )
        ));
		
		
		// Install the settings
		$settings = array(
			array(
				'slug' => 'app_id',
				'title' => 'APP ID',
				'description' => 'Define Facebook APP ID',
				'type' => 'text',
				'default' => "1436311703303596",
				'value' => '',
				'options' => '',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'facebook_js',
				'order' => 10,
			),
			array(
				'slug' => 'app_secret',
				'title' => 'APP Secret',
				'description' => 'Define Facebook APP secret',
				'type' => 'text',
				'default' => "61e3e7de302e001f6ade38f3ffebea8e",
				'value' => '',
				'options' => '',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'facebook_js',
				'order' => 9,
			)		
		);

		foreach ($settings as $setting)
		{
			if ( ! $this->db->insert('settings', $setting))
			{
				return false;
			}
		}
		$entry_data = array(
		    'mod_name' => "Facebook JS",
		    'mod_slug'   => "facebook_js",
		    'mod_desc'   => "Wifi Login with Facebook by Javascript",		    
		);
		$this->streams->entries->insert_entry($entry_data, 'modules', 'login_modules');
		return TRUE;		
	}

	public function uninstall()
	{
		$this->load->driver('Streams');
			
        // For this teardown we are using the simple remove_namespace
		$this->streams->utilities->remove_namespace('facebook_js');
		$this->db->delete('settings', array('module' => 'facebook_js'));
		
		$params = array(
		    'stream'    => 'modules',
		    'namespace' => 'login_modules',
		    'where' 	=> "mod_slug = 'facebook_js'"
		);		
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			foreach ($entries['entries'] as $entry) {
				$this->streams->entries->delete_entry($entry["id"], "modules", "login_modules");
			}
		}

		
		return TRUE;
	}

	public function upgrade($old_version)
	{
		$this->load->driver('Streams');
		$entry_data = array(
		    'mod_name' => "Facebook JS",
		    'mod_slug'   => "facebook_js",
		    'mod_desc'   => "Wifi Login with Facebook by Javascript",		    
		);
		$this->streams->entries->insert_entry($entry_data, 'modules', 'login_modules');
		
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
    
    
}
/* End of file details.php */
