<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Facebook_js_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		
	}
	
	public function get_settings($location_id = 0)
	{
		$this->load->driver('Streams');
		$data = array();
		
		$params = array(
				'stream'    => 'settings',
    			'namespace' => 'facebook_js',
    			'where'		=> "`loc_id` = '".$location_id."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		
		if ($entries['total']) {
			return $entries['entries'][0];
		} else {
			return array();
		}
	}
	
	public function get_fb_settings(){
		$data = array(
			'app_id' => Settings::get('app_id'),
			'app_secret' => Settings::get('app_secret')
		);
		return $data;
	}
	
	public function get_html($location_id, $device_type, $settings = null, $userdata_key = null)
	{		
		if ($settings['sharing']['val'] == 'Disable') {
			$scope = "email%2Cpublic_profile";
		} else {
			$scope = "email%2Cpublic_profile%2Cpublish_actions";
		}		
		
		$fb_settings = $this->pyrocache->model('facebook_js_m', 'get_fb_settings', array(), 7200);
		
		$redirect_uri = BASE_URL.'facebook_js/callback/'. $location_id;
		if ($userdata_key) {
			$state = $userdata_key;
		} else {
			$state = 0;
		}
		
		$redirect_uri = urlencode($redirect_uri);
		$link = 'https://www.facebook.com/v2.3/dialog/oauth?client_id='.$fb_settings['app_id'].'&redirect_uri='. $redirect_uri .'&state=' . $state . '&sdk=php-sdk-4.5.0&response_type=code&scope='.$scope;
		$login_url = '<a class="wifi-login-link" href="'. $link . '">'. $settings['caption'] .'</a>';
		
		$data = array(
			'html' => $login_url,
			'link' => $link,
			'caption' => $settings['caption'],
			'settings' => $settings
		);
		
		return $data;
	}

}
