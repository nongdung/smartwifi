<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin extends Admin_Controller
{
	protected $section = 'facebook';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('facebook_js');
		$this->load->driver('Streams');
    }

    /**
     * List all billing plans
     */
    public function index()
    {

		redirect('admin/splash/methods');
		
    }
    

	public function settings($loc_id ='')
	{
		//Check loc_id
		$loc_detail = $this->streams->entries->get_entry($loc_id,'list','location');
		if (!$loc_detail && ($loc_detail->created_by_user_id <> $this->current_user->id)) {
			$this->session->set_flashdata('error', "This location is not yours");
			redirect('admin/location');
		}
		$this->load->model('facebook_js_m');
		
		$settings = $this->facebook_js_m->get_settings($loc_id);
		//print_r($settings);exit;
		$extra = array(
		    'return'            => 'admin/splash/methods',
		    'success_message'   => "Updated",
		    'failure_message'   => "Error...!",
		    'title'             => "Settings"
		);
		$hidden = array('loc_id');
		$defaults = array('loc_id' => $loc_id);
		$tabs = array(
			array(
		        'title'     => "Generals",
		        'id'        => 'general-tab',
		        'fields'    => array('status', 'page_id', 'wifi_login_account', 'position', 'caption', 'wait_time', 'button_color')
		    ),
		    array(
		        'title'     => "Share & Checkin",
		        'id'        => 'additional-tab',
		        'fields'    => array('sharing', 'post_name', 'post_link', 'post_caption', 'post_desc', 'post_picture', 'post_location')
		    )
		);
		
		if (! empty($settings)) {
			//delete cache
			$this->pyrocache->delete_cache("facebook_js_m", "get_settings", array($loc_id));
			
			//Update
			$this->streams->cp->entry_form('settings', 'facebook_js', 'edit', $settings['id'], true, $extra, $skips = array('loc_id'),$tabs);
		} else {
			//Add new
			$this->streams->cp->entry_form('settings', 'facebook_js', 'new',null, true, $extra, array(),$tabs,$hidden, $defaults);
		}
	}

}