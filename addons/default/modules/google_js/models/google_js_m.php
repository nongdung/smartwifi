<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Google_js_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		
	}
	
	public function get_settings($location_id)
	{
		$this->load->driver('Streams');
		
		$params = array(
		    'stream'    => 'settings',
		    'namespace' => 'google_js',
		    'where'		=> "`loc_id` = '".$location_id."'"
		);
		
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			return $entries['entries'][0];
		} else {
			return array();
		}
		
		
	}
	
	public function get_gg_settings(){
		$data = array(
			'client_id' => Settings::get('client_id'),
			'client_secret' => Settings::get('client_secret'),
			'email' => Settings::get('email'),
			'api_key' => Settings::get('api_key'),
			'redirect_uri' => Settings::get('redirect_uri'),
			'plus_login' => Settings::get('plus_login')
		);
		return $data;
	}
	
	public function get_html($location_id, $device_type, $settings = null, $userdata_key = null)
	{
		$gg_settings = $this->pyrocache->model('google_js_m', 'get_gg_settings', array(), 7200);
		$redirect_uri = urlencode($gg_settings['redirect_uri']);
		$client_id = $gg_settings['client_id'];
		if ($gg_settings['plus_login']){
			$plus_login = "+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.login";
		} else {
			$plus_login ="";
		}
		
		if ($userdata_key) {
			$state = $userdata_key;
		} else {
			$state = 0;
		}
		
		//$settings = $this->pyrocache->model('click_m', 'get_settings', array($location_id), 30);
		$link_tmp = "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=".$redirect_uri."&client_id=".$client_id."&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile".$plus_login."&access_type=online&approval_prompt=auto&state=" . $state;

		$login_url = "<a class='wifi-login-link' href='".$link_tmp."'>". $settings['caption']."</a>";
		
		$data = array(
			'html' => $login_url,
			'link' => $link_tmp,
			'caption' => $settings['caption'],
			'settings' => $settings
		);		
		return $data;
	}

}
