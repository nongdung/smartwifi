<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Google_logs_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		
	}
	
	public function get_by_merchant_id($merchant_id)
	{
		$result = $this->db
					->select('g_id')
					->where('merchant_id', $merchant_id)
					->get('google_logs')
					->result_array();
		
		return $result;
	}

	public function check_g_id($g_id)
	{
		$result = $this->db
					->select('g_id')
					->where('g_id', $g_id)
					->get('google_logs');
		if ($result->num_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function insert_raw_logs($data)
	{
		$data_insert = array(
			'g_id'	=> $data['g_id'],
			'loc_id' => $data['loc_id'],
			'merchant_id' => $data['merchant_id'],
			'mac' => $data['mac'],
			'created' => date('Y-m-d H:i:s')
		);
		
		$this->db->insert('google_raw_logs', $data_insert); 
	}	

}
