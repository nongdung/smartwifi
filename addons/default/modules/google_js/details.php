<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Google_js extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Google JS'
			),
			'description' => array(
				'en' => 'Wifi Login with Google JS'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',		
		);
		
		if (group_has_role('hotspot', 'admin')) {
			$info['sections']['gg_fields'] = array(
					'name' 	=> 'Settings Fields', // These are translated from your language file
					'uri' 	=> 'admin/google_js/fields',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'global:add',
							'uri' 	=> 'admin/google_js/fields/create',
							'class' => 'add'
							)
					)
			);			
		}
									
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		$this->load->language('google_js/google_js');
		$this->db->delete('settings', array('module' => 'google_js'));
		//add stream
		if ( ! $this->streams->streams->add_stream('Google JS Settings', 'settings', 'google_js', 'google_js_', null)) return false;
		
		//Get account stream ID	
		$account_stream = $this->streams->streams->get_stream('accounts', 'wifi_accounts');
		$loc_stream = $this->streams->streams->get_stream('list', 'location');
			
		$choice = 'Enable
		Disable';
		$fields = array(
			array(
                'name' => 'Status',
                'slug' => 'status',
                'namespace' => 'google_js',
                'type' => 'choice',
                'extra'         => array('default_value' => 'Enable','choice_data' => $choice, 'choice_type' => 'dropdown'),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Wifi Login Account',
                'slug' => 'wifi_login_account',
                'namespace' => 'google_js',
                'type' => 'relationship',
                'extra' => array('choose_stream' => $account_stream->id),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Position',
                'slug' => 'position',
                'namespace' => 'google_js',
                'type' => 'integer',
                'extra' => array('max_length' => 1,'default_value' => 1),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Location ID',
                'slug' => 'loc_id',
                'namespace' => 'google_js',
                'type' => 'relationship',
                'extra' => array('choose_stream' => $loc_stream->id),
                'assign' => 'settings',
                'unique' => TRUE,
                'required' => true
            ),
            array(
                'name' => 'Caption',
                'slug' => 'caption',
                'namespace' => 'google_js',
                'type' => 'text',
                'extra' => array('max_length' => 64,'default_value' => "Login with Google"),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Wait Time',
                'slug' => 'wait_time',
                'namespace' => 'google_js',
                'type' => 'integer',
                'extra' => array('max_length' => 2,'default_value' => 5),
                'assign' => 'settings',
            ),
            array(
                'name' => 'CSS Class',
                'slug' => 'css_class',
                'namespace' => 'google_js',
                'type' => 'text',
                'extra' => array('max_length' => 64,'default_value' => "btn btn-primary btn-block"),
                'assign' => 'settings',
            ),
		);

        $this->streams->fields->add_fields($fields);		

        $this->streams->streams->update_stream('settings', 'google_js', array(
            'view_options' => array(
            	'status',
                'wifi_login_account',
                'position'
            )
        ));
		
		// Install the settings
		$settings = array(
			array(
				'slug' => 'client_id',
				'title' => 'Client ID',
				'description' => 'Define Google client ID in console',
				'type' => 'text',
				'default' => "627425815086-ji9ooii0gg3dsav2u8ps2261sfqhuqoe.apps.googleusercontent.com",
				'value' => '',
				'options' => '',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'google_js',
				'order' => 1010,
			),
			array(
				'slug' => 'client_secret',
				'title' => 'Client Secrete',
				'description' => 'Define Google client secret in console',
				'type' => 'text',
				'default' => "SL3XOtFFSGzYO9AmVNuaUL6V",
				'value' => '',
				'options' => '',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'google_js',
				'order' => 1009,
			),
			array(
				'slug' => 'email',
				'title' => 'Email',
				'description' => 'Define Google email in console',
				'type' => 'text',
				'default' => "627425815086-ji9ooii0gg3dsav2u8ps2261sfqhuqoe@developer.gserviceaccount.com",
				'value' => '',
				'options' => '',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'google_js',
				'order' => 1008,
			),
			array(
				'slug' => 'api_key',
				'title' => 'API Key',
				'description' => 'Define Google api key in console',
				'type' => 'text',
				'default' => "AIzaSyC_7GQmdujjTsb5PLKU4BZDL4EO79XjpqU",
				'value' => '',
				'options' => '',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'google_js',
				'order' => 1007,
			),
			array(
				'slug' => 'redirect_uri',
				'title' => 'Redirect URI',
				'description' => 'Define redirect url for Google app',
				'type' => 'text',
				'default' => "http://smartwifi.vn/google_js/login",
				'value' => '',
				'options' => '',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'google_js',
				'order' => 1006,
			),
			array(
				'slug' => 'plus_login',
				'title' => 'Plus Login',
				'description' => 'Google login with plus scope',
				'type' => 'radio',
				'default' => false,
				'value' => '',
				'options' => '1=Enabled|0=Disabled',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => 'google_js',
				'order' => 1005,
			),			
		);

		foreach ($settings as $setting)
		{
			if ( ! $this->db->insert('settings', $setting))
			{
				return false;
			}
		}
		
		$entry_data = array(
		    'mod_name' => "Google JS",
		    'mod_slug'   => "google_js",
		    'mod_desc'   => "Wifi Login with Google by Javascript",		    
		);
		$this->streams->entries->insert_entry($entry_data, 'modules', 'login_modules');
		
		return TRUE;
		
	}

	public function uninstall()
	{
		$this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
		$this->streams->utilities->remove_namespace('google_js');
		$this->db->delete('settings', array('module' => 'google_js'));
		
		$params = array(
		    'stream'    => 'modules',
		    'namespace' => 'login_modules',
		    'where' 	=> "mod_slug = 'google_js'"
		);		
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			foreach ($entries['entries'] as $entry) {
				$this->streams->entries->delete_entry($entry["id"], "modules", "login_modules");
			}
		}
		
		return TRUE;
	}


	public function upgrade($old_version)
	{
		$entry_data = array(
		    'mod_name' => "Google JS",
		    'mod_slug'   => "google_js",
		    'mod_desc'   => "Wifi Login with Google by Javascript",		    
		);
		$this->streams->entries->insert_entry($entry_data, 'modules', 'login_modules');
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
    
    
    public function admin_menu(&$menu)
    {	

	}
}
/* End of file details.php */
