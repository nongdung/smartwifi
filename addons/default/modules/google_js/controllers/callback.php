<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	splash Module
 */
class Callback extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
		//$this->pyrocache->write("ABCD", 'dungnn\testcache');
		$this->load->model('devices/device_m');
		$this->load->model('google_js_m');
		$this->load->model('splash/splash_m');
		$this->load->model('hotspot/merchant_m');
		$this->load->model('hotspot/service_class_m');
		$this->load->model('location/location_m');
		$this->load->model('splash/hotspot_theme_m');
		
		//Detect protocol
		if (isset($_SERVER['HTTPS']) &&
		    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
		    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
		  $protocol = 'https://';
		}
		else {
		  $protocol = 'http://';
		}
		
		//Detect Apple devices
		preg_match("/iPhone|iPad|iPod|OS X|Captive/", $_SERVER['HTTP_USER_AGENT'], $matches);
		$isApple = !empty($matches);
		if ($isApple && isset($_GET['state']) && $_GET['state'] && $this->config->item('sess_use_redis')) {
			$this->load->library("splash/sw_redis");
			if ($this->sw_redis->exists($_GET['state'])){
				//read data from state key
				$redis_userdata = $this->sw_redis->get($_GET['state']);
				if (!empty($redis_userdata)) {
					$this->session->set_userdata($redis_userdata);
				}
			}
			
		}
		
		$user_data = $this->session->all_userdata();
		

		if (isset($user_data['location_id'])) {
			$location_id = $user_data['location_id'];
		} else {
			//need more work
			$location_id = 1;
		}
		if (! $this->agent->is_mobile()) {
			$device_type = "desktop";
		}
		$gg_settings = $this->pyrocache->model('google_js_m', 'get_gg_settings', array(), 7200);
		$settings = $this->pyrocache->model('google_js_m', 'get_settings', array($location_id), 3600);
		$location_detail = $this->pyrocache->model('location_m', 'get_detail', array($location_id), 3600);
		//print_r($settings);exit;
		
		$merchant_info = $this->merchant_m->get_info($location_detail['created_by']['user_id']);		
		if ($merchant_info['end_date'] < time()) {
			//Drop this location to Free class
			$service_class = $this->service_class_m->get_by_name("Free");			
		} else {
			$service_class = $this->service_class_m->get_by_id($merchant_info['class_id']);
		}
		
		//Get theme detail
		$theme = $this->hotspot_theme_m->getTheme($location_detail['theme']);
		$theme_detail = $this->hotspot_theme_m->getThemeDetail($location_detail['theme'], $location_detail['id']);
		$model = $theme['theme_slug']."_m";
		$this->load->model($theme['theme_slug']."/".$model);
		$theme_info = $this->$model->get_info();		
		$this->$model->set_assets($device_type);
		
		//print_r($wait_template_details);
		Asset::add_path('smartwifi', 'assets/smartwifi/');
		Asset::js('smartwifi::basic.js', FALSE, 'smartwifi');
		Asset::js('smartwifi::google.js', FALSE, 'smartwifi');
		Asset::js('smartwifi::swController.js', FALSE, 'smartwifi');
		Asset::js('jquery::jquery.countdown.min.js', FALSE, 'smartwifi');
		
		$this->template
				->title("Wifi Login")
				->set('location_detail', $location_detail)				
				->set('gg_settings', $gg_settings)
				->set('settings', $settings)
				->set('theme_slug', $theme['theme_slug'])
				->set('theme_detail', $theme_detail)
				->set('device_type', $device_type)				
				->set('protocol', $protocol)
				->set('static_host', $this->config->item('static_host'))			
				->set_layout("hotspot/".$theme['theme_slug']."/login.html")			
				->build('googlecallback');
	}
}
