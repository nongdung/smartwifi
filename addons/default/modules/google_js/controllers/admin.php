<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin extends Admin_Controller
{
	protected $section = 'googlejs';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('google_js');
		$this->load->driver('Streams');
    }

    /**
     * List all billing plans
     */
    public function index()
    {
    	
		redirect('admin/splash/methods');
		
    }
    

	public function settings($loc_id ='')
	{
		//Check loc_id
		$loc_detail = $this->streams->entries->get_entry($loc_id,'list','location');
		if (!$loc_detail && ($loc_detail->created_by_user_id <> $this->current_user->id)) {
			$this->session->set_flashdata('error', "This location is not yours");
			redirect('admin/location');
		}
		$this->load->model('google_js_m');
		
		$settings = $this->google_js_m->get_settings($loc_id);
		//print_r($settings);exit;
		$extra = array(
		    'return'            => 'admin/splash/methods',
		    'success_message'   => "Updated",
		    'failure_message'   => "Error...!",
		    'title'             => "Settings"
		);
		$hidden = array('loc_id');
		$defaults = array('loc_id' => $loc_id);
		$tabs = array(
			array(
		        'title'     => "Generals",
		        'id'        => 'general-tab',
		        'fields'    => array('status', 'page_id', 'wifi_login_account', 'position', 'caption', 'wait_time', 'button_color')
		    ),
		    array(
		        'title'     => "Share & Checkin",
		        'id'        => 'additional-tab',
		        'fields'    => array('sharing', 'post_name', 'post_link', 'post_caption', 'post_desc', 'post_picture', 'post_location')
		    )
		);
		
		if (! empty($settings)) {
			//delete cache
			$this->pyrocache->delete_cache("google_js_m", "get_settings", array($loc_id));
			
			//Update
			$this->streams->cp->entry_form('settings', 'google_js', 'edit', $settings['id'], true, $extra, $skips = array('loc_id'));
		} else {
			//Add new
			$this->streams->cp->entry_form('settings', 'google_js', 'new',null, true, $extra, array(),false,$hidden, $defaults);
		}
		/*		
		//Check loc_id
		$loc_detail = $this->streams->entries->get_entry($loc_id,'list','location');
		if (!$loc_detail && ($loc_detail->created_by_user_id <> $this->current_user->id)) {
			$this->session->set_flashdata('error', "This location is not yours");
			redirect('admin/location');
		}
		
		$validation_rule = array(
            array(
                'field' => 'position',
                'label' => 'Position',
                'rules' => 'trim|max_length[1]|is_natural_no_zero|required'
            ),
            array(
                'field' => 'caption',
                'label' => 'Login Button Caption',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'wifi_login_account',
                'label' => 'Wifi Login Account',
                'rules' => 'required'
            ),
            array(
                'field' => 'wait_time',
                'label' => 'Waiting Time',
                'rules' => 'trim|max_length[2]|is_natural_no_zero'
            ),
            array(
                'field' => 'css_class',
                'label' => 'CSS Class',
                'rules' => 'trim|max_length[64]'
            ),
            array(
                'field' => 'loc_id',
                'label' => 'Location ID',
                'rules' => 'required'
            )
		);
		
		$this->form_validation->set_rules($validation_rule);
		
		if ($this->form_validation->run()){
			if ($this->input->post("id")) {
				//Update existing record
				$entry_data = array(
					'status' => $this->input->post("status"),
				    'wifi_login_account' => $this->input->post("wifi_login_account"),
				    'position'   => $this->input->post("position"),				   
				    'caption'   => $this->input->post("caption"),
				    'wait_time'   => $this->input->post("wait_time"),
				    'css_class'   => $this->input->post("css_class"),
				);
				$this->streams->entries->update_entry($this->input->post("id"),$entry_data, 'settings', 'google_js');
			} else {
				//insert new record
				$entry_data = array(
					'status' => $this->input->post("status"),
				    'wifi_login_account' => $this->input->post("wifi_login_account"),
				    'position'   => $this->input->post("position"),
					'loc_id'   => $this->input->post("loc_id"),
					'caption'   => $this->input->post("caption"),
				    'wait_time'   => $this->input->post("wait_time"),
				    'css_class'   => $this->input->post("css_class"),
				);
				$this->streams->entries->insert_entry($entry_data, 'settings', 'google_js');
			}
			
			//Delete cache file
			$this->pyrocache->delete_cache("google_js_m", "get_settings", array($this->input->post("loc_id")));
			$this->session->set_flashdata('success', "Updated");
			redirect('admin/splash/methods');
			
		}
		
		$params = array(
		    'stream'    => 'settings',
		    'namespace' => 'google_js',
		    'where'		=> "`loc_id` ='".$loc_id."'"
		);
		
		$entries = $this->streams->entries->get_entries($params);

		//Get wifi login accounts
		$params = array(
		    'stream'    => 'accounts',
		    'namespace' => 'wifi_accounts',
		);
		$accounts = $this->streams->entries->get_entries($params);
		$accounts_list = array(null => "-----");
		foreach ($accounts['entries'] as $account) {
			$accounts_list[$account['id']] = $account['account_name'];
		}
		//print_r($accounts_list);exit;
		
		$this->template
				->title($this->module_details['name'], lang('google:settings'))
				->set('entries',$entries)
				->set('loc_id',$loc_id)
				->set('accounts_list',$accounts_list)
				->build('admin/settings');
		*/
	}

}