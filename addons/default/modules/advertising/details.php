<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Advertising extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Advertising'
			),
			'description' => array(
				'en' => 'Basic advert management'
			),
			'frontend' => FALSE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',
			'roles' => array(
                'merchant','manager', 'admin'
            )		
		);
		
		$info['sections']['statistics'] = array(
			'name' 	=> 'advertising:statistics', // These are translated from your language file
			'uri' 	=> 'admin/advertising/statistics',
					
		);
		
		$info['sections']['advert'] = array(
					'name' 	=> 'advertising:adverts', // These are translated from your language file
					'uri' 	=> 'admin/advertising/advert',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'advertising:add_advert',
							'uri' 	=> 'admin/advertising/advert/add',
							'class' => 'add'
							)
					)
		);
		
		$info['sections']['campaign'] = array(
					'name' 	=> 'advertising:campaigns', // These are translated from your language file
					'uri' 	=> 'admin/advertising/campaign',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'advertising:add_campaign',
							'uri' 	=> 'admin/advertising/campaign/add',
							'class' => 'add'
							)
					)
		);
		
		
		if (group_has_role('hotspot', 'admin')) {
			$info['sections']['campaign_fields'] = array(
					'name' 	=> 'advertising:campaign_fields', // These are translated from your language file
					'uri' 	=> 'admin/advertising/campaign-fields',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'streams:add_field',
							'uri' 	=> 'admin/advertising/campaign-fields/add',
							'class' => 'add'
							)
					)
			);
			$info['sections']['advert_fields'] = array(
					'name' 	=> 'advertising:advert_fields', // These are translated from your language file
					'uri' 	=> 'admin/advertising/advert-fields',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'streams:add_field',
							'uri' 	=> 'admin/advertising/advert-fields/add',
							'class' => 'add'
							)
					)
			);			
		}
								
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		$this->load->library('files/files');
		$this->load->model('files/file_folders_m');
		
		$this->db->delete('settings', array('module' => 'advertising'));
		//add stream
		if ( ! $campaign_stream_id = $this->streams->streams->add_stream('Advert Campaign', 'campaign', 'advertising', 'ads_', null)) return false;
		if ( ! $this->streams->streams->add_stream('Advert Set', 'advert', 'advertising', 'ads_', null)) return false;
		$choice = 'Enable
		Disable';
		
		//Campaign Fields
		$fields = array(
			array(
			    'namespace' => 'advertising',
				'assign' => 'campaign',
                'name' => 'Campaign Status',
                'slug' => 'campaign_status',
                'type' => 'choice',
                'extra'         => array('default_value' => 'Disable','choice_data' => $choice, 'choice_type' => 'dropdown'),
                'required' => true
            ),
            array(
			    'namespace' => 'advertising',
				'assign' => 'campaign',
                'name' => 'Campaign Name',
                'slug' => 'campaign_name',
                'type' => 'text',
                'extra' => array('max_length' => 400),
                'title_column' => true,
                'required' => true
            ),
            array(
			    'namespace' => 'advertising',
				'assign' => 'campaign',
                'name' => 'Start Time',
                'slug' => 'start_time',
                'type' => 'datetime',
                'extra' => array('use_time' => 'yes', 'storage' => 'unix', 'input_type' => 'datepicker'),
                'required' => true
            ),
            array(
			    'namespace' => 'advertising',
				'assign' => 'campaign',
                'name' => 'End Time',
                'slug' => 'end_time',
                'type' => 'datetime',
                'extra' => array('use_time' => 'yes', 'storage' => 'unix', 'input_type' => 'datepicker'),
            ),
            array(
			    'namespace' => 'advertising',
				'assign' => 'campaign',
                'name' => 'Target',
                'slug' => 'target',
                'type' => 'text',
                'extra' => array('max_length' => 200),
            ),
		);
		if (! $this->streams->fields->add_fields($fields)) return false;
		$this->streams->streams->update_stream('campaign', 'advertising', array(
            'view_options' => array(
                'campaign_name',
                'campaign_status',               
                'start_time',
                'end_time'
             )
        ));
		
		//Advert Fields
		$fields = array(
			array(
			    'namespace' => 'advertising',
				'assign' => 'advert',
                'name' => 'Advert Status',
                'slug' => 'advert_status',
                'type' => 'choice',
                'extra'         => array('default_value' => 'Disable','choice_data' => $choice, 'choice_type' => 'dropdown'),
                'required' => true
            ),
            array(
			    'namespace' => 'advertising',
				'assign' => 'advert',
                'name' => 'Advert Name',
                'slug' => 'advert_name',
                'type' => 'text',
                'extra' => array('max_length' => 400),
                'required' => true
            ),
            array(
			    'namespace' => 'advertising',
				'assign' => 'advert',
                'name' => 'Website URL',
                'slug' => 'website_url',
                'type' => 'text',
                'extra' => array('max_length' => 200),
                'required' => true
            ),
            /*
			 * Created this field later in Fields Stream
			 * 
            array(
			    'namespace' => 'advertising',
				'assign' => 'advert',
                'name' => 'Advert Image',
                'slug' => 'advert_image',
                'type' => 'image',
                'extra' => array('max_length' => 400),
                'required' => true
            ),
            */
            array(
			    'namespace' => 'advertising',
				'assign' => 'advert',
                'name' => 'Advert Headeline',
                'slug' => 'advert_headline',
                'type' => 'text',
                'extra' => array('max_length' => 25),
            ),
            array(
			    'namespace' => 'advertising',
				'assign' => 'advert',
                'name' => 'Advert Text',
                'slug' => 'advert_text',
                'type' => 'text',
                'extra' => array('max_length' => 90),
            ),
            array(
			    'namespace' => 'advertising',
				'assign' => 'advert',
                'name' => 'Advert Link Description',
                'slug' => 'advert_link_desc',
                'type' => 'text',
                'extra' => array('max_length' => 200),
            ),
            array(
			    'namespace' => 'advertising',
				'assign' => 'advert',
                'name' => 'Call-To-Action Button',
                'slug' => 'cta_button',
                'type' => 'text',
                'extra' => array('max_length' => 12),
            ),
            array(
			    'namespace' => 'advertising',
				'assign' => 'advert',
                'name' => 'Display Link',
                'slug' => 'advert_display_link',
                'type' => 'text',
                'extra' => array('max_length' => 100),
            ),
			array(
			    'namespace' => 'advertising',
				'assign' => 'advert',
                'name' => 'Campaign',
                'slug' => 'advert_campaign',
                'type' => 'relationship',
                'extra' => array('choose_stream' => $campaign_stream_id),
                'required' => true
            )
		);
		if (! $this->streams->fields->add_fields($fields)) return false;
		$this->streams->streams->update_stream('advert', 'advertising', array(
            'view_options' => array(
                'advert_name',
                'advert_status',               
                'website_url',
                'advert_campaign'
             )
        ));

		return TRUE;
		
	}

	public function uninstall()
	{
		$this->load->driver('Streams');
		$this->streams->utilities->remove_namespace('advertising');
		$this->db->delete('settings', array('module' => 'advertising'));

		return TRUE;
	}


	public function upgrade($old_version)
	{
		$this->load->driver('Streams');
		$this->streams->streams->update_stream('campaign', 'advertising', array(
            'view_options' => array(
                'campaign_name',
                'campaign_status',               
                'start_time',
                'end_time'
             )
        ));
		
		$this->streams->streams->update_stream('advert', 'advertising', array(
            'view_options' => array(
                'advert_name',
                'advert_status',               
                'website_url',
                'advert_campaign'
             )
        ));
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
	
	public function admin_menu(&$menu)
    {
    	if (group_has_role('hotspot', 'merchant')) {
	    	$menu['Advertising'] = array(
	    		'Statistics' => 'admin/advertising/statistics',
	        	'Campaign' => 'admin/advertising/campaign',
	        	'Advert' => 'admin/advertising/advert', 
	        );
		}
		add_admin_menu_place('Advertising', 2);
	}

}
/* End of file details.php */
