<?php
//messages
$lang['advertising:filters']			=	'Filters';
$lang['advertising:advertising']			=	'Advertising';

$lang['advertising:detail']			=	'Detail >>';

$lang['advertising:no_items']			=	'No items';

$lang['advertising:campaigns']			=	'Campaigns';
$lang['advertising:campaign']			=	'Campaign';
$lang['advertising:campaign_name']			=	'Campaign Name';
$lang['advertising:add_campaign']			=	'Add Campaign';
$lang['advertising:edit_campaign']			=	'Edit Campaign';
$lang['advertising:campaign_management']			=	'Campaign Management';
$lang['advertising:campaign_fields']			=	'Campaign Fields';
$lang['advertising:campaign_statistics']			=	'Campaign Statistics';

$lang['advertising:adverts']			=	'Adverts';
$lang['advertising:adverts_statistics']			=	'Adverts Statistics';
$lang['advertising:advert_name']			=	'Advert Name';
$lang['advertising:add_advert']			=	'Add Advert';
$lang['advertising:edit_advert']			=	'Edit Advert';
$lang['advertising:advert_management']			=	'Advert Management';
$lang['advertising:advert_fields']			=	'Advert Fields';

$lang['advertising:submit_success']			=	'Submit Success';
$lang['advertising:submit_failure']			=	'Submit Failed';

$lang['advertising:statistics']			=	'Statistics';
$lang['advertising:status']			=	'Status';
$lang['advertising:start_time']			=	'Start Time';
$lang['advertising:end_time']			=	'End Time';

$lang['advertising:time']			=	'Time';
$lang['advertising:today']			=	'Today';
$lang['advertising:yesterday']			=	'Yesterday';
$lang['advertising:last-7-days']			=	'Last 7 days';
$lang['advertising:last-30-days']			=	'Last 30 days';
$lang['advertising:this-week']			=	'This week';
$lang['advertising:last-week']			=	'Last week';
$lang['advertising:this-month']			=	'This month';
$lang['advertising:last-month']			=	'Last month';
$lang['advertising:last-3-months']			=	'Last 3 months';
$lang['advertising:location']			=	'Location';
$lang['advertising:device']			=	'Device';
?>