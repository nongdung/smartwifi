<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Blog Permissions
$lang['advertising:role_merchant']	= 'Merchant';
$lang['advertising:role_manager']     = 'Manager';
$lang['advertising:role_admin']     = 'Admin';