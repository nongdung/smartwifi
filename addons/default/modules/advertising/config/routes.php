<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	www.your-site.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://www.codeigniter.com/user_guide/general/routing.html
*/
$route['advertising/admin/statistics(/:num)?']			= 'admin_statistics/index$1';
$route['advertising/admin/statistics(/:any)?']			= 'admin_statistics$1';

$route['advertising/admin/campaign(/:num)?']			= 'admin_campaign/index$1';
$route['advertising/admin/campaign(/:any)?']			= 'admin_campaign$1';

$route['advertising/admin/campaign-fields(/:num)?']			= 'admin_campaign_fields/index$1';
$route['advertising/admin/campaign-fields(/:any)?']			= 'admin_campaign_fields$1';

$route['advertising/admin/advert(/:num)?']			= 'admin_advert/index$1';
$route['advertising/admin/advert(/:any)?']			= 'admin_advert$1';

$route['advertising/admin/advert-fields(/:num)?']			= 'admin_advert_fields/index$1';
$route['advertising/admin/advert-fields(/:any)?']			= 'admin_advert_fields$1';

$route['advertising/admin(/:any)?']			= 'admin_campaign/index$1';
