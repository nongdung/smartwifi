<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Advertising_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		$this->load->driver('Streams');
	}
	
	function get_advert($merchant_id  = null)
	{
		if (! $merchant_id) return array();
		
		$advert_set = array();
		$campaigns = $this->get_available_campaign($merchant_id);
		foreach ($campaigns as $campaign_id) {
			$adverts = $this->get_available_advert($campaign_id);
			foreach ($adverts as $advert) {
				array_push($advert_set, $advert);
			}
		}
		
		return $advert_set;
	}
	
	/*
	 * Return array of available campaign_id
	 */	
	public function get_available_campaign($merchant_id = null)
	{
		$cur_time = time();
		$params = array(
				'stream'    => 'campaign',
    			'namespace' => 'advertising',
    			'where'		=> "`created_by` = '".$merchant_id."' AND `start_time` <= '".$cur_time."' AND `campaign_status` = 'Enable'"
		);
		$entries = $this->streams->entries->get_entries($params);
		
		$data = array();
		
		foreach ($entries['entries'] as $campaign) {
			if (($cur_time <= $campaign['end_time']) || !$campaign['end_time']) {
				array_push($data, $campaign['id']);
			}
		}
		
		return $data;
	}
	
	public function get_available_advert($campaign_id = null)
	{
		$params = array(
				'stream'    => 'advert',
    			'namespace' => 'advertising',
    			'where'		=> "`advert_campaign` = '".$campaign_id."' AND `advert_status` = 'Enable'"
		);
		$entries = $this->streams->entries->get_entries($params);
		return $entries['entries'];
	}
	
	public function get_all_advert($merchant_id = null)
	{
		if (! $merchant_id) return array();
		$params = array(
				'stream'    => 'advert',
    			'namespace' => 'advertising',
    			'where'		=> "`created_by` = '".$merchant_id."'"
		);
		$entries = $this->streams->entries->get_entries($params);

		return $entries['entries'];
	}
	
	public function get_adverts_of_campaign($merchant_id = null, $campaign_id = null)
	{
		if (! $merchant_id || ! $campaign_id) return array();
		$params = array(
				'stream'    => 'advert',
    			'namespace' => 'advertising',
    			'where'		=> "`created_by` = '".$merchant_id."' AND `advert_campaign` = '".$campaign_id."'"
		);
		$entries = $this->streams->entries->get_entries($params);

		return $entries['entries'];
	}
	
	/*
	 * Return array of all campaigns
	 */	
	public function get_all_campaign($merchant_id = null)
	{
		if (! $merchant_id) return array();
		$params = array(
				'stream'    => 'campaign',
    			'namespace' => 'advertising',
    			'where'		=> "`created_by` = '".$merchant_id."'"
		);
		$entries = $this->streams->entries->get_entries($params);

		return $entries['entries'];
	}
	
	
	public function get_campaign_statistics($period = null, $merchant_id = null, $location_id = null, $device_mac = null)
	{
		$this->load->config('piwik');
		//$campaign_list = $this->pyrocache->model('advertising_m', 'get_all_campaign', array($this->current_user->id), 7200);
		
		$url = $this->config->item('piwik_protocol')."://";
		$url .= $this->config->item('piwik_host')."/index.php";
		$url .= "?module=API&method=Contents.getContentNames";
		$url .= "&idSite=".$this->config->item('piwik_site_id');
		
		if (! $period) {
			$url .= "&period=month&date=yesterday";
		} else {
			$url .= "&period=".$period;
		}
		
		$segment = "";
		if ($merchant_id) {
			if (! strlen($segment)) {
				$segment = "&segment=customVariablePageName1==merchantId;customVariablePageValue1==".$merchant_id;
			} else {
				$segment .= ";customVariablePageName1==merchantId;customVariablePageValue1==".$merchant_id;
			}	
		}
		
		if ($location_id) {
			if (! strlen($segment)) {
				$segment = "&segment=customVariablePageName2==locationId;customVariablePageValue2==".$location_id;
			} else {
				$segment .= ";customVariablePageName2==locationId;customVariablePageValue2==".$location_id;
			}	
		}
		
		if ($device_mac) {
			if (! strlen($segment)) {
				$segment = "&segment=customVariablePageName3==locationId;customVariablePageValue3==".$device_mac;
			} else {
				$segment .= ";customVariablePageName3==deviceMac;customVariablePageValue3==".$device_mac;
			}	
		}
		$url .= $segment;
		
		$url .= "&format=JSON&filter_limit=20";
		$url .= "&token_auth=".$this->config->item('piwik_view_token');
		
		$fetched = @file_get_contents($url);
	
		if ($fetched === false) {
			$data = array();
		} else {
			$data = json_decode($fetched, true);
		}
		//$content = unserialize($fetched);
		
		return $data;
	}

	public function get_advert_statistics($period = null, $merchant_id = null, $location_id = null, $device_mac = null)
	{
		$this->load->config('piwik');
		//$campaign_list = $this->pyrocache->model('advertising_m', 'get_all_advert', array($this->current_user->id), 7200);
		
		$url = $this->config->item('piwik_protocol')."://";
		$url .= $this->config->item('piwik_host')."/index.php";
		$url .= "?module=API&method=Contents.getContentPieces";
		$url .= "&idSite=".$this->config->item('piwik_site_id');
		
		if (! $period) {
			$url .= "&period=month&date=yesterday";
		} else {
			$url .= "&period=".$period;
		}
		
		$segment = "";
		if ($merchant_id) {
			if (! strlen($segment)) {
				$segment = "&segment=customVariablePageName1==merchantId;customVariablePageValue1==".$merchant_id;
			} else {
				$segment .= ";customVariablePageName1==merchantId;customVariablePageValue1==".$merchant_id;
			}	
		}
		
		if ($location_id) {
			if (! strlen($segment)) {
				$segment = "&segment=customVariablePageName2==locationId;customVariablePageValue2==".$location_id;
			} else {
				$segment .= ";customVariablePageName2==locationId;customVariablePageValue2==".$location_id;
			}	
		}
		
		if ($device_mac) {
			if (! strlen($segment)) {
				$segment = "&segment=customVariablePageName3==locationId;customVariablePageValue3==".$device_mac;
			} else {
				$segment .= ";customVariablePageName3==deviceMac;customVariablePageValue3==".$device_mac;
			}	
		}
		$url .= $segment;
		
		$url .= "&format=JSON&filter_limit=20";
		$url .= "&token_auth=".$this->config->item('piwik_view_token');
		
		$fetched = @file_get_contents($url);
	
		if ($fetched === false) {
			$data = array();
		} else {
			$data = json_decode($fetched, true);
		}
		//$content = unserialize($fetched);
		
		return $data;
	}

	public function check_campaign_owner($campaign_id = null, $merchant_id = null)
	{
		if (! $merchant_id || !$campaign_id) return false;
		
		$entry = $this->streams->entries->get_entry($campaign_id, 'campaign', 'advertising');

		if ($entry->created_by <> $merchant_id) { return false; } else { return true;}
	}
	
	public function check_advert_owner($advert_id = null, $merchant_id = null)
	{
		if (! $merchant_id || !$advert_id) return false;
		
		$entry = $this->streams->entries->get_entry($advert_id, 'advert', 'advertising');

		if ($entry->created_by <> $merchant_id) { return false; } else { return true;}
	}

}
