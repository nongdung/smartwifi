<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_statistics extends Admin_Controller
{
	protected $section = 'statistics';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('advertising');
		$this->load->driver('Streams');
		$this->load->model('advertising_m');
		role_or_die('advertising', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all adverts
     */
    public function index()
    {
    	$this->load->model('location/location_m');
		$this->load->model('devices/device_m');
		//echo date("Y-m-d", mktime(0, 0, 0, date("m")-3, 1, date("Y")));
		//echo date("Y-m-d", mktime(0, 0, 0, date("m"), 0, date("Y")));exit;
		$date_range = '';

			switch ($this->input->post('f_time')) {
				case 'today':
					$date_range = 'day&date=today';
					break;
				case 'yesterday':
					$date_range = 'day&date=yesterday';
					break;
				case 'last-7-days':
					$date_range = 'range&date=last7';
					break;
				case 'last-30-days':
					$date_range = 'range&date=last30';
					break;
				case 'this-week':
					$date_range = 'week&date=today';
					break;
				case 'last-week':
					$last_monday = date("Y-m-d", strtotime("last week monday"));
					$last_sunday = date("Y-m-d", strtotime("last week sunday"));
					$date_range = 'range&date='.$last_monday.','.$last_sunday;
					break;
				case 'this-month':
					$date_range = 'month&date=today';
					break;
				case 'last-month':
					$ini_day = date("Y-m-d", strtotime("first day of last month"));
					$last_day = date("Y-m-d", strtotime("last day of last month"));
					$date_range = 'range&date='.$ini_day.','.$last_day;
					break;
				case 'last-3-months':
					$ini_day = date("Y-m-d", mktime(0, 0, 0, date("m")-3, 1, date("Y")));
					$last_day = date("Y-m-d", mktime(0, 0, 0, date("m"), 0, date("Y")));
					$date_range = 'range&date='.$ini_day.','.$last_day;
					break;
				default:
					$date_range = 'range&date=2016-01-01,today';
					break;
			}

		
    	//$statistics_set = $this->advertising_m->get_campaign_statistics($date_range, $this->current_user->id, $this->input->post('f_location'), $this->input->post('f_device'));
		$statistics_set = $this->pyrocache->model('advertising_m', 'get_campaign_statistics', array($date_range, $this->current_user->id, $this->input->post('f_location'), $this->input->post('f_device')), 1800);
		
		$campaign_list = $this->pyrocache->model('advertising_m', 'get_all_campaign', array($this->current_user->id), 7200);    	
    	
		//print_r($statistics_set);exit;
		
		foreach ($campaign_list as $key => $campaign) {
			foreach ($statistics_set as $statistics) {
				if (isset($statistics['label']) && ($campaign['id'] == $statistics['label'])) {
					foreach ($statistics as $skey => $svalue) {
						$campaign_list[$key][$skey] = $svalue;
					}
				}
			}
		}
		
		//Get Location list for filter
		$location_list = $this->pyrocache->model('location_m', 'get_location_list', array($this->current_user->id), 7200);
		
		//Get Device list for filter
		$device_list = $this->pyrocache->model('device_m', 'get_device_list', array($this->current_user->id), 7200);
		
		$this->input->is_ajax_request() && $this->template->set_layout(false);
		
    	$this->template
			->title(lang('advertising:statistics'))
			->append_js('admin/filter.js')
			->set('campaign_list', $campaign_list)
			->set('location_list', $location_list)
			->set('device_list', $device_list);

		$this->input->is_ajax_request() ? $this->template->build('admin/partials/table') : $this->template->build('admin/statistics');
    }

	public function campaign($campaign_id = null)
	{
		if (! $campaign_id) {
			redirect ('admin/advertising/statistics');
		}
		
		if (! $this->advertising_m->check_campaign_owner($campaign_id, $this->current_user->id)) {
			$this->session->set_flashdata('error', "That is not your campaign!");
			redirect('admin/advertising/statistics');
		}
		
		
		$this->load->model('location/location_m');
		$this->load->model('devices/device_m');

		$date_range = '';

			switch ($this->input->post('f_time')) {
				case 'today':
					$date_range = 'day&date=today';
					break;
				case 'yesterday':
					$date_range = 'day&date=yesterday';
					break;
				case 'last-7-days':
					$date_range = 'range&date=last7';
					break;
				case 'last-30-days':
					$date_range = 'range&date=last30';
					break;
				case 'this-week':
					$date_range = 'week&date=today';
					break;
				case 'last-week':
					$last_monday = date("Y-m-d", strtotime("last week monday"));
					$last_sunday = date("Y-m-d", strtotime("last week sunday"));
					$date_range = 'range&date='.$last_monday.','.$last_sunday;
					break;
				case 'this-month':
					$date_range = 'month&date=today';
					break;
				case 'last-month':
					$ini_day = date("Y-m-d", strtotime("first day of last month"));
					$last_day = date("Y-m-d", strtotime("last day of last month"));
					$date_range = 'range&date='.$ini_day.','.$last_day;
					break;
				case 'last-3-months':
					$ini_day = date("Y-m-d", mktime(0, 0, 0, date("m")-3, 1, date("Y")));
					$last_day = date("Y-m-d", mktime(0, 0, 0, date("m"), 0, date("Y")));
					$date_range = 'range&date='.$ini_day.','.$last_day;
					break;
				default:
					$date_range = 'range&date=2016-01-01,today';
					break;
			}

		
    	//$statistics_set = $this->advertising_m->get_campaign_statistics($date_range, $this->current_user->id, $this->input->post('f_location'), $this->input->post('f_device'));
		$statistics_set = $this->pyrocache->model('advertising_m', 'get_advert_statistics', array($date_range, $this->current_user->id, $this->input->post('f_location'), $this->input->post('f_device')), 1800);
		
		$advert_list = $this->pyrocache->model('advertising_m', 'get_adverts_of_campaign', array($this->current_user->id, $campaign_id), 7200);    	
    	//print_r($advert_list);exit;
		
		
		foreach ($advert_list as $key => $advert) {
			foreach ($statistics_set as $statistics) {
				if (isset($statistics['label']) && ($advert['id'] == $statistics['label'])) {
					foreach ($statistics as $skey => $svalue) {
						$advert_list[$key][$skey] = $svalue;
					}
				}
			}
		}
		//print_r($advert_list);exit;
		//Get Location list for filter
		$location_list = $this->pyrocache->model('location_m', 'get_location_list', array($this->current_user->id), 7200);
		
		//Get Device list for filter
		$device_list = $this->pyrocache->model('device_m', 'get_device_list', array($this->current_user->id), 7200);
		
		$this->input->is_ajax_request() && $this->template->set_layout(false);
		
    	$this->template
			->title(lang('advertising:adverts_statistics'))
			->append_js('admin/filter.js')
			->set('advert_list', $advert_list)
			->set('location_list', $location_list)
			->set('device_list', $device_list);

		$this->input->is_ajax_request() ? $this->template->build('admin/partials/advert_table') : $this->template->build('admin/advert_statistics');	
	}

}