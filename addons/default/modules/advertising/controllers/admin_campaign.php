<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_campaign extends Admin_Controller
{
	protected $section = 'campaign';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('advertising');
		$this->load->driver('Streams');
		$this->load->model('advertising_m');
		role_or_die('advertising', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all campaigns
     */
    public function index()
    {    	
        //$campaign_list = $this->advertising_m->get_all_campaign($this->current_user->id);
		$campaign_list = $this->pyrocache->model('advertising_m', 'get_all_campaign', array($this->current_user->id), 7200);
        $this->template
        			->set('campaign_list', $campaign_list)
					->build('admin/campaign_list');
    }
	
	public function add()
	{
		$campaigns = $this->advertising_m->get_all_campaign($this->current_user->id);
		if (count($campaigns) >= 3) {
			$this->session->set_flashdata('error', "You can't create over 10 campaigns !");
			redirect('admin/advertising/campaign');
		}
		
		//delete cached
		$this->pyrocache->delete_cache("advertising_m", "get_all_campaign", array($this->current_user->id));
		
		$extra = array(
		    'return'            => 'admin/advertising/campaign',
		    'success_message'   => lang('advertising:submit_success'),
		    'failure_message'   => lang('advertising:submit_failure'),
		    'title'             => lang('advertising:add_campaign')
		);

		$this->streams->cp->entry_form('campaign', 'advertising', 'new', null, true, $extra);
		
		
	}

	public function delete($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/advertising/campaign');			
		}	
		
		if (! $this->advertising_m->check_campaign_owner($id, $this->current_user->id)) {
			$this->session->set_flashdata('error', "That is not your campaign!");
			redirect('admin/advertising/campaign');
		}

		//ok, now we delete
		$this->streams->entries->delete_entry($id, 'campaign', 'advertising');
		
		//delete cached
		$this->pyrocache->delete_cache("advertising_m", "get_all_campaign", array($this->current_user->id));
		
		$this->session->set_flashdata('success', "Deleted!");
		redirect('admin/advertising/campaign');		
		
	}
	
	public function edit($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/advertising/campaign');			
		}
		
		if (! $this->advertising_m->check_campaign_owner($id, $this->current_user->id)) {
			$this->session->set_flashdata('error', "That is not your campaign!");
			redirect('admin/advertising/campaign');
		}
		
		//delete cached
		$this->pyrocache->delete_cache("advertising_m", "get_all_campaign", array($this->current_user->id));
		
		//ok, now we edit
		$extra = array(
		    'return'            => 'admin/advertising/campaign',
		    'success_message'   => lang('advertising:submit_success'),
		    'failure_message'   => lang('advertising:submit_error'),
		    'title'             => lang('advertising:edit_campaign'),
		);
		$this->streams->cp->entry_form('campaign', 'advertising', 'edit', $id, true, $extra);
			
		
	}
}