<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_advert extends Admin_Controller
{
	protected $section = 'advert';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('advertising');
		$this->load->driver('Streams');
		$this->load->model('advertising_m');
		role_or_die('advertising', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all adverts
     */
    public function index()
    {    	

        $advert_list = $this->pyrocache->model('advertising_m', 'get_all_advert', array($this->current_user->id), 7200);
        $this->template
        			->set('advert_list', $advert_list)
					->build('admin/advert_list');
    }
	
	public function add()
	{
		$adverts = $this->advertising_m->get_all_advert($this->current_user->id);
		if (count($adverts) >= 5) {
			$this->session->set_flashdata('error', "You can't create over 10 adverts !");
			redirect('admin/advertising/advert');
		}
		
		//delete cached
		$this->pyrocache->delete_cache("advertising_m", "get_all_advert", array($this->current_user->id));
		
		$extra = array(
		    'return'            => 'admin/advertising/advert',
		    'success_message'   => lang('advertising:submit_success'),
		    'failure_message'   => lang('advertising:submit_failure'),
		    'title'             => lang('advertising:add_advert')
		);

		$this->streams->cp->entry_form('advert', 'advertising', 'new', null, true, $extra);
	}

	public function delete($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/advertising/advert');			
		}
		
		if (! $this->advertising_m->check_advert_owner($id, $this->current_user->id)) {
			$this->session->set_flashdata('error', "That is not your advert!");
			redirect('admin/advertising/advert');
		}
			
		//delete cached
		$this->pyrocache->delete_cache("advertising_m", "get_all_advert", array($this->current_user->id));
		
		//ok, now we delete
		$this->streams->entries->delete_entry($id, 'advert', 'advertising');
		$this->session->set_flashdata('success', "Deleted!");
		redirect('admin/advertising/advert');		
		
	}
	
	public function edit($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/advertising/advert');			
		}
		
		if (! $this->advertising_m->check_advert_owner($id, $this->current_user->id)) {
			$this->session->set_flashdata('error', "That is not your advert!");
			redirect('admin/advertising/advert');
		}
		
		//delete cached
		$this->pyrocache->delete_cache("advertising_m", "get_all_advert", array($this->current_user->id));
		
		//ok, now we edit
		$extra = array(
		    'return'            => 'admin/advertising/advert',
		    'success_message'   => lang('advertising:submit_success'),
		    'failure_message'   => lang('advertising:submit_error'),
		    'title'             => lang('advertising:edit_advert'),
		);
		$this->streams->cp->entry_form('advert', 'advertising', 'edit', $id, true, $extra);
			
		
	}
}