<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<section class="title">
	<h4><?php echo lang('advertising:adverts_statistics');?></h4>	
</section>
		
<section class="item">
	<div class="content">
		<?php echo $this->load->view('admin/partials/filters') ?>
	
		<div id="filter-stage">
			<?php echo $this->load->view('admin/partials/advert_table') ?>	
		</div>
		
	</div>
</section>
<?php
//print_r($advert_list);