
<section class="title">
	<h4><?php echo lang('advertising:adverts');?></h4>	
</section>
		
<section class="item">
	<div class="content">
		<?php if (count($advert_list)):?>
    		<table>
				<thead>
					<tr>
						<th>#</th>
						<th><?php echo lang('advertising:advert_name'); ?></th>
						<th><?php echo lang('advertising:status'); ?></th>
						<th>URL</th>
						<th><?php echo lang('advertising:campaign'); ?></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php $i=1;?>
					<?php foreach( $advert_list as $advert ): ?>
					<tr>
						<td><?php echo $i++; ?></td>
						<td><?php echo $advert['advert_name']; ?></td>
						<td><?php echo $advert['advert_status']['val']; ?></td>
						<td><?php echo $advert['website_url']; ?></td>
						<td><?php echo $advert['advert_campaign']['campaign_name']; ?></td>
						<td class="actions">
							<?php echo					
							anchor('admin/advertising/advert/edit/'.$advert['id'], lang('global:edit'), 'class="button"').' '.
							anchor('admin/advertising/advert/delete/'.$advert['id'], 	lang('global:delete'), array('class'=>'button confirm')); ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
    	<?php else:?>
    		<div class="no_data"><?php echo lang('advertising:no_items'); ?></div>
    	<?php endif;?>
	</div>
	<?php //print_r($advert_list);?>
</section>