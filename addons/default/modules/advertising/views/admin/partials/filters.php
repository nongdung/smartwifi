<fieldset id="filters">
	<legend><?php echo lang('global:filters') ?></legend>
	
		<?php echo form_open(''); ?>
		<?php echo form_hidden('f_module', $module_details['slug']); ?>
		
		<ul>
			<li>
				<?php echo lang('advertising:time', 'f_time') ?>
				<?php
					$f_time = array(
						'today' => lang('advertising:today'),
						'yesterday' => lang('advertising:yesterday'),
						'last-7-days'	=> lang('advertising:last-7-days'),
						'last-30-days'	=> lang('advertising:last-30-days'),
						'this-week'	=> lang('advertising:this-week'),
						'last-week'	=> lang('advertising:last-week'),
						'this-month'	=> lang('advertising:this-month'),
						'last-month'	=> lang('advertising:last-month'),
						'last-3-months'	=> lang('advertising:last-3-months'),
						'all' =>lang('global:select-all')
					);
				?>
				<?php echo form_dropdown('f_time', $f_time, 'yesterday'); ?>
    		</li>
	
			<li>
            	<?php echo lang('advertising:location', 'f_location') ?>
            	<?php echo form_dropdown('f_location', $location_list) ?>
        	</li>
        	
        	<li>
            	<?php echo lang('advertising:device', 'f_device') ?>
            	<?php echo form_dropdown('f_device', $device_list) ?>
        	</li>
	
			<li><?php echo anchor(current_url() . '#', lang('buttons:cancel'), 'class="cancel"') ?></li>
		</ul>
		
		<?php echo form_close() ?>

</fieldset>
