<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<?php if (count($advert_list)):?>
    		<table>
				<thead>
					<tr>
						<th>#</th>
						<th><?php echo lang('advertising:advert_name'); ?></th>
						<th><?php echo lang('advertising:status'); ?></th>
						<th><?php echo lang('advertising:campaign'); ?></th>
						<th>Impressions</th>
						<th>Click</th>
						<th>Interaction rate</th>
						
					</tr>
				</thead>
				<tbody>
					<?php $i=1;?>
					<?php foreach( $advert_list as $advert ): ?>
					<tr>
						<td><?php echo $i++; ?></td>
						<td><?php echo $advert['advert_name']; ?></td>
						<td><?php echo $advert['advert_status']['val']; ?></td>
						<td><?php echo $advert['advert_campaign']['campaign_name']; ?></td>
						<td><?php if (isset($advert['nb_impressions'])) echo $advert['nb_impressions']; else echo "-"; ?></td>
						<td><?php if (isset($advert['nb_interactions'])) echo $advert['nb_interactions']; else echo "-"; ?></td>
						<td><?php if (isset($advert['interaction_rate'])) echo $advert['interaction_rate']; else echo "-"; ?></td>
						
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
<?php else:?>
    		<div class="no_data"><?php echo lang('advertising:no_items'); ?></div>
<?php endif;?>