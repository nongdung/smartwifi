<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<?php if (count($campaign_list)):?>
    		<table>
				<thead>
					<tr>
						<th>#</th>
						<th><?php echo lang('advertising:campaign_name'); ?></th>
						<th><?php echo lang('advertising:status'); ?></th>
						<th><?php echo lang('advertising:start_time'); ?></th>
						<th><?php echo lang('advertising:end_time'); ?></th>
						<th>Impressions</th>
						<th>Click</th>
						<th>Interaction rate</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php $i=1;?>
					<?php foreach( $campaign_list as $campaign ): ?>
					<tr>
						<td><?php echo $i++; ?></td>
						<td><?php echo $campaign['campaign_name']; ?></td>
						<td><?php echo $campaign['campaign_status']['val']; ?></td>
						<td><?php echo date('Y-m-d H:i:s',$campaign['start_time']); ?></td>
						<td><?php if (isset($campaign['end_time']) && intval($campaign['end_time'])) echo date('Y-m-d H:i:s',$campaign['end_time']); ?></td>
						<td><?php if (isset($campaign['nb_impressions'])) echo $campaign['nb_impressions']; else echo "-"; ?></td>
						<td><?php if (isset($campaign['nb_interactions'])) echo $campaign['nb_interactions']; else echo "-"; ?></td>
						<td><?php if (isset($campaign['interaction_rate'])) echo $campaign['interaction_rate']; else echo "-"; ?></td>
						<td>
							<?php
							echo anchor('admin/advertising/statistics/campaign/'.$campaign['id'], lang('advertising:detail'), 'class="button"');
							?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
<?php else:?>
    		<div class="no_data"><?php echo lang('advertising:no_items'); ?></div>
<?php endif;?>