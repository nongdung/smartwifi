
<section class="title">
	<h4><?php echo lang('advertising:campaigns');?></h4>	
</section>
		
<section class="item">
	<div class="content">
		<?php if (count($campaign_list)):?>
    		<table>
				<thead>
					<tr>
						<th>#</th>
						<th><?php echo lang('advertising:campaign_name'); ?></th>
						<th><?php echo lang('advertising:status'); ?></th>
						<th><?php echo lang('advertising:start_time'); ?></th>
						<th><?php echo lang('advertising:end_time'); ?></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php $i=1;?>
					<?php foreach( $campaign_list as $campaign ): ?>
					<tr>
						<td><?php echo $i++; ?></td>
						<td><?php echo $campaign['campaign_name']; ?></td>
						<td><?php echo $campaign['campaign_status']['val']; ?></td>
						<td><?php echo date('Y-m-d H:i:s',$campaign['start_time']); ?></td>
						<td><?php echo date('Y-m-d H:i:s',$campaign['end_time']); ?></td>
						<td class="actions">
							<?php echo					
							anchor('admin/advertising/campaign/edit/'.$campaign['id'], lang('global:edit'), 'class="button"').' '.
							anchor('admin/advertising/campaign/delete/'.$campaign['id'], 	lang('global:delete'), array('class'=>'button confirm')); ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
    	<?php else:?>
    		<div class="no_data"><?php echo lang('advertising:no_items'); ?></div>
    	<?php endif;?>
	</div>
	<?php //print_r($campaign_list);?>
</section>