<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="prelogin">
	<div class="container" style="color:{{ theme_detail:prelogin_text_color }};">
		<h3>Welcome to <?php echo $location_detail['loc_name'];?></h3>
		<a style="background-color: {{ theme_detail:prelogin_button_color }};" class="btn" href="<?php echo site_url("splash/login/".$location_detail['id']."/".$device_type);?>">Wifi Login</a>
		<p class="terms">Việc bạn sử dụng dịch vụ wifi này đồng nghĩa với việc bạn đồng ý với <a style="color: {{ theme_detail:prelogin_link_color }};" href="#" target="_blank">những điều khoản của chúng tôi.</a></p>
		<p class="not-agree">Nếu bạn không đồng ý, vui lòng không kết nối qua mạng wifi này</p>
		<p id="msg"></p>
	</div>
	
</div>
<script>

    	swController.queryObj = queryObj;
    	swController.host = queryObj.uamip;
    	swController.port = parseInt(queryObj.uamport);

</script>