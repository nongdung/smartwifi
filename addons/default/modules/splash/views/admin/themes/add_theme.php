<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('splash:add_theme'); ?></h4>
</section>

<section class="item">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
	<div class="form_inputs">
			<ul>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="name"><?php echo lang('splash:theme_name');?><span>*</span></label>
					<div class="input"><?php echo form_input('name', ''); ?></div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="theme_slug">Theme Slug<span>*</span></label>
					<div class="input"><?php echo form_input('theme_slug', ''); ?></div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
						<label for="status"><?php echo lang('splash:theme_type');?><span>*</span></label>
						<div class="input">
							<?php
							$options = array('-----' => null,'login'  => 'Login Page', 'wait'    => 'Wait Page', 'success' => 'Success Page');							
							echo form_dropdown('theme_type', $options);
							?>
						</div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="stream_slug"><?php echo lang('splash:stream_slug');?><span>*</span></label>
					<div class="input"><?php echo form_input('stream_slug', ''); ?></div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="namespace_slug"><?php echo lang('splash:namespace_slug');?><span>*</span></label>
					<div class="input"><?php echo form_input('namespace_slug', ''); ?></div>
				</li>				
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="screenshot"><?php echo lang('splash:screenshot');?></label>
					<div class="input"><?php echo form_upload('screenshot', ''); ?></div>
				</li>			
			</ul>
		</div>
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
		</div>
	<?php echo form_close(); ?>
</section>