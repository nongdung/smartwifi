<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('hotspot:list_equipment'); ?></h4>
</section>

<section class="item">
	
	<?php if ($entries['total'] > 0): ?>
	<?php echo $this->load->view('admin/partials/equipment_filters') ?>
	<?php echo form_open('admin/hotspot/equipment/delete');?>	
	    <div id="filter-stage">
			<?php echo $this->load->view('admin/equipment/ajax_list') ?>
		</div>		
		<div class="table_action_buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
		</div>
	<?php echo form_close(); ?>
	
	<?php else: ?>
		<div class="no_data"><?php echo lang('hotspot:no_items'); ?></div>
	<?php endif;?>
</section>