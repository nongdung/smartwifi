<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('location:select_theme'); ?> <i>(<?php echo lang('location:location'); ?>: <?php echo $location_detail['loc_name'];?>)</i></h4>
</section>

<section class="item">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
	<div class="one_full">
		<ul id="quad">			
			<?php foreach ($themes as $theme):?>
				<li>					
					<?php echo $theme['screenshot']['thumb_img'];?>
					<br/>
					<a href="<?php echo $theme['screenshot']['image'];?>" target="_blank">View Full Image</a>
					<br />
					<?php echo form_radio('theme',$theme['id'],($theme['id'] == $location_detail['theme'])?'checked="checked"':null). " ". $theme['name'];?>
					<br/>
				</li>
			<?php endforeach;?>			
			
		</ul>
	</div>
	<div class="one_full">
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save') )); ?>
			<?php echo anchor('admin/location', lang('cancel_label'), 'class="btn gray cancel"');?>
		</div>
	</div>
	<?php form_close();?>
	<?php
	//print_r($location_detail);
	?>
</section>