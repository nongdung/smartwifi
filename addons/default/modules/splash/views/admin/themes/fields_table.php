<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('splash:fields_table'); ?></h4>
</section>

<section class="item">
		<?php echo $fields_table;?>
		<div class="buttons">
			<?php echo anchor('admin/splash/themes/add_field/'.$id, "Add field", 'class="btn blue"');?>
		</div>
</section>