<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('cp:splash_list'); ?></h4>
</section>

<section class="item">
	<div class="content">
    	<?php if ($entries['total']):?>
    		<table>
				<thead>
					<tr>
						<th>#</th>
						<th><?php echo lang('location:location_name'); ?></th>
						<th>Theme</th>
						<?php if (group_has_role('hotspot', 'admin')):?>
							<th>Created_by</th>
						<?php endif;?>
						<th></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="8">
							<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php $i=1;?>
					<?php foreach( $entries['entries'] as $entry ): ?>
					<tr>
						<td><?php echo $i++; ?></td>
						<td><?php echo $entry['loc_name']; ?></td>						
						<td>
							<?php
								if ($entry['theme']) { 
									foreach ($full_theme_list as $theme) {
										if ($theme['id'] == $entry['theme']) {
											echo $theme['name'];
										}
									}
									$theme_caption = lang('location:change_theme');								
								} else {
									echo "---";
									$theme_caption = lang('location:select_theme');
								}
							?>							
						</td>

						<?php if (group_has_role('hotspot', 'admin')):?>
							<td><?php echo $entry['created_by']['email'];?></td>
						<?php endif;?>
					
						<td class="actions">
							<?php echo anchor('admin/splash/themes/select/'.$entry['id'], $theme_caption, 'class="btn gray"');?>
							<?php
								foreach ($full_theme_list as $theme) {
									if ($theme['id'] == $entry['theme']) {
										echo anchor('admin/'. $theme['theme_slug'] . "/".$entry['id'], lang('location:theme_detail'), 'class="btn blue"');
									}
								} 
							        			
							?>					
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
    	<?php else:?>
    		<div class="no_data"><?php echo lang('location:no_items'); ?></div>
    	<?php endif;?>
    </div>
	
</section>