<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('splash:template_options'); ?> | <?php echo $theme_name; ?> theme</h4>
</section>

<section class="item">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		<div class="form_inputs">
			<ul>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="redirect">Redirect URL<span>*</span></label>
					<div class="input"><?php echo form_input('redirect', isset($options['redirect'])?$options['redirect']:''); ?></div>
				</li>
				
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="intro_title">Intro Title<span>*</span></label>
					<div class="input"><?php echo form_input('intro_title', isset($options['intro_title'])?$options['intro_title']:''); ?></div>
				</li>
				
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="intro">Intro Details<span>*</span></label>
					<div class="input"><?php echo form_textarea('intro', isset($options['intro'])?$options['intro']:'','class="wysiwyg-simple one-half"'); ?></div>
				</li>

			</ul>
		</div>
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
		</div>
	<?php echo form_close(); ?>
</section>