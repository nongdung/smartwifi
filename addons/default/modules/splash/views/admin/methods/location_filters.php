<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<fieldset id="filters">
	<legend><?php echo lang('global:filters') ?></legend>

	<?php echo form_open('', '', array('f_module' => $module_details['slug'])) ?>
		<ul>
			<li class="">
        		<label for="f_loc_id"><?php echo lang('cp:location') ?></label>
        		<?php
        			if (isset($selected_loc)) {
						echo form_dropdown('f_loc_id', $location_list,$selected_loc);
					} else {
						echo form_dropdown('f_loc_id', $location_list);
					}
        			 
        		?>
    		</li>

			<li class="">
				<?php echo anchor(current_url() . '#', lang('buttons:cancel'), 'class="cancel"') ?>
			</li>
		</ul>
	<?php echo form_close() ?>
</fieldset>
