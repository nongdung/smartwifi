<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('cp:login_methods'); ?></h4>
</section>

<section class="item">
	<?php echo $this->load->view('admin/methods/location_filters') ?>
	<div id="filter-stage">
			<?php echo $this->load->view('admin/methods/ajax_methods_list') ?>
	</div>	
</section>