
		<table>
			<thead>
				<tr>					
	    			<th>#</th>
	    			<th><?php echo lang('splash:module_name'); ?></th>
	    			<th><?php echo lang('splash:loc_name'); ?></th>
	    			<th><?php echo lang('splash:status'); ?></th>
	    			<th></th>
				</tr>
			</thead>
			<tbody>
				<?php $i=1; ?>			    
				<?php foreach( $list_methods as $loc_id => $loc_detail ): ?>

						<?php foreach ($location_list as $key => $value) {
							if ($loc_id == $key){ $loc_name = $value;}
						}?>


					<?php foreach( $loc_detail as $module => $settings ): ?>
						<tr>
							<td><?php echo $i++; ?></td>
							<td><?php echo $settings['name']; ?></td>
							<td><?php echo $loc_name; ?></td>
							<td>
								<?php
								if ($settings['status'] == "Enable") {
									echo "<span style='color:blue;'>Enable</span>";
								} else {
									echo "<span style='color:red;'>Disable</span>";
								} 
								?>						
							</td>
							<td class="actions">
								<?php echo
								anchor('admin/'.$module.'/settings/'.$settings['loc_id'], lang('splash:settings'), 'class="button"'); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
