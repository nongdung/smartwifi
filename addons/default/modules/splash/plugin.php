<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Plugin_Splash extends Plugin
{
	
	/**
	 * Item List
	 * Usage:
	 * 
	 * {{ sample:items limit="5" order="asc" }}
	 *      {{ id }} {{ name }} {{ slug }}
	 * {{ /sample:items }}
	 *
	 * @return	array
	 */
	function login_form()
	{
		# Read query parameters which we care about                
        parse_str($_SERVER['QUERY_STRING'],$_GET);
		$res = isset($_GET['res'])?$_GET['res']:'';
		$challenge = isset($_GET['challenge'])?$_GET['challenge']:'0';
		$uamip = isset($_GET['uamip'])?$_GET['uamip']:'0';
		$uamport = isset($_GET['uamport'])?$_GET['uamport']:'0';
		$userurl = isset($_GET['userurl'])?$_GET['userurl']:'';
		$nasid = isset($_GET['nasid'])?$_GET['nasid']:'';
		$reason = isset($_GET['reason'])?$_GET['reason']:'';
		$sessionid = isset($_GET['sessionid'])?$_GET['sessionid']:'';
		$mac = isset($_GET['mac'])?$_GET['mac']:''; //client's MAC address
		$ip = isset($_GET['ip'])?$_GET['ip']:'';
		$called = isset($_GET['called'])?$_GET['called']:''; //gateway's MAC address	        
		
		//check nasid
		
		$form = array();
		
		switch ($res) {
			case 'notyet':
				$form = $this->_display_notyet();
				break;
			case 'failed':
				
				break;
			case 'success':
				
				break;
			case 'already':
				
				break;
			case 'logoff':
				
				break;
			
			default:
				$form['error'] = "";
				break;
		}
		
		if (!($challenge AND $uamip AND $uamport)) {
			//if client is not in hotspot
			$form = '';
		}
		return $form;
	}

	private function _display_notyet()
	{
		/*		
		$click_through = $this->attribute('click_through');
		$facebook_login = $this->attribute('facebook_login');
		$facebook_like = $this->attribute('facebook_like');
		$facebook_share = $this->attribute('facebook_share');
		$vipcode = $this->attribute('vipcode');
		$register_sms = $this->attribute('register_sms');
		$register_email = $this->attribute('register_email');
		*/
		
		$challenge = isset($_GET['challenge'])?$_GET['challenge']:'';
		$uamip = isset($_GET['uamip'])?$_GET['uamip']:'';
		$uamport = isset($_GET['uamport'])?$_GET['uamport']:'';
		$userurl = isset($_GET['userurl'])?$_GET['userurl']:'';
		
		$click_through = '<div id="click-through">';
		$click_through .= form_open('splash/click_through');
		$click_through .= form_hidden('uamip',$uamip);
		$click_through .= form_hidden('uamport',$uamport);
		$click_through .= form_hidden('challenge',$challenge);		
		$click_through .= form_submit('submit','Click here to access Internet');
		$click_through .= form_close();
		$click_through .= '</div>';
		$form['click_through'] = $click_through;
		
		return $form;
		
	}
}

/* End of file plugin.php */