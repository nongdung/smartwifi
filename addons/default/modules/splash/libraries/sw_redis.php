<?php

/**
 * sw_redis: SmartWifi redis library
 * 
 * @version    v1.0
 * @author     NgocDung Nong
 * @license    MIT License
 * @copyright  2011 NgocDung Nong
 */
class sw_redis_Exception extends Exception {}


class Sw_redis {
	private $redis = '';
	public $CI;
	/**
	 * Loads in the config and sets the variables
	 */
	public function __construct($config = array())
	{
		$this->CI = get_instance();
		$this->redis = new Redis();
		$this->redis->connect($this->CI->config->item('redis_host'), $this->CI->config->item('redis_port'));
		//$this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_PHP); //php5.4 can not work
		if (isset($config['prefix']) && $config['prefix'] != '') {
			$this->redis->setOption(Redis::OPT_PREFIX, $config['prefix']);
		} else {
			$this->redis->setOption(Redis::OPT_PREFIX, "data:");
		}
		
		
	}
	
	public function setPrefix($prefix = null)
	{
		if ($prefix) {
			$this->redis->setOption(Redis::OPT_PREFIX, $prefix);
		}
	}
	public function get($key='')
	{
		return json_decode($this->redis->get($key), TRUE);
	}
	
	public function cache_get($key='')
	{
		return unserialize($this->redis->get($key));
	}
	
	public function set($key="", $value='', $ttl = 7200)
	{
		$this->redis->setex($key, $ttl, json_encode($value, JSON_UNESCAPED_UNICODE));
	}
	
	public function cache_set($key="", $value='', $ttl = 7200)
	{
		$this->redis->setex($key, $ttl, serialize($value));
	}
	
	public function delete($key='')
	{
		$this->redis->delete($key);
	}
	
	public function exists($key='')
	{
		return $this->redis->exists($key);
	}

	public function keygen()
	{
		$tmp_key = '';
		do
		{
			$tmp_key .= mt_rand(0, mt_getrandmax());
			$new_key = $tmp_key . $this->CI->input->ip_address();
			$key = md5(uniqid($new_key, TRUE));
			
		}
		while (strlen($tmp_key) < 32 || $this->redis->exists($key));

		// To make the session ID even more secure we'll combine it with the user's IP
		

		// Turn it into a hash and return
		return $key;
	}
	
	public function close()
	{
		$this->redis->close();
	}
	
	public function hset($key = null, $field = null, $value = null)
	{
		if (!$key || !$field || !$value) return false;
		return $this->redis->hset($key, $field, $value);
	}
	
	public function hmset($key = null, $data = array(), $ttl = null)
	{
		if (!$key || !count($data)) {
			return false;
		}
		if ($this->redis->hmset($key, (array)$data)){
			if ($ttl) $this->redis->expire($key, $ttl);
			return true;
		} else {
			return false;
		}
	}
	
	public function hget($key = null, $field = null)
	{
		if (!$key || !$field) return false;
		return $this->redis->hget($key, $field);
	}
	
	public function hgetall($key='')
	{
		return $this->redis->hgetall($key);
	}
	
	public function hincrby($key='', $field ='', $value = 1)
	{
		return $this->redis->hincrby($key, $field, $value);
	}
	
	public function close_this()
	{
		$this->redis->close();
	}
}

/* End of file asset.php */