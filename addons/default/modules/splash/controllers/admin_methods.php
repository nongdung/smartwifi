<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	splash Module
 */
class Admin_methods extends Admin_Controller
{
	protected $section = 'methods';

	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->lang->load('splash');
		$this->load->driver('Streams');
		role_or_die('hotspot', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');

	}

	/**
	 * List all items
	 */
	public function index()
	{
		//Get merchant info
		//$loc_id = $this->uri->segment(4);
		$this->load->model('hotspot/service_class_m');
		$this->load->model('hotspot/merchant_m');		
		$merchant_info = $this->merchant_m->get_info($this->current_user->id); //merchant_id = 2
		//result: Array ( [merchant_id] => 1 [start_date] => 1430438400 [end_date] => 1433030400 [current_cash] => [created_by] => 1 [class_id] => 1 [methods] => Array ( [0] => facebook [1] => click [2] => google ) [themes] => Array ( [0] => default [1] => resto1 ) [class_name] => Free [max_location] => 3 [max_devices] => 4 [monthly_limit_session] => 6000 )
		if ($merchant_info['end_date'] < time()) {
			//Drop this location to Free class
			$service_class = $this->service_class_m->get_by_name("Free");			
		} else {
			$service_class = $this->service_class_m->get_by_id($merchant_info['class_id']);
		}
		
		$methods = $this->service_class_m->get_login_modules($service_class['class_id']); 
		
		//location list
		$location_list= array(null => lang("global:select-all"));
		
		$params = array(
				'stream'    => 'list',
    			'namespace' => 'location',
    			'order_by'	=> 'id',
    			'sort'		=> 'asc',
    			'where'		=> "`created_by` = '".$this->current_user->id."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			foreach ($entries['entries'] as $item) {
				$location_list[$item['id']] = $item['loc_name'];
			}
		} else {
			$this->session->set_flashdata('error', "Please add a location before adding devices!");
			redirect('admin/location/add');
		}
		
		if ($this->input->post('f_loc_id')) {
			$loc_id = $this->input->post('f_loc_id');
			foreach ($methods as $method) {				
					//Get setting of this login method
					$params = array(
					    'stream'    => 'settings',
					    'namespace' => $method,
					    'where'		=> '`created_by` = '.$this->current_user->id. ' AND `loc_id` = '.$loc_id
					);
					
					$entries = $this->streams->entries->get_entries($params);

					
					if ($entries['total']){
						$method_settings[$method] = array(
							'id'	=> isset($entries['entries'][0]['id'])?$entries['entries'][0]['id']:0,					
							'name' => $method,
							'status' => isset($entries['entries'][0]['status']['val'])?$entries['entries'][0]['status']['val']:"Disable",
							'loc_id' => $loc_id,//isset($entries['entries'][0]['loc_id'])?$entries['entries'][0]['loc_id']['id']:'0',
							'loc_name' => isset($entries['entries'][0]['loc_id'])?$entries['entries'][0]['loc_id']['loc_name']:'N/A'
						);
					} else {
						$method_settings[$method] = array(
								'id'	=> 0,					
								'name' => $method,
								'status' => "Disable",
								'loc_id' => $loc_id,//isset($entries['entries'][0]['loc_id'])?$entries['entries'][0]['loc_id']['id']:'0',
								'loc_name' => 'N/A'
						);
					}				
			}
			$list_methods = array($loc_id => $method_settings);
		
		} elseif ($this->uri->segment(4)) {
			$loc_id = $this->uri->segment(4);
			//Check loc_id
			$params = array(
				'stream'    => 'list',
    			'namespace' => 'location',
    			'order_by'	=> 'id',
    			'sort'		=> 'asc',
    			'where'		=> "`created_by` = '".$this->current_user->id."' AND `id` = '".$loc_id."'"
			);
			$test_entries = $this->streams->entries->get_entries($params);
			if (!$test_entries['total']) {
				redirect('admin/splash/methods');
			}
			
			foreach ($methods as $method) {						
							//Get setting of this login method
							$params = array(
							    'stream'    => 'settings',
							    'namespace' => $method,
							    'where'		=> '`created_by` = '.$this->current_user->id. ' AND `loc_id` = '.$loc_id
							);
							
							$entries = $this->streams->entries->get_entries($params);
							
							if ($entries['total']){
								$method_settings[$method] = array(
									'id'	=> isset($entries['entries'][0]['id'])?$entries['entries'][0]['id']:0,					
									'name' => $method,
									'status' => isset($entries['entries'][0]['status']['val'])?$entries['entries'][0]['status']['val']:"Disable",
									'loc_id' => $loc_id,//isset($entries['entries'][0]['loc_id'])?$entries['entries'][0]['loc_id']['id']:'0',
									'loc_name' => isset($entries['entries'][0]['loc_id'])?$entries['entries'][0]['loc_id']['loc_name']:'N/A'
								);
							}else{
								$method_settings[$method] = array(
									'id'	=> 0,					
									'name' => $method,
									'status' => "Disable",
									'loc_id' => $loc_id,//isset($entries['entries'][0]['loc_id'])?$entries['entries'][0]['loc_id']['id']:'0',
									'loc_name' => 'N/A'
								);
							}
					}
					$list_methods[$this->uri->segment(4)] = $method_settings;
		} else {

			foreach ($location_list as $loc_id => $loc_name) {
				if ($loc_id) {
					foreach ($methods as $method) {						
							//Get setting of this login method
							//print_r($method);
							$params = array(
							    'stream'    => 'settings',
							    'namespace' => $method,
							    'where'		=> '`created_by` = '.$this->current_user->id. ' AND `loc_id` = '.$loc_id
							);
							
							$entries = $this->streams->entries->get_entries($params);
							
							if ($entries['total']){
								$method_settings[$method] = array(
									'id'	=> isset($entries['entries'][0]['id'])?$entries['entries'][0]['id']:0,					
									'name' => $method,
									'status' => isset($entries['entries'][0]['status']['val'])?$entries['entries'][0]['status']['val']:"Disable",
									'loc_id' => $loc_id,//isset($entries['entries'][0]['loc_id'])?$entries['entries'][0]['loc_id']['id']:'0',
									'loc_name' => isset($entries['entries'][0]['loc_id'])?$entries['entries'][0]['loc_id']['loc_name']:'N/A'
								);
							}else{
								$method_settings[$method] = array(
									'id'	=> 0,					
									'name' => $method,
									'status' => "Disable",
									'loc_id' => $loc_id,//isset($entries['entries'][0]['loc_id'])?$entries['entries'][0]['loc_id']['id']:'0',
									'loc_name' => 'N/A'
								);
							}
					}

					$list_methods[$loc_id] = $method_settings;
				} //end of if $loc_id
			} //end of foreach location_list
		} //end of if f_loc_id
		
		
		$this->input->is_ajax_request() and $this->template->set_layout(false);
		
		if ($this->uri->segment(4) && ! $this->input->is_ajax_request()) {
			$this->template->set('selected_loc', $this->uri->segment(4));
		}
		$this->template
			->title(lang('splash:methods_list'))
			->set_partial('filters', 'admin/methods/location_filters')
			->append_js('admin/filter.js')
			//->set('loc_id', $loc_id)
			->set('location_list', $location_list)
			->set('list_methods', $list_methods);
			//->set('current_methods_settings',$current_methods_settings);
			
		$this->input->is_ajax_request()
			? $this->template->build('admin/methods/ajax_methods_list')
            : $this->template->build('admin/methods/methods_list');
	}
}
