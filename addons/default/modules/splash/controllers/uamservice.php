<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		NgocDung Nong - SmartWifi VN Dev Team
 * @website		http://nongdung.com
 * @package 	SmartWifi
 * @subpackage 	splash Module
 */
class Uamservice extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->lang->load('splash');
	}


	public function index($uamsecret='')
	{
		$this->load->model('uam_m');
		
		$uamsecret =trim($uamsecret);
    	$password = trim($_GET["password"]); 
    	$challenge = trim($_GET["challenge"]);
		if ($uamsecret){
			$pappassword = $this->uam_m->return_new_pwd($password,$challenge,$uamsecret);
		}else{
			$pappassword = $this->uam_m->return_new_pwd($password,$challenge,'smartwifi');
		}
		header('Content-type: application/javascript');
		print "swJSON.reply({'response':'".$pappassword."'})";
	}	
}
