<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	splash Module
 */
class Prelogin extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->lang->load('splash');

	}

	public function index()
	{
		/*
		//Check MD5
		if (! $this->_check_URL()) {			
			redirect('http://wifun.vn');
		}
		*/ 
		
		$this->load->model('devices/device_m');
		$userdata = $this->session->all_userdata();
		
		//Detect protocol
		if (isset($_SERVER['HTTPS']) &&
		    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
		    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
		  $protocol = 'https://';
		}
		else {
		  $protocol = 'http://';
		}
		
		if ($this->agent->is_mobile()){
			$device_type = "";
		} else {
			$device_type = "desktop";
		}
		
		$data = array();
		if ( isset($_GET['res']) && ($_GET['res'] != '')) { $data['res'] = $_GET['res']; }
		if (isset($_GET['uamip']) && ($_GET['uamip'] != '')) { $data['uamip'] = $_GET['uamip']; } else { $data['uamip'] = '172.16.0.1'; }
		if (isset($_GET['uamport']) && ($_GET['uamport'] != '')) { $data['uamport'] = $_GET['uamport']; } else { $data['uamport'] = '3990'; }
		if (isset($_GET['challenge']) && ($_GET['challenge'] != '')) { $data['challenge'] = $_GET['challenge']; }
		if (isset($_GET['called']) && ($_GET['called'] != '')) {
			$data['called'] = $_GET['called']; 
		} elseif (isset($userdata['called'])) {
			$data['called'] = $userdata['called'];
		} else{
			$data['called'] = "";
		}
		if (isset($_GET['mac']) && ($_GET['mac'] != '')) { $data['mac'] = $_GET['mac']; }
		if (isset($_GET['ip']) && ($_GET['ip'] != '')) { $data['ip'] = $_GET['ip']; }
		if (isset($_GET['nasid']) && ($_GET['nasid'] != '')) { $data['nasid'] = $_GET['nasid']; }
		if (isset($_GET['userurl']) && ($_GET['userurl'] != '')) { $data['userurl'] = $_GET['userurl']; }
		if (isset($_GET['md']) && ($_GET['md'] != '')) { $data['md'] = $_GET['md']; }

		//check valid redirect login url
		/*
		if (! $this->_check_URL()) {
			$this->session->set_flashdata('error', "Invalid login URL");
			redirect('http://'.$data['uamip'].":".$data['uamport']."/prelogin");
		}
		*/		
		
		if (!isset($data['called']) || ($data['called'] == "")) {
			redirect("http://172.16.0.1:3990/prelogin");
		}
		$called = $data['called'];
		$router = $this->pyrocache->model('device_m', 'get_location', array($called), 7200);
		//print_r($router);exit;
		if (isset($router['location_id']['id'])) {
			$loc_id = $router['location_id']['id'];
			$merchant_id = $router['created_by']['user_id'];
		} else {
			$loc_id = 1;
			$merchant_id = 1;
			//$router = $this->pyrocache->model('device_m', 'get_from_loc', array($loc_id), 7200);
		}
		$data['location_id'] = $loc_id;
		$data['merchant_id'] = $merchant_id;
		$this->session->set_userdata($data);
		
		//set cookie
		$cookie = array(
		    'name'   => 'gateway',
		    'value'  => json_encode($data),
		    'expire' => '1800',
		    'domain' => 'localhost',
		    'path'   => '/'
		);		
		$this->input->set_cookie($cookie);
		
		switch ($_GET['res']) {
			case 'success':
				if ($this->agent->is_mobile()) {
					redirect('splash/success/'.$loc_id);
				} else {
					redirect('splash/success/'.$loc_id."/desktop");
				}
				
				break;
			/*
			case "bakup":
				if ($this->agent->is_mobile()) {
					redirect('splash/login/'.$loc_id);
				} else {
					redirect('splash/login/'.$loc_id."/desktop");
				}
				break;
			 * 
			 */
			default:
				/*
				 * comment out this block if you don't use prelogin
				 * 
				if ($this->agent->is_mobile()) {
					redirect('splash/login/'.$loc_id);
				} else {
					redirect('splash/login/'.$loc_id."/desktop");
				}
				*/
				 
				//Get location info
				$this->load->model('location/location_m');
				$this->load->model('hotspot_theme_m');		
				$location_detail = $this->pyrocache->model('location_m', 'get_detail', array( (int)$loc_id ), 7200);
				
				//print_r($location_detail);exit;
				
				$theme = $this->pyrocache->model('hotspot_theme_m', 'getTheme', array($location_detail['theme']), 7200);
				$theme_detail = $this->pyrocache->model('hotspot_theme_m', 'getThemeDetail', array($location_detail['theme'], $location_detail['id']), 7200);
				
				//Setup necessary assets
				$theme_model = $theme['theme_slug']."_m";
				$this->load->model($theme['theme_slug']."/".$theme_model);
				$theme_info = $this->$theme_model->get_info();		
				$this->$theme_model->set_prelogin_assets($device_type);
				//print_r($theme_detail);exit;
				$this->template
							->set('theme_detail', $theme_detail)
							->set('theme_slug', $theme['theme_slug'])
							->set('location_detail',$location_detail);
				
				$this->template
						->title("Please wait")
						->set("userdata", $data)
						->set("device_type", $device_type)
						->set('location_id', $loc_id)
						->set('protocol', $protocol)
						->set('static_host', $this->config->item('static_host'))
						->set('theme_detail', $theme_detail)
						->set_layout("hotspot/".$theme['theme_slug']."/prelogin.html")
						->build("prelogin");
				break;
		}
		
	}

	public function _check_URL()
	{
		$result = FALSE;
		$uamsecret = 'smartwifi';
		
		$protocol = isset($_SERVER["https"])?"https://":"http://";
		$login_url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		
		$last_pos = strrpos($login_url,'&');
		$url_without_md = substr($login_url, 0, $last_pos);
					
		$md_check = substr($login_url,$last_pos + 4); 
			
				
		$mymd5 = strtoupper(md5($url_without_md.$uamsecret));

		if ($mymd5 == $md_check) {
			//Login URL not valid
			$result = TRUE;
		}
		
		return $result;
	}
}
