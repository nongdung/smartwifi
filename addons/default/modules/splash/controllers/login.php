<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		NgocDung Nong - SmartWifi VN Dev Team
 * @website		http://nongdung.com
 * @package 	SmartWifi
 * @subpackage 	splash Module
 */
class Login extends Public_Controller
{
	protected $store_data_for_apple = false;
	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->lang->load('splash');
		if ($this->config->item('sess_use_redis')) {
			$this->load->library('splash/sw_redis');
		}
	}

	/**
	 * List all items
	 */
	public function index($loc_id = null, $device_type = null)
	{		
		$device_type = $this->uri->segment(4);
		$this->load->model('hotspot/service_class_m');
		$this->load->model('husers/husers_m');
		$this->load->model('splash_m');
		$this->load->model('hotspot_theme_m');
		$this->load->model('advertising/advertising_m');
		$this->load->model('proser/proser_m');
		
		//Detect protocol
		if (isset($_SERVER['HTTPS']) &&
		    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
		    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
		  $protocol = 'https://';
		}
		else {
		  $protocol = 'http://';
		}
		
		//Detect Apple devices and store userdata to new key
		if ($_SERVER['HTTP_USER_AGENT']) preg_match("/iPhone|iPad|iPod|OS X|Captive/", $_SERVER['HTTP_USER_AGENT'], $matches);
		$isApple = !empty($matches);
		if ($isApple && $this->config->item('sess_use_redis') && $this->store_data_for_apple) {
			$userdata_key = $this->sw_redis->keygen();
			$userdata = array();
			$tmp_userdata = $this->session->all_userdata();
			$hotspot_keys = array("res", "uamip", "uamport", "ip", "mac", "called", "challenge", "userurl", "md", "reason", "nasid", "location_id");
			foreach ($hotspot_keys as $key) {
				if (isset($tmp_userdata[$key])) $userdata[$key] = $tmp_userdata[$key];
			}
			
			if (!empty($userdata)) {
				$this->sw_redis->set($userdata_key,$userdata);
				$this->template->set("userdata_key", $userdata_key);
			}
		} else {
			$userdata_key = null;
		}
		
		//Get client info
		$client_info = $this->husers_m->getClientInfo($this->session->userdata("mac"));
		
		//Get location info
		$this->load->model('location/location_m');		
		$location_detail = $this->pyrocache->model('location_m', 'get_detail', array($loc_id), 7200);
		
		//print_r($location_detail);exit;
		//Array ( [id] => 1 [created] => 1436097339 [updated] => 1448100398 [ordering_count] => 1 [loc_name] => BIDV Hang Voi [long_lat] => [city] => Array ( [id] => [created] => [updated] => [created_by] => [ordering_count] => [city_name] => [city_code] => [province_id] => ) [address] => 30 Hang Voi [info] => [status] => 1 [theme] => 6 [redirect_url] => http://smartwifi.com.vn [created_by] => Array ( [user_id] => 1 [email] => nongdung@gmail.com [username] => nongdung [display_name] => NgocDung Nong ) [province] => Array ( [id] => 1 [created] => 1435575082 [updated] => [created_by] => 1 [ordering_count] => 1 [province_name] => Hà Nội [province_code] => hanoi ) [last] => 1 [odd_even] => odd [count] => 1 )
		
		if (empty($location_detail)) {
			echo "location invalid";exit;
		}
		
		//Get merchant info
		$this->load->model('hotspot/merchant_m');
				
		//$merchant_info = $this->merchant_m->get_info($location_detail['created_by']['user_id']); //merchant_id = 2
		$merchant_info = $this->pyrocache->model('merchant_m', 'get_info', array($location_detail['created_by']['user_id']), 7200);
		
		//print_r($merchant_info);exit;
		//result: Array ( [merchant_id] => 1 [start_date] => 1430438400 [end_date] => 1433030400 [current_cash] => [created_by] => 1 [class_id] => 1 [methods] => Array ( [0] => facebook [1] => click [2] => google ) [themes] => Array ( [0] => default [1] => resto1 ) [class_name] => Free [max_location] => 3 [max_devices] => 4 [monthly_limit_session] => 6000 )
		if (empty($merchant_info) || ($merchant_info['class_name'] == "Free") || (isset($merchant_info['end_date']) && $merchant_info['end_date'] < time())) {
			//Drop this location to Free class
			//$service_class = $this->service_class_m->get_by_name("Free");
			$service_class = $this->pyrocache->model('service_class_m', 'get_by_name', array("Free"), 7200);			
		} else {
			//$service_class = $this->service_class_m->get_by_id($merchant_info['class_id']);
			$service_class = $this->pyrocache->model('service_class_m', 'get_by_id', array($merchant_info['class_id']), 7200);
		}
		
		//$theme = $this->hotspot_theme_m->getTheme($location_detail['theme']);
		$theme = $this->pyrocache->model('hotspot_theme_m', 'getTheme', array($location_detail['theme']), 7200);
		
		//$theme_detail = $this->hotspot_theme_m->getThemeDetail($location_detail['theme'], $location_detail['id']);
		$theme_detail = $this->pyrocache->model('hotspot_theme_m', 'getThemeDetail', array($location_detail['theme'], $location_detail['id']), 7200);

		//print_r($theme_detail);exit;	
		
		//Get advert set
		$advert_set = $this->pyrocache->model('advertising_m', 'get_advert', array($merchant_info['merchant_id']), 3600);
		if (count($advert_set)) {
			$advert_random_id = mt_rand(0, count($advert_set)-1);
			$this->template->set('advert', $advert_set[$advert_random_id]);
		}
		
		//Get products and services (prosers)
		//Get sections
		$_sections = array();
		$proser_sections = $this->pyrocache->model('proser_m', 'get_sections', array($location_detail['created_by']['user_id']));
		
		foreach ($proser_sections as $section)
		{
			$_sections[$section->id] = $section->title;
		}
		//Get prosers of all location
		$params = array(
			'author_id' => $location_detail['created_by']['user_id'],
			'location' => 0,
			'limit' => 10
		);
		//$all_loc_prosers = $this->proser_m->get_items_by($params);
		$all_loc_prosers = $this->pyrocache->model('proser_m', 'get_items_by', array($params));
		//print_r($params);
		$params['location'] = $location_detail['id'];
		
		$this_loc_prosers = $this->pyrocache->model('proser_m', 'get_items_by', array($params));
		
		//Login methods
		$wifi_login_html = array();
		$wifi_method_position = array();	
			
		$methods = $this->pyrocache->model('service_class_m', 'get_login_modules', array($service_class['class_id']), 7200);
		
		foreach ($methods as $method) {
			$this->load->model($method."/".$method."_m");
			$method_model = $method."_m";
			//$method_settings = $this->$method_model->get_settings($loc_id);
			$method_settings  = $this->pyrocache->model($method_model,'get_settings', array($loc_id),3600);
			if (isset($method_settings['status']['val']) && ($method_settings['status']['val'] == 'Enable')) {
				$wifi_login_html[$method] = $this->$method_model->get_html($loc_id, $device_type, $method_settings, $userdata_key);
				$wifi_method_position[$method] = $method_settings['position'];
			}
			
		}
		
		//Now, we arrange methods
		asort($wifi_method_position);
		
		//Determine show ads or not
		$showAds = $this->splash_m->showAds($location_detail['created_by']['user_id']);
		$this->template->set('showAds', $showAds);
		
		//Setup necessary assets
		$theme_model = $theme['theme_slug']."_m";
		$this->load->model($theme['theme_slug']."/".$theme_model);
		$theme_info = $this->$theme_model->get_info();		
		$this->$theme_model->set_assets($device_type);
		
		Asset::add_path('smartwifi', 'assets/smartwifi/');
				
		Asset::js('smartwifi::basic.js');
		Asset::js('smartwifi::swController.js', FALSE, 'smartwifi');
		
		$this->template
			->title('Wifi Login')
			->set('theme_slug', $theme['theme_slug'])
			->set('theme_detail',$theme_detail)
			->set('location_detail',$location_detail)
			->set('wifi_login_html', $wifi_login_html)
			->set('wifi_method_position', $wifi_method_position)			
			->set('device_type', $device_type)
			->set("client_info", $client_info)
			->set('all_loc_prosers', $all_loc_prosers)
			->set('this_loc_prosers', $this_loc_prosers)
			->set('sections', $_sections)
			->set('protocol', $protocol)
			->set('static_host', $this->config->item('static_host'))			
			//->set_layout($theme_info['layout'])
			->set_layout("hotspot/".$theme['theme_slug']."/login.html")
			->build('landing/login');
	}
}
