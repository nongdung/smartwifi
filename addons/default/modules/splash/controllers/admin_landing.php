<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	splash Module
 */
class Admin_landing extends Admin_Controller
{
	protected $section = 'landing';

	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->lang->load('splash');
		$this->load->language('location/location');
		$this->load->driver('Streams');
		role_or_die('hotspot', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');

	}

	/**
	 * List all items
	 */
	public function index()
	{
		$this->load->model('splash/hotspot_theme_m'); 
		
		$params = array(
		    'stream'    => 'list',
		    'namespace' => 'location',
		    'paginate'	=> 'yes',
		    'pag_segment' => 4,
		    'limit'	=> 15		        
		);
		
		if (!group_has_role('hotspot', 'admin')) {
			$params['where'] = "`created_by` = '".$this->current_user->id."'";
		}
		$entries = $this->streams->entries->get_entries($params);
		$theme_list = $this->hotspot_theme_m->getThemeList($this->current_user->id);
		$full_theme_list = $this->hotspot_theme_m->getFullThemeList($this->current_user->id);		
		$this->template
					->title(lang('splash:landing'))
					->set('entries',$entries)
					->set('theme_list', $theme_list)
					->set('full_theme_list', $full_theme_list)
					->build('admin/landing_page');
	}

}
