<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		NgocDung Nong - SmartWifi VN Dev Team
 * @website		http://nongdung.com
 * @package 	SmartWifi
 * @subpackage 	splash Module
 */
class Success extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->lang->load('splash');
	}

	/**
	 * List all items
	 */
	public function index($loc_id = null, $device_type = null)
	{
		//$device_type = $this->uri->segment(4);
		$this->load->model('hotspot/service_class_m');
		$this->load->model('splash_m');
		$this->load->model('hotspot_theme_m');
		$this->load->model('husers/husers_m');
		$this->load->model('facebook_js/facebook_js_m');
		$this->load->model('coupon/coupon_m');
		$this->load->model('advertising/advertising_m');
		
		//Detect protocol
		if (isset($_SERVER['HTTPS']) &&
		    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
		    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
		  $protocol = 'https://';
		}
		else {
		  $protocol = 'http://';
		}
		
		//Get global facebook settings
		$fb_settings = $this->pyrocache->model('facebook_js_m', 'get_fb_settings', array(), 7200);
		$merchant_fb_settings = $this->pyrocache->model('facebook_js_m', 'get_settings', array($loc_id), 7200);
		
		//Get location info
		$this->load->model('location/location_m');		
		$location_detail = $this->pyrocache->model('location_m', 'get_detail', array($loc_id), 7200);
		
		//print_r($location_detail);exit;
		//Array ( [id] => 1 [created] => 1436097339 [updated] => 1436442407 [ordering_count] => 1 [loc_name] => BIDV Hang Voi [long_lat] => [city] => Array ( [id] => [created] => [updated] => [created_by] => [ordering_count] => [city_name] => [city_code] => [province_id] => ) [address] => 30 Hang Voi [info] => [status] => 1 [login_page_template] => 1 [wait_page_template] => 2 [success_page_template] => 3 [created_by] => Array ( [user_id] => 1 [email] => nongdung@gmail.com [username] => nongdung [display_name] => NgocDung Nong ) [province] => Array ( [id] => 1 [created] => 1435575082 [updated] => [created_by] => 1 [ordering_count] => 1 [province_name] => HÃ  Ná»™i [province_code] => hanoi ) [last] => 1 [odd_even] => odd [count] => 1 )
		
		if (empty($location_detail)) {
			echo "location invalid";exit;
		}
		
		//Get client info
		$client_info = $this->husers_m->getClientInfo($this->session->userdata("mac"));
		
		//Check Coupon
		$hasValidCouponCampaign = $this->pyrocache->model('coupon_m', 'hasValidCampaign',array($location_detail['created_by']['user_id']), 7200);
		if (!empty($hasValidCouponCampaign)) {
			$location_detail["hasCouponCampaign"] = 1;
		} else {
			$location_detail["hasCouponCampaign"] = 0;
		}
		
		//$profile_detail = $this->pyrocache->model('husers_m', 'getClientInfoFromId', array($client_info['profile_id']), 7200);
		
		//Get merchant info
		$this->load->model('hotspot/merchant_m');		
		//$merchant_info = $this->merchant_m->get_info($location_detail['created_by']['user_id']); //merchant_id = 2
		$merchant_info = $this->pyrocache->model('merchant_m', 'get_info', array($location_detail['created_by']['user_id']), 7200);
		//result: Array ( [merchant_id] => 1 [start_date] => 1430438400 [end_date] => 1433030400 [current_cash] => [created_by] => 1 [class_id] => 1 [methods] => Array ( [0] => facebook [1] => click [2] => google ) [themes] => Array ( [0] => default [1] => resto1 ) [class_name] => Free [max_location] => 3 [max_devices] => 4 [monthly_limit_session] => 6000 )
		if (empty($merchant_info) || ($merchant_info['class_name'] == "Free") || (isset($merchant_info['end_date']) && $merchant_info['end_date'] < time())) {
			//Drop this location to Free class
			//$service_class = $this->service_class_m->get_by_name("Free");
			$service_class = $this->pyrocache->model('service_class_m', 'get_by_name', array("Free"), 7200);			
		} else {
			//$service_class = $this->service_class_m->get_by_id($merchant_info['class_id']);
			$service_class = $this->pyrocache->model('service_class_m', 'get_by_id', array($merchant_info['class_id']), 7200);
		}
		
		//Get advert set
		$advert_set = $this->pyrocache->model('advertising_m', 'get_advert', array($merchant_info['merchant_id']), 3600);
		if (count($advert_set)) {
			$advert_random_id = mt_rand(0, count($advert_set)-1);
			$this->template->set('advert', $advert_set[$advert_random_id]);
		}
		
		//Determine show ads or not
		$showAds = $this->splash_m->showAds($location_detail['created_by']['user_id']);
		$this->template->set('showAds', $showAds);
		
		//print_r($advert_set);exit; 
		//$theme = $this->hotspot_theme_m->getTheme($location_detail['theme']);
		$theme = $this->pyrocache->model('hotspot_theme_m', 'getTheme', array($location_detail['theme']), 7200);
		
		//$theme_detail = $this->hotspot_theme_m->getThemeDetail($location_detail['theme'], $location_detail['id']);
		$theme_detail = $this->pyrocache->model('hotspot_theme_m', 'getThemeDetail', array($location_detail['theme'], $location_detail['id']), 7200);

		$model = $theme['theme_slug']."_m";
		$this->load->model($theme['theme_slug']."/".$model);
		$theme_info = $this->$model->get_info();		
		$this->$model->set_assets($device_type);
		
		Asset::add_path('smartwifi', 'assets/smartwifi/');
		Asset::js('smartwifi::basic.js');
		Asset::js('smartwifi::swController.js', FALSE, 'smartwifi');			
		Asset::js('smartwifi::sw-coupon.js');
		Asset::js('smartwifi::facebook.js');
		
		$swAjaxSetup = array(
			'csrf_token_name' => $this->security->get_csrf_token_name(),
			'csrf_hash' => $this->security->get_csrf_hash()
		);
		
		$this->template
			->title('Wifi Login Success')
			->set('theme_slug', $theme['theme_slug'])
			->set('fb_settings', $fb_settings)
			->set('merchant_fb_settings', $merchant_fb_settings)
			->set('theme_detail',$theme_detail)
			->set('device_type', $device_type)
			->set('location_detail', $location_detail)
			->set("client_info", $client_info)
			->set('protocol', $protocol)
			->set('static_host', $this->config->item('static_host'))
			//->set('advert', $advert_set[$advert_random_id])
			->set('swAjaxSetup', json_encode($swAjaxSetup))
			->set_layout("hotspot/".$theme['theme_slug']."/success.html")
			->build('landing/success');
	}	
}
