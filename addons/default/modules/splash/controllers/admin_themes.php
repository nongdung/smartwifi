<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	splash Module
 */
class Admin_themes extends Admin_Controller
{
	protected $section = 'themes';

	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->lang->load('splash');
		$this->lang->load('location/location');
		$this->load->driver('Streams');
		role_or_die('hotspot', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');

	}

	/**
	 * List all items
	 */
	public function index()
	{
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
		$extra['buttons'] = array(
		    array(
		        'label'     => 'Fields',
		        'url'       => 'admin/splash/themes/fields/-entry_id-'
		    ),
		    array(
		        'label'     => 'Edit',
		        'url'       => 'admin/splash/themes/edit/-entry_id-'
		    ),
		    array(
		        'label'     => lang('global:delete'),
		        'url'       => 'admin/splash/themes/delete/-entry_id-',
		        'confirm'   => true
		    )
		);		
		
		$this->streams->cp->entries_table('themes', 'splash', 15, 'admin/splash/themes/index', true,$extra);
	}

	public function add()
	{
		$this->load->library('files/files');
		$this->load->model('files/file_folders_m');
		$themes_folder = $this->file_folders_m->get_by('name', 'hotspot_themes');
		
		$validation_rules = array(
			array(
					'field' => 'name',
					'label' => 'Theme Name',
					'rules' => 'trim|required|max_length[64]'
			),
			array(
					'field' => 'theme_slug',
					'label' => 'Theme Slug',
					'rules' => 'trim|required|max_length[64]'
			),
			array(
					'field' => 'stream_slug',
					'label' => 'Stream Slug',
					'rules' => 'trim|required|max_length[64]|alpha_dash'
			),
			array(
					'field' => 'namespace_slug',
					'label' => 'Namespace Slug',
					'rules' => 'trim|required|max_length[64]|alpha_dash'
			),
			array(
					'field' => 'theme_type',
					'label' => 'Theme Type',
					'rules' => 'trim|required'
			),
		);
		$this->form_validation->set_rules($validation_rules);
		if ($this->form_validation->run()){
			//Check stream slug and namespace
			$stream_check = $this->db
									->where('stream_slug',$this->input->post('stream_slug'))
									->where('stream_namespace',$this->input->post('namespace_slug'))
									->get('data_streams');
			if ($stream_check->num_rows() > 0) {
				$this->streams->streams->delete_stream($this->input->post('stream_slug'), $this->input->post('namespace_slug'));
				$this->session->set_flashdata('error', 'stream existed!');
				redirect('admin/splash/themes/add');
			} elseif ($this->streams->streams->add_stream($this->input->post('name'), $this->input->post('stream_slug'), $this->input->post('namespace_slug'), 'theme_', null)) {
				$entry_data = array(
					    'name' => $this->input->post('name'),
					    'theme_slug'   => $this->input->post('theme_slug'),
					    'stream_slug'   => $this->input->post('stream_slug'),
					    'namespace_slug'   => $this->input->post('namespace_slug'),
					    'theme_type'   => $this->input->post('theme_type'),
				);
				$results = Files::upload($themes_folder->id,null,'screenshot');
				if ($results['status']) {
					$entry_data['screenshot'] = $results['data']['id'];					
				}
				$this->streams->entries->insert_entry($entry_data, 'themes', 'splash');
				$this->session->set_flashdata('success', "Added");
				redirect('admin/splash/themes');
			}		
			
		}
		
		$this->template
				->build('admin/themes/add_theme');
	}
	
	public function fields($id = 0)
	{
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
		if (! $id) {
			redirect('admin/splash/themes');
		}
		$theme = $this->streams->entries->get_entry($id, 'themes', 'splash', $format = true);
		$buttons = array(
			array(
				'url'		=> 'admin/splash/themes/edit_field/'.$id.'/-assign_id-', 
				'label'		=> $this->lang->line('global:edit')
			),
			array(
				'url'		=> 'admin/splash/themes/delete_field/'.$id.'/-assign_id-',
				'label'		=> $this->lang->line('global:delete'),
				'confirm'	=> true
			)
		);


		$assign_table = $this->streams->cp->assignments_table(
								$theme->stream_slug,
								$theme->namespace_slug,
								15,
								'admin/splash/themes/fields/'.$id,
								false,
								array('buttons' => $buttons));
		$this->template
				->title('Field table')
				->set('fields_table', $assign_table)
				->set('id', $id)
				->build('admin/themes/fields_table');
	}

	public function add_field($theme_id)
	{
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
		if (! $theme_id) {
			redirect('admin/splash/themes');
		}
		$theme = $this->streams->entries->get_entry($theme_id, 'themes', 'splash', $format = true);
		if ($theme) {
			$extra['title'] 		= lang('streams:add_field');
			$extra['show_cancel'] 	= true;
			$extra['cancel_uri'] 	= 'admin/splash/themes/fields/'.$theme_id;
	
			$this->streams->cp->field_form($theme->stream_slug, $theme->namespace_slug, 'new', 'admin/splash/themes/fields/'.$theme_id, null, array(), true, $extra);
		} else {
			$this->session->set_flashdata('error', "Theme is not existed!");
			redirect('admin/splash/themes');
		}
		
	}
	
	public function edit_field($theme_id, $assign_id)
	{
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
		if (! $theme_id) {
			redirect('admin/splash/themes');
		}
		if ( ! $assign_id)
		{
			show_error(lang('streams:cannot_find_assign'));
		}
		
		$theme = $this->streams->entries->get_entry($theme_id, 'themes', 'splash', $format = true);
		
		if (! $theme) {
			redirect('admin/splash/themes');
		}
		
		$extra = array(
			'title'			=> lang('streams:edit_field'),
			'show_cancel'	=> true,
			'cancel_uri'	=> 'admin/splash/themes/fields/'.$theme_id
		);

		$this->streams->cp->field_form($theme->stream_slug, $theme->namespace_slug, 'edit', 'admin/splash/themes/fields/'.$theme_id, $assign_id, array(), true, $extra);
	}

	public function delete_field($theme_id = null, $assign_id = null)
	{
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');		
		if ( ! $assign_id || ! $theme_id)
		{
			show_error(lang('streams:cannot_find_assign'));
		}
	
		// Tear down the assignment
		if ( ! $this->streams->cp->teardown_assignment_field($assign_id))
		{
		    $this->session->set_flashdata('notice', lang('streams:field_delete_error'));
		}
		else
		{
		    $this->session->set_flashdata('success', lang('streams:field_delete_success'));			
		}
	
		redirect('admin/splash/themes/fields/'.$theme_id);
	}
		
	public function delete($id = 0)
	{
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
		if (! $id) {
			redirect('admin/splash/themes');
		}
		$this->load->library('files/files');
		$this->load->model('files/file_folders_m');
		
		$theme = $this->streams->entries->get_entry($id, 'themes', 'splash', $format = true);
		$stream_slug = $theme->stream_slug;
		$stream_namespace = $theme->namespace_slug;
		$screenshot = $theme->screenshot;
		
		//delete screenshot image
		Files::delete_file($screenshot['id']);
		
		//check and delete stream
		$stream_check = $this->db
								->where('stream_slug', $stream_slug)
								->where('stream_namespace',$stream_namespace)
								->get('data_streams');
		if ($stream_check->num_rows() > 0) {
			//Now we delete this theme stream
			$this->streams->streams->delete_stream($stream_slug, $stream_namespace);	
		}

		$this->streams->entries->delete_entry($id,'themes','splash');
		$this->session->set_flashdata('success', "Theme deleted!");
		redirect('admin/splash/themes');
	}
	
	public function edit($id = null)
	{
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/splash/themes');			
		}
		
		//ok, now we edit
		$extra = array(
		    'return'            => 'admin/splash/themes',
		    'success_message'   => lang('hotspot:submit_success'),
		    'failure_message'   => lang('hotspot:submit_error'),
		    'title'             => 'Edit Theme'
		);
		$skips = array('stream_slug', 'namespace_slug');
		$this->streams->cp->entry_form('themes', 'splash', 'edit', $id, true, $extra, $skips);
			
		
	}
	
	function main_theme_fields()
	{
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
		$buttons = array(
			array(
				'url'		=> 'admin/splash/themes/main_edit_field/-assign_id-', 
				'label'		=> $this->lang->line('global:edit')
			),
			array(
				'url'		=> 'admin/splash/themes/main_delete_field/-assign_id-',
				'label'		=> $this->lang->line('global:delete'),
				'confirm'	=> true
			)
		);


		$this->streams->cp->assignments_table(
								'themes',
								'splash',
								15,
								'admin/splash/themes/main_theme_fields/index',
								TRUE,
								array('buttons' => $buttons));
	}
	
	public function main_edit_field($assign_id)
	{
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
		$extra = array(
			'title'			=> lang('streams:edit_field'),
			'show_cancel'	=> true,
			'cancel_uri'	=> 'admin/splash/themes/main_theme_fields'
		);

		$this->streams->cp->field_form('themes', 'splash', 'edit', 'admin/splash/themes/main_theme_fields', $assign_id, array(), true, $extra);
	}

	function main_delete_field($assign_id = null)
	{		
		if ( ! $assign_id )
		{
			show_error(lang('streams:cannot_find_assign'));
		}
	
		// Tear down the assignment
		if ( ! $this->streams->cp->teardown_assignment_field($assign_id))
		{
		    $this->session->set_flashdata('notice', lang('streams:field_delete_error'));
		}
		else
		{
		    $this->session->set_flashdata('success', lang('streams:field_delete_success'));			
		}
	
		redirect('admin/splash/themes/main_theme_fields');
	}
	
	function select($location_id)
	{
		$this->load->model('hotspot_theme_m');
		$this->load->model('location/location_m');
		
		if (! $this->location_m->checkLocationOwner($location_id,$this->current_user->id)) {
			$this->session->set_flashdata('notice', "You don't have right to edit that location");
			redirect('admin/location');
		}
				
		$validation_rules = array(
			array(
					'field' => 'theme',
					'label' => 'Theme',
					'rules' => 'trim|required|is_natural'
			),					
		);
		$this->form_validation->set_rules($validation_rules);
		if ($this->form_validation->run()){
			$entry_data = array(
		        'theme'    => $this->input->post('theme')
		    );
			$this->streams->entries->update_entry($location_id, $entry_data, 'list', 'location');
			redirect('admin/location');
		}
		
		$location_detail = $this->location_m->get_detail($location_id);
		
		$themes = $this->hotspot_theme_m->getFullThemeList($this->current_user->id)	;	
		$this->template
					->set('title', "Select theme")
					->append_css("module::style.css")
					->set('themes', $themes)
					->set('location_detail', $location_detail)
					->build('admin/themes/select');
	}

}
