<?php
//messages
$lang['splash:success']			=	'Thành công';
$lang['splash:error']			=	'Có lỗi xảy ra!';
$lang['splash:no_items']		=	'No Items';
$lang['splash:filters']		=	'Bộ lọc';

//page titles
$lang['splash:template']			=	'Template';
$lang['splash:login_template']			=	'Login Template';
$lang['splash:wait_template']			=	'Wait Template';
$lang['splash:success_template']			=	'Success Template';
$lang['splash:login_template']			=	'Login Template';
$lang['splash:add_login_template']			=	'Add Login Template';
$lang['splash:wait_template']			=	'Wait Template';
$lang['splash:add_wait_template']			=	'Add Wait Template';
$lang['splash:success_template']			=	'Success Template';
$lang['splash:add_success_template']			=	'Add Success Template';

$lang['splash:add_template']			=	'Add Template | Step 1';
$lang['splash:add_template2']			=	'Add Template | Step 2';
$lang['splash:template_options']			=	'Options';
$lang['splash:edit_options']			=	'Edit Template Options';

$lang['splash:theme']			=	'Mẫu trang chào';
$lang['splash:select_a_theme']			=	'Hãy chọn một mẫu trang chào';
$lang['splash:add_theme']			=	'Thêm mẫu trang chào';
$lang['splash:edit_theme']			=	'Sửa mẫu trang chào';
$lang['splash:theme_name']			=	'Tên mẫu trang chào';
$lang['splash:theme_type']			=	'Kiểu mẫu trang chào';
$lang['splash:stream_slug']			=	'Stream Slug';
$lang['splash:namespace_slug']			=	'Namespace Slug';
$lang['splash:screenshot']			=	'Screenshot';
$lang['splash:fields_table']			=	'Fields Table';

$lang['splash:not_owner_of_template']			=	'You are not owner of the template';
$lang['splash:not_free_theme']			=	'Not a free theme';

//Methods
$lang['splash:methods']			=	'Phương thức đăng nhập';
$lang['splash:methods_list']			=	'Danh sách phương thức đăng nhập';
$lang['splash:module_name']			=	'Tên phương thức';
$lang['splash:loc_name']			=	'Tên địa điểm';
$lang['splash:status']			=	'Trạng thái';
$lang['splash:settings']			=	'Cài đặt';

$lang['splash:landing']			=	'Trang chào';

?>