<?php
//messages
$lang['splash:success']			=	'It worked';
$lang['splash:error']			=	'It didn\'t work';
$lang['splash:no_items']		=	'No Items';
$lang['splash:filters']		=	'Filters';

//page titles
$lang['splash:template']			=	'Template';
$lang['splash:login_template']			=	'Login Template';
$lang['splash:wait_template']			=	'Wait Template';
$lang['splash:success_template']			=	'Success Template';
$lang['splash:login_template']			=	'Login Template';
$lang['splash:add_login_template']			=	'Add Login Template';
$lang['splash:wait_template']			=	'Wait Template';
$lang['splash:add_wait_template']			=	'Add Wait Template';
$lang['splash:success_template']			=	'Success Template';
$lang['splash:add_success_template']			=	'Add Success Template';

$lang['splash:add_template']			=	'Add Template | Step 1';
$lang['splash:add_template2']			=	'Add Template | Step 2';
$lang['splash:template_options']			=	'Options';
$lang['splash:edit_options']			=	'Edit Template Options';

$lang['splash:theme']			=	'Themes';
$lang['splash:select_a_theme']			=	'Please select a theme';
$lang['splash:add_theme']			=	'Add Theme';
$lang['splash:edit_theme']			=	'Edit Theme';
$lang['splash:theme_name']			=	'Theme Name';
$lang['splash:theme_type']			=	'Theme Type';
$lang['splash:stream_slug']			=	'Stream Slug';
$lang['splash:namespace_slug']			=	'Namespace Slug';
$lang['splash:screenshot']			=	'Screenshot';
$lang['splash:fields_table']			=	'Fields Table';

$lang['splash:not_owner_of_template']			=	'You are not owner of the template';
$lang['splash:not_free_theme']			=	'Not a free theme';

//Methods
$lang['splash:methods']			=	'Methods';
$lang['splash:methods_list']			=	'Login Method List';
$lang['splash:module_name']			=	'Module';
$lang['splash:loc_name']			=	'Location';
$lang['splash:status']			=	'Status';
$lang['splash:settings']			=	'Settings';

$lang['splash:landing']			=	'Landing Page';

?>