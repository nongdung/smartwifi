<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	www.your-site.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://www.codeigniter.com/user_guide/general/routing.html
*/

// front-end
$route['splash/login/test(/:any)?']			= 'login/test';
$route['splash/login(/:any)?']			= 'login/index$1';

$route['splash/uamservice(/:any)?']			= 'uamservice/index$1';
$route['splash/wait(/:any)?']			= 'wait/index$1';
$route['splash/success(/:any)?']			= 'success/index$1';
//$route['splash(/:any)?']			= 'login$1';

//backend
$route['splash/admin/methods(/:num)?']			= 'admin_methods/index$1';
$route['splash/admin/methods(/:any)?']			= 'admin_methods$1';

$route['splash/admin/landing(/:num)?']			= 'admin_landing/index$1';
$route['splash/admin/landing(/:any)?']			= 'admin_landing$1';

$route['splash/admin/themes(/:num)?']			= 'admin_themes/index$1';
$route['splash/admin/themes(/:any)?']			= 'admin_themes$1';