<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Splash extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Splash - Landing Page',
				'vi' => 'Trang chào'
			),
			'description' => array(
				'en' => 'Hotspot Login splash page',
				'vi' => 'Trang đăng nhập wifi'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',
			'sections' => array(
				'landing' => array(
					'name' 	=> 'cp:splash_list', // These are translated from your language file
					'uri' 	=> 'admin/splash/landing'
				),
				'methods' => array(
						'name' 	=> 'cp:login_methods', // These are translated from your language file
						'uri' 	=> 'admin/splash/methods',
				)
			)
		);
		if (group_has_role('hotspot', 'admin')) {
			$info['sections']['themes'] = array(
					'name' 	=> 'splash:theme', // These are translated from your language file
					'uri' 	=> 'admin/splash/themes',
						'shortcuts' => array(
							'create' => array(
								'name' 	=> 'splash:add_theme',
								'uri' 	=> 'admin/splash/themes/add',
								'class' => 'add'
							)
						)
			);
		}
		return $info;
	}

	public function install()
	{		
		$this->load->driver('Streams');
		$this->load->language('splash/splash');
		$this->db->delete('settings', array('module' => 'splash'));
		$this->streams->utilities->remove_namespace('splash');
		
		$this->load->library('files/files');
		$this->load->model('files/file_folders_m');
		
		//Create folder for template images upload
		//Files::create_folder(0, 'splash_templates');
		Files::create_folder(0, 'hotspot_themes');
		
		//get $folder->id		
		//$folder = $this->file_folders_m->get_by('name', 'splash_templates');
		$themes_folder = $this->file_folders_m->get_by('name', 'hotspot_themes');
		
		//add stream
		if ( ! $theme_id = $this->streams->streams->add_stream('Themes', 'themes', 'splash', 'hotspot_', null)) return false;
		if ( ! $this->streams->streams->add_stream('Templates', 'templates', 'splash', 'hotspot_', null)) return false;
		
		$theme_type = 'login
		wait
		success';
		$template_type = 'Mobile
		Desktop';
		
		$fields = array(
            array(
                'name' => 'Theme Name',
                'slug' => 'name',
                'namespace' => 'splash',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'themes',
                'title_column' => true,
                'required' => true                
            ),
            array(
                'name' => 'Theme Slug',
                'slug' => 'theme_slug',
                'namespace' => 'splash',
                'type' => 'slug',
                'assign' => 'themes',
                'required' => true
            ),
            array(
                'name' => 'Stream Slug',
                'slug' => 'stream_slug',
                'namespace' => 'splash',
                'type' => 'slug',
                'assign' => 'themes',
                'required' => true
            ),
            array(
                'name' => 'Namespace Slug',
                'slug' => 'namespace_slug',
                'namespace' => 'splash',
                'type' => 'slug',
                'assign' => 'themes',
                'required' => true
            ),
            array(
                'name' => 'Theme Type',
                'slug' => 'theme_type',
                'namespace' => 'splash',
                'type' => 'choice',
                'extra'         => array('choice_data' => $theme_type, 'choice_type' => 'dropdown'),
                'assign' => 'themes',
                'required' => true
            ),
            array(
                'name' => 'Screen Shot',
                'slug' => 'screenshot',
                'namespace' => 'splash',
                'type' => 'image',
                'assign' => 'themes',
                'extra' => array('folder' => $themes_folder->id, 'allowed_types' => 'jpg|gif|png|jpeg')              
            ),
            
			//Template stream
			array(
                'name' => 'Template Name',
                'slug' => 'template_name',
                'namespace' => 'splash',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'templates',
                'title_column' => true,
                'required' => true                
            ),
            array(
                'name' => 'Type',
                'slug' => 'template_type',
                'namespace' => 'splash',
                'type' => 'choice',
                'extra'         => array('choice_data' => $template_type, 'choice_type' => 'radio'),
                'assign' => 'templates',
                'required' => true                
            ),            
            array(
                'name' => 'Theme',
                'slug' => 'theme_id',
                'namespace' => 'splash',
                'type' => 'relationship',
                'assign' => 'templates',
                'extra' => array('choose_stream' => $theme_id),
                'required' => true                
            ),
            array(
                'name' => 'Entry ID',
                'slug' => 'entry_id',
                'namespace' => 'splash',
                'type' => 'integer',
                'extra' => array('max_length' => 11),
                'assign' => 'templates',             
            ),
            array(
                'name' => 'Default Template',
                'slug' => 'isdefault',
                'namespace' => 'splash',
                'type' => 'integer',
                'extra' => array('max_length' => 1),
                'assign' => 'templates',             
            ),
        );

        $this->streams->fields->add_fields($fields);
		
		$this->streams->streams->update_stream('themes', 'splash', array(
            'view_options' => array(
                'name',
                'theme_slug',
                'stream_slug',
                'namespace_slug',
                'theme_type',
                'screenshot'
            )
        ));
		
		$this->streams->streams->update_stream('templates', 'splash', array(
            'view_options' => array(
            	'template_name',
            	'template_type',				
                'theme_id',
                'isdefault'
            )
        ));
		
		return TRUE;
	}

	public function uninstall()
	{
		$this->load->driver('Streams');
        $this->streams->utilities->remove_namespace('splash');
		$this->db->delete('settings', array('module' => 'splash'));
		
		$this->load->library('files/files');
		$this->load->model('files/file_folders_m');
		$folder = $this->file_folders_m->get_by('name', 'splash_templates');
		$themes_folder = $this->file_folders_m->get_by('name', 'hotspot_themes');
		Files::delete_folder($folder->id);
		Files::delete_folder($themes_folder->id);
		
		return TRUE;
	}


	public function upgrade($old_version)
	{
		$this->load->driver('Streams');
        $this->streams->streams->update_stream('themes', 'splash', array(
            'view_options' => array(
                'name',
                'theme_slug',
                'stream_slug',
                'namespace_slug',
                'theme_type',
                'screenshot'
            )
        ));
		return TRUE;
	}
	
	public function admin_menu(&$menu)
    {
    	$menu['lang:cp:splash']['lang:cp:splash_list'] = '/admin/splash/landing';
		$menu['lang:cp:splash']['lang:cp:login_methods'] = '/admin/splash/methods';
		
		
		if (group_has_role('hotspot', 'admin')) {
            $menu['lang:cp:splash']['Themes'] = '/admin/splash/themes';
			$menu['Admin']['Theme Fields']   = 'admin/splash/themes/main_theme_fields';
        }
			
		add_admin_menu_place('lang:cp:splash', 2);
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
}
/* End of file details.php */
