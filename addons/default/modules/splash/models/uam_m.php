<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Uam_m extends MY_Model {
	function __construct()
		{
        // Call the Model constructor
        parent::__construct();
    	}
		
	function return_new_pwd($pwd,$challenge,$uamsecret){

        $hex_chal = pack('H32', $challenge);//Hex the challenge
        $newchal = pack('H*', md5($hex_chal.$uamsecret));  //Add it to with $uamsecret (shared between chilli an this script)
        $response = md5("\0" . $pwd . $newchal); //md5 the lot
        $newpwd = pack('a32', $pwd); //pack again
        $md5pwd = implode ('', unpack('H32', ($newpwd ^ $newchal))); //unpack again
        return $md5pwd;
    }
}