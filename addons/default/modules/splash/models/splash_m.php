<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Splash_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		$this->load->driver('Streams');
	}
	
	public function get_loc_details($mac = 0)
	{
		
		$data = array();
		
		$params = array(
				'stream'    => 'equipment',
    			'namespace' => 'hotspot',
    			'where'		=> "mac ='".$mac."'"
		);
		$equipments = $this->streams->entries->get_entries($params);
		
		$class = $this->streams->entries->get_entry($equipments['entries'][0]['loc_id']['class'],'loc_class','hotspot');
		$category = $this->streams->entries->get_entry($equipments['entries'][0]['loc_id']['category'],'loc_cat','hotspot');
		$province = $this->streams->entries->get_entry($equipments['entries'][0]['loc_id']['province'],'province','hotspot');
		$city = $this->streams->entries->get_entry($equipments['entries'][0]['loc_id']['city'],'city','hotspot');
		$district = $this->streams->entries->get_entry($equipments['entries'][0]['loc_id']['district'],'district','hotspot');
		
		if ($equipments['total'] > 0) {
			$data['status'] = $equipments['entries'][0]['loc_id']['status'];
			$data['merchant_id'] = $equipments['entries'][0]['loc_id']['merchant_id'];
			$data['created_by'] = $equipments['entries'][0]['loc_id']['created_by'];
			$data['location_detail'] = $equipments['entries'][0]['loc_id']['location_detail'];
			if (isset($class->class_code)) { $data['class'] = $class->class_code;}
			if (isset($category->category_code)) { $data['category'] = $category->category_code;}
			if (isset($province->province_code)) { $data['province'] = $province->province_code;}
			if (isset($city->city_code)) { $data['city'] = $city->city_code;}
			if (isset($district->district_code)) { $data['district'] = $district->district_code;}
			$data['address'] = $equipments['entries'][0]['loc_id']['address'];
			$data['hotline'] = $equipments['entries'][0]['loc_id']['hotline'];
			$data['info'] = $equipments['entries'][0]['loc_id']['info'];
			$data['template_id'] = $equipments['entries'][0]['loc_id']['template_id'];
		}
		
		return $data;
		
	}

	public function get_template_info($template_id = 0)
	{		
		$params = array(
				'stream'    => 'templates',
    			'namespace' => 'splash',
    			'where'		=> "`id` = '".$template_id."'"
		);
		$template = $this->streams->entries->get_entries($params);
		
		if ($template['total']) {
			return $template['entries'][0];
		}		
		return array();
	}
	
	public function get_template_details($template_id = 0)
	{
		$params = array(
				'stream'    => 'templates',
    			'namespace' => 'splash',
    			'where'		=> "`id` = '".$template_id."'"
		);
		$template_info = $this->streams->entries->get_entries($params);
		if (!$template_info['total']) {
			return array();
		} else {
			$params = array(
				'stream'    => $template_info['entries'][0]['theme_id']['stream_slug'],
    			'namespace' => $template_info['entries'][0]['theme_id']['namespace_slug'],
    			'where'		=> "`id` = '".$template_info['entries'][0]['entry_id']."'"
			);
			$template_details = $this->streams->entries->get_entries($params);
			if ($template_details['total']) {
				return $template_details['entries'][0];
			} else {
				return array();
			}
			
		}
		
	}
	
	function sort_methods($methods)
	{
		//Now, we arrange methods
		$tmp =array();
		foreach ($methods as $method => $config) {
			$tmp[$method] = $config['settings']['position'];
			//$tmp[$config['settings']['position']] = array($method => $config);
		}
		asort($tmp);
		
		$result = array();
		foreach ($tmp as $method => $position) {
			$result[$method] = $methods[$method];
		}
		return $result;
	}
	
	public function get_pappassword($password, $challenge, $uamsecret)
	{
		if ((strlen($challenge) <> 32) || (!$password) || ( !$uamsecret )) {
			return false;
		}
		$hexchal = pack ("H32", $challenge);
        $newchal = pack ("H*", md5($hexchal . $uamsecret));
        $newpwd = pack("a32", $password);
        $pappassword = implode ("", unpack("H32", ($newpwd ^ $newchal)));
		
		return $pappassword;
	}
	
	/*
	 * Return TRUE if this template using a theme in class_id
	 */
	
	public function check_theme($template_id, $class_id)
	{
		$this->load->model('hotspot/service_class_m');
		$params = array(
				'stream'    => 'templates',
    			'namespace' => 'splash',
    			'where'		=> "`id` = '".$template_id."'"
		);
		$template = $this->streams->entries->get_entries($params);
		$result = false;
		if ($template['total']) {
			$this_class = $this->service_class_m->get_by_id($class_id);
			foreach ($this_class['themes'] as $theme_id) {
				if ($template['entries'][0]['theme_id']["id"] == $theme_id) {
					$result = true;
				}
			}
		}
		
		return $result;
	}
	
	public function get_default_template_id($merchant_id, $type, $template_type = "Mobile")
	{
		$params = array(
				'stream'    => 'templates',
    			'namespace' => 'splash',
    			'where'		=> "`default_hotspot_templates`.`created_by` = '".$merchant_id."' AND `default_hotspot_templates`.`isdefault` = '1' AND `rel_theme_id`.`theme_type` = '".$type."' AND `default_hotspot_templates`.`template_type` = '".$template_type."'"
		);
		$template = $this->streams->entries->get_entries($params);
		if ($template['total']) {
			return $template['entries'][0]['id'];
		} else {
			return 0;
		}		

	}

	public function showAds($merchantId='')
	{
		$this->load->model('husers/husers_m');
		$this->load->model('hotspot/merchant_m');
		$this->load->model('hotspot/service_class_m');
		
		$showAds = false;
		
		$merchant_info = $this->pyrocache->model('merchant_m', 'get_info', array($merchantId), 7200);
		//return $this->pyrocache->model('husers_m', 'getLoginStatsByMerchantId', array(1,'day','2016-04-26'), 1800);
		if (empty($merchant_info) || ($merchant_info['class_name'] == "Free") || (isset($merchant_info['end_date']) && $merchant_info['end_date'] < time())) {
			//Drop this location to Free class
			//$service_class = $this->service_class_m->get_by_name("Free");
			$service_class = $this->pyrocache->model('service_class_m', 'get_by_name', array("Free"), 7200);			
		} else {
			//$service_class = $this->service_class_m->get_by_id($merchant_info['class_id']);
			$service_class = $this->pyrocache->model('service_class_m', 'get_by_id', array($merchant_info['class_id']), 7200);
		}
		
		if (($service_class['class_name'] == "Free") || ($service_class['class_name'] == "Startup")) {
			//Check current number of login
			$showAds = $this->_check_by_login_number($merchantId, $service_class['class_name']);
		} elseif ($service_class['class_name'] == "Business"){
			$showAds = false;
		} else {
			//Get login statistics, cache for 30m
			$stats = $this->pyrocache->model('husers_m', 'getLoginStatsByMerchantId', array(1,'day','2016-04-26'), 1800);
			
			switch ($service_class['class_name']) {
				case 'Standard':
					if (isset($stats['total_login']) && ($stats['total_login'] >= 200)) {
						$showAds = $this->_check_by_login_number($merchantId, "Free");
					} else {
						$showAds = false;
					}
					
					break;
				
				case 'Silver':
					if (isset($stats['total_login']) && ($stats['total_login'] >= 500)) {
						$showAds = $this->_check_by_login_number($merchantId, "Free");
					} else {
						$showAds = false;
					}
					
					break;
				
				case 'Silver Plus':
					if (isset($stats['total_login']) && ($stats['total_login'] >= 1000)) {
						$showAds = $this->_check_by_login_number($merchantId, "Free");
					} else {
						$showAds = false;
					}
					
					break;
				
				case 'Gold':
					if (isset($stats['total_login']) && ($stats['total_login'] >= 1500)) {
						$showAds = $this->_check_by_login_number($merchantId, "Free");
					} else {
						$showAds = false;
					}
					
					break;
					
				case 'Platinum':
					if (isset($stats['total_login']) && ($stats['total_login'] >= 2500)) {
						$showAds = $this->_check_by_login_number($merchantId, "Free");
					} else {
						$showAds = false;
					}
					
					break;
				
				default:
					$showAds = false;
					break;
			}
		}
		
		
		return $showAds;
	}

	private function _check_by_login_number($merchantId='', $service_class = "Free")
	{
		$this->load->library('sw_redis');
		$this->sw_redis->setPrefix("merchant:");
		
		$login_number = (int) $this->sw_redis->hget($merchantId, 'total_login');
		//$this->sw_redis->close_this();
		
		if ($service_class == "Free") {
			return (bool) ($login_number % 2);
			
		} else {
			return (bool) ($login_number % 4);
		}
		
	}

}
