<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Hotspot_theme_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		$this->load->driver('Streams');
	}

	public function getThemeName($themeID)
	{
		$query = $this->db
						->select('name')
						->where('`id` = '.$themeID)
						->get('hotspot_themes')
						->result_array();

		if (!empty($query)) {
			return $query[0]['name'];
		} else {
			return "";
		}		
		
	}
	
	function getTheme($themeID)
	{
		$params = array(
					'stream'    => 'themes',
	    			'namespace' => 'splash',
	    			'where'		=> "`id` ='".$themeID."'"
		);

		$themes = $this->streams->entries->get_entries($params);

		if ($themes['total']) {
			return $themes['entries'][0];
		} else {
			return array();
		}
			
	}
	
	public function getThemeDetail($themeID, $location_id)
	{		
		$theme = $this->getTheme($themeID);
		$params = array(
					'stream'    => $theme['stream_slug'],
	    			'namespace' => $theme['namespace_slug'],	    			
	    			'where'		=> "`location_id` ='".$location_id."'"
		);

		$theme_detail = $this->streams->entries->get_entries($params);
		if ($theme_detail['total']) {
			return $theme_detail['entries'][0];
		} else {
			return array();
		}
					
	}
	
	function getThemeList($merchant_id)
	{
		$this->load->model('hotspot/service_class_m');
		$this->load->model('hotspot/merchant_m');
		
		$theme_list = array(null => "-----");		
		$merchant_info = $this->merchant_m->get_info($merchant_id); //merchant_id = 2
		
		if ($merchant_info['end_date'] < time()) {
			//Drop this location to Free class
			$service_class = $this->service_class_m->get_by_name("Free");			
		} else {
			$service_class = $this->service_class_m->get_by_id($merchant_info['class_id']);
		}
		foreach ($service_class['themes'] as $theme_id) {
			$theme_list[$theme_id] = $this->hotspot_theme_m->getThemeName($theme_id);
		}
		
		return $theme_list;
	}
	
	function getFullThemeList($merchant_id)
	{
		$this->load->model('hotspot/service_class_m');
		$this->load->model('hotspot/merchant_m');
		
		$theme_list = array();		
		$merchant_info = $this->merchant_m->get_info($merchant_id); //merchant_id = 2
		
		if ($merchant_info['end_date'] < time()) {
			//Drop this location to Free class
			$service_class = $this->service_class_m->get_by_name("Free");			
		} else {
			$service_class = $this->service_class_m->get_by_id($merchant_info['class_id']);
		}
		foreach ($service_class['themes'] as $theme_id) {
			$theme_detail = $this->hotspot_theme_m->getTheme($theme_id);
			array_push($theme_list, $theme_detail);
		}
		
		return $theme_list;
	}
	
	function getThemeFromLocation($location_id)
	{
		$params = array(
					'stream'    => 'list',
	    			'namespace' => 'location',
	    			'where'		=> "`id` ='".$themeID."'"
		);

		$themes = $this->streams->entries->get_entries($params);
	}	
}
