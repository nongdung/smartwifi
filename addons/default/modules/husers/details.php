<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Husers extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Wifi Hotspot Users',
				'vi' => 'Người dùng wifi'
			),
			'description' => array(
				'en' => 'Wifi users management and Logs',
				'vi' => 'Quản lý người dùng wifi'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',
			'roles' => array(
                'merchant','manager', 'admin'
            )		
		);
		
		if (group_has_role('husers', 'admin')) {
			$info['sections']['fields'] = array(
					'name' 	=> 'husers:fields', // These are translated from your language file
					'uri' 	=> 'admin/husers/fields',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'streams:add_field',
							'uri' 	=> 'admin/husers/fields/add',
							'class' => 'add'
							)
					)
			);			
		}
								
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		
		$this->db->delete('settings', array('module' => 'husers'));
		
		// User profile stream
		if ( ! $this->streams->streams->add_stream('Hotspot User Profile', 'profiles', 'husers', 'husers_', null)) {return false;}
		$fields = array(
			array(
                'name' => 'Name',
                'slug' => 'name',
                'namespace' => 'husers',
                'type' => 'text',
                'extra' => array('max_length' => 32),
                'assign' => 'profiles'
            ),
            array(
                'name' => 'First Name',
                'slug' => 'first_name',
                'namespace' => 'husers',
                'type' => 'text',
                'extra' => array('max_length' => 32),
                'assign' => 'profiles'
            ),		
            array(
                'name' => 'Last Name',
                'slug' => 'last_name',
                'namespace' => 'husers',
                'type' => 'text',
                'extra' => array('max_length' => 32),
                'assign' => 'profiles'
            ),
            array(
                'name' => 'Email',
                'slug' => 'email',
                'namespace' => 'husers',
                'type' => 'email',
                'assign' => 'profiles'
            ),
            array(
                'name' => 'Mobile',
                'slug' => 'mobile',
                'namespace' => 'husers',
                'type' => 'text',
                'extra' => array('max_length' => 16),
                'assign' => 'profiles'
            ),
			array(
                'name' => 'Social Network ID',
                'slug' => 'social_network_id',
                'namespace' => 'husers',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'profiles'
            ),
            array(
                'name' => 'Profile Type',
                'slug' => 'profile_type',
                'namespace' => 'husers',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'profiles'
            ),
            array(
                'name' => 'Gender',
                'slug' => 'gender',
                'namespace' => 'husers',
                'type' => 'integer',
                'extra' => array('max_length' => 1),
                'assign' => 'profiles'
            ),
            array(
                'name' => 'Birthday',
                'slug' => 'birthday',
                'namespace' => 'husers',
                'type' => 'datetime',
                'extra' => array('use_time' => 'no', 'storage' => 'datetime', 'input_type' => 'datepicker'),
                'assign' => 'profiles'
            ),   
            array(
                'name' => 'Age Range',
                'slug' => 'age_range',
                'namespace' => 'husers',
                'type' => 'text',
                'extra' => array('max_length' => 32),
                'assign' => 'profiles'
            ),
            
            array(
                'name' => 'Language',
                'slug' => 'language',
                'namespace' => 'husers',
                'type' => 'text',
                'extra' => array('max_length' => 32),
                'assign' => 'profiles'
            ),
            array(
                'name' => 'Locale',
                'slug' => 'locale',
                'namespace' => 'husers',
                'type' => 'text',
                'extra' => array('max_length' => 32),
                'assign' => 'profiles'
            ),
            array(
                'name' => 'Time Zone',
                'slug' => 'timezone',
                'namespace' => 'husers',
                'type' => 'text',
                'extra' => array('max_length' => 32),
                'assign' => 'profiles'
            ),
            array(
                'name' => 'Picture URL',
                'slug' => 'pic_url',
                'namespace' => 'husers',
                'type' => 'text',                
                'assign' => 'profiles'
            )
		);
		$this->streams->fields->add_fields($fields);
		
		//Table husers_current_profiles
		$this->dbforge->drop_table('husers_current_profiles');
		$fields = array(
			'client_mac' => array('type' => 'VARCHAR', 'constraint' => '32'),
			'profile_id' => array('type' => 'INT', 'constraint' => '9'),
			'email' => array('type' => 'VARCHAR', 'constraint' => '64', 'null' => TRUE,'default' => NULL),
			'social_network_id' => array('type' => 'VARCHAR', 'constraint' => '64','null' => TRUE,'default' => NULL),
			'profile_type' => array('type' => 'VARCHAR', 'constraint' => '64','null' => TRUE,'default' => NULL),
			'mobile' => array('type' => 'VARCHAR', 'constraint' => '12','null' => TRUE,'default' => NULL),
			'name' => array('type' => 'VARCHAR', 'constraint' => '64','null' => TRUE,'default' => NULL),
			'pic_url' => array('type' => 'VARCHAR', 'constraint' => '255','null' => TRUE,'default' => NULL),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('client_mac', TRUE);
		$this->dbforge->add_key('profile_id');
		$this->dbforge->add_key('email');
		$create_table_profiles = $this->dbforge->create_table('husers_current_profiles');
		
		//raw log
		$sql = "DROP TABLE IF EXISTS `default_husers_raw_logs`;";
		$this->db->query($sql);
		
		$sql = "CREATE TABLE `default_husers_raw_logs` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `client_mac` varchar(64) DEFAULT NULL,
                  `gw_mac` varchar(64) DEFAULT NULL,
                  `profile_id` int(9) DEFAULT NULL,
                  `location_id` int(9) DEFAULT NULL,
                  `merchant_id` int(5) DEFAULT NULL,
                  `action` varchar(16) DEFAULT NULL,
                  `action_result` tinyint(1) DEFAULT NULL,
                  `created` datetime NOT NULL,                  
                   PRIMARY KEY (`id`),
				  INDEX (`gw_mac`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";
		$this->db->query($sql);
		
		$this->streams->streams->update_stream('profiles', 'husers', array(
            'view_options' => array(
            	'id',
            	'name',                
                'email',               
                'first_name',
                'last_name',
                'social_network_id',
                'profile_type'
            )
        ));
		
		return TRUE;
		
	}

	public function uninstall()
	{
		$this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
		$this->streams->utilities->remove_namespace('husers');
		$this->db->delete('settings', array('module' => 'husers'));
		
		$this->dbforge->drop_table('husers_current_profiles');
		
		$sql = "DROP TABLE IF EXISTS `default_husers_raw_logs`;";
		$this->db->query($sql);
		
		return TRUE;
	}


	public function upgrade($old_version)
	{
		$fields = array(
			'pic_url' => array('type' => 'VARCHAR', 'constraint' => '255','null' => TRUE,'default' => NULL),
		);
		if ($this->dbforge->add_column('husers_current_profiles', $fields))
		{
			return true;
		} else {
			return false;
		}
		
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
	
	public function admin_menu(&$menu)
    {
		if (group_has_role('husers', 'admin')) {
			$menu['Admin']['Wifi User Profile Fields']   = 'admin/husers/fields';
		}
		if (group_has_role('husers', 'merchant')) {
			$menu['lang:cp:reports']['lang:cp:users_list']   = 'admin/husers/visitors';
			add_admin_menu_place('lang:cp:reports', 5);			
		}

	}

}
/* End of file details.php */
