<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_visitors extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('husers');
		$this->load->driver('Streams');
		
    }

    /**
    public function all()
    {
    	role_or_die('husers', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
    	
    	$extra['title'] = 'Visitor Profiles';
		
		$extra['buttons'] = array(
			array(
		        'label'     => lang('global:delete'),
		        'url'       => 'admin/husers/delete/-entry_id-',
		        'confirm'   => true
		    )
		);
		
		$extra['filters'] = array();		
		$this->streams->cp->entries_table('profiles', 'husers', 25, 'admin/husers/index', true, $extra);
    }
	*/
	
	function delete($profile_id)
	{
		role_or_die('husers', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
		if ($profile_id) {
			$this->streams->entries->delete_entry($profile_id,"profiles", "husers");
			$this->session->set_flashdata('success', "Deleted");
			redirect('admin/husers');
		} else {
			$this->session->set_flashdata('error', "Failed to delete");
			redirect('admin/husers');
		}
		
	}
	
	function index()
	{		
		$where = "WHERE r.merchant_id='".$this->current_user->id."'";
		$filters = $this->session->all_userdata();
		$new_filters = array();		
		
		if ($this->input->post('f_profile_type')) {
			$f_profile_type = $this->input->post('f_profile_type');			
			$new_filters['f_profile_type'] = $this->input->post('f_profile_type');
		} elseif (isset($filters['f_profile_type']) && $filters['f_profile_type']) {
			$f_profile_type = $filters['f_profile_type']."'";			
		} else {			
			$f_profile_type = NULL;
		}
		
		switch ($f_profile_type) {
			case 'facebook':
				$where2 = " AND p.profile_type='".$this->input->post('f_profile_type')."'";
				$where3 = " AND r.profile_type='".$this->input->post('f_profile_type')."'";
				break;
			case 'google':
				$where2 = " AND p.profile_type='".$this->input->post('f_profile_type')."'";
				$where3 = " AND r.profile_type='".$this->input->post('f_profile_type')."'";
				break;
			default:
				$where2 = $where3 = "";
				break;
		}
		
		if ($this->input->post('f_location')) {
			$where .= " AND `r`.`location_id` = '".$this->input->post('f_location')."'";
			$new_filters['f_location'] = $this->input->post('f_location');
		} elseif (isset($filters['f_location'])) {
			$where .= " AND `r`.`location_id` = '".$filters['f_location']."'";
		}
		
		if ($this->input->post('f_time')) {
			$f_time = $this->input->post('f_time');
			$new_filters['f_time'] = $this->input->post('f_time');
		} elseif (isset($filters['f_time'])) {
			$f_time = $filters['f_time'];
		} else {
			$f_time = NULL;
		}
		
		if ($this->input->post('f_order_by')) {
			$f_order_by = $this->input->post('f_order_by');
			$new_filters['f_order_by'] = $this->input->post('f_order_by');
		} elseif (isset($filters['f_order_by'])) {
			$f_order_by = $filters['f_order_by'];
		} else {
			$f_order_by = NULL;
		}
				
		switch ($f_time) {
			case 'today':
				$where .= " AND DATE_FORMAT(`r`.`created`, '%Y-%m-%d') = CURRENT_DATE";
				break;
			
			case 'yesterday':
				$where .= " AND `r`.`created` BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 day) AND CURDATE()";
				break;
				
			case 'last-7-days':
				$where .= " AND `r`.`created` >= DATE_ADD(CURDATE(), INTERVAL -7 day)";
				break;
				
			case 'last-30-days':
				$where .= " AND `r`.`created` >= DATE_ADD(CURDATE(), INTERVAL -30 day)";
				break;
				
			case 'this-week':
				$where .= " AND WEEKOFYEAR(`r`.`created`)=WEEKOFYEAR(NOW())";
				break;
				
			case 'last-week':
				$where .= " AND WEEKOFYEAR(`r`.`created`)=WEEKOFYEAR(NOW())-1";
				break;
				
			case 'this-month':
				$where .= " AND `r`.`created` >= DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY)";
				break;
				
			case 'last-month':
				$where .= " AND `r`.`created` BETWEEN DATE_SUB(DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY), INTERVAL 1 MONTH) AND DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())) DAY)";
				break;	
				
			case 'last-3-months':
				$where .= " AND `r`.`created` BETWEEN DATE_SUB(DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY), INTERVAL 3 MONTH) AND DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())) DAY)";
				break;
			default:
				
				break;
		}
		
		$sql = "
			SELECT u.id, u.name, u.email, u.gender, u.locale, u.pic_url, u.social_network_id, u.profile_type, u.language, u.mobile, u.count, v.action, v.last_login
			FROM
				(SELECT
					p.id,
					p.name,			
			        p.email,
			        p.gender,
			        p.locale,
			        p.pic_url,
			        p.social_network_id,
			        p.profile_type,
			        p.language,
			        p.mobile,
					COUNT(p.id) AS count
					FROM default_husers_profiles AS p			
					LEFT JOIN default_husers_raw_logs AS r ON p.id = r.profile_id "
					.$where.$where2.
					" GROUP BY r.profile_id					
				) AS u
			LEFT JOIN
				(
					SELECT p.profile_id, p.action, p.created AS last_login FROM 
					(
						SELECT `profile_id`, MAX(`created`) AS last_login FROM `default_husers_raw_logs` AS r "
						.$where.
						" GROUP BY `profile_id`
					) AS r
					INNER JOIN default_husers_raw_logs AS p ON p.profile_id = r.profile_id AND p.created = r.last_login
			    ) AS v
			ON
			u.id = v.profile_id			
		";		
		
		switch ($f_order_by) {
			case 'login_count':
				$sql .= " ORDER BY u.count DESC";
				break;
			case 'login_time':
				$sql .= " ORDER BY v.last_login DESC";
				break;
			default:				
				break;
		}
		
		//Set session data
		if (!empty($new_filters)) {
			$this->session->set_userdata($new_filters);
		}
		
		//Get total row
		//$result = $this->db->query($sql);
		$total_rows = $this->pyrocache->model("husers_m", "totalrows", array($sql), 7200);

		$this->load->library('pagination');
		$config['base_url'] = "admin/husers/visitors/index";
		$config['total_rows'] = $total_rows;
		$config['per_page'] = 25;
		$config['uri_segment'] = 5;
		
		$pagination  = create_pagination($config['base_url'], $config['total_rows'], $config['per_page'], 5);
		
		if ($this->uri->segment(5)) {
			$offset = (intval($this->uri->segment(5))-1) * 25;
			$limit = " LIMIT 25 OFFSET ".$offset;
		} else {
			$limit = " LIMIT 25";
		}
		
		$new_sql = $sql.$limit.";";
		//print_r($new_sql);exit;
		$result = $this->db->query($new_sql);
		$visitors = $result->result_array();
		
		//location list
		$location_list= array(null => lang('global:select-all'));
		$params = array(
				'stream'    => 'list',
    			'namespace' => 'location',
    			'where'		=> "`created_by` = '".$this->current_user->id."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			foreach ($entries['entries'] as $item) {
				$location_list[$item['id']] = $item['loc_name'];				
			}
		}

		//Asset::js('jquery/jquery.excanvas.min.js');
		//Asset::js('jquery/jquery.flot.js');
		//Asset::js('jquery/jquery.flot.pie.js');
		
		$this->input->is_ajax_request() && $this->template->set_layout(false);
		$this->template
					//->set_partial('filters', 'admin/partials/filters')
					->title($this->module_details['name'],lang("cp:reports"))
					->append_css('module::admin.css')
					->set('visitors', $visitors)
					->set('filters', $filters)
					->set('pagination',	$pagination)
					->append_js('admin/filter.js')
					->set('location_list', $location_list);
					
					
		$this->input->is_ajax_request() ? $this->template->build('admin/tables/visitors') : $this->template->build('admin/visitors_list');
	}

	function download()
	{
		$where = "WHERE r.merchant_id='".$this->current_user->id."'";
		$filters = $this->session->all_userdata();

		if (isset($filters['f_profile_type']) && $filters['f_profile_type']) {
			$f_profile_type = $filters['f_profile_type']."'";			
		} else {			
			$f_profile_type = NULL;
		}
		
		switch ($f_profile_type) {
			case 'facebook':
				$where2 = " AND p.profile_type='".$this->input->post('f_profile_type')."'";
				break;
			case 'google':
				$where2 = " AND p.profile_type='".$this->input->post('f_profile_type')."'";
				break;
			default:
				$where2 = "";
				break;
		}
		
		if (isset($filters['f_location'])) {
			$where .= " AND `r`.`location_id` = '".$filters['f_location']."'";
		}
		
		if (isset($filters['f_time'])) {
			$f_time = $filters['f_time'];
		} else {
			$f_time = NULL;
		}
		
		if (isset($filters['f_order_by'])) {
			$f_order_by = $filters['f_order_by'];
		} else {
			$f_order_by = NULL;
		}
				
		switch ($f_time) {
			case 'today':
				$where .= " AND DATE_FORMAT(`r`.`created`, '%Y-%m-%d') = CURRENT_DATE";
				break;
			
			case 'yesterday':
				$where .= " AND `r`.`created` BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 day) AND CURDATE()";
				break;
				
			case 'last-7-days':
				$where .= " AND `r`.`created` >= DATE_ADD(CURDATE(), INTERVAL -7 day)";
				break;
				
			case 'last-30-days':
				$where .= " AND `r`.`created` >= DATE_ADD(CURDATE(), INTERVAL -30 day)";
				break;
				
			case 'this-week':
				$where .= " AND WEEKOFYEAR(`r`.`created`)=WEEKOFYEAR(NOW())";
				break;
				
			case 'last-week':
				$where .= " AND WEEKOFYEAR(`r`.`created`)=WEEKOFYEAR(NOW())-1";
				break;
				
			case 'this-month':
				$where .= " AND `r`.`created` >= DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY)";
				break;
				
			case 'last-month':
				$where .= " AND `r`.`created` BETWEEN DATE_SUB(DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY), INTERVAL 1 MONTH) AND DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())) DAY)";
				break;	
				
			case 'last-3-months':
				$where .= " AND `r`.`created` BETWEEN DATE_SUB(DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY), INTERVAL 3 MONTH) AND DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())) DAY)";
				break;
			default:
				
				break;
		}
		
		$sql = "
			SELECT u.id, u.name, u.email, u.gender, u.locale, u.pic_url, u.social_network_id, u.profile_type, u.language, u.mobile, u.count, v.action, v.last_login
			FROM
				(SELECT
					p.id,
					p.name,			
			        p.email,
			        p.gender,
			        p.locale,
			        p.pic_url,
			        p.social_network_id,
			        p.profile_type,
			        p.language,
			        p.mobile,
					COUNT(p.id) AS count
					FROM default_husers_profiles AS p			
					LEFT JOIN default_husers_raw_logs AS r ON p.id = r.profile_id "
					.$where.$where2.
					" GROUP BY r.profile_id					
				) AS u
			LEFT JOIN
				(
					SELECT p.profile_id, p.action, p.created AS last_login FROM 
					(
						SELECT `profile_id`, MAX(`created`) AS last_login FROM `default_husers_raw_logs` AS r "
						.$where.
						" GROUP BY `profile_id`
					) AS r
					INNER JOIN default_husers_raw_logs AS p ON p.profile_id = r.profile_id AND p.created = r.last_login
			    ) AS v
			ON
			u.id = v.profile_id			
		";		
		
		switch ($f_order_by) {
			case 'login_count':
				$sql .= " ORDER BY u.count DESC";
				break;
			case 'login_time':
				$sql .= " ORDER BY v.last_login DESC";
				break;
			default:				
				break;
		}

		$result = $this->db->query($sql);
		if ($result->num_rows()) {
			$data = $result->result_array();
		
			$this->load->helper('download');
			$this->load->library('format');
			force_download('visitors_logs.csv', $this->format->factory($data)->{'to_csv'}());
		} else {
			redirect("admin/husers/visitors");
		}
		
	}
}