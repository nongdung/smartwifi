<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	splash Module
 */
class Apis extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		// Load all the required classes
		$this->load->model('devices/device_m');
	}
	
	function get_statistics()
	{
		$data = array(
			'visits' => array(
				array(strtotime("2015/01/01")*1000,mt_rand(10, 100)),
				array(strtotime("2015/01/02")*1000,mt_rand(10, 100)),
				array(strtotime("2015/01/03")*1000,mt_rand(10, 100)),
				array(strtotime("2015/01/04")*1000,mt_rand(10, 100)),
				array(strtotime("2015/01/05")*1000,mt_rand(10, 100))
			),
			'views' => array(
				array(strtotime("2015/01/01")*1000,mt_rand(10, 100)),
				array(strtotime("2015/01/02")*1000,mt_rand(10, 100)),
				array(strtotime("2015/01/03")*1000,mt_rand(10, 100)),
				array(strtotime("2015/01/04")*1000,mt_rand(10, 100)),
				array(strtotime("2015/01/05")*1000,mt_rand(10, 100))
			)
		);
		$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data));
	}
	
	function get_client(){
		$validation_rules = array(
			array(
					'field' => 'client_mac',
					'label' => 'Client MAC',
					'rules' => 'trim|required|callback__check_mac'
			),			
		);
		$this->form_validation->set_rules($validation_rules);
		if ($this->form_validation->run()){
			$result = $this->db
						->where("client_mac",$this->input->post("client_mac"))
						->get("husers_current_profiles")
						->result_array();
			if (!empty($result)) {
				$data = $result[0];				
			} else {
				$data = array();
			}
		} else {
			$data = array(
				"error"=>array(
					"message" => "invalid MAC address",
					"type"		=> "mac",
					"code"		=> 101
				)
			);
		}
						
		header_remove("Pragma");
		$this->output->set_header("Cache-Control: max-age=3600, public");	
			
		$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data));
	}
	
	function create_anonymous_profile(){
		$this->load->driver('Streams');
		$validation_rules = array(
			array(
					'field' => 'client_mac',
					'label' => 'Client MAC',
					'rules' => 'trim|required|callback__check_mac'
			),			
		);
		
		$this->form_validation->set_rules($validation_rules);
		if ($this->form_validation->run()){
			$profile_id = $this->streams->entries->insert_entry(array(), 'profiles', 'husers');
			if ($profile_id) {
				$entry_data = array(
						'client_mac' => $this->input->post('client_mac'),
						'profile_id' => $profile_id
				);
				
				//insert to current profile table
				if ($this->db->insert('husers_current_profiles', $entry_data)) {
					$data = $entry_data;
				} else {
					//delete the profile_id
					$this->streams->entries->delete_entry($profile_id, 'profiles', 'husers');
					$data = array(
						"error"=>array(
							"message" => "failed to bind anonymous profile to this MAC",
							"type"		=> "profile",
							"code"		=> 100
						)
					);
				}
				
			} else {
				$data = array(
					"error"=>array(
						"message" => "Cannot create anonymous profile",
						"type"		=> "profile",
						"code"		=> 101
					)
				);
			}			
		} else {
			$data = array(
						"error"=>array(
							"message" => "invalid MAC",
							"type"		=> "profile",
							"code"		=> 102
						)
			);
		}

		$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data));

	}
	
	function create_profile()
	{
		$this->load->driver('Streams');
		$this->load->model('husers_m');
		
		$validation_rules = array(
			array(
					'field' => 'client_mac',
					'label' => 'Client MAC',
					'rules' => 'trim|required|callback__check_mac'
			),
			array(
					'field' => 'profile_id',
					'label' => 'Profile ID',
					'rules' => 'trim|is_natural_no_zero'
			),
			array(
					'field' => 'email',
					'label' => 'Email',
					'rules' => 'trim|valid_email|xss_clean'
			)			
		);
		switch ($this->input->post("profile_type")) {
			case 'facebook':
				$rule = array(
					'field' => 'social_network_id',
					'label' => 'Social Network ID',
					'rules' => 'trim|required|alpha_dash'
				);
				array_push($validation_rules,$rule);
				break;
			case 'google':
				$rule = array(
					'field' => 'social_network_id',
					'label' => 'Social Network ID',
					'rules' => 'trim|required|alpha_dash'
				);
				array_push($validation_rules,$rule);
				break;
			case 'email':
				$rule = array(
					'field' => 'email',
					'label' => 'Email',
					'rules' => 'trim|required|valid_email'
				);
				array_push($validation_rules,$rule);
				break;
			case 'mobile':
				$rule = array(
					'field' => 'mobile',
					'label' => 'Mobile',
					'rules' => 'trim|required|numeric'
				);
				array_push($validation_rules,$rule);
				break;
			default:
				
				break;
		}
		
		$this->form_validation->set_rules($validation_rules);
		if ($this->form_validation->run()){
			
			switch ($this->input->post("profile_type")) {
				case 'facebook':
					//check exist profile associated with this fb_id
					$profile_tmp = $this->husers_m->get_profile($this->input->post("social_network_id"),"facebook");					
					break;
				case 'google':
					$profile_tmp = $this->husers_m->get_profile($this->input->post("social_network_id"),"google");
					break;
				case 'email':
					$profile_tmp = $this->husers_m->get_profile($this->input->post("email"),"email");
					break;
				case 'mobile':
					$profile_tmp = $this->husers_m->get_profile($this->input->post("mobile"),"mobile");
					break;
				default:
					$profile_tmp = array();
					break;
			}
			
			
			$profile_data = array();
			if ($this->input->post('email')) $profile_data["email"] = $this->input->post('email');
			if ($this->input->post('pic_url')) $profile_data["pic_url"] = $this->input->post('pic_url');
			if ($this->input->post('social_network_id')) $profile_data["social_network_id"] = $this->input->post('social_network_id');				
			if ($this->input->post("profile_type")) $profile_data["profile_type"] = $this->input->post('profile_type');
			if ($this->input->post('mobile')) $profile_data["mobile"] = $this->input->post('mobile');
			if ($this->input->post('name')) $profile_data["name"] = $this->input->post('name');
			$profile_data["client_mac"] = $this->input->post("client_mac");
			
			if (!empty($profile_tmp) && $profile_tmp['id']) {
				//update husers_current_profiles with existing profile_id							
				$profile_data["profile_id"] = $profile_tmp['id'];
				$this->db->where("client_mac",$this->input->post("client_mac"))->update("husers_current_profiles",$profile_data);				
								
				if ($this->input->post('anonymous') == "1") {
					//then delete current anonymous profile in profiles table
					$this->streams->entries->delete_entry($this->input->post('profile_id'), 'profiles', 'husers');
				}
				
				$data = $profile_data;	
			} else {				
				//This is new profile => Create & Update current_profile table
				if ($this->input->post('anonymous') == "1") {
					//update anonymous profile
					$profile_id = $this->streams->entries->update_entry($this->input->post("profile_id"),$this->input->post(), 'profiles', 'husers');
				} else {
					$profile_id = $this->streams->entries->insert_entry($this->input->post(), 'profiles', 'husers');
				}
								
				$profile_data["profile_id"] = $profile_id;
				
				//Then Update current_profile table
				$this->db->where("client_mac",$this->input->post("client_mac"))->update("husers_current_profiles",$profile_data);
				$data = $profile_data;
			}
			
		} else {
			$data = array(
				"error"=>array(
					"message" => "invalid data",
					"type"		=> "profile",
					"code"		=> 100
				)
			);
		}
		header('Content-Type: application/json');		
		echo json_encode($data);
	}

	function update_profile()
	{
		$this->load->driver('Streams');
		$this->load->model('husers_m');
		
		$validation_rules = array(
			array(
					'field' => 'client_mac',
					'label' => 'Client MAC',
					'rules' => 'trim|required|callback__check_mac'
			),
			array(
					'field' => 'profile_id',
					'label' => 'Profile ID',
					'rules' => 'trim|is_natural_no_zero|required'
			),
			array(
					'field' => 'email',
					'label' => 'Email',
					'rules' => 'trim|valid_email|xss_clean'
			)			
		);
		switch ($this->input->post("profile_type")) {
			case 'facebook':
				$rule = array(
					'field' => 'social_network_id',
					'label' => 'Social Network ID',
					'rules' => 'trim|required|alpha_dash'
				);
				array_push($validation_rules,$rule);
				break;
			case 'google':
				$rule = array(
					'field' => 'social_network_id',
					'label' => 'Social Network ID',
					'rules' => 'trim|required|alpha_dash'
				);
				array_push($validation_rules,$rule);
				break;
			case 'email':
				$rule = array(
					'field' => 'email',
					'label' => 'Email',
					'rules' => 'trim|required|valid_email'
				);
				array_push($validation_rules,$rule);
				break;
			case 'mobile':
				$rule = array(
					'field' => 'mobile',
					'label' => 'Mobile',
					'rules' => 'trim|required|numeric'
				);
				array_push($validation_rules,$rule);
				break;
			default:
				
				break;
		}
		
		$this->form_validation->set_rules($validation_rules);
		if ($this->form_validation->run()){
			$profile_data = array();
			if ($this->input->post('email')) $profile_data["email"] = $this->input->post('email');
			if ($this->input->post('pic_url')) $profile_data["pic_url"] = $this->input->post('pic_url');
			if ($this->input->post('social_network_id')) $profile_data["social_network_id"] = $this->input->post('social_network_id');				
			if ($this->input->post("profile_type")) $profile_data["profile_type"] = $this->input->post('profile_type');
			if ($this->input->post('mobile')) $profile_data["mobile"] = $this->input->post('mobile');
			if ($this->input->post('name')) $profile_data["name"] = $this->input->post('name');
			$profile_data["client_mac"] = $this->input->post("client_mac");

			$this->db
						->where("client_mac",$this->input->post("client_mac"))
						->where("profile_id",$this->input->post("profile_id"))
						->update("husers_current_profiles",$profile_data);				

			$this->streams->entries->update_entry($this->input->post("profile_id"),$this->input->post(), 'profiles', 'husers');

			$data = $profile_data;

		} else {
			$data = array(
				"error"=>array(
					"message" => "invalid data",
					"type"		=> "profile",
					"code"		=> 100
				)
			);
		}
		header('Content-Type: application/json');		
		echo json_encode($data);
	}

	
	function create_raw_log()
	{
		$validation_rules = array(
			array(
					'field' => 'client_mac',
					'label' => 'Client MAC',
					'rules' => 'trim|required|callback__check_mac'
			),
			array(
					'field' => 'gw_mac',
					'label' => 'GW MAC',
					'rules' => 'trim|required|callback__check_mac'
			),
			array(
					'field' => 'profile_id',
					'label' => 'Profile ID',
					'rules' => 'trim|integer'
			),
			array(
					'field' => 'location_id',
					'label' => 'Location ID',
					'rules' => 'trim|integer'
			),
			array(
					'field' => 'action',
					'label' => 'Action',
					'rules' => 'trim|alpha_dash'
			),
			array(
					'field' => 'action_result',
					'label' => 'Action Result',
					'rules' => 'trim|is_natural'
			),			
		);
		$this->form_validation->set_rules($validation_rules);
		$insert_data = $this->input->post();
		unset($insert_data['csrf_hash_name']);
		$insert_data['created'] = date("Y-m-d H:i:s");
		if ($this->form_validation->run() && $this->db->insert('husers_raw_logs', $insert_data)){
			$data = array(
				"success"=>array(
					"message" => "log created",
					"type"		=> "raw",
					"code"		=> 200
				)
			);
			
			//TODO: Increase login_number by one
			$this->load->library('sw_redis');
			$this->sw_redis->setPrefix("merchant:");
			$this->sw_redis->hincrby($insert_data['merchant_id'], 'total_login', 1);
			//$this->sw_redis->close_this();
			
		} else {
			$data = array(
				"error"=>array(
					"message" => "Validation error or Cannot insert log",
					"type"		=> "raw",
					"code"		=> 100
				)
			);
		}
		header('Content-Type: application/json');		
		echo json_encode($data);
	}

	
	function _check_mac($client_mac)
	{
		$this->form_validation->set_message('_check_mac', "invalid MAC address", "MAC Address");
		return $this->device_m->is_valid_mac($client_mac);
	}
	
	
	
}
