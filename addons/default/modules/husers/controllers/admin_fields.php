<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_fields extends Admin_Controller
{
	protected $section = 'fields';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('husers');
		$this->load->driver('Streams');
		role_or_die('husers', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all billing plans
     */
    public function index()
    {
    	$buttons = array(
			array(
				'url'		=> 'admin/husers/fields/edit/-assign_id-', 
				'label'		=> $this->lang->line('global:edit')
			),
			array(
				'url'		=> 'admin/husers/fields/delete/-assign_id-', 
				'label'		=> $this->lang->line('global:delete'),
				'confirm'	=> true
			)
		);		
		$this->streams->cp->assignments_table(
								"profiles",
								"husers",
								15,
								'admin/husers/fields/index',
								true,
								array('buttons' => $buttons));
    }
	
	public function edit($assign_id)
	{		
		if ( ! $assign_id)
		{
			show_error(lang('streams:cannot_find_assign'));
		}
		
		$extra = array(
			'title'			=> lang('streams:edit_field'),
			'show_cancel'	=> true,
			'cancel_uri'	=> 'admin/husers/fields/'
		);

		$this->streams->cp->field_form("profiles", "husers", 'edit', 'admin/husers/fields/', $assign_id, array(), true, $extra);
	}
	
	public function delete($assign_id = null)
	{		
		if ( ! $assign_id)
		{
			show_error(lang('streams:cannot_find_assign'));
		}
	
		// Tear down the assignment
		if ( ! $this->streams->cp->teardown_assignment_field($assign_id))
		{
		    $this->session->set_flashdata('notice', lang('streams:field_delete_error'));
		}
		else
		{
		    $this->session->set_flashdata('success', lang('streams:field_delete_success'));			
		}
	
		redirect('admin/husers/fields/');
	}
	
	function add()
	{
		$extra['title'] 		= lang('streams:add_field');
		$extra['show_cancel'] 	= true;
		$extra['cancel_uri'] 	= 'admin/husers/fields';

		$this->streams->cp->field_form('profiles', 'husers', 'new', 'admin/husers/fields', null, array(), true, $extra);		
	}

}