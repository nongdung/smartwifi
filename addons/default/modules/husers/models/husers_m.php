<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Husers_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		$this->_table = "husers_raw_logs";
	}
	
	function get_profile($data = null, $type = null)
	{
		switch ($type) {
			case 'facebook':
				$field = "social_network_id";
				$this->db->where("profile_type", "facebook");
				break;
			case 'google':
				$field = "social_network_id";
				$this->db->where("profile_type", "google");
				break;
			case 'email':
				$field = "email";
				break;
			case 'mobile':
				$field = "mobile";
				break;
			default:
				return array();
				break;
		}
		
		$query = $this->db
						->where($field, $data)
						->get("husers_profiles");
		if ($query->num_rows() > 0) {
			$data = $query->result_array();
			return $data[0];
		} else {
			return array();
		}
		
	}
	
	public function totalrows($sql = "")
	{
		if ($sql != "") {
			return $this->db->query($sql)->num_rows();
		} else {
			return 0;
		}
	}
	
	public function getClientInfo($client_mac = null)
	{
		if (! $client_mac) return array();
		
		if ($this->config->item('sess_use_redis')) {
			$this->load->library('splash/sw_redis');
			$this->sw_redis->setPrefix("client:");
			
			//Try to read from cache
			$client_info = $this->sw_redis->get($client_mac);
		} 
		
		if (isset($client_info) && $client_info) {
			//Return result If data exist in cache
			return $client_info;
		} else {
			$data = array();
			$result = $this->db
							->where("client_mac",$client_mac)
							->get("husers_current_profiles")
							->result_array();
			if (!empty($result)) {
				$data = $result[0];
				//Now save data to redis cache
				if ($this->config->item('sess_use_redis')) {
					$this->sw_redis->set($client_mac, $data, 7200);
				}				
			} 
			
			return $data;
		}
		
		
	}
	
	public function getClientInfoFromId($profile_id = null)
	{
		if (! $profile_id) return array();
		$data = array();
		$this->load->driver('Streams');
		$entry =  $this->streams->entries->get_entry($profile_id, 'profiles', 'husers');
		foreach ($entry as $key => $value) {
			$data[$key] = $value;
		}
		return $data;
	}
	
	
	/*
	 * $period = 'day' or 'range', $from/$to using format YYYY-MM-DD
	 * 
	 */
	public function getLoginStatsByMerchantId($merchantId='', $period = 'day', $from='', $to='')
	{
		
		if ($period == 'day') {
			$where_time = "created >= '" . $from . " 00:00:00' AND created <= '" . $from . " 23:59:59'";
		} else {
			$where_time = "created >= '" . $from . " 00:00:00' AND created <= '" . $to . " 23:59:59'";
		}
		
		//Get total login
		$total_login = $this->db->select('count(id) as total_rows')
								->where('merchant_id', $merchantId)
								->where($where_time)
								->count_all_results('husers_raw_logs');
		$data['total_login'] = $total_login;
		
		$login = $this->db->select('action, count(id) as count')
								->where('merchant_id', $merchantId)
								->where($where_time)
								->group_by('action')
								->get('husers_raw_logs')->result();
		foreach ($login as $row) {
			$data[$row->action] = $row->count;
		}
		return $data;
	}

}
