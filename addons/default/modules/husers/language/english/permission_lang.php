<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Blog Permissions
$lang['husers:role_merchant']	= 'Merchant';
$lang['husers:role_manager']     = 'Manager';
$lang['husers:role_admin']     = 'Admin';