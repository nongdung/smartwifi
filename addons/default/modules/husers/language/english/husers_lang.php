<?php
//messages
$lang['husers:section_name']			=	'Hotspot Users';
$lang['husers:fields']			=	'Fields';
$lang['husers:visitors']			=	'Visitors';
$lang['husers:visitor_logs']			=	'Visitor Logs';
$lang['husers:users']			=	'Users';
$lang['husers:users_list']			=	'Users List';

//Visitors Filter
$lang['husers:time']			=	'Time';
$lang['husers:today']			=	'Today';
$lang['husers:yesterday']			=	'Yesterday';
$lang['husers:last-7-days']			=	'Last 7 days';
$lang['husers:last-30-days']			=	'Last 30 days';
$lang['husers:this-week']			=	'This week';
$lang['husers:last-week']			=	'Last week';
$lang['husers:this-month']			=	'This month';
$lang['husers:last-month']			=	'Last month';
$lang['husers:last-3-months']			=	'Last 3 months';
$lang['husers:location']			=	'Location';
$lang['husers:no_visitors']			=	'No visitors';

$lang['husers:male']			=	'Male';
$lang['husers:female']			=	'Female';

$lang['husers:time']			=	'Time';
$lang['husers:order_by']			=	'Order by';
$lang['husers:profile_type']			=	'Profile type';
$lang['husers:login_time']			=	'Login time';
$lang['husers:login_count']			=	'Login count';
$lang['husers:user_name']			=	'User\'s name';
$lang['husers:avatar']			=	'Avatar';
$lang['husers:profile_type']			=	'Profile type';
$lang['husers:gender']			=	'Gender';
$lang['husers:language']			=	'Language';
$lang['husers:last_login_type']			=	'Last login type';
$lang['husers:last_login']			=	'Last login';
?>