<?php
//messages
$lang['husers:section_name']			=	'Hotspot Users';
$lang['husers:fields']			=	'Fields';
$lang['husers:visitors']			=	'Khách hàng';
$lang['husers:visitors_list']			=	'Danh sách khách hàng';
$lang['husers:users']			=	'Người dùng';
$lang['husers:users_list']			=	'Danh sách người dùng';
$lang['husers:visitor_logs']			=	'Visitor Logs';

//Visitors Filter
$lang['husers:time']			=	'Time';
$lang['husers:today']			=	'Today';
$lang['husers:yesterday']			=	'Yesterday';
$lang['husers:last-7-days']			=	'Last 7 days';
$lang['husers:last-30-days']			=	'Last 30 days';
$lang['husers:this-week']			=	'This week';
$lang['husers:last-week']			=	'Last week';
$lang['husers:this-month']			=	'This month';
$lang['husers:last-month']			=	'Last month';
$lang['husers:last-3-months']			=	'Last 3 months';
$lang['husers:location']			=	'Location';
$lang['husers:no_visitors']			=	'No visitors';

$lang['husers:male']			=	'Male';
$lang['husers:female']			=	'Female';

$lang['husers:time']			=	'Thời gian';
$lang['husers:order_by']			=	'Sắp xếp theo';
$lang['husers:profile_type']			=	'Kiểu hồ sơ';
$lang['husers:login_time']			=	'Thời gian đăng nhập';
$lang['husers:login_count']			=	'Số lần đăng nhập';
$lang['husers:user_name']			=	'Tên người dùng';
$lang['husers:avatar']			=	'Ảnh đại diện';
$lang['husers:profile_type']			=	'Kiểu hồ sơ';
$lang['husers:gender']			=	'Giới tính';
$lang['husers:language']			=	'Ngôn ngữ';
$lang['husers:last_login_type']			=	'Kiểu đăng nhập cuối';
$lang['husers:last_login']			=	'Lần đăng nhập cuối';
?>