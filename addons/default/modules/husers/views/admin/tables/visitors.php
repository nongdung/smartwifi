<?php if ( ! empty($visitors)): ?>

	<table border="0" class="table-list" cellspacing="0">
		<thead>
			<tr>				
				<th>#</th>
				<th><?php echo lang('husers:avatar');?></th>
				<th><?php echo lang('husers:user_name');?></th>
				<th><?php echo lang('husers:profile_type');?></th>
				<th>Email</th>
				<th>Mobile</th>
				<th><?php echo lang('husers:gender');?></th>
				<th><?php echo lang('husers:language');?></th>
				<th><?php echo lang('husers:login_count');?></th>
				<th><?php echo lang('husers:last_login_type');?></th>
				<th><?php echo lang('husers:last_login');?></th>
			</tr>
		</thead>
	
		<tfoot>
			<tr>
				<td colspan="11">
					<div class="inner"><?php if ($pagination) echo $pagination['links'];?></div>
				</td

			</tr>
		</tfoot>
	
		<tbody>
			<?php
				if ($this->uri->segment(5)) {
					$i=($this->uri->segment(5)-1)*25+1;
				} else {
					$i=1;
				}			 
				
			?>
			<?php foreach ($visitors as $visitor): ?>
				<tr>
					<td><?php echo $i++ ?></td>
					<td><?php 
						if ($visitor['pic_url']) {
							echo "<img height=\"30\" width=\"30\" src=\"".$visitor['pic_url']."\"/>";
						} else {
							echo "<span class=\"anonymous\"></span>";
						}
						?>
					</td>
					<td>
						<?php
						switch ($visitor['profile_type']) {
							case 'facebook':
								echo anchor('https://www.facebook.com/'.$visitor['social_network_id'], $visitor['name'], 'target="_blank"');
								break;
							case 'google':
								echo anchor('https://plus.google.com/'.$visitor['social_network_id'], $visitor['name'], 'target="_blank"');
								break;
							default:
								if ($visitor['name']) {
									echo $visitor['name'];
								} else {
									echo "Anonymous";
								}																
								break;
						} 						
						?>
					</td>
					<td>
						<?php
						switch ($visitor['profile_type']) {
							case 'facebook':
								echo "<span class=\"facebook\"></span>";
								break;
							case 'google':
								echo "<span class=\"google-plus\"></span>";
								break;
							default:
								
								break;
						} 						
						?>
					</td>
					<td><?php echo $visitor['email'];?></td>
					<td><?php echo $visitor['mobile'];?></td>
					<td>
						<?php
						if (!isset($visitor['gender'])) {
							echo "N/A";
						} else {
							switch ($visitor['gender']) {
								case 0:
									//echo lang("husers:male");
									echo "<span class=\"male\"></span>";
									break;
								case 1:
									//echo lang("husers:female");
									echo "<span class=\"female\"></span>";
									break;
								default:
									echo "N/A";
									break;
							} 
						}												
						?>
					</td>
					<td><?php echo $visitor['language'];?></td>
					<td><?php echo $visitor['count'];?></td>
					<td><?php echo $visitor['action'];?></td>
					<td><?php echo $visitor['last_login'];?></td>
			<?php endforeach ?>
		</tbody>
	</table>
<?php else: ?>

	<div class="no_data"><?php echo lang('husers:no_visitors') ?></div>
	
<?php endif ?>
