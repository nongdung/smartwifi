<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

<section class="title">
	<h4><?php echo lang("husers:users_list");?></h4>
</section>

<section class="item">
	<div class="one_full">
		<?php echo $this->load->view('admin/partials/filters') ?>
	
		<div id="filter-stage">		
			<?php echo $this->load->view('admin/tables/visitors') ?>		
		</div>
	</div>
</section>