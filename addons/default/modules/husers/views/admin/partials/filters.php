<fieldset id="filters">

	<legend><?php echo lang('global:filters') ?></legend>
	
		<?php echo form_open(''); ?>
		<?php echo form_hidden('f_module', $module_details['slug']); ?>
		
		<ul>
			<li>
				<?php echo lang('husers:time', 'f_time') ?>
				<?php
					$f_time = array(
						'today' => lang('husers:today'),
						'yesterday' => lang('husers:yesterday'),
						'last-7-days'	=> lang('husers:last-7-days'),
						'last-30-days'	=> lang('husers:last-30-days'),
						'this-week'	=> lang('husers:this-week'),
						'last-week'	=> lang('husers:last-week'),
						'this-month'	=> lang('husers:this-month'),
						'last-month'	=> lang('husers:last-month'),
						'last-3-months'	=> lang('husers:last-3-months'),
						'all' =>lang('global:select-all')
					);
				?>
				<?php echo form_dropdown('f_time', $f_time, isset($filters['f_time'])?$filters['f_time']:'all') ?>
    		</li>

	
			<li>
            	<?php echo lang('cp:location', 'f_location') ?>
            	<?php echo form_dropdown('f_location', $location_list) ?>
        	</li>
        	
        	<li>
            	<?php echo lang('husers:order_by', 'f_order_by') ?>
            	<?php echo form_dropdown('f_order_by', array('login_time' => lang('husers:login_time'), 'login_count' => lang('husers:login_count')), isset($filters['f_order_by'])?$filters['f_order_by']:'login_time'); ?>
        	</li>
        	
        	<li>
            	<?php echo lang('husers:profile_type', 'f_profile_type') ?>
            	<?php echo form_dropdown('f_profile_type', array('all' => lang('global:select-all'), 'facebook' => 'Facebook', 'google' => 'Google'), isset($filters['f_profile_type'])?$filters['f_profile_type']:'0'); ?>
        	</li>
	
			<li><?php echo anchor(current_url() . '#', lang('buttons:cancel'), 'class="cancel"') ?></li>
		</ul>
		
		<?php echo form_close() ?>

		<?php
			echo anchor("admin/husers/visitors/download", "Download", 'class = "btn blue last"');
		?>

</fieldset>
