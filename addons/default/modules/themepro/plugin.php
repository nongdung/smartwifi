<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Plugin_Themepro extends Plugin
{

	function header()
	{
		$device_type = $this->attribute('device_type');
		if ($device_type == 'desktop') {
			$view_file = 'partials/desktop/header';
		} else {
			$view_file = 'partials/mobile/header';
		}
		
		$form = $this->module_view(
					    'themepro', // Module name
					    $view_file, // View filename
					    array(), // Data
					    TRUE // Return?
		);
		 
		//$form = $this->load->view('theme_default/front/css.php',TRUE);		
		return $form;
	}
	
	function login()
	{
		$device_type = $this->attribute('device_type');
		if ($device_type == 'desktop') {
			$view_file = 'partials/desktop/login';
		} else {
			$view_file = 'partials/mobile/login';
		}
		
		$form = $this->module_view(
					    'themepro', // Module name
					    $view_file, // View filename
					    array(), // Data
					    TRUE // Return?
		);
		 	
		return $form;
	}
	
	function custom_css()
	{
		$form = $this->module_view(
					    'themepro', // Module name
					    "partials/custom_css", // View filename
					    array(), // Data
					    TRUE // Return?
		);
		 	
		return $form;
	}	
	
	function success()
	{
		$device_type = $this->attribute('device_type');
		if ($device_type == 'desktop') {
			$view_file = 'partials/desktop/success';
		} else {
			$view_file = 'partials/mobile/success';
		}
		
		$form = $this->module_view(
					    'themepro', // Module name
					    $view_file, // View filename
					    array(), // Data
					    TRUE // Return?
		);
		 	
		return $form;
	}
	
	function footer()
	{
		$device_type = $this->attribute('device_type');
		if ($device_type == 'desktop') {
			$view_file = 'partials/desktop/footer';
		} else {
			$view_file = 'partials/mobile/footer';
		}
		
		$form = $this->module_view(
					    'themepro', // Module name
					    $view_file, // View filename
					    array(), // Data
					    TRUE // Return?
		);
		 
		//$form = $this->load->view('theme_default/front/css.php',TRUE);		
		return $form;
	}

}

/* End of file plugin.php */