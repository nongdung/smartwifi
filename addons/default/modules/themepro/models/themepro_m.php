<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Themepro_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		$module_name = "themepro";		
		$dir = ADDONPATH.'modules/';		
		// Check shared addons
	    if (is_dir(SHARED_ADDONPATH.'modules/'.$module_name)) {
	        $dir = SHARED_ADDONPATH.'modules/';
	    } elseif ( ! is_dir($dir.$module_name)) {
	        $core_path = defined('PYROPATH') ? PYROPATH : APPPATH;
	        $dir = $core_path.'modules';
	    }
		
		Asset::add_path($module_name, $dir.$module_name."/");		
	}
	
	public function set_assets($device_type)
	{
		Asset::add_path('font-awesome', 'assets/font-awesome/');
		Asset::add_path('jquery', 'assets/jquery/');
		
		Asset::css('themepro::pure-min.css');
		Asset::css('themepro::grids-responsive-min.css');
		Asset::css('themepro::marketing.css');
		
		Asset::css('font-awesome::font-awesome.min.css');
		
		if ($device_type == "desktop") {
			//Asset::css('themepro::desktop.css');
		} else {
			//Asset::css('themepro::mobile.css');
		}

		//Asset::js('jquery::jquery-1.11.3.min.js');
	}
	
	public function get_info()
	{
		$data = array(
			'platform' => 'PureCSS',
			'jquery' => false,
			'layout'	=> 'hotspot/themepro.html'
		);
		return $data;
	}
}
