<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Theme_fields_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		$this->module = "themepro";			
	}
	
	function get_skip_fields($options)
	{
		$input_fields = explode(",",Settings::get($options));
		$skip = array();
		
		$query = $this->db
						->select('field_slug')
						->where('field_namespace', $this->module)
						->get('data_fields')
						->result_array();
		foreach ($query as $row) {
			if (! in_array($row['field_slug'], $input_fields)) {
				//put this field to skip array
				array_push($skip, $row['field_slug']);
			}
		}

		return $skip;
	}
}
