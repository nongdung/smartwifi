<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 *
 * @author      Nong Dung - SmartWifi Dev Team
 * @website     http://smartwifi.com.vn
 * @package     Hotspot
 * @subpackage  Themepro Module
 */
class Admin extends Admin_Controller
{
	//protected $section = 'themepro';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('themepro');
		$this->load->driver('Streams');
		role_or_die('hotspot', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all billing plans
     */
    public function index($location_id = null)
    {
    	$this->load->model('location/location_m');
    	if (! $this->location_m->checkLocationOwner($location_id,$this->current_user->id)) {
			$this->session->set_flashdata('notice', "You don't have right to edit that location");
			redirect('admin/location');
		}
		
    	$params = array(
		    'stream'    => 'themepro',
		    'namespace' => 'themepro',
		    'where'		=> "`created_by` ='".$this->current_user->id."' AND `location_id` = '".$location_id."'"
		);
		
		$entries = $this->streams->entries->get_entries($params);
		
		$location_detail = $this->location_m->get_detail($location_id);
    	$this->template
    				->set('title', lang('themepro:overview'))
					->set('entries', $entries['entries'])
					->set('location_detail',$location_detail)
    				->build('admin/overview');
    }
	
	function edit($location_id)
    {
    	//$this->load->model('theme_fields_m');
    	//$skip_fields = $this->theme_fields_m->get_skip_fields('general_fields');
		//print_r($skip_fields);exit;
		//Get theme id
		$params = array(
		    'stream'    => 'themes',
		    'namespace' => 'splash',
		    'where'		=> "`stream_slug` ='themepro' AND `namespace_slug` = 'themepro'"
		);		
		$theme = $this->streams->entries->get_entries($params);
		if (! $theme['total']) {
			redirect("admin");
		}

		$skip_fields=array();
    	$params = array(
		    'stream'    => 'themepro',
		    'namespace' => 'themepro',
		    'where'		=> "`created_by` ='".$this->current_user->id."' AND `location_id` = '".$location_id."'"
		);		
		$entries = $this->streams->entries->get_entries($params);
		//print_r($entries);exit;
		$extra = array(
			    'return'            => 'admin/themepro/'.$location_id,
			    'success_message'   => lang('global:success'),
			    'failure_message'   => lang('global:error'),
			    'title'             => 'Edit Fields'
		);
		
		$tabs = array(
		    array(
		        'title'     => "General fields",
		        'id'        => 'general-tab',
		        'fields'    => explode(",","location_id,logo")
		    ),
		    
		    array(
		        'title'     => "Agreement Terms",
		        'id'        => 'agreement-tab',
		        'fields'    => array("agreement_terms")
		    ),
		    array(
		        'title'     => "Userguide",
		        'id'        => 'userguide-tab',
		        'fields'    => array("userguide")
		    )

		);
		$hiddens = array('location_id');
		$defaults = array('location_id' => $location_id);
		
		if ($entries['total']) {
			//delete cached
			$this->pyrocache->delete_cache("hotspot_theme_m", "getThemeDetail", array($theme['entries'][0]['id'], $location_id));
			//Edit
			$this->streams->cp->entry_form('themepro', 'themepro', 'edit', $entries['entries'][0]['id'], true, $extra, $skip_fields, $tabs,$hiddens, $defaults);
			
			
		} else {
			//add new
			$this->streams->cp->entry_form('themepro', 'themepro', 'new', null, true, $extra, $skip_fields, $tabs,$hiddens, $defaults);
		}
		
				
		//$this->template->build('admin/general');
    }  

}