<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Themepro extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Theme PRO'
			),
			'description' => array(
				'en' => 'Theme for commercial login'
			),
			'frontend' => FALSE,
			'backend' => TRUE,
			'menu' => FALSE,
			'skip_xss' => TRUE	
		);

		if (group_has_role('hotspot', 'admin')) {			
			$info['sections'] ['themepro_admin_fields'] = array(
				'name'	=> "Fields",
				'uri'	=> "admin/themepro/fields",
				'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'global:add',
							'uri' 	=> 'admin/themepro/fields/add',
							'class' => 'add'
							)
					)
			);		
		}							
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		$stream_check = $this->db
								->where('stream_slug','themepro')
								->where('stream_namespace', 'themepro')
								->get('data_streams');
		if ($stream_check->num_rows() > 0) { return FALSE;}
		
		$add_theme_stream = $this->streams->streams->add_stream("Theme Pro", 'themepro', 'themepro', null, null);
		if ($add_theme_stream) {
			//add theme info to main table
			$entry_data = array(
				'name' => "Theme Pro",
				'theme_slug'   => 'themepro',
				'stream_slug'   => 'themepro',
				'namespace_slug'   => 'themepro', //equipvalent to module name
			);
			$result = $this->streams->entries->insert_entry($entry_data, 'themes', 'splash');
			if (! $result) {
				$this->streams->streams->delete_stream('themepro', 'themepro');
				return FALSE;
			}
			//add fields for this theme
			$fields = array(
	            array(
	                'name' => 'Location_id',
	                'slug' => 'location_id',
	                'namespace' => 'themepro',
	                'type' => 'integer',
	                'extra' => array('max_length' => 11),
	                'assign' => 'themepro',
	                'locked' => TRUE           
	            )
			);
			$this->streams->fields->add_fields($fields);
			
		} else {
			return FALSE;
		}
				
		return TRUE;		
	}

	public function uninstall()
	{
		$this->load->driver('Streams');
        $this->streams->utilities->remove_namespace('themepro');
		$this->db->delete('settings', array('module' => 'themepro'));
		
		//delete theme record in splash:themes
		$params = array(
		    'stream'    => 'themes',
		    'namespace' => 'splash',
		    'where'		=> "`namespace_slug` ='themepro'"
		);
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			$this->streams->entries->delete_entry($entries['entries'][0]['id'], "themes", "splash");
		}
				
		return TRUE;
	}


	public function upgrade($old_version)
	{
				
		return TRUE;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
    
    
    public function admin_menu(&$menu)
    {	

	}
}
/* End of file details.php */
