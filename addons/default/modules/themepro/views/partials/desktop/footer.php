<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

	</div>
	<div class="push"></div>
</div>

<div class="footer l-box is-center">
	{{ location_detail:loc_name }} | Address: {{ location_detail:address }} | {{ location_detail:info }}
</div>
