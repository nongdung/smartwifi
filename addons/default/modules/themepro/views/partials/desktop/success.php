<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

{{ if exists theme_detail:success_message }}
	<div class="container width-800" style="margin-top:10px;">
		<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-primary">
					  <div class="panel-body">
					  	{{ theme_detail:success_message }}
					  </div>
					</div>
				</div>
		</div>
	</div>	
{{ endif }}