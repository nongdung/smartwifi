<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="wrapper">
	<div class="header">
	    <div class="home-menu pure-menu pure-menu-horizontal pure-menu-fixed">
	        <a class="pure-menu-heading" href="">
	        	<img id="footer-img" alt="Logo" class="pure-img-responsive" src="{{ url:site }}files/thumb/{{ theme_detail:logo:id }}/auto/40" />
	        </a>
			
	        <ul class="pure-menu-list">
	            <li class="pure-menu-item pure-menu-selected"><a href="#" class="pure-menu-link">Home</a></li>
	            <li class="pure-menu-item"><a href="#" class="pure-menu-link">Tour</a></li>
	            <li class="pure-menu-item"><a href="#" class="pure-menu-link">Sign Up</a></li>
	        </ul>
	    </div>
	</div>
	
	<div class="content-wrapper">