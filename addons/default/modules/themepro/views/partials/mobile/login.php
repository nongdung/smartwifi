<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

    <div class="content">
        <div class="pure-g">
        	
            <div class="l-box-lrg pure-u-1 pure-u-md-3-8">
				
				<?php foreach ($wifi_method_position as $method => $value) :?>
					<?php if ($method == 'modpro'):?>
						<?php echo $wifi_login_html[$method]['html']; ?>
					<?php endif;?>
				<?php endforeach;?>
			</div>
        	
        	{{ if theme_detail:agreement_terms OR theme_detail:userguide }}
            <div class="l-box-lrg pure-u-1 pure-u-md-5-8">
            	
            	{{ if theme_detail:agreement_terms }}
                	<h4>Điều khoản sử dụng/Agreement</h4>
                	{{ theme_detail:agreement_terms }}
				{{ endif }}
				
				{{ if theme_detail:userguide }}
                	<h4>Hướng dẫn sử dụng/User guide</h4>
                	{{ theme_detail:userguide }}
                {{ endif }}
                
            </div>
            {{ endif }}
            
        </div>

    </div>

<script src="{{ asset:js_url file="jquery::jquery-1.11.3.min.js" }}"></script>
<script>
	$('.wifi-login-link').click(function(e) {
		if (swController.clientState == 0) {
			var my = $(e.target);
			my.html('Logging in...');
			//$('#sw-status-wait').fadeIn();
			
		};
	});
	
	var swUser = {};
	$(document).ready(function(){
		$.ajaxSetup({
        	data: {
            	csrf_hash_name: "<?php echo $this->security->get_csrf_hash(); ?>"
        	}
    	});    		
		$.post( "husers/apis/get_client", {client_mac : "<?php echo $this->session->userdata('mac');?>"}, function( jsonp ) {
			if (!jsonp.error) {
				swUser = jsonp;
				if (jsonp.name) {
					$('#c-name').html("Hi " + jsonp.name + "!<br/>");
				} else {
					$('#c-name').html("");
				}
				if (!jsonp || !jsonp.profile_id) {
					//Create anonymous profile
					userdata = {
						'client_mac' : "<?php echo $this->session->userdata('mac')?>",																
					};
								
					$.post( "husers/apis/create_anonymous_profile", userdata, function( response ) {
						swUser = response;
						console.log(JSON.stringify(response));
					});
				}
			} else {
				$('#c-name').html();
				$('#error-msg').html("Error:" + jsonp.error.message);
				console.log(JSON.stringify(jsonp));
			}
		});
		
		cookie_account = SmartWifi.readCookie("sw_modpro_account");
		if (cookie_account) {
			var sw_modpro_account = cookie_account.split("-");
			if (sw_modpro_account[0]) $('#modpro-username').val(sw_modpro_account[0]);
			if (sw_modpro_account[1]) $('#modpro-password').val(sw_modpro_account[1]);
			$('#modpro-remember').prop("checked", true);
		};
		
    });
    $('.login-link').bind("click",function(){
    	if (document.getElementById('agreement').checked) {
            $('#error-msg').html("");
            return true;
            
        } else {
            $('#error-msg').html("Bạn cần đồng ý với điều khoản sử dụng của chúng tôi!");
            //$('#error-msg')..blink();
            setInterval(function(){
	            $("#error-msg").fadeOut(function () {
	                $(this).fadeIn();
	            });
	        } ,2000);
            return false;
        }
        
    });
    $('#agreement').bind("change",function(){
    	if (document.getElementById('agreement').checked) {
            $('#error-msg').html("");
        }
    });

</script>
