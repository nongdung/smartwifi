<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<style>
	.sw-wrapper {
	    background-color:{{ theme_detail:body_background_color }};
	    color:{{ theme_detail:body_text_color }};
	}
	.agreement{font-size:0.8em;}
	.agreement a{
		color: {{ theme_detail:link_color }};
	}
	.readmore{color:{{ theme_detail:link_color }};}
	.mastfoot{
		background-color:{{ theme_detail:footer_background_color }}; 
		color:{{ theme_detail:footer_text_color }};
	}
	{{ if exists theme_detail:custom_css and theme_detail:custom_css != "" }}
		<?php 
			$custom_css = html_entity_decode($theme_detail['custom_css']);
			echo $custom_css;
		?>
	{{ endif }}
</style>