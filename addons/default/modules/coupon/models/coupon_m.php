<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Coupon_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		
		
	}
	
	function checkCoupon($profile_id = null, $owner_id = null, $location_id = null)
	{
		$valid_campaigns = $this->hasValidCampaign($owner_id, $location_id);
		
	}
	
	public function hasValidCampaign($owner_id, $location_id = null)
	{
		$this->load->driver('Streams');
		$current_time = time();
		$params = array(
				'stream'    => 'campaign',
    			'namespace' => 'coupon',
    			'where'		=> "`created_by` = '".$owner_id."' AND `campaign_start_time` <= '".$current_time."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		$data = array();
		foreach ($entries['entries'] as $entry) {
				if (! isset($entry['campaign_end_time'])) {
					array_push($data,$entry['id']);
				} elseif ($current_time <= $entry['campaign_end_time']) {
					array_push($data,$entry['id']);
				}
		}
		
		return $data;
		
	}
	
	public function getCampaignDetail($campaign_id = null)
	{
		$this->load->driver('Streams');
		$data = array();
		if (! $campaign_id) return array();
		$campaign_detail = $this->streams->entries->get_entry($campaign_id, 'campaign', 'coupon');
		foreach ($campaign_detail as $key => $value) {
			$data[$key] = $value;
		}
		return $data;
	}
	
	public function getGroupDetail($group_id = null)
	{
		$this->load->driver('Streams');
		$data = array();
		if (! $group_id) return array();
		$group_detail = $this->streams->entries->get_entry($group_id, 'group', 'coupon');
		foreach ($group_detail as $key => $value) {
			$data[$key] = $value;
		}
		return $data;
	}
	
	public function getSettings($merchant_id = null)
	{
		$this->load->driver('Streams');
		if (!$merchant_id) return array();
		
		$params = array(
				'stream'    => 'settings',
    			'namespace' => 'coupon',
    			'where'		=> "`created_by` = '".$merchant_id."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		
		if ($entries['total']) {
			return $entries['entries'][0];
		} else {
			return array();
		}
	}
	
	public function generateCoupon($campaign_id = null, $profile_id = null)
	{
		$data =array();
		if ($campaign_id && $profile_id) {
			$campaign_detail = $this->pyrocache->model('coupon_m', 'getCampaignDetail', array($campaign_id), 7200);
			$code = $this->_genCode($campaign_detail['created_by']);

			//$campaign_detail['created_by']
			//Try to insert code to databases
			$entry_data = array(
			    'code' => $code,
			    'code_group'   => $campaign_detail['used_group'],
			    'owner' => $profile_id,
			    'merchant_id' => $campaign_detail['created_by'],
			);
			if ($this->streams->entries->insert_entry($entry_data, 'coupon', 'coupon')) {
				$group_detail = $this->pyrocache->model('coupon_m', 'getGroupDetail', array($campaign_detail['used_group']), 7200);
				$data = array(
					'code' => $code,
					'valid_to' => date("Y-m-d H:i:s", strtotime("+ " . $group_detail['valid_days'] . " days"))
				);
			}

		}
		return $data;
	}
	
	function _genCode($merchant_id = null)
	{
		$this->load->driver('Streams');
		$valid = false;
		while (! $valid) {
			$code = $this->_randomString(4);
			$params = array(
					'stream'    => 'coupon',
	    			'namespace' => 'coupon',
	    			'where'		=> "`code` = '".$code."' AND `created_by` = '".$merchant_id."'"
			);
			$entries = $this->streams->entries->get_entries($params);
			if (! $entries['total']) $valid = true;
		}
		return $code;
	}
	

	function _randomString($length, $keyspace = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
		    $str = '';
		    $max = mb_strlen($keyspace, '8bit') - 1;
		    for ($i = 0; $i < $length; ++$i) {
		        $str .= $keyspace[mt_rand(0, $max)];
		    }
		    return $str.mt_rand(0, 99);
	}
	
	public function sendEmail($mail_to = null ,$coupon = array(), $settings = array())
	{
		$ci =& get_instance();
		$ci->load->library('email');
		if($ci->email->valid_email($mail_to) && !empty($coupon) && !empty($settings)) {
			
			
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = $settings['smtp_host'];
			$config['smtp_port'] = $settings['smtp_port'];
			$config['smtp_user'] = $settings['smtp_username'];
			$config['smtp_pass'] = $settings['smtp_password'];
			$config['mailtype'] = 'html';
			$config['crlf']		= "\n";
			$config['newline'] = '\r\n';
			$config['charset']	= "utf-8";

			$ci->email->initialize($config);
			$this->email->set_newline("\r\n");
			$ci->email->clear(TRUE);
			$ci->email->to($mail_to);
			$ci->email->reply_to($settings['mail_from']['email_address']);
			$ci->email->from($settings['mail_from']['email_address']);
			$ci->email->subject($settings['email_subject']);

			
			$body = $settings['email_message']. '<br/><br/>- Coupon Code: <span style="font-size: 2.1em; color: blue;">'.$coupon['code'].'</span>';
			$body .= '<br/>- Valid to: ' . $coupon['valid_to'] . '<br/><br/><b><i>Best regards,</i></b>';

			$ci->email->message($body);	

			if ($ci->email->send()) {return true;} else {return false;}
		} else {
			return false;
		}
	}

}
