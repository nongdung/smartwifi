<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	www.your-site.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://www.codeigniter.com/user_guide/general/routing.html
*/
$route['coupon/admin/campaign(/:num)?']			= 'admin_campaign/index$1';
$route['coupon/admin/campaign(/:any)?']			= 'admin_campaign$1';

$route['coupon/admin/campaign-fields(/:num)?']			= 'admin_campaign_fields/index$1';
$route['coupon/admin/campaign-fields(/:any)?']			= 'admin_campaign_fields$1';

$route['coupon/admin/settings(/:num)?']			= 'admin_settings/index$1';
$route['coupon/admin/settings(/:any)?']			= 'admin_settings$1';

$route['coupon/admin/settings-fields(/:num)?']			= 'admin_settings_fields/index$1';
$route['coupon/admin/settings-fields(/:any)?']			= 'admin_settings_fields$1';

$route['coupon/admin/groups(/:num)?']			= 'admin_groups/index$1';
$route['coupon/admin/groups(/:any)?']			= 'admin_groups$1';

$route['coupon/admin/groups-fields(/:num)?']			= 'admin_groups_fields/index$1';
$route['coupon/admin/groups-fields(/:any)?']			= 'admin_groups_fields$1';

$route['coupon/admin/codes(/:num)?']			= 'admin_codes/index$1';
$route['coupon/admin/codes(/:any)?']			= 'admin_codes$1';

$route['coupon/admin/codes-fields(/:num)?']			= 'admin_codes_fields/index$1';
$route['coupon/admin/codes-fields(/:any)?']			= 'admin_codes_fields$1';

$route['coupon/admin(/:any)?']			= 'admin_codes/index$1';
