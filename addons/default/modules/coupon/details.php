<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Coupon extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Coupon'
			),
			'description' => array(
				'en' => 'Basic coupon management'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',
			'roles' => array(
                'staff','merchant','manager', 'admin'
            )		
		);
		
		$info['sections']['campaign'] = array(
					'name' 	=> 'coupon:campaign', // These are translated from your language file
					'uri' 	=> 'admin/coupon/campaign',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'global:add',
							'uri' 	=> 'admin/coupon/campaign/add',
							'class' => 'add'
							)
					)
		);
		$info['sections']['coupon'] = array(
					'name' 	=> 'coupon:coupons', // These are translated from your language file
					'uri' 	=> 'admin/coupon',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'global:add',
							'uri' 	=> 'admin/coupon/codes/add',
							'class' => 'add'
							)
					)
		);
		$info['sections']['group'] = array(
					'name' 	=> 'coupon:groups', // These are translated from your language file
					'uri' 	=> 'admin/coupon/groups',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'coupon:add_group',
							'uri' 	=> 'admin/coupon/groups/add',
							'class' => 'add'
							)
					)
		);
		$info['sections']['settings'] = array(
					'name' 	=> 'coupon:settings', // These are translated from your language file
					'uri' 	=> 'admin/coupon/settings',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'coupon:add_settings',
							'uri' 	=> 'admin/coupon/settings/add',
							'class' => 'add'
							)
					)
		);
		
		if (group_has_role('hotspot', 'admin')) {
			$info['sections']['groups_fields'] = array(
					'name' 	=> 'coupon:groups_fields', // These are translated from your language file
					'uri' 	=> 'admin/coupon/groups-fields',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'streams:add_field',
							'uri' 	=> 'admin/coupon/groups-fields/add',
							'class' => 'add'
							)
					)
			);
			$info['sections']['codes_fields'] = array(
					'name' 	=> 'coupon:codes_fields', // These are translated from your language file
					'uri' 	=> 'admin/coupon/codes-fields',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'streams:add_field',
							'uri' 	=> 'admin/coupon/codes-fields/add',
							'class' => 'add'
							)
					)
			);
			$info['sections']['settings_fields'] = array(
					'name' 	=> 'coupon:settings_fields', // These are translated from your language file
					'uri' 	=> 'admin/coupon/settings-fields',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'streams:add_field',
							'uri' 	=> 'admin/coupon/settings-fields/add',
							'class' => 'add'
							)
					)
			);
			$info['sections']['campaign_fields'] = array(
					'name' 	=> 'coupon:campaign_fields', // These are translated from your language file
					'uri' 	=> 'admin/coupon/campaign-fields',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'streams:add_field',
							'uri' 	=> 'admin/coupon/campaign-fields/add',
							'class' => 'add'
							)
					)
			);			
		}
								
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		
		$this->db->delete('settings', array('module' => 'coupon'));
		//add stream
		if ( ! $this->streams->streams->add_stream('Settings', 'settings', 'coupon', 'coupon_', null)) return false;
		if ( ! $group_stream_id = $this->streams->streams->add_stream('Coupon Group', 'group', 'coupon', 'coupon_', null)) return false;
		if ( ! $this->streams->streams->add_stream('Coupon codes', 'coupon', 'coupon', 'coupon_', null)) return false;
		if ( ! $this->streams->streams->add_stream('Coupon Campaign', 'campaign', 'coupon', 'coupon_', null)) return false;

		$user_profile_stream = $this->streams->streams->get_stream('profiles', 'husers');
		
		//Settings Fields
		$fields = array(
			array(
			    'namespace' => 'coupon',
				'assign' => 'settings',
                'name' => 'Mail From',
                'slug' => 'mail_from',
                'type' => 'email',
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'settings',
                'name' => 'Mail Reply',
                'slug' => 'mail_reply',
                'type' => 'email',
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'settings',
                'name' => 'SMTP Host',
                'slug' => 'smtp_host',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                //'title_column' => true,
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'settings',
                'name' => 'SMTP Port',
                'slug' => 'smtp_port',
                'type' => 'integer',
                'extra' => array('max_length' => 5, 'default_value' => 465),
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'settings',
                'name' => 'SMTP Username',
                'slug' => 'smtp_username',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'settings',
                'name' => 'SMTP Password',
                'slug' => 'smtp_password',
                'type' => 'encrypt',
                'extra' => array('hide_typing' => 'yes'),
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'settings',
                'name' => 'Email Subject',
                'slug' => 'email_subject',
                'type' => 'text',
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'settings',
                'name' => 'Email Message',
                'slug' => 'email_message',
                'type' => 'wysiwyg',
                'extra' => array('editor_type' => 'advanced'),
                'required' => true
            ),
            
			//Coupon Group fields
			array(
			    'namespace' => 'coupon',
				'assign' => 'group',
                'name' => 'Group Name',
                'slug' => 'group_name',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'title_column' => true,
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'group',
                'name' => 'Discount Value',
                'slug' => 'discount_value',
                'type' => 'text',
                'extra' => array('max_length' => 12),
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'group',
                'name' => 'Valid Days',
                'slug' => 'valid_days',
                'type' => 'integer',
                'extra' => array('max_length' => 5, 'default_value' => 30),
            ),
            
			//Coupon fields
            array(
			    'namespace' => 'coupon',
				'assign' => 'coupon',
                'name' => 'Code',
                'slug' => 'code',
                'type' => 'text',
                'extra' => array('max_length' => 16),
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'coupon',
                'name' => 'Group',
                'slug' => 'code_group',
                'type' => 'relationship',
                'extra' => array('choose_stream' => $group_stream_id),
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'coupon',
                'name' => 'Owner',
                'slug' => 'owner',
                'type' => 'relationship',
                'extra' => array('choose_stream' => $user_profile_stream->id),
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'coupon',
                'name' => 'Merchant ID',
                'slug' => 'merchant_id',
                'type' => 'integer',
                'extra' => array('default_value' => 1),
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'coupon',
                'name' => 'Status',
                'slug' => 'status',
                'type' => 'integer',
                'extra' => array('max_length' => 1, 'default_value' => 0),
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'coupon',
                'name' => 'Used Time',
                'slug' => 'used_time',
                'type' => 'datetime',
                'extra' => array('use_time' => 'yes', 'storage' => 'unix'),
            ),
		);
		if (! $this->streams->fields->add_fields($fields)) return false;
		
		$this->streams->streams->update_stream('coupon', 'coupon', array(
            'view_options' => array(
                'code',
                'code_group',
                'owner',               
                'created',
                'status',
                'used_time'
             )
        ));
		$this->streams->streams->update_stream('group', 'coupon', array(
            'view_options' => array(
                'group_name',
                'discount_value',               
                'valid_days'
             )
        ));
		
		//Campaign Fields
		$fields = array(
			array(
			    'namespace' => 'coupon',
				'assign' => 'campaign',
                'name' => 'Campaign Name',
                'slug' => 'campaign_name',
                'type' => 'text',
                'title_column' => true,
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'campaign',
                'name' => 'Used Group',
                'slug' => 'used_group',
                'type' => 'relationship',
                'extra' => array('choose_stream' => $group_stream_id),
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'campaign',
                'name' => 'Priority',
                'slug' => 'priority',
                'type' => 'integer',
                'extra' => array('max_length' => 2, 'default_value' => 1),
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'campaign',
                'name' => 'Coupon Limit',
                'slug' => 'coupon_limit',
                'type' => 'integer',
                'extra' => array('max_length' => 2, 'default_value' => 1),
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'campaign',
                'name' => 'Start Time',
                'slug' => 'campaign_start_time',
                'type' => 'datetime',
                'extra' => array('use_time' => 'yes', 'storage' => 'unix', 'input_type' => 'datepicker'),
                'required' => true
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'campaign',
                'name' => 'End Time',
                'slug' => 'campaign_end_time',
                'type' => 'datetime',
                'extra' => array('use_time' => 'yes', 'storage' => 'unix', 'input_type' => 'datepicker')
            ),
            array(
			    'namespace' => 'coupon',
				'assign' => 'campaign',
                'name' => 'Target',
                'slug' => 'target',
                'type' => 'text',
            ),
        );
		if (! $this->streams->fields->add_fields($fields)) return false;
		
		$this->streams->streams->update_stream('campaign', 'coupon', array(
            'view_options' => array(
                'campaign_name',
                'used_group',
                'campaign_start_time',
                'campaign_end_time'
             )
        ));

		return TRUE;
		
	}

	public function uninstall()
	{
		$this->load->driver('Streams');
		$this->streams->utilities->remove_namespace('coupon');
		$this->db->delete('settings', array('module' => 'coupon'));

		return TRUE;
	}


	public function upgrade($old_version)
	{
		$this->load->driver('Streams');
		 
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
	
	public function admin_menu(&$menu)
    {
    	if (group_has_role('hotspot', 'merchant')) {
	    	$menu['Coupon'] = array(
	    		'Campaigns' => 'admin/coupon/campaign',
	    		'Coupons' => 'admin/coupon/codes', 
	        	'Coupon Groups' => 'admin/coupon/groups',
	        	'Settings' => 'admin/coupon/settings', 
	        );
		}
		add_admin_menu_place('Coupon', 2);
	}

}
/* End of file details.php */
