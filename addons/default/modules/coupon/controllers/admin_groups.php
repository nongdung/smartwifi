<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_groups extends Admin_Controller
{
	protected $section = 'group';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('coupon');
		$this->load->driver('Streams');
		role_or_die('coupon', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all adverts
     */
    public function index()
    {    	
		$extra = array();
        $extra['title'] = lang('coupon:groups_list');

        $extra['buttons'] = array(
            array(
                'label' => lang('global:edit'),
                'url' => 'admin/coupon/groups/edit/-entry_id-'
            ),
            array(
                'label' => lang('global:delete'),
                'url' => 'admin/coupon/groups/-entry_id-',
                'confirm' => true
            )
        );

        $this->streams->cp->entries_table('group', 'coupon', 15, 'admin/coupon/groups/index', true, $extra);
    }
	
	public function add()
	{
		$extra = array(
		    'return'            => 'admin/coupon/groups',
		    'success_message'   => lang('coupon:submit_success'),
		    'failure_message'   => lang('coupon:submit_failure'),
		    'title'             => lang('coupon:add_group')
		);

		$this->streams->cp->entry_form('group', 'coupon', 'new', null, true, $extra);
	}

	public function delete($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/coupon/groups');			
		}	
		
		//ok, now we delete
		$this->streams->entries->delete_entry($id, 'group', 'coupon');
		$this->session->set_flashdata('success', "Deleted!");
		redirect('admin/coupon/groups');		
		
	}
	
	public function edit($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/coupon/groups');			
		}
		
		//ok, now we edit
		$extra = array(
		    'return'            => 'admin/coupon/groups',
		    'success_message'   => lang('coupon:submit_success'),
		    'failure_message'   => lang('coupon:submit_error'),
		    'title'             => lang('coupon:edit_group'),
		);
		$this->streams->cp->entry_form('group', 'coupon', 'edit', $id, true, $extra);
			
		
	}
}