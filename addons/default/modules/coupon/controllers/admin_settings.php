<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_settings extends Admin_Controller
{
	protected $section = 'settings';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('coupon');
		$this->load->driver('Streams');
		//$this->load->model('advertising_m');
		role_or_die('coupon', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all adverts
     */
    public function index()
    {    	
    	
		$extra = array();
        $extra['title'] = lang('coupon:settings');

        $extra['buttons'] = array(
            array(
                'label' => lang('global:edit'),
                'url' => 'admin/coupon/settings/edit/-entry_id-'
            ),
            array(
                'label' => lang('global:delete'),
                'url' => 'admin/coupon/settings/-entry_id-',
                'confirm' => true
            )
        );

        $this->streams->cp->entries_table('settings', 'coupon', 15, 'admin/coupon/settings/index', true, $extra);
    }
	
	public function add()
	{
		$extra = array(
		    'return'            => 'admin/coupon/settings',
		    'success_message'   => lang('coupon:submit_success'),
		    'failure_message'   => lang('coupon:submit_failure'),
		    'title'             => lang('coupon:add_settings')
		);

		$this->streams->cp->entry_form('settings', 'coupon', 'new', null, true, $extra);
	}

	public function delete($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/coupon/settings');			
		}	
		
		//ok, now we delete
		$this->streams->entries->delete_entry($id, 'settings', 'coupon');
		$this->session->set_flashdata('success', "Deleted!");
		redirect('admin/coupon/settings');		
		
	}
	
	public function edit($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/coupon/settings');			
		}
		//delete cached
		$this->pyrocache->delete_cache("coupon_m", "getSettings", array($this->current_user->id));
		//ok, now we edit
		$extra = array(
		    'return'            => 'admin/coupon/settings',
		    'success_message'   => lang('coupon:submit_success'),
		    'failure_message'   => lang('coupon:submit_error'),
		    'title'             => lang('coupon:edit_settings'),
		);
		$this->streams->cp->entry_form('settings', 'coupon', 'edit', $id, true, $extra);
			
		
	}
}