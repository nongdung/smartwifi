<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_campaign extends Admin_Controller
{
	protected $section = 'campaign';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('coupon');
		$this->load->driver('Streams');
		//$this->load->model('advertising_m');
		role_or_die('coupon', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all adverts
     */
    public function index()
    {    	
    	
		$extra = array();
        $extra['title'] = lang('coupon:campaign_list');

        $extra['buttons'] = array(
            array(
                'label' => lang('global:edit'),
                'url' => 'admin/coupon/campaign/edit/-entry_id-'
            ),
            array(
                'label' => lang('global:delete'),
                'url' => 'admin/coupon/campaign/-entry_id-',
                'confirm' => true
            )
        );

        $this->streams->cp->entries_table('campaign', 'coupon', 15, 'admin/coupon/campaign/index', true, $extra);
    }
	
	public function add()
	{
		$extra = array(
		    'return'            => 'admin/coupon/campaign',
		    'success_message'   => lang('coupon:submit_success'),
		    'failure_message'   => lang('coupon:submit_failure'),
		    'title'             => lang('coupon:add_campaign')
		);

		$this->streams->cp->entry_form('campaign', 'coupon', 'new', null, true, $extra);
	}

	public function delete($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/coupon');			
		}	
		
		//Delete cache
		$this->pyrocache->delete_cache("coupon_m", "hasValidCampaign", array( $this->current_user->id));
		
		//ok, now we delete
		$this->streams->entries->delete_entry($id, 'coupon', 'coupon');
		$this->session->set_flashdata('success', "Deleted!");
		redirect('admin/coupon/campaign');		
		
	}
	
	public function edit($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/coupon/campaign');			
		}
		
		//ok, now we edit
		$extra = array(
		    'return'            => 'admin/coupon/campaign',
		    'success_message'   => lang('coupon:submit_success'),
		    'failure_message'   => lang('coupon:submit_error'),
		    'title'             => lang('coupon:edit_campaign'),
		);
		
		//Delete cache
		$this->pyrocache->delete_cache("coupon_m", "hasValidCampaign", array( $this->current_user->id));
		
		$this->streams->cp->entry_form('campaign', 'coupon', 'edit', $id, true, $extra);
			
		
	}
}