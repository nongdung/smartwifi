<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	splash Module
 */
class Admin_campaign_fields extends Admin_Controller
{
	protected $section = 'campaign_fields';

	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->lang->load('coupon');
		$this->load->driver('Streams');
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');

	}

	/**
	 * List all items
	 */
	public function index()
	{
		$buttons = array(
			array(
				'url'		=> 'admin/coupon/campaign-fields/edit/-assign_id-', 
				'label'		=> $this->lang->line('global:edit')
			),
			array(
				'url'		=> 'admin/coupon/campaign-fields/delete/-assign_id-',
				'label'		=> $this->lang->line('global:delete'),
				'confirm'	=> true,
				'locked'	=> TRUE
			)
		);
		$this->streams->cp->assignments_table(
								'campaign',
								'coupon',
								25,
								'admin/coupon/campaign-fields/index',
								TRUE,
								array('buttons' => $buttons));
	}
	
	function add()
	{
		$extra['title'] 		= lang('streams:add_field');
		$extra['show_cancel'] 	= true;
		$extra['cancel_uri'] 	= 'admin/coupon/campaign-fields';

		$this->streams->cp->field_form('campaign', 'coupon', 'new', 'admin/coupon/campaign-fields', null, array(), true, $extra);
	}
	
	function delete($assign_id)
	{
		if ( ! $assign_id )
		{
			show_error(lang('streams:cannot_find_assign'));
		}
	
		// Tear down the assignment
		if ( ! $this->streams->cp->teardown_assignment_field($assign_id))
		{
		    $this->session->set_flashdata('notice', lang('streams:field_delete_error'));
		}
		else
		{
		    $this->session->set_flashdata('success', lang('streams:field_delete_success'));			
		}
	
		redirect('admin/coupon/campaign-fields');
	}


	function edit($assign_id)
	{
		if ( ! $assign_id )
		{
			show_error(lang('streams:cannot_find_assign'));
		}

		$extra = array(
			'title'			=> lang('streams:edit_field'),
			'show_cancel'	=> true,
			'cancel_uri'	=> 'admin/coupon/campaign-fields'
		);

		$this->streams->cp->field_form('campaign', 'coupon', 'edit', 'admin/coupon/campaign-fields', $assign_id, array(), true, $extra);
	}

}
