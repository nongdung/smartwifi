<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		NgocDung Nong - SmartWifi VN Dev Team
 * @website		http://nongdung.com
 * @package 	SmartWifi
 * @subpackage 	splash Module
 */
class Apis extends Public_Controller
{
	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->lang->load('coupon');
		if ($this->config->item('sess_use_redis')) {
			$this->load->library('splash/sw_redis');
		}
	}

	public function index()
	{		
		
		$this->load->model('coupon_m');
		
	}
	public function check()
	{
		$this->load->model('husers/husers_m');
		$this->load->model('coupon/coupon_m');
		$this->load->model('location/location_m');
				
		$location_detail = $this->pyrocache->model('location_m', 'get_detail', array($this->session->userdata('location_id')), 7200);

		$data = array();
		if ($this->input->post('profile_id')) {
			//Check if this profile would have a coupon?
			$profile = $this->pyrocache->model('husers_m', 'getClientInfoFromId', array($this->input->post('profile_id')), 7200);
			$CouponCampaignList = $this->pyrocache->model('coupon_m', 'hasValidCampaign',array($location_detail['created_by']['user_id'], $location_detail['id']), 7200);
			
			$current_time = time();
			$priority = 0;
			foreach ($CouponCampaignList as $campaign_id) {
				$campaign_detail = $this->pyrocache->model('coupon_m', 'getCampaignDetail', array($campaign_id), 7200);
				//$target = json_decode($campaign_detail['target'], true);
				
				if ($current_time >= $campaign_detail['campaign_start_time'] && (($current_time <= $campaign_detail['campaign_end_time']) || !$campaign_detail['campaign_end_time'])) {
						
					//Gender check
					$gender_check = false;
					if ($campaign_detail['target_gender']['key'] == '2') {
						$gender_check = true;
					} elseif ($profile['gender'] == $campaign_detail['target_gender']['key']) {
						$gender_check = true;
					}
					
					//Facebook check
					$fb_check = false;
					if ($campaign_detail['target_facebook']['key'] == '2') {
						$fb_check = true;
					} elseif ((($campaign_detail['target_facebook']['key'] == '0') && $this->session->userdata('fb_share_checkin'))
							|| (($campaign_detail['target_facebook']['key'] == '1') && $this->session->userdata('fb_login'))) {
						$fb_check = true;
					}
					
					//Check Limit
					/*
					 * Todo: Add index to created_by, owner, created . Status: Not yet
					 * Todo: Cache coupon query. Status: Not yet
					 */
					$limit_check = false;
					if ($gender_check && $fb_check) {
						$this->load->driver('Streams');
						$params = array(
							'stream'    => 'coupon',
				    		'namespace' => 'coupon',
				    		'where'		=> "`merchant_id` = '".$campaign_detail['created_by']."' AND `owner` = '".$this->input->post('profile_id')."' AND DATE(`created`) = DATE(NOW())"
						);
						$entries = $this->streams->entries->get_entries($params);
						$limit_check = ! $entries['total'];
					}
						
					if ($gender_check && $fb_check && $limit_check && ($campaign_detail['priority'] > $priority)) {
						$priority = $campaign_detail['priority'];
						$group_detail = $this->pyrocache->model('coupon_m', 'getGroupDetail', array($campaign_detail['used_group']), 7200);
						$data['campaign_id'] = $campaign_id;
						$data['value'] = $group_detail['discount_value'];
						$data['email'] = $profile['email']['email_address'];
						$data['campaign_text'] = $campaign_detail['campaign_text'];
					}
				}
			}
			
		} 
		
		//$data = $location_detail;
		header_remove("Pragma");
		$this->output->set_header("Cache-Control: max-age=600, public");	
			
		$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data));
	}

	public function getCoupon()
	{
		if ($this->input->post('email') && $this->input->post('campaign_id') && $this->input->post('profile_id')) {
			$this->load->model('coupon_m');
			$mail_settings = $this->pyrocache->model('coupon_m', 'getSettings', array($this->session->userdata('merchant_id')), 7200);
			
			//Generate a coupon
			$coupon = array();
			while (empty($coupon)) {
				$coupon = $this->coupon_m->generateCoupon(intval($this->input->post('campaign_id')), intval($this->input->post('profile_id')));
			}
			
			//Now we send email to customer
			$result = $this->coupon_m->sendEmail($this->input->post('email'), $coupon, $mail_settings);
			
			if ($result) $data['status'] = 'success'; else $data['status'] = 'failed';
			$data['data'] = $coupon;
		} else {
			$data['status'] = 'failed';
		}
		
		$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data));
	}
}
