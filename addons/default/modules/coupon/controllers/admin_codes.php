<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_codes extends Admin_Controller
{
	protected $section = 'coupon';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('coupon');
		$this->load->driver('Streams');
		//$this->load->model('advertising_m');
		role_or_die('coupon', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all adverts
     */
    public function index()
    {    	
    	
		$extra = array();
        $extra['title'] = lang('coupon:codes_list');

        $extra['buttons'] = array(
            array(
                'label' => lang('global:edit'),
                'url' => 'admin/coupon/codes/edit/-entry_id-'
            ),
            array(
                'label' => lang('global:delete'),
                'url' => 'admin/coupon/codes/delete/-entry_id-',
                'confirm' => true
            )
        );
		

        $this->streams->cp->entries_table('coupon', 'coupon', 15, 'admin/coupon/codes/index', true, $extra);
    }
	
	public function add()
	{
		$extra = array(
		    'return'            => 'admin/coupon',
		    'success_message'   => lang('coupon:submit_success'),
		    'failure_message'   => lang('coupon:submit_failure'),
		    'title'             => lang('coupon:add_coupon')
		);

		$this->streams->cp->entry_form('coupon', 'coupon', 'new', null, true, $extra);
	}

	public function delete($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/coupon');			
		}	
		
		//ok, now we delete
		$this->streams->entries->delete_entry($id, 'coupon', 'coupon');
		$this->session->set_flashdata('success', "Deleted!");
		redirect('admin/coupon');		
		
	}
	
	public function edit($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/coupon');			
		}
		
		//ok, now we edit
		$extra = array(
		    'return'            => 'admin/coupon',
		    'success_message'   => lang('coupon:submit_success'),
		    'failure_message'   => lang('coupon:submit_error'),
		    'title'             => lang('coupon:edit_coupon'),
		);
		$this->streams->cp->entry_form('coupon', 'coupon', 'edit', $id, true, $extra);
			
		
	}
}