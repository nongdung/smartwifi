<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Blog Permissions
$lang['coupon:role_staff']	= 'Staff';
$lang['coupon:role_merchant']	= 'Merchant';
$lang['coupon:role_manager']     = 'Manager';
$lang['coupon:role_admin']     = 'Admin';