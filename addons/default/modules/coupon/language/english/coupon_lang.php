<?php
//messages
$lang['coupon:filters']			=	'Filters';
$lang['coupon:coupon']			=	'Coupon';
$lang['coupon:add_coupon']			=	'Add Coupon';
$lang['coupon:edit_coupon']			=	'Edit Coupon';
$lang['coupon:coupons']			=	'Coupons';

$lang['coupon:groups']			=	'Groups';
$lang['coupon:groups_list']			=	'Groups List';
$lang['coupon:add_group']			=	'Add Group';
$lang['coupon:edit_group']			=	'Edit Group';

$lang['coupon:settings']			=	'Settings';
$lang['coupon:add_settings']			=	'Add Settings';
$lang['coupon:edit_settings']			=	'Edit Settings';

$lang['coupon:codes_fields']			=	'Codes Fields';
$lang['coupon:groups_fields']			=	'Groups Fields';
$lang['coupon:settings_fields']			=	'Settings Fields';
$lang['coupon:campaign_fields']			=	'Campaign Fields';

$lang['coupon:submit_success']			=	'Submit Success';
$lang['coupon:submit_failure']			=	'Submit Failed';

$lang['coupon:codes_list']			=	'Codes List';

$lang['coupon:campaign']			=	'Campaign';
$lang['coupon:campaign_list']			=	'Campaign List';
$lang['coupon:add_campaign']			=	'Add Campaign';
$lang['coupon:edit_campaign']			=	'Edit Campaign';
?>