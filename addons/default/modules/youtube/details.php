<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Youtube extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Youtube Login'
			),
			'description' => array(
				'en' => 'Wifi Login with Youtube'
			),
			'frontend' => FALSE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',		
		);
		
		if (group_has_role('hotspot', 'merchant')) {
			$info['sections']['youtube'] = array(
					'name' 	=> 'youtube:section_name', // These are translated from your language file
					'uri' 	=> 'admin/youtube'
			);			
		}							
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		$this->load->language('youtube/youtube');
		$this->db->delete('settings', array('module' => 'youtube'));
		//add stream
		if ( ! $this->streams->streams->add_stream('Youtube Settings', 'settings', 'youtube', 'yt_', null)) return false;
		
		//Get account stream ID	
		$account_stream = $this->streams->streams->get_stream('accounts', 'wifi_accounts');
		$loc_stream = $this->streams->streams->get_stream('list', 'location');
			
		$choice = 'Enable
		Disable';
		$fields = array(
			array(
                'name' => 'Status',
                'slug' => 'status',
                'namespace' => 'youtube',
                'type' => 'choice',
                'extra'         => array('default_value' => 'Enable','choice_data' => $choice, 'choice_type' => 'dropdown'),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Wifi Login Account',
                'slug' => 'wifi_login_account',
                'namespace' => 'youtube',
                'type' => 'relationship',
                'extra' => array('choose_stream' => $account_stream->id),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Position',
                'slug' => 'position',
                'namespace' => 'youtube',
                'type' => 'integer',
                'extra' => array('max_length' => 1,'default_value' => 1),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Location ID',
                'slug' => 'loc_id',
                'namespace' => 'youtube',
                'type' => 'relationship',
                'extra' => array('choose_stream' => $loc_stream->id),
                'assign' => 'settings',
                'unique' => TRUE,
                'required' => true
            ),
            array(
                'name' => 'Text',
                'slug' => 'text',
                'namespace' => 'youtube',
                'type' => 'wysiwyg',
                'extra' => array('editor_type' => "simple"),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Video ID',
                'slug' => 'video_id',
                'namespace' => 'youtube',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'settings',
            ),
		);

        $this->streams->fields->add_fields($fields);		

        $this->streams->streams->update_stream('settings', 'google_js', array(
            'view_options' => array(
            	'status',
                'wifi_login_account',
                'position',
                'video_id'
            )
        ));
		
		$entry_data = array(
		    'mod_name' => "Youtube",
		    'mod_slug'   => "youtube",
		    'mod_desc'   => "Wifi Login with Youtube",		    
		);
		$this->streams->entries->insert_entry($entry_data, 'modules', 'login_modules');
		
		return TRUE;
		
	}

	public function uninstall()
	{
		$this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
		$this->streams->utilities->remove_namespace('youtube');
		$this->db->delete('settings', array('module' => 'youtube'));
		
		$params = array(
		    'stream'    => 'modules',
		    'namespace' => 'login_modules',
		    'where' 	=> "mod_slug = 'youtube'"
		);		
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			foreach ($entries['entries'] as $entry) {
				$this->streams->entries->delete_entry($entry["id"], "modules", "login_modules");
			}
		}
		
		return TRUE;
	}


	public function upgrade($old_version)
	{
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
    
    
    public function admin_menu(&$menu)
    {	

	}
}
/* End of file details.php */
