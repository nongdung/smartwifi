<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Youtube_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		
	}
	
	public function get_settings($location_id)
	{
		$this->load->driver('Streams');
		
		$params = array(
		    'stream'    => 'settings',
		    'namespace' => 'youtube',
		    'where'		=> "`loc_id` = '".$location_id."'"
		);
		
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			return $entries['entries'][0];
		} else {
			return array();
		}
		
		
	}
	
	public function get_html($location_id, $device_type, $settings = null, $userdata_key = null)
	{
		$ci = & get_instance();
		$form = $ci->load->view('youtube/loginform', array("settings" => $settings, "device_type" => $device_type), true);

		$data = array(
			'html' => $form,
			'link' => "",
			'caption' => ""
		);		
		return $data;
	}

}
