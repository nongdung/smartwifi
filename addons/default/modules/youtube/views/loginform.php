<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

<div id="youtube-login-form" class="youtube-login-form wifi-login-form">
	<div class="embed-responsive embed-responsive-16by9">
	<div id="player"></div>
	</div>
	<?php if ($settings['text'] && $settings['text'] != "") echo $settings['text']."<br/>";?>
	<div id="yt-msg"></div>
	
</div>

<script>
	var swYoutube = {};
      var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '<?php if ($device_type == "desktop") echo "390"; else echo "320";?>',
          width: '<?php if ($device_type == "desktop") echo "640"; else echo "480";?>',
          videoId: '<?php echo $settings['video_id']; ?>',
		  playerVars: {'controls':0, 'disablekb': 1, 'modestbranding':1,'rel':0,'autoplay':0},
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
      }

      function onPlayerReady(event) {
        //event.target.playVideo();
	
      }

      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.ENDED && !done) {
        	console.log("Bat dau login wifi");
			//swYoutube();
			swYoutube.Connect();
        	done = true;
        }
      }
      
	swYoutube.Connect = function(){
		swController.onUpdate = swYoutube.updateUI;	
		if ( swController.clientState == 0 ) {
			if (typeof woopraTracker === 'object') {
				woopra.track("Youtube Login", {
	        			Type: "Attempt"
				});
			}
			
			//Piwik tracking
			if (typeof _paq === 'object') {
				_paq.push(['trackEvent', 'Wifi', 'Login Attempt', 'Youtube']);
			}

			setTimeout('swController.logon(\'<?php echo $settings['wifi_login_account']['login_username'];?>\',\'<?php echo $settings['wifi_login_account']['login_password'];?>\')', 500);
			setTimeout(function() {
					var yt_result = {action: 'youtube' };
					if (swController.clientState == 1){
						yt_result.action_result = 1;		
						if (typeof woopraTracker === 'object') {
							woopra.track("Youtube Login", {
	        						Type: "Success"
							});
						}
	
					} else {
						yt_result.action_result = 0;
						if (typeof woopraTracker === 'object') {
							woopra.track("Youtube Login", {
	        						Type: "Failed"
							});
						}
					}
					
					SmartWifi.raw_log(yt_result);
					
					window.scrollTo(0,0);
			},1500);

		} else {
			$('#yt-msg').html('<div class="text-danger">Error! Wifi is not ready!</div>');
			return false;
		}
	};
	
	swYoutube.updateUI = function(){
		if ( swController.clientState == 1 ) {
			if (window.swLocation && window.swLocation.redirect_url && (window.swLocation.redirect_url != "")) {
				window.location.assign(window.swLocation.redirect_url);
			} else if (window.swLocation && window.swLocation.location_id && (window.swLocation.location_id != "")){
				if (window.queryObj.device_type == 'desktop') {
					window.location.assign("/splash/success/" + window.swLocation.location_id + "/desktop");
				} else{
					window.location.assign("/splash/success/" + window.swLocation.location_id);
				};
			} else {
				$('#yt-msg').html('<div class="text-info">Wifi Login success! Thanks for watching!</div>');
			};
		}
	};
</script>