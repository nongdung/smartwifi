<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('youtube:settings'); ?></h4>
</section>

<section class="item">
	
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		<div class="one_full">
			<?php 
				if ($entries['total'] > 0){
					echo form_hidden("id",$entries['entries'][0]['id']);	
				}
				echo form_hidden("loc_id",$loc_id);				
			?>
			
		
			<div class="form_inputs">
				<ul>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="status">Status</label>
						<div class="input">
							<?php
							$options = array('Enable'  => 'Enable', 'Disable'    => 'Disable');							
							echo form_dropdown('status', $options, isset($entries['entries'][0]['status'])?$entries['entries'][0]['status']['val']:'Disable');
							?>
						</div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="wifi_login_account">Wifi Login Account<span>*</span></label>
						<div class="input">
							<?php
							echo form_dropdown('wifi_login_account', $accounts_list, isset($entries['entries'][0]['wifi_login_account']['id'])?$entries['entries'][0]['wifi_login_account']['id']:null);
							?>
						</div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="position">Position<span>*</span></label>
						<div class="input"><?php echo form_input('position', isset($entries['entries'][0]['position'])?$entries['entries'][0]['position']:1); ?></div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="text">Text</label>
						<div class="input"><?php echo form_textarea('text', isset($entries['entries'][0]['text'])?$entries['entries'][0]['text']:"",'class="wysiwyg-simple one-half"'); ?></div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="video_id">Video ID<span>*</span></label>
						<div class="input"><?php echo form_input('video_id', isset($entries['entries'][0]['video_id'])?$entries['entries'][0]['video_id']:""); ?></div>
					</li>
						
				</ul>
			</div>
		</div>
		<div class="one_full">
			<div class="buttons">
				<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save') )); ?>
				
				<?php echo anchor("admin/splash/methods","Cancel",'class = "btn gray cancel"');?>
			</div>
		</div>
		<?php echo form_close(); ?>
</section>