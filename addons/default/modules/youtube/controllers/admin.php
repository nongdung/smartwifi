<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin extends Admin_Controller
{
	protected $section = 'youtube';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('youtube');
		$this->load->driver('Streams');
    }

    /**
     * List all billing plans
     */
    public function index()
    {
    	
		redirect('admin/splash/methods');
		
    }
    

	public function settings($loc_id ='')
	{		
		//Check loc_id
		$loc_detail = $this->streams->entries->get_entry($loc_id,'list','location');
		if (!$loc_detail && ($loc_detail->created_by_user_id <> $this->current_user->id)) {
			$this->session->set_flashdata('error', "This location is not yours");
			redirect('admin/location');
		}
		
		$validation_rule = array(
            array(
                'field' => 'position',
                'label' => 'Position',
                'rules' => 'trim|max_length[1]|is_natural_no_zero|required'
            ),
            array(
                'field' => 'text',
                'label' => 'Text',
                'rules' => 'trim|xss_clean'
            ),
            array(
                'field' => 'wifi_login_account',
                'label' => 'Wifi Login Account',
                'rules' => 'required'
            ),
            array(
                'field' => 'video_id',
                'label' => 'Video ID',
                'rules' => 'trim|max_length[64]'
            ),
            array(
                'field' => 'loc_id',
                'label' => 'Location ID',
                'rules' => 'required'
            )
		);
		
		$this->form_validation->set_rules($validation_rule);
		
		if ($this->form_validation->run()){
			if ($this->input->post("id")) {
				//Update existing record
				$entry_data = array(
					'status' => $this->input->post("status"),
				    'wifi_login_account' => $this->input->post("wifi_login_account"),
				    'position'   => $this->input->post("position"),				   
				    'text'   => $this->input->post("text"),
				    'video_id'   => $this->input->post("video_id")
				);
				$this->streams->entries->update_entry($this->input->post("id"),$entry_data, 'settings', 'youtube');
			} else {
				//insert new record
				$entry_data = array(
					'status' => $this->input->post("status"),
				    'wifi_login_account' => $this->input->post("wifi_login_account"),
				    'position'   => $this->input->post("position"),
					'loc_id'   => $this->input->post("loc_id"),
					'text'   => $this->input->post("text")
				);
				$this->streams->entries->insert_entry($entry_data, 'settings', 'youtube');
			}
			
			//Delete cache file
			$this->pyrocache->delete_cache("youtube_m", "get_settings", array($this->input->post("loc_id")));
			$this->session->set_flashdata('success', "Updated");
			redirect('admin/splash/methods');
			
		}
		
		$params = array(
		    'stream'    => 'settings',
		    'namespace' => 'youtube',
		    'where'		=> "`loc_id` ='".$loc_id."'"
		);
		
		$entries = $this->streams->entries->get_entries($params);

		//Get wifi login accounts
		$params = array(
		    'stream'    => 'accounts',
		    'namespace' => 'wifi_accounts',
		);
		$accounts = $this->streams->entries->get_entries($params);
		$accounts_list = array(null => "-----");
		foreach ($accounts['entries'] as $account) {
			$accounts_list[$account['id']] = $account['account_name'];
		}
		//print_r($accounts_list);exit;
		
		$this->template
				->title($this->module_details['name'], lang('youtube:settings'))
				->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
				->set('entries',$entries)
				->set('loc_id',$loc_id)
				->set('accounts_list',$accounts_list)
				->build('admin/settings');
	}

}