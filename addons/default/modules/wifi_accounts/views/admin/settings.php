<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('facebook:settings'); ?></h4>
</section>

<section class="item">
	
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		<div class="one_quarter">
			<?php 
				if ($entries['total'] > 0){
					echo form_hidden("id",$entries['entries'][0]['id']);	
				}				
			?>
		
			<div class="form_inputs">
				<ul>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="page_id">Facebook Page ID</label>
						<div class="input"><?php echo form_input('page_id', isset($entries['entries'][0]['page_id'])?$entries['entries'][0]['page_id']:''); ?></div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="username">Username<span>*</span><i>&nbsp;(for wifi login)</i></label>
						<div class="input"><?php echo form_input('username', isset($entries['entries'][0]['username'])?$entries['entries'][0]['username']:'facebook@smartwifi.vn'); ?></div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="password">Password<span>*</span><i>&nbsp;(for wifi login)</i></label>
						<div class="input"><?php echo form_input('password', isset($entries['entries'][0]['password'])?$entries['entries'][0]['password']:'facebook'); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="btn_style">Login Style<span>*</span></label>
						<div class="input">
							<?php
							$options = array('one_third'  => '1/3 Width', 'one_half'    => '1/2 Width', 'fullwidth' => 'Full Width');							
							echo form_dropdown('btn_style', $options, isset($entries['entries'][0]['btn_style'])?$entries['entries'][0]['btn_style']:'one_half');
							?>
						</div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="position">Position<span>*</span></label>
						<div class="input"><?php echo form_input('position', isset($entries['entries'][0]['position'])?$entries['entries'][0]['position']:1); ?></div>
					</li>
						
				</ul>
			</div>
		</div>
		
		<div class="one_quarter">
			<div class="form_inputs">
				<ul>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="sharing">Sharing<span>*</span></label>
						<div class="input">
							<?php
							$options = array('Enable'  => 'Enable', 'Disable'    => 'Disable');							
							echo form_dropdown('sharing', $options, isset($entries['entries'][0]['sharing'])?$entries['entries'][0]['sharing']['val']:'Disable');
							?>
						</div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="post_name">Post Name</label>
						<div class="input"><?php echo form_input('post_name', isset($entries['entries'][0]['post_name'])?$entries['entries'][0]['post_name']:''); ?></div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="post_link">Link to Share</label>
						<div class="input"><?php echo form_input('post_link', isset($entries['entries'][0]['post_link'])?$entries['entries'][0]['post_link']:''); ?></div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="post_caption">Post Caption</label>
						<div class="input"><?php echo form_input('post_caption', isset($entries['entries'][0]['post_caption'])?$entries['entries'][0]['post_caption']:''); ?></div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="post_desc">Post Description</label>
						<div class="input"><?php echo form_textarea('post_desc', isset($entries['entries'][0]['post_desc'])?$entries['entries'][0]['post_desc']:'','style="width:100%; height:60px;"'); ?></div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="post_picture">Post Picture Link</label>
						<div class="input"><?php echo form_input('post_picture', isset($entries['entries'][0]['post_picture'])?$entries['entries'][0]['post_picture']:''); ?></div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="post_location">Post Location<i>&nbsp;(for check-in only)</i></label>
						<div class="input"><?php echo form_input('post_location', isset($entries['entries'][0]['post_location'])?$entries['entries'][0]['post_location']:''); ?></div>
					</li>					

				</ul>
			</div>
		</div>
		
		<div class="one_half" style="padding-left: 15px;">
			<h3>The following is what displayed on customer's timeline when you enable sharing:</h3>
			<?php print_r($entries) ?>
		</div>
		
		<div class="one_full">
			<div class="buttons">
				<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
			</div>
		</div>
		<?php echo form_close(); ?>



</section>