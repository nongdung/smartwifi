<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin extends Admin_Controller
{
	protected $section = 'login_accounts';
    public function __construct()
    {
        parent::__construct();

		$this->load->driver('Streams');
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all billing plans
     */
    public function index()
    {		
		$extra = array();
        $extra['title'] = 'Account List';

        $extra['buttons'] = array(
            array(
                'label' => lang('global:edit'),
                'url' => 'admin/wifi_accounts/edit/-entry_id-'
            ),
            array(
                'label' => lang('global:delete'),
                'url' => 'admin/wifi_accounts/delete/-entry_id-',
                'confirm' => true
            )
        );
		//print_r($this->streams->streams->get_stream('accounts', 'hotspot_core'));exit;
        $this->streams->cp->entries_table('accounts', 'wifi_accounts', 15, 'admin/hotspot/wifi_accounts/index', true, $extra);
    }
    
    /**
     * Create a billingplan
     */
    public function delete($id = null)
    {
    	$this->streams->entries->delete_entry($id, 'accounts', 'wifi_accounts');
		$this->session->set_flashdata('success', "Deleted!");
		redirect('admin/wifi_accounts');	      
    }
	
	public function add()
	{
		$extra = array(
		    'return'            => 'admin/wifi_accounts',
		    'success_message'   => "Account added",
		    'failure_message'   => "Fail to add account!",
		    'title'             => 'Add login accounts'
		);
		
		$this->streams->cp->entry_form('accounts', 'wifi_accounts', 'new', null, true, $extra, $skips = array());
	}
	
	public function edit($id = null)
	{
		$extra = array(
		    'return'            => 'admin/wifi_accounts',
		    'success_message'   => "Account editted",
		    'failure_message'   => "Fail to edit account!",
		    'title'             => 'Edit login accounts'
		);
		
		$this->streams->cp->entry_form('accounts', 'wifi_accounts', 'edit', $id, true, $extra, $skips = array());
	}

}