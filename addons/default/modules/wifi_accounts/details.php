<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Wifi_accounts extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Wifi Login Accounts'
			),
			'description' => array(
				'en' => 'Define Hotspot Login Accounts'
			),
			'frontend' => FALSE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',		
		);
		if (group_has_role('hotspot', 'admin')) {
			$info['sections']['login_accounts'] = array(
					'name' 	=> 'Accounts', // These are translated from your language file
					'uri' 	=> 'admin/wifi_accounts',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'global:add',
							'uri' 	=> 'admin/wifi_accounts/add',
							'class' => 'add'
							)
					)
			);			
		}						
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		$this->db->delete('settings', array('module' => 'wifi_accounts'));
		//add stream
		if ( ! $this->streams->streams->add_stream('Hotspot Login Accounts', 'accounts', 'wifi_accounts', 'hcore_', null)) return false;		
		$fields = array(
            array(
                'name' => 'Account Name',
                'slug' => 'account_name',
                'namespace' => 'wifi_accounts',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'accounts',
                'title_column' => true,
                'required' => true              
            ),
            array(
                'name' => 'Login Username',
                'slug' => 'login_username',
                'namespace' => 'wifi_accounts',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'accounts',
                'required' => true
            ),
            array(
                'name' => 'Login Password',
                'slug' => 'login_password',
                'namespace' => 'wifi_accounts',
                'type' => 'text',
                'extra' => array('max_length' => 32),
                'assign' => 'accounts',
                'required' => true
            )
		);

        $this->streams->fields->add_fields($fields);

        $this->streams->streams->update_stream('accounts', 'wifi_accounts', array(
            'view_options' => array(
                'account_name',
                'login_username',
                'login_password'
            )
        ));
		return TRUE;
		
	}

	public function uninstall()
	{
		$this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
		$this->streams->utilities->remove_namespace('wifi_accounts');
		$this->db->delete('settings', array('module' => 'wifi_accounts'));
		
		return TRUE;
	}


	public function upgrade($old_version)
	{
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
    
    
    public function admin_menu(&$menu)
    {	
		if (group_has_role('hotspot', 'admin')) {
			$menu['Admin']['Login Accounts']   = 'admin/wifi_accounts';
		}
	}
}
/* End of file details.php */
