<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<fieldset id="filters">
	<legend><?php echo lang('global:filters') ?></legend>

	<?php echo form_open('', '', array('f_module' => $module_details['slug'])) ?>
		<ul>
			<li class="">
        		<label for="f_loc_id">Location</label>
        		<?php echo form_input('f_loc_id', '', 'style="width: 55%;"') ?>
    		</li>
			<li class="">
        		<label for="f_version">Firmware Version</label>
        		<?php echo form_input('f_version', '', 'style="width: 55%;"') ?>
    		</li>
			<li class="">
        		<label for="h_version">Firmware Version</label>
        		<?php echo form_input('h_version', '', 'style="width: 55%;"') ?>
    		</li>


			<li class="">
				<?php echo anchor(current_url() . '#', lang('buttons:cancel'), 'class="cancel"') ?>
			</li>
		</ul>
	<?php echo form_close() ?>
</fieldset>
