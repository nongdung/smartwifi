<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4>Account Information</h4>
</section>

<section class="item">
    <div class="content">
    	<table class="table-list" border="0" cellspacing="0">
    		<tr>
    			<td style="width:20%;">UserID</td>
    			<td style="width:80%;"><?php echo $this->current_user->id;?></td>
    		</tr>
    		<tr>
    			<td style="width:20%;">Username</td>
    			<td style="width:80%;"><?php echo $this->current_user->username;?></td>
    		</tr>
    		<tr>
    			<td style="width:20%;">Name</td>
    			<td style="width:80%;"><?php echo $this->current_user->first_name." ".$this->current_user->last_name;?></td>
    		</tr>
    		<tr>
    			<td style="width:20%;">Phone number</td>
    			<td style="width:80%;"><?php if (isset($detail[0]['merchant_phone'])) {echo $detail[0]['merchant_phone'];}?></td>
    		</tr>
    		<tr>
    			<td style="width:20%;">Current Cash</td>
    			<td style="width:80%;"><?php if (isset($detail[0]['current_cash'])) {echo $detail[0]['current_cash'];}?></td>
    		</tr>
    		<tr>
    			<td style="width:20%;">Service Level</td>
    			<td style="width:80%;"><?php if (isset($detail[0]['service_level'])) { echo $detail[0]['service_level'];}?></td>
    		</tr>
    		<tr>
    			<td style="width:20%;">Bonus Point</td>
    			<td style="width:80%;"><?php if (isset($detail[0]['bonus_point'])) {echo $detail[0]['bonus_point'];}?></td>
    		</tr>
    		    		<tr>
    			<td style="width:20%;">Ads remaining</td>
    			<td style="width:80%;"><?php if (isset($detail[0]['remain_ads'])) {echo $detail[0]['remain_ads'];}?></td>
    		</tr>
    	</table>
    </div>
</section>


<script type="text/javascript">
	var attempt =[];
	var success = [];
	var myfailed = [];
	$(document).ready(function(){

                $.ajax({
                    url: 'http://analytics.wifun.vn/index.php?module=API&method=Events.getAction&idSite=1&period=day&date=last10&format=json&token_auth=c975cf8316edde7ac3106d0ad031099c&segment=eventCategory==Hotspot+Login;customVariablePageName1==MerchantID;customVariablePageValue1==<?php echo $this->current_user->id;?>',
                    dataType: 'jsonp',
                    success: function(dataWeGotViaJsonp){                         
                      $.each( dataWeGotViaJsonp, function( key, val ) {
                      	
                      	var mydate = new Date(key).getTime();
                      	
                      	if (val.length > 0) {
	                      	var i;
	                      	var attempt_flag = false;
	                      	var success_flag = false;
	                      	var failed_flag = false;
	                      	var attempt_event = 0;
	                      	var success_event = 0;
	                      	var failed_event = 0;
							for (i = 0; i < val.length; ++i) {
							    switch (val[i].label) {
							    	case "Attempt":
							    		attempt_flag = true;
							    		attempt_event = val[i].nb_events;							    		
							    		break;
							    	case "Success":
							    		success_flag = true;
							    		success_event = val[i].nb_events;
							    		break;
							    	case "Failed":
							    		failed_flag = true;
							    		failed_event = val[i].nb_events;
							    		break;
							    }
							}
							attempt.push([mydate, attempt_event]);
							success.push([mydate, success_event]);
							myfailed.push([mydate, failed_event]);
						} else {
							var temp = [mydate, 0];
							attempt.push(temp);
							success.push(temp);
							myfailed.push(temp);
						}
                      						    
					  });

					  $(function($) {
					
							var buildGraph = function() {
								$.plot($('#analytics'), [{ label: 'Attempt', data: attempt },{ label: 'Success', data: success },{ label: 'Failed', data: myfailed }], {
									lines: { show: true },
									points: { show: true },
									grid: { hoverable: true, backgroundColor: '#fefefe' },
									series: {
										lines: { show: true, lineWidth: 1 },
										shadowSize: 0
									},
									xaxis: { mode: "time" },
									yaxis: { min: 0},
									selection: { mode: "x" }
								});
							}
							// create the analytics graph when the page loads
							buildGraph();
					
							// re-create the analytics graph on window resize
							$(window).resize(function(){
								buildGraph();
							});
							
							function showTooltip(x, y, contents) {
								$('<div id="tooltip">' + contents + '</div>').css( {
									position: 'absolute',
									display: 'none',
									top: y + 5,
									left: x + 5,
									'border-radius': '3px',
									'-moz-border-radius': '3px',
									'-webkit-border-radius': '3px',
									padding: '3px 8px 3px 8px',
									color: '#ffffff',
									background: '#000000',
									opacity: 0.80
								}).appendTo("body").fadeIn(500);
							}
						 
							var previousPoint = null;
							
							$("#analytics").bind("plothover", function (event, pos, item) {
								$("#x").text(pos.x.toFixed(2));
								$("#y").text(pos.y.toFixed(2));
						 
									if (item) {
										if (previousPoint != item.dataIndex) {
											previousPoint = item.dataIndex;
											
											$("#tooltip").remove();
											var x = item.datapoint[0],
												y = item.datapoint[1];
											
											showTooltip(item.pageX, item.pageY,
														item.series.label + " : " + y);
										}
									}
									else {
										$("#tooltip").fadeOut(500);
										previousPoint = null;            
									}
							});
						
						});
					  
                    }
                });    	
    });
</script>
<div class="one_full">
		<section class="title">
			<h4>Statistics: Hotspot Login | Last 10 days</h4>
		</section>	
		<section class="item">
			<div class="content">
				<div id="analytics"></div>
			</div>
		</section>
</div>
