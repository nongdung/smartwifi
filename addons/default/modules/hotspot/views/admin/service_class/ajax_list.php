<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<table>
			<thead>
				<tr>
					<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
	    			<th style="width:5%;">#</th>
	    			<th style="width:20%;">MAC Address</th>
	    			<th style="width:10%;">Manufacturer</th>
	    			<th style="width:5%;">Hardware Version</th>
	    			<th style="width:5%;">Firmware Version</th>
	    			<th style="width:10%;">Info</th>
	    			<th style="width:10%;">Location</th>
	    			<th style="width:25%;"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="5">
						<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
					</td>
				</tr>
			</tfoot>
			<tbody>
			    <?php $i=1; ?>
				<?php foreach( $entries['entries'] as $entry ): ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $entry['id']); ?></td>
					<td><?php echo $i++; ?></td>
					<td><?php echo $entry['mac']; ?></td>
					<td><?php echo $entry['manufacturer']; ?></td>
					<td><?php echo $entry['h_version'];?></td>
					<td><?php echo $entry['f_version'];?></td>
					<td><?php echo $entry['equipment_info'];?></td>
					<td><?php echo $entry['loc_id']['loc_name'];?></td>
					<td class="actions">
						<?php echo
						anchor('admin/hotspot/equipment/settings/'.$entry['id'], lang('hotspot:settings'), 'class="button"').' '.						
						anchor('admin/hotspot/equipment/edit/'.$entry['id'], lang('hotspot:edit'), 'class="button"').' '.
						anchor('admin/hotspot/equipment/delete/'.$entry['id'], 	lang('hotspot:delete'), array('class'=>'button confirm')); ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
</table>