<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('hotspot:eqp_settings'); ?></h4>
</section>

<section class="item">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		<div class="form_inputs">
			<ul>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="bypass_ios_popup">Bypass iOS Popup<span>*</span></label>
					<div class="input"><?php echo form_dropdown('bypass_ios_popup', array('0' => 'Disable', '1' => 'Enable'), isset($settings['bypass_ios_popup'])?$settings['bypass_ios_popup']:'1'); ?></div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="login_with_facebook">Login with Facebook<span>*</span></label>
					<div class="input"><?php echo form_dropdown('login_with_facebook', array('0' => 'Disable', '1' => 'Enable'), isset($settings['login_with_facebook'])?$settings['login_with_facebook']:'1'); ?></div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="login_with_twitter">Login with Twitter<span>*</span></label>
					<div class="input"><?php echo form_dropdown('login_with_twitter', array('0' => 'Disable', '1' => 'Enable'), isset($settings['login_with_twitter'])?$settings['login_with_twitter']:'0'); ?></div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="login_with_youtube">Login with Youtube<span>*</span></label>
					<div class="input"><?php echo form_dropdown('login_with_youtube', array('0' => 'Disable', '1' => 'Enable'), isset($settings['login_with_youtube'])?$settings['login_with_youtube']:'1'); ?></div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="login_with_google">Login with Google<span>*</span></label>
					<div class="input"><?php echo form_dropdown('login_with_google', array('0' => 'Disable', '1' => 'Enable'), isset($settings['login_with_google'])?$settings['login_with_google']:'1'); ?></div>
				</li>
			</ul>
		</div>
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
		</div>
	<?php echo form_close(); ?>
</section>