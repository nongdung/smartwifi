<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('hotspot:add_service_class'); ?></h4>
</section>

<section class="item">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		<div class="form_inputs">
			<ul>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="class_name"><?php echo lang('hotspot:service_class_name'); ?><span>*</span></label>
					<div class="input"><?php echo form_input('class_name', ''); ?></div>
				</li>									
			</ul>
			<ul>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="max_location"><?php echo lang('hotspot:max_location'); ?></label>
					<div class="input"><?php echo form_input('max_location', '3'); ?></div>
				</li>									
			</ul>
			<ul>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="max_devices"><?php echo lang('hotspot:max_devices'); ?></label>
					<div class="input"><?php echo form_input('max_devices', '3'); ?></div>
				</li>									
			</ul>
			<ul>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="monthly_limit_session"><?php echo lang('hotspot:monthly_limit_session'); ?></label>
					<div class="input"><?php echo form_input('monthly_limit_session', '3000'); ?></div>
				</li>									
			</ul>
		</div>
		
		<label for="methods">Methods</label>
		<table class="table-list" border="0" cellspacing="0">
			<thead>
				<?php foreach ($methods as $method) :?>
				<td><?php echo $method['mod_name'];?></td>
				<?php endforeach;?>	
			</thead>

			<tr>
				<?php foreach ($methods as $method) :?>
				<td><?php echo form_checkbox("methods[]",$method['id']);?></td>
				<?php endforeach;?>					
			</tr>
			
		</table>
		
		<br />
		<label for="themes">Themes</label>
		<table class="table-list" border="0" cellspacing="0">
			<thead>
				<?php foreach ($themes as $theme) :?>
				<td><?php echo $theme['name'];?></td>
				<?php endforeach;?>	
			</thead>

			<tr>
				<?php foreach ($themes as $theme) :?>
				<td><?php echo form_checkbox("themes[]",$theme['id']);?></td>
				<?php endforeach;?>					
			</tr>
			
		</table>
					
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
		</div>
	<?php echo form_close(); ?>
</section>