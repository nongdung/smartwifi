<?php
//messages
$lang['hotspot:new_merchant']			=	'New Merchant';
$lang['hotspot:edit_merchant']			=	'Edit Merchant';

$lang['hotspot:service_class']			=	'Service Class';
$lang['hotspot:add_service_class']			=	'Add Service Class';
$lang['hotspot:service_class_name']			=	'Service Class Name';

$lang['hotspot:merchant_list']			=	'Merchant List';
$lang['hotspot:max_location']			=	'Max Location';
$lang['hotspot:max_devices']			=	'Max Devices';
$lang['hotspot:monthly_limit_session']			=	'Monthly Limit Session';

$lang['hotspot:submit_success']			=	'Submit success!';
$lang['hotspot:submit_failure']			=	'Submit failed!';

?>