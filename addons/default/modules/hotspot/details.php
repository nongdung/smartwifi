<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Hotspot extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'SmartWifi Hotspot'
			),
			'description' => array(
				'en' => 'SmartWifi Hotspot Core.'
			),
			'frontend' => FALSE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',
			'roles' => array(
                'sub_merchant', 'merchant', 'advertiser', 'manager', 'admin'
            )			
		);
		
		if (group_has_role('hotspot', 'admin') || group_has_role('hotspot', 'manager')) {
			$info['sections']['merchant'] = array(
					'name' 	=> 'hotspot:merchant_list', // These are translated from your language file
					'uri' 	=> 'admin/hotspot/merchant',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'hotspot:new_merchant',
							'uri' 	=> 'admin/hotspot/merchant/add',
							'class' => 'add'
							)
					)
			);			
		}
		
		if (group_has_role('hotspot', 'admin')) {
			$info['sections']['service_class'] = array(
					'name' 	=> 'hotspot:service_class', // These are translated from your language file
					'uri' 	=> 'admin/hotspot/service-class',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'hotspot:add_service_class',
							'uri' 	=> 'admin/hotspot/service-class/add',
							'class' => 'add'
							)
					)
			);			
		}


							
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		$this->load->language('hotspot/hotspot');
		$this->db->delete('settings', array('module' => 'hotspot'));
		$this->streams->utilities->remove_namespace('hotspot_core');
		//add stream
		if ( ! $service_class_id = $this->streams->streams->add_stream('Core Service Class', 'service_class', 'hotspot_core', 'hcore_', null)) return false;
		if ( ! $this->streams->streams->add_stream('Core Service', 'service', 'hotspot_core', 'hcore_', null)) return false;		
		
		$fields = array(
            array(
                'name' => 'Class Name',
                'slug' => 'class_name',
                'namespace' => 'hotspot_core',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'service_class',
                'title_column' => true,
                'required' => true                
            ),
            array(
                'name' => 'Methods',
                'slug' => 'methods',
                'namespace' => 'hotspot_core',
                'type' => 'textarea',
                'assign' => 'service_class',
                'required' => true
            ),
            array(
                'name' => 'Themes',
                'slug' => 'themes',
                'namespace' => 'hotspot_core',
                'type' => 'textarea',
                'assign' => 'service_class',
                'required' => true
            ),
            array(
                'name' => 'Max Location',
                'slug' => 'max_location',
                'namespace' => 'hotspot_core',
                'type' => 'integer',
                'assign' => 'service_class',
                'extra' => array('default' => 3, 'max_length' => 3),
                'required' => true
            ),
            array(
                'name' => 'Max Devices',
                'slug' => 'max_devices',
                'namespace' => 'hotspot_core',
                'type' => 'integer',
                'assign' => 'service_class',
                'extra' => array('default' => 3, 'max_length' => 3),
                'required' => true
            ),
            array(
                'name' => 'Monthly Limit Session',
                'slug' => 'monthly_limit_session',
                'namespace' => 'hotspot_core',
                'type' => 'integer',
                'assign' => 'service_class',
                'extra' => array('default' => 3000, 'max_length' => 6),
                'required' => true
            ),
            //End of service_class stream            
            array(
                'name' => 'Merchant ID',
                'slug' => 'merchant_id',
                'namespace' => 'hotspot_core',                
                'type' => 'integer',
                'assign' => 'service',
                'required' => true,
                'unique'        => true                
            ),
            array(
                'name' => 'Service Class',
                'slug' => 'class',
                'namespace' => 'hotspot_core',
                'extra' => array('choose_stream' => $service_class_id),
                'type' => 'relationship',
                'assign' => 'service',                
            ),
            array(
                'name' => 'Start Date',
                'slug' => 'start_date',
                'namespace' => 'hotspot_core',
                'extra' => array('use_time' => 'yes', 'storage' => 'unix', 'input_type' => 'datepicker'),
                'type' => 'datetime',
                'assign' => 'service',                
            ),
            array(
                'name' => 'End Date',
                'slug' => 'end_date',
                'namespace' => 'hotspot_core',
                'extra' => array('use_time' => 'yes', 'storage' => 'unix', 'input_type' => 'datepicker'),
                'type' => 'datetime',
                'assign' => 'service',                
            ),
            array(
                'name' => 'Current Cash',
                'slug' => 'current_cash',
                'namespace' => 'hotspot_core',
                'extra' => array('default' => 0),
                'type' => 'integer',
                'assign' => 'service',                
            ),
		);

        $this->streams->fields->add_fields($fields);
		
		//Core Logging table
		$sql = "CREATE TABLE `default_hotspot_log` (
                  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                  `merchant_id` int(11) unsigned,
                  `message` varchar(255) NOT NULL,
                  `created` varchar(255) NOT NULL,                  
                   PRIMARY KEY (`id`),
                   KEY `merchant_id` (`merchant_id`)
        ) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8; ";
		
		//Delete before create table		   
		$this->db->query("DROP TABLE IF EXISTS `default_hotspot_log`;");
		
		$this->db->query($sql);

        $this->streams->streams->update_stream('service_class', 'hotspot_core', array(
            'view_options' => array(
                'class_name'            )
        ));
		
		$this->streams->streams->update_stream('service', 'hotspot_core', array(
            'view_options' => array(
                'merchant_id',
                'class',
                'start_date',
                'end_date',
                'current_cash'
            )
        ));
        
        //Install login modules streams
        if ( ! $this->streams->streams->add_stream('Login Modules', 'modules', 'login_modules', 'hcore_', null)) return false;
		$fields = array(
            array(
                'name' => 'Module Name',
                'slug' => 'mod_name',
                'namespace' => 'login_modules',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'modules',
                'title_column' => true,
                'required' => true                
            ),
            array(
                'name' => 'Module Slug',
                'slug' => 'mod_slug',
                'namespace' => 'login_modules',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'modules',
                'required' => true
            ),
            array(
                'name' => 'Description',
                'slug' => 'mod_desc',
                'namespace' => 'login_modules',
                'type' => 'textarea',
                'assign' => 'modules'
            )
        );
		$this->streams->fields->add_fields($fields);
        
        
		return TRUE;
		
	}

	public function uninstall()
	{
		$this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
        // utility in the Streams API Utilties driver.
        $this->streams->utilities->remove_namespace('hotspot_core');
        //$this->streams->utilities->remove_namespace('hotspot_logs');
		$this->streams->utilities->remove_namespace('login_modules');
		$this->db->delete('settings', array('module' => 'hotspot'));
		
		//$sql = "DROP TABLE IF EXISTS `default_hotspot_raw_logs`;";
		//$this->db->query($sql);
		
		return TRUE;
	}


	public function upgrade($old_version)
	{
		$this->load->driver('Streams');
		$this->streams->utilities->remove_namespace('hotspot_logs');
		$sql = "DROP TABLE IF EXISTS `default_hotspot_raw_logs`;";
		$this->db->query($sql);		
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
    
    
    public function admin_menu(&$menu)
    {
		if (group_has_role('hotspot', 'admin') || group_has_role('hotspot', 'manager')) {
			$menu['Admin']['Merchant List']   = 'admin/hotspot/merchant';
		}	
		if (group_has_role('hotspot', 'admin')) {
            
			$menu['Admin']['Service Class']   = 'admin/hotspot/service-class';
        }		
		add_admin_menu_place('Admin', 1);

	}
}
/* End of file details.php */
