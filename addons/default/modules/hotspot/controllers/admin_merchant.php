<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_merchant extends Admin_Controller
{
	protected $section = 'merchant';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('hotspot/hotspot');
		$this->load->driver('Streams');
		role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all billing plans
     */
    public function index()
    {
    	
        $extra = array();
        $extra['title'] = 'Merchant List';

        $extra['buttons'] = array(
            array(
                'label' => lang('global:edit'),
                'url' => 'admin/hotspot/merchant/edit/-entry_id-'
            ),
            array(
                'label' => lang('global:delete'),
                'url' => 'admin/hotspot/merchant/delete/-entry_id-',
                'confirm' => true
            )
        );

        $this->streams->cp->entries_table('service', 'hotspot_core', 15, 'admin/hotspot/merchant/index', true, $extra);
    }
	
	
	public function add()
	{
		$extra = array(
		    'return'            => 'admin/hotspot/merchant',
		    'success_message'   => lang('hotspot:submit_success'),
		    'failure_message'   => lang('hotspot:submit_failure'),
		    'title'             => lang('hotspot:new_merchant')
		);


		$this->streams->cp->entry_form('service', 'hotspot_core', 'new', null, true, $extra);
	}
    
	public function edit($id = null)
	{
		$extra = array(
		    'return'            => 'admin/hotspot/merchant',
		    'success_message'   => lang('hotspot:submit_success'),
		    'failure_message'   => lang('hotspot:submit_failure'),
		    'title'             => lang('hotspot:edit_merchant')
		);


		$this->streams->cp->entry_form('service', 'hotspot_core', 'edit', $id, true, $extra);
	}

	public function delete($id='')
	{
		$this->streams->entries->delete_entry($id, 'service', 'hotspot_core');
		$this->session->set_flashdata('success', "Deleted!");
		redirect('admin/hotspot/merchant');	   
	}

}