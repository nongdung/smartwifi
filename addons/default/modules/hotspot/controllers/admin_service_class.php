<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_service_class extends Admin_Controller
{
	protected $section = 'service_class';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('hotspot/hotspot');
		$this->load->driver('Streams');
        role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all billing plans
     */
    public function index()
    {
        $extra = array();
        $extra['title'] = 'Service Class List';

        $extra['buttons'] = array(
            array(
                'label' => lang('global:edit'),
                'url' => 'admin/hotspot/service-class/edit/-entry_id-'
            ),
            array(
                'label' => lang('global:delete'),
                'url' => 'admin/hotspot/service-class/delete/-entry_id-',
                'confirm' => true
            )
        );

        $this->streams->cp->entries_table('service_class', 'hotspot_core', 15, 'admin/hotspot/service-class/index', true, $extra);
    }
    

	public function add()
	{
		$this->config->load("hotspot");

		$validation_rules = array(
			array(
                'field' => 'class_name',
                'label' => 'Class name',
                'rules' => 'trim|max_length[64]|required'
            ),
            array(
                'field' => 'max_location',
                'label' => 'Max Location',
                'rules' => 'is_natural|max_length[3]'
            ),
            array(
                'field' => 'max_devices',
                'label' => 'Max Devices',
                'rules' => 'is_natural|max_length[3]'
            ),
            array(
                'field' => 'monthly_limit_session',
                'label' => 'Monthly Limit Session',
                'rules' => 'is_natural|max_length[6]'
            )
		
		);
		
		$this->form_validation->set_rules($validation_rules);
		
		if ($this->form_validation->run()){
			//save to database
			$entry_data = array(
			    'class_name' => $this->input->post("class_name"),
			    'methods'   => base64_encode(serialize($this->input->post("methods"))),
			    'themes'   => base64_encode(serialize($this->input->post("themes"))),
			    'max_location' => $this->input->post("max_location"),
			    'max_devices' => $this->input->post("max_devices"),
			    'monthly_limit_session' => $this->input->post("monthly_limit_session"),
			);
			$this->streams->entries->insert_entry($entry_data, 'service_class', 'hotspot_core');
			$this->session->set_flashdata('success', "Updated");
			redirect('admin/hotspot/service-class');
		}
		
		$params = array(
				'stream'    => 'themes',
    			'namespace' => 'splash',    			
		);
		$themes = $this->streams->entries->get_entries($params);
		
		$params = array(
				'stream'    => 'modules',
    			'namespace' => 'login_modules',    			
		);
		$methods = $this->streams->entries->get_entries($params);
		
		$this->template
			->title("Add service class")
			//->set("methods",$this->config->item("methods"))
			//->set("themes",$this->config->item("themes"))
			->set("methods",$methods['entries'])
			->set("themes",$themes['entries'])
			->build('admin/service_class/add');
		
	}

	public function delete($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/hotspot/service-class');			
		}	
		
		//ok, now we delete
		$this->streams->entries->delete_entry($id, 'service_class', 'hotspot_core');
		$this->session->set_flashdata('success', "Deleted!");
		redirect('admin/hotspot/service-class');		
		
	}
	
	public function edit($id = null)
	{
		$this->config->load("hotspot");
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/hotspot/service-class');			
		}
		
		//ok, now we edit
		$entry = $this->streams->entries->get_entry($id, 'service_class', 'hotspot_core');
		$class_name = $entry->class_name;
		$current_methods = unserialize(base64_decode($entry->methods));
		$current_themes = unserialize(base64_decode($entry->themes));
		
		$validation_rules = array(
			array(
                'field' => 'class_name',
                'label' => 'Class name',
                'rules' => 'trim|max_length[64]|required'
            ),
            array(
                'field' => 'max_location',
                'label' => 'Max Location',
                'rules' => 'is_natural|max_length[3]'
            ),
            array(
                'field' => 'max_devices',
                'label' => 'Max Devices',
                'rules' => 'is_natural|max_length[3]'
            ),
            array(
                'field' => 'monthly_limit_session',
                'label' => 'Monthly Limit Session',
                'rules' => 'is_natural|max_length[6]'
            )
		
		);
		
		$this->form_validation->set_rules($validation_rules);
		
		if ($this->form_validation->run()){
			//save to database
			$entry_data = array(
			    'class_name' => $this->input->post("class_name"),
			    'methods'   => base64_encode(serialize($this->input->post("methods"))),
			    'themes'   => base64_encode(serialize($this->input->post("themes"))),
			    'max_location' => $this->input->post("max_location"),
			    'max_devices' => $this->input->post("max_devices"),
			    'monthly_limit_session' => $this->input->post("monthly_limit_session"),
			);
			$this->streams->entries->update_entry($this->input->post("id"),$entry_data, 'service_class', 'hotspot_core');
			$this->session->set_flashdata('success', "Updated");
			redirect('admin/hotspot/service-class');
		}
		
		//print_r($current_methods);exit;
		
		$params = array(
				'stream'    => 'themes',
    			'namespace' => 'splash',    			
		);
		$themes = $this->streams->entries->get_entries($params);
		
		$params = array(
				'stream'    => 'modules',
    			'namespace' => 'login_modules',    			
		);
		$methods = $this->streams->entries->get_entries($params);
		
		$this->template
			->title("Edit service class")
			->set("methods",$methods['entries'])
			->set("themes",$themes['entries'])
			->set('class_name',$class_name)
			->set('current_methods', $current_methods)
			->set('current_themes', $current_themes)
			->set('max_location', $entry->max_location)
			->set('max_devices', $entry->max_devices)
			->set('monthly_limit_session', $entry->monthly_limit_session)
			->set("id", $id)
			->build('admin/service_class/edit');
			
		
	}

}