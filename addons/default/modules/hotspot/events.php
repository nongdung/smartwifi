<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Hotspot Events Class
 * 
 * @package     PyroCMS
 * @subpackage  Hotspot Module
 * @category    events
 * @author      SmartWifi Dev Team
 */
class Events_Hotspot
{
    protected $ci;
    
    public function __construct()
    {
        $this->ci =& get_instance();
        
        // register the public_controller event when this file is autoloaded
        Events::register('merchant_created', array($this, 'assign_free_class'));
     }
    
    // this will be triggered by the Events::trigger('public_controller') code in Public_Controller.php
    public function assign_free_class($merchant_id = null)
    {
        // you can load a model or etc here if you like using $this->ci->load();        
        $this->ci->load->driver('Streams');
		$this->ci->load->model('users/user_m');
		$user = $this->ci->user_m->get(array('id' => $merchant_id));
		if ($user) {
			//Check if user exist in service table
			$params = array(
			    'stream'    => 'service',
			    'namespace' => 'hotspot_core',
			    'where'		=> "merchant_id = '".$merchant_id."'",
			    'limit'		=> 1
			);			
			$entries = $this->ci->streams->entries->get_entries($params);
			if (! $entries['total']) {
				$entry_data = array(
				    'merchant_id' => $merchant_id,
				    'class'   => 1 //Free class
				);
				if ($this->ci->streams->entries->insert_entry($entry_data, 'service', 'hotspot_core')) {
					return true;
				}
			} else {
				return false;
			}
		}
		return false;
    }
    
}
/* End of file events.php */