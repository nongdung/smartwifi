<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Service_class_m extends MY_Model {
	function __construct()
		{
        // Call the Model constructor
        parent::__construct();
			$this->load->driver('Streams');
    	}
		
	function get_by_name($class_name){
        $info = array();
		$params = array(
		    'stream'    => 'service_class',
		    'namespace' => 'hotspot_core',
		    'where'		=> "`class_name` = '".$class_name."'"
		);
		$entries = $this->streams->entries->get_entries($params);		
		if ($entries['total']) {
			$info['class_id'] = $entries['entries'][0]['id'];	
			$info['created_by'] = $entries['entries'][0]['created_by'];
			$info['class_name'] = $entries['entries'][0]['class_name'];
			$info['methods'] = unserialize(base64_decode($entries['entries'][0]['methods']));
			$info['themes'] = unserialize(base64_decode($entries['entries'][0]['themes']));			
			$info['max_location'] = $entries['entries'][0]['max_location'];
			$info['max_devices'] = $entries['entries'][0]['max_devices'];
			$info['monthly_limit_session'] = $entries['entries'][0]['monthly_limit_session'];	
		}	
        return $info;
    }
	
	function get_by_id($class_id){
        $info = array();
		$params = array(
		    'stream'    => 'service_class',
		    'namespace' => 'hotspot_core',
		    'where'		=> "`id` = '".$class_id."'"
		);
		$entries = $this->streams->entries->get_entries($params);		
		if ($entries['total']) {
			$info['class_id'] = $entries['entries'][0]['id'];	
			$info['created_by'] = $entries['entries'][0]['created_by'];
			$info['class_name'] = $entries['entries'][0]['class_name'];
			$info['methods'] = unserialize(base64_decode($entries['entries'][0]['methods']));
			$info['themes'] = unserialize(base64_decode($entries['entries'][0]['themes']));			
			$info['max_location'] = $entries['entries'][0]['max_location'];
			$info['max_devices'] = $entries['entries'][0]['max_devices'];
			$info['monthly_limit_session'] = $entries['entries'][0]['monthly_limit_session'];	
		}	
        return $info;
    }
	
	function get_login_modules($class_id)
	{
		$login_modules = array();
		$params = array(
		    'stream'    => 'service_class',
		    'namespace' => 'hotspot_core',
		    'where'		=> "`id` = '".$class_id."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			$module_ids = unserialize(base64_decode($entries['entries'][0]['methods']));
			foreach ($module_ids as $module_id) {
				$module = $this->streams->entries->get_entry($module_id, "modules", "login_modules");
				array_push($login_modules, $module->mod_slug);
			}
		}
		
		return $login_modules;	
	}
}