<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Merchant_m extends MY_Model {
	function __construct()
		{
        // Call the Model constructor
        parent::__construct();
			$this->load->driver('Streams');
    	}
		
	function get_info($merchant_id){
        $info = array();
		$params = array(
		    'stream'    => 'service',
		    'namespace' => 'hotspot_core',
		    'where'		=> "`merchant_id` = '".$merchant_id."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			$info['merchant_id'] = $merchant_id;
			$info['start_date'] = $entries['entries'][0]['start_date'];
			$info['end_date'] = $entries['entries'][0]['end_date'];
			$info['current_cash'] = $entries['entries'][0]['current_cash'];
			$info['created_by'] = $entries['entries'][0]['created_by']['user_id'];
			
			$info['class_id'] = $entries['entries'][0]['class']['id'];
			$info['class_name'] = $entries['entries'][0]['class']['class_name'];
			/*			
			$info['methods'] = unserialize(base64_decode($entries['entries'][0]['class']['methods']));
			$info['themes'] = unserialize(base64_decode($entries['entries'][0]['class']['themes']));
			$info['class_name'] = $entries['entries'][0]['class']['class_name'];
			$info['max_location'] = $entries['entries'][0]['class']['max_location'];
			$info['max_devices'] = $entries['entries'][0]['class']['max_devices'];
			$info['monthly_limit_session'] = $entries['entries'][0]['class']['monthly_limit_session'];
			*/	
		}	
        return $info;
    }
}