<?php
$config['methods'] = array("facebook" => "Facebook", "facebook_js" => "Facebook JS", "facebook_like" => "Facebook Like", "facebook_story" => "Facebook Story", "youtube" => "Youtube", "click" => "Click", "google_js" => "Google JS", "email" => "Email", "textmatch" => "Text Match", "survey" => "Survey", "sms" => "SMS", "pro" => "PRO");
$config['themes'] = array("default" => "Default", "resto1" => "Restaurant1", "pro" => "PRO");
