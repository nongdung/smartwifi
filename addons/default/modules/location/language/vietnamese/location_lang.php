<?php
$lang['location:location']			=	'Địa điểm';
$lang['location:theme']			=	'Theme';
$lang['location:location_list']			=	'Danh sách địa điểm';
$lang['location:add_location']			=	'Thêm điểm';
$lang['location:edit_location']			=	'Sửa điểm';

$lang['location:submit_success']			=	'Thành công!';
$lang['location:submit_failure']			=	'Có lỗi xảy ra!';
$lang['location:add_province']			=	'Add province';
$lang['location:add_city']			=	'Add city';

$lang['location:location_name']			=	'Tên điểm';
$lang['location:long_lat']			=	'Longtitue/Latitude';
$lang['location:province']			=	'Tỉnh';
$lang['location:city']			=	'Thành phố';
$lang['location:address']			=	'Địa chỉ';
$lang['location:status']			=	'Trạng thái';
$lang['location:template']			=	'Template';
$lang['location:login_page_template']			=	'Login Page Template';
$lang['location:wait_page_template']			=	'Wait Page Template';
$lang['location:success_page_template']			=	'Success Page Template';
$lang['location:info']			=	'Thông tin';
$lang['location:redirect_url']			=	'Redirect URL';

$lang['location:no_items']			=	'No items';
$lang['location:devices']			=	'Thiết bị';
$lang['location:settings']			=	'Cài đặt';

$lang['location:create_fields']			=	'Create fields';

$lang['location:products_and_services_heading']			=	'Tiêu đề sản phẩm và dịch vụ';

$lang['location:select_theme']			=	'Chọn mẫu trang chào';
$lang['location:change_theme']			=	'Thay mẫu trang chào';
$lang['location:theme_detail']			=	'Chi tiết trang chào';
$lang['location:login_methods']			=	'Phương thức đăng nhập';
$lang['location:action']			=	'Hành động';
?>