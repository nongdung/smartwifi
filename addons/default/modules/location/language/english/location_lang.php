<?php
$lang['location:location']			=	'Location';
$lang['location:theme']			=	'Theme';
$lang['location:location_list']			=	'Location list';
$lang['location:add_location']			=	'Add location';
$lang['location:edit_location']			=	'Edit location';

$lang['location:submit_success']			=	'Submit success!';
$lang['location:submit_failure']			=	'Submit failed!';
$lang['location:add_province']			=	'Add province';
$lang['location:add_city']			=	'Add city';

$lang['location:location_name']			=	'Location Name';
$lang['location:long_lat']			=	'Longtitue/Latitude';
$lang['location:province']			=	'Province';
$lang['location:city']			=	'City';
$lang['location:address']			=	'Address';
$lang['location:status']			=	'Status';
$lang['location:template']			=	'Template';
$lang['location:login_page_template']			=	'Login Page Template';
$lang['location:wait_page_template']			=	'Wait Page Template';
$lang['location:success_page_template']			=	'Success Page Template';
$lang['location:info']			=	'Info';
$lang['location:redirect_url']			=	'Redirect URL';

$lang['location:no_items']			=	'No items';
$lang['location:devices']			=	'Devices';
$lang['location:settings']			=	'Settings';

$lang['location:create_fields']			=	'Create fields';

$lang['location:products_and_services_heading']			=	'Products and Services Heading';

$lang['location:select_theme']			=	'Select Theme';
$lang['location:change_theme']			=	'Change Theme';
$lang['location:theme_detail']			=	'Theme Detail';
$lang['location:login_methods']			=	'Login Methods';
$lang['location:action']			=	'Action';
?>