<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Blog Permissions
$lang['hotspot:role_sub_merchant']		= 'Sub Merchant';
$lang['hotspot:role_merchant']	= 'Merchant';
$lang['hotspot:role_advertiser'] 	= 'Advertiser';
$lang['hotspot:role_manager']     = 'Manager';
$lang['hotspot:role_admin']     = 'Admin';