<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Location extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'SmartWifi Hotspot Location',
				'vi' => 'Địa điểm'
			),
			'description' => array(
				'en' => 'Manage hotspot locations.',
				'vi' => 'Quản lý điểm triển khai wifi'
			),
			'frontend' => FALSE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',			
		);
		
		$info['sections']['location'] = array(
					'name' 	=> 'cp:location', // These are translated from your language file
					'uri' 	=> 'admin/location',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'location:add_location',
							'uri' 	=> 'admin/location/add',
							'class' => 'add'
							)
					)
			);
		
		if (group_has_role('hotspot', 'admin')) {
			$info['sections']['province'] = array(
					'name' 	=> 'Province', // These are translated from your language file
					'uri' 	=> 'admin/location/province',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'location:add_province',
							'uri' 	=> 'admin/location/province/add',
							'class' => 'add'
							)
					)
			);
			$info['sections']['city'] = array(
					'name' 	=> 'City', // These are translated from your language file
					'uri' 	=> 'admin/location/city',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'location:add_city',
							'uri' 	=> 'admin/location/city/add',
							'class' => 'add'
							)
					)
			);
			$info['sections']['location_fields'] = array(
					'name' 	=> 'Location Fields', // These are translated from your language file
					'uri' 	=> 'admin/location/fields',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'location:create_fields',
							'uri' 	=> 'admin/location/fields/create',
							'class' => 'add'
							)
					)
			);
		}
							
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		$this->db->delete('settings', array('module' => 'location'));
		$this->streams->utilities->remove_namespace('location');
		//add stream
		if ( ! $loc_id = $this->streams->streams->add_stream('Location List', 'list', 'location', 'loc_', null)) return false;
		if ( ! $province_id = $this->streams->streams->add_stream('Province', 'province', 'location', 'loc_', null)) return false;
		if ( ! $city_id = $this->streams->streams->add_stream('City', 'city', 'location', 'loc_', null)) return false;
		
		$fields = array(
            array(
                'name' => 'Location Name',
                'slug' => 'loc_name',
                'namespace' => 'location',
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'list',
                'title_column' => true,
                'required' => true                
            ),
            array(
                'name' => 'Longtitude/Latitude',
                'slug' => 'long_lat',
                'namespace' => 'location',
                'extra' => array('max_length' => 64),
                'type' => 'text',
                'assign' => 'list',                
            ),
            array(
                'name' => 'Province',
                'slug' => 'province',
                'namespace' => 'location',                
                'type' => 'relationship',
                'extra' => array('choose_stream' => $province_id),
                'assign' => 'list',
                'required' => true                
            ),            
            array(
                'name' => 'City',
                'slug' => 'city',
                'namespace' => 'location',                
                'type' => 'relationship',
                'extra' => array('choose_stream' => $city_id),
                'assign' => 'list'                
            ),
            array(
                'name' => 'Address',
                'slug' => 'address',
                'namespace' => 'location',                
                'type' => 'text',
                'extra' => array('max_length' => 128),
                'assign' => 'list',
                'required' => true                  
            ),
            array(
                'name' => 'Information',
                'slug' => 'info',
                'namespace' => 'location',                
                'type' => 'textarea',
                'assign' => 'list'                
            ),
            array(
                'name' => 'Status',
                'slug' => 'status',
                'namespace' => 'location',
                'type' => 'integer',
                'assign' => 'list',
                'required' => true
            ),
            array(
                'name' => 'Theme',
                'slug' => 'theme',
                'namespace' => 'location',
                'type' => 'integer',
                'assign' => 'list',
                'required' => true
            ),
            array(
                'name' => 'Redirect URL',
                'slug' => 'redirect_url',
                'namespace' => 'location',                
                'type' => 'text',                
                'assign' => 'list',                    
            ),
            /*
            array(
                'name' => 'Login Page Template',
                'slug' => 'login_page_template',
                'namespace' => 'location',
                'type' => 'integer',
                'assign' => 'list',
                'required' => true
            ),
            array(
                'name' => 'Wait Page Template',
                'slug' => 'wait_page_template',
                'namespace' => 'location',
                'type' => 'integer',
                'assign' => 'list',
                'required' => true
            ),
            array(
                'name' => 'Success Page Template',
                'slug' => 'success_page_template',
                'namespace' => 'location',
                'type' => 'integer',
                'assign' => 'list',
                'required' => true
            ),
            array(
                'name' => 'Login Page Template',
                'slug' => 'desktop_login_template',
                'namespace' => 'location',
                'type' => 'integer',
                'assign' => 'list',
                'required' => true
            ),
            array(
                'name' => 'Wait Page Template',
                'slug' => 'desktop_wait_template',
                'namespace' => 'location',
                'type' => 'integer',
                'assign' => 'list',
                'required' => true
            ),
            array(
                'name' => 'Success Page Template',
                'slug' => 'desktop_success_template',
                'namespace' => 'location',
                'type' => 'integer',
                'assign' => 'list',
                'required' => true
            ),
			*/ 
            //End of Location Stream

			//Start of Province stream
			array(
			    'name'          => 'Province Name',
			    'slug'          => 'province_name',
			    'namespace'     => 'location',
			    'type'          => 'text',
			    'extra' => array('max_length' => 64),
			    'assign'        => 'province',
			    'title_column' => true,
			    'required' => true		
			),
			array(
			    'name'          => 'Province Code',
			    'slug'          => 'province_code',
			    'namespace'     => 'location',
			    'type'          => 'text',
			    'assign'        => 'province',
			    'required' => true		
			),			
			//End of Province stream
			//Start of City stream
			array(
			    'name'          => 'City Name',
			    'slug'          => 'city_name',
			    'namespace'     => 'location',
			    'type'          => 'text',
			    'extra' => array('max_length' => 64),
			    'assign'        => 'city',
			    'title_column' => true,
			    'required' => true		
			),
			array(
			    'name'          => 'City Code',
			    'slug'          => 'city_code',
			    'namespace'     => 'location',
			    'type'          => 'text',
			    'assign'        => 'city',
			    'required' => true		
			),
			array(
                'name' => 'Province ID',
                'slug' => 'province_id',
                'namespace' => 'location',                
                'type' => 'relationship',
                'extra' => array('choose_stream' => $province_id),
                'assign' => 'city',
                'required' => true                
            ),  
			//End of City stream
        );

        $this->streams->fields->add_fields($fields);

        $this->streams->streams->update_stream('list', 'location', array(
            'view_options' => array(
                'loc_name',
                'long_lat',               
                'province',
                'city',
                'address',
                'info',
                'status',
                'theme',
                'redirect_url'
                /*
                'login_page_template',
                'wait_page_template',
                'success_page_template',
                'desktop_login_template',
                'desktop_wait_template',
                'desktop_success_template'
				 */ 
            )
        ));

		$this->streams->streams->update_stream('province', 'location', array(
            'view_options' => array(
                'province_name',
                'province_code'
            )
        ));
		
		$this->streams->streams->update_stream('city', 'location', array(
            'view_options' => array(
                'city_name',
                'city_code',
                'province_id'
            )
        ));

		
		return TRUE;
		
	}

	public function uninstall()
	{
		$this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
        // utility in the Streams API Utilties driver.
        $this->streams->utilities->remove_namespace('location');
		$this->db->delete('settings', array('module' => 'location'));
		return TRUE;
	}


	public function upgrade($old_version)
	{
		$this->load->driver('Streams');
		$fields = array(
			array(
                'name' => 'Theme',
                'slug' => 'theme',
                'namespace' => 'location',
                'type' => 'integer',
                'assign' => 'list',
                'required' => true
            ),
		);
		$this->streams->fields->add_fields($fields);

        $this->streams->streams->update_stream('list', 'location', array(
            'view_options' => array(
                'loc_name',
                'long_lat',               
                'province',
                'city',
                'address',
                'info',
                'status',
                'theme'
                /*
                'login_page_template',
                'wait_page_template',
                'success_page_template',
                'desktop_login_template',
                'desktop_wait_template',
                'desktop_success_template'
				 * 
				 */ 
            )
        ));
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
    
    
    public function admin_menu(&$menu)
    {
        $menu['lang:cp:management'] = array(
        	'lang:cp:devices' => 'admin/devices',
            'lang:cp:location'           => 'admin/location'   
            
        );
		add_admin_menu_place('lang:cp:management', 3);
		if (group_has_role('hotspot', 'admin')) {
            $menu['Admin']['Province List']   = 'admin/location/province';
			$menu['Admin']['City List']   = 'admin/location/city';			
        }

	}
}
/* End of file details.php */
