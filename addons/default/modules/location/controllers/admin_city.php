<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_city extends Admin_Controller
{
	protected $section = 'city';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('location');
		$this->load->driver('Streams');
        role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all billing plans
     */
    public function index()
    {
        $extra = array();
        $extra['title'] = 'City List';

        $extra['buttons'] = array(
            array(
                'label' => lang('global:edit'),
                'url' => 'admin/location/city/edit/-entry_id-'
            ),
            array(
                'label' => lang('global:delete'),
                'url' => 'admin/location/city/delete/-entry_id-',
                'confirm' => true
            )
        );

        $this->streams->cp->entries_table('city', 'location', 15, 'admin/location/city/index', true, $extra);
    }
    

	public function add()
	{
		$extra = array(
		    'return'            => 'admin/location/city',
		    'success_message'   => lang('hotspot:submit_success'),
		    'failure_message'   => lang('hotspot:submit_error'),
		    'title'             => 'Add city'
		);

		$this->streams->cp->entry_form('city', 'location', 'new', null, true, $extra);	
		
	}

	public function delete($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/location/city');			
		}	
		
		//ok, now we delete
		$this->streams->entries->delete_entry($id, 'city', 'location');
		$this->session->set_flashdata('success', "Deleted!");
		redirect('admin/location/city');		
		
	}
	
	public function edit($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/location/city');			
		}
		
		//ok, now we edit
		$extra = array(
		    'return'            => 'admin/location/city',
		    'success_message'   => lang('hotspot:submit_success'),
		    'failure_message'   => lang('hotspot:submit_error'),
		    'title'             => 'Edit city'
		);
		$this->streams->cp->entry_form('city', 'location', 'edit', $id, true, $extra);
			
		
	}
	

}