<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin extends Admin_Controller
{
	protected $section = 'location';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('location');
		$this->load->driver('Streams');
        role_or_die('hotspot', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all billing plans
     */
    public function index()
    {
    	$this->load->model('splash/hotspot_theme_m');    	
		   	
		$params = array(
		    'stream'    => 'list',
		    'namespace' => 'location',
		    'paginate'	=> 'yes',
		    'pag_segment' => 4,
		    'limit'	=> 15		        
		);
		
		if (!group_has_role('hotspot', 'admin')) {
			$params['where'] = "`created_by` = '".$this->current_user->id."'";
		}
		$entries = $this->streams->entries->get_entries($params);
		$theme_list = $this->hotspot_theme_m->getThemeList($this->current_user->id);
		$full_theme_list = $this->hotspot_theme_m->getFullThemeList($this->current_user->id);
		$this->template
			->title(lang('location:location_list'))
			->set('entries',$entries)
			->set('theme_list', $theme_list)
			->set('full_theme_list', $full_theme_list)
			->append_js('dropit.js')
			->append_css('dropit.css')
			->build('admin/location/list');
    }
    
    /**
     * Create a location
     */
    public function add()
    {
    	$this->load->model('location_m');
		$this->load->model('hotspot/service_class_m');
		$this->load->model('hotspot/merchant_m');
		$this->load->model('splash/hotspot_theme_m');
		   	
		$validation_rules = array(
			array(
					'field' => 'loc_name',
					'label' => 'Location Name',
					'rules' => 'trim|required|max_length[64]'
			),
			array(
					'field' => 'province',
					'label' => 'Province',
					'rules' => 'trim|required'
			),
			array(
					'field' => 'address',
					'label' => 'Address',
					'rules' => 'trim|required|max_length[64]'
			),
			array(
					'field' => 'theme',
					'label' => 'Theme',
					'rules' => 'trim|is_natural'
			),			
		);		
		
		$this->form_validation->set_rules($validation_rules);
		
		if ($this->form_validation->run()){
			$entry_data = array(
			    'loc_name' => $this->input->post('loc_name'),
			    'long_lat' => $this->input->post('long_lat'),
			    'province' => $this->input->post('province'),
			    'city' => $this->input->post('city'),
			    'status' => $this->input->post('status'),
			    'address' => $this->input->post('address'),
			    'theme' => $this->input->post('theme'),			    
			    'redirect_url' => $this->input->post('redirect_url'),
			    'info' => $this->input->post('info'),
			);
			if ($this->streams->entries->insert_entry($entry_data, 'list', 'location')){
				$this->session->set_flashdata('success', "Success");
				redirect('admin/location');
			} else {
				$this->session->set_flashdata('error', "Failed !");
				redirect('admin/location');
			}
		}
		
		$province= array(null => "-----");
		$params = array(
				'stream'    => 'province',
    			'namespace' => 'location',
		);
		$province_list = $this->streams->entries->get_entries($params);
		if ($province_list['total']) {
			foreach ($province_list['entries'] as $item) {
				$province[$item['id']] = $item['province_name'];
			}
		}
		
		//THEME LIST
		$theme_list = $this->hotspot_theme_m->getThemeList($this->current_user->id);

		//City list
		$city_list= array(null => "-----");
		$params = array(
				'stream'    => 'city',
    			'namespace' => 'location',
		);
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			foreach ($entries['entries'] as $item) {
				$city_list[$item['id']] = $item['city_name'];
			}
		}		

		$this->template
			->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			->title(lang('hotspot:add_location'))
			->set('province',$province)
			->set('theme_list',$theme_list)			
			->set('city_list',$city_list)		
			->build('admin/location/add');    
    }

	public function delete($id = null)
	{
		role_or_die('hotspot', 'admin', 'admin/location', 'Sorry, you can\'t delete location.');
		if (! $id) {
			redirect('admin/location');
		} else {
			$entry = $this->streams->entries->get_entry($id, 'list','location');
			if (!group_has_role('hotspot', 'admin') && ($entry->merchant_id <> $this->current_user->id)) {
				$this->session->set_flashdata('error', "This location is not yours!");
				redirect('admin/location');
			}
			
			//Check equipment
			$params = array(
				'stream'    => 'list',
    			'namespace' => 'equipment',
    			'where'		=> "loc_id ='".$id."'"
			);
			$equipments = $this->streams->entries->get_entries($params);
			if ($equipments['total'] > 0) {
				$this->session->set_flashdata('error', "Please delete equipments in this location first!");
				redirect('admin/location');
			}
			
			//OK, now we can delete
			$this->streams->entries->delete_entry($id,'list','location');			
			
			$this->session->set_flashdata('success', "Location deleted!");
			redirect('admin/location');
		}
		
	}
	
	public function edit($id = null)
	{
		$this->load->model('location_m');
		$this->load->model('splash/hotspot_theme_m');
		
		if (! $id) {
			redirect('admin/location');
		} else {
		
			$loc_detail = $this->streams->entries->get_entry($id, 'list','location');
			//print_r($loc_detail);exit;

			if (!group_has_role('hotspot', 'admin') && ($loc_detail->created_by <> $this->current_user->id)) {
				$this->session->set_flashdata('error', "You don't have right or this location is not yours!");
				redirect('admin/location');
			}	
		
			$validation_rules = array(
			array(
					'field' => 'loc_name',
					'label' => 'Location Name',
					'rules' => 'trim|required|max_length[64]'
			),
			array(
					'field' => 'province',
					'label' => 'Province',
					'rules' => 'trim|required'
			),
			array(
					'field' => 'address',
					'label' => 'Address',
					'rules' => 'trim|required|max_length[64]'
			),
			array(
					'field' => 'theme',
					'label' => 'Theme',
					'rules' => 'trim|is_natural'
			),
			array(
					'field' => 'products_and_services_heading',
					'label' => 'Products and Services Heading',
					'rules' => 'trim|max_length[90]'
			)
		);
		
		$this->form_validation->set_rules($validation_rules);
		
		if ($this->form_validation->run()){			
			$entry_data = array(
			    'loc_name' => $this->input->post('loc_name'),
			    'long_lat' => $this->input->post('long_lat'),
			    'province' => $this->input->post('province'),
			    'city' => $this->input->post('city'),
			    'status' => $this->input->post('status'),
			    'address' => $this->input->post('address'),
			    'theme' => $this->input->post('theme'),			    
			    'redirect_url' => $this->input->post('redirect_url'),
			    'products_and_services_heading' => $this->input->post('products_and_services_heading'),
			    'info' => $this->input->post('info')			    
			);

			if ($this->streams->entries->update_entry($this->input->post('id'),$entry_data, 'list', 'location')){
				
				//Todo: Delete location detail cache for each MAC address
				$this->pyrocache->delete_cache("location_m", "get_detail", array( (int) $this->input->post('id')));
				
				$this->session->set_flashdata('success', "Update Success");
				redirect('admin/location');
			} else {
				$this->session->set_flashdata('error', "Failed !");
				redirect('admin/location');
			}
		}
		
		
		$province= array(null => "-----");
		$params = array(
				'stream'    => 'province',
    			'namespace' => 'location',
		);
		$province_list = $this->streams->entries->get_entries($params);
		if ($province_list['total']) {
			foreach ($province_list['entries'] as $item) {
				$province[$item['id']] = $item['province_name'];
			}
		}
		
		
		
		//City list
		$city_list= array(null => "-----");
		$params = array(
				'stream'    => 'city',
    			'namespace' => 'location',
		);
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			foreach ($entries['entries'] as $item) {
				$city_list[$item['id']] = $item['city_name'];
			}
		}
		
		$theme_list = $this->hotspot_theme_m->getThemeList($this->current_user->id);

		$this->template
			->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			->title(lang('location:edit_location'))
			->set('province',$province)
			->set('theme_list',$theme_list)			
			->set('city_list',$city_list)
			->set('loc_detail',$loc_detail)
			->build('admin/location/edit'); 
			
			
		}
	}

	public function settings($id='')
	{
		$this->session->set_flashdata('error', "in developemt...");
		redirect('admin/location');
	}

	public function fields()
	{
		
	}	

}