<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin_province extends Admin_Controller
{
	protected $section = 'province';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('location');
		$this->load->driver('Streams');
        role_or_die('hotspot', 'admin', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all billing plans
     */
    public function index()
    {
        $extra = array();
        $extra['title'] = 'Province List';

        $extra['buttons'] = array(
            array(
                'label' => lang('global:edit'),
                'url' => 'admin/location/province/edit/-entry_id-'
            ),
            array(
                'label' => lang('global:delete'),
                'url' => 'admin/location/province/delete/-entry_id-',
                'confirm' => true
            )
        );

        $this->streams->cp->entries_table('province', 'location', 15, 'admin/location/province/index', true, $extra);
    }
    

	public function add()
	{
		$extra = array(
		    'return'            => 'admin/location/province',
		    'success_message'   => lang('location:submit_success'),
		    'failure_message'   => lang('location:submit_failure'),
		    'title'             => 'Add province'
		);

		$this->streams->cp->entry_form('province', 'location', 'new', null, true, $extra);	
		
	}

	public function delete($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/location/province');			
		}	
		
		//ok, now we delete
		$this->streams->entries->delete_entry($id, 'province', 'location');
		$this->session->set_flashdata('success', "Deleted!");
		redirect('admin/location/province');		
		
	}
	
	public function edit($id = null)
	{
		if (! $id) {
			$this->session->set_flashdata('error', "Failed!");
			redirect('admin/location/province');			
		}
		
		//ok, now we edit
		$extra = array(
		    'return'            => 'admin/location/province',
		    'success_message'   => lang('hotspot:submit_success'),
		    'failure_message'   => lang('hotspot:submit_error'),
		    'title'             => 'Edit province'
		);
		$this->streams->cp->entry_form('province', 'location', 'edit', $id, true, $extra);
			
		
	}
	

}