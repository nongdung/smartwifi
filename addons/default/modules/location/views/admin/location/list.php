<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('location:location_list');?></h4>
</section>

<section class="item">
    <div class="content">
    	<?php if ($entries['total']):?>
    		<table>
				<thead>
					<tr>
						<th>#</th>
						<th><?php echo lang('location:location_name'); ?></th>
						<th><?php echo lang('location:address'); ?></th>
						<th><?php echo lang('location:province'); ?></th>
						<th>Theme</th>
						<th><?php echo lang('location:status'); ?></th>
						<?php if (group_has_role('hotspot', 'admin')):?>
							<th>Created_by</th>
						<?php endif;?>
						<th></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="8">
							<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php $i=1;?>
					<?php foreach( $entries['entries'] as $entry ): ?>
					<tr>
						<td><?php echo $i++; ?></td>
						<td><?php echo $entry['loc_name']; ?></td>
						<td><?php echo $entry['address']; ?></td>
						<td><?php echo $entry['province']['province_name']; ?></td>
						<td>
							<?php
								if ($entry['theme']) { 
									foreach ($full_theme_list as $theme_tmp) {
										if ($theme_tmp['id'] == $entry['theme']) {
											echo $theme_tmp['name'];
										}
									}								
								} else {
									echo "---";
								}
							?>
						</td>
						<td><?php if ($entry['status']){echo '<span style="color:blue;">Enable</span>';} else {echo '<span style="color:red;">Disable</span>';} ?></td>
							
						<?php if (group_has_role('hotspot', 'admin')):?>
							<td><?php echo $entry['created_by']['email'];?></td>
						<?php endif;?>
					
						<td>						

							<ul class="menu">
							    <li>
							        <a class="btn gray"><?php echo lang('location:action');?> &nbsp;<span class="arrow-down"></span> &nbsp;</a>
							        <ul>
							        	<li><?php echo anchor('admin/splash/themes/select/'.$entry['id'], lang('location:select_theme'), 'class="button"');?></li>
							        	
							        	<?php if ($entry['theme']):?>
							        	<li>							        		
							        		<?php
							        			foreach ($full_theme_list as $theme) {
													if ($theme['id'] == $entry['theme']) {
														echo anchor('admin/'. $theme['theme_slug'] . "/".$entry['id'], lang('location:theme_detail'), 'class="button"');
													}
												} 
							        			
							        		?>
							        	</li>
							        	<?php endif;?>
							        	
							            <li><?php echo anchor('admin/splash/methods/'.$entry['id'], lang('location:login_methods'), 'class="button"');?></li>
							            <li><?php echo anchor('admin/devices/location/'.$entry['id'], lang('location:devices'), 'class="button"');?></li>
							            <li><?php echo anchor('admin/location/edit/'.$entry['id'], lang('global:edit'), 'class="button"');?></li>
							            <li><?php echo anchor('admin/location/delete/'.$entry['id'], 	lang('global:delete'), array('class'=>'button confirm'));?></li>
							        </ul>
							    </li>
							</ul>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
    	<?php else:?>
    		<div class="no_data"><?php echo lang('location:no_items'); ?></div>
    	<?php endif;?>
    </div>

</section>
<script>
$(document).ready(function() {
    $('.menu').dropit();
});
</script>