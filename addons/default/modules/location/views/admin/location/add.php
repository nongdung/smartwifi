<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('location:add_location');?></h4>
</section>

<section class="item">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		<div class="form_inputs">
			<div class="one_full">
				<ul>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="loc_name"><?php echo lang('location:location_name');?><span>*</span></label>
						<div class="input"><?php echo form_input('loc_name', ''); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="long_lat"><?php echo lang('location:long_lat');?></label>
						<div class="input"><?php echo form_input('long_lat', ''); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="province"><?php echo lang('location:province');?><span>*</span></label>
						<div class="input"><?php echo form_dropdown('province', $province); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
							<label for="city"><?php echo lang('location:city');?></label>
							<div class="input"><?php echo form_dropdown('city', $city_list); ?></div>
						</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="address"><?php echo lang('location:address');?><span>*</span></label>
						<div class="input"><?php echo form_input('address', '','style="width:350px;"'); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="status"><?php echo lang('location:status');?><span>*</span></label>
						<div class="input"><?php echo form_dropdown('status', array('0'=>"Off",'1'=>"On"), '1'); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="theme"><?php echo lang('location:theme');?><span>*</span></label>
						<div class="input"><?php echo form_dropdown('theme', $theme_list, '0'); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="redirect_url"><?php echo lang('location:redirect_url');?><span>*</span></label>
						<div class="input"><?php echo form_input('redirect_url', '','style="width:350px;"'); ?></div>
					</li>
				</ul>
			</div>
			<div class="one_full">
				<ul>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="products_and_services_heading"><?php echo lang('location:products_and_services_heading');?></label>
						<div class="input"><?php echo form_input('products_and_services_heading', 'Menu'); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="info"><?php echo lang('location:info');?></label>
						<div class="input"><?php echo form_textarea('info', '','class="wysiwyg-simple one-half"'); ?></div>
					</li>
					
				</ul>
			</div>
		</div>
		<div class="one_full">
			<div class="buttons">
				<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
			</div>
		</div>
	<?php echo form_close(); ?>
</section>