<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('location:edit_location');?></h4>
</section>

<section class="item">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		<?php echo form_hidden('id',$loc_detail->id);?>
		<div class="form_inputs">
			<div class="one_full">
				<ul>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="loc_name"><?php echo lang('location:location_name');?><span>*</span></label>
						<div class="input"><?php echo form_input('loc_name', $loc_detail->loc_name); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="long_lat"><?php echo lang('location:long_lat');?></label>
						<div class="input"><?php echo form_input('long_lat', $loc_detail->long_lat); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="province"><?php echo lang('location:province');?><span>*</span></label>
						<div class="input"><?php echo form_dropdown('province', $province, $loc_detail->province); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
							<label for="city"><?php echo lang('location:city');?></label>
							<div class="input"><?php echo form_dropdown('city', $city_list, $loc_detail->city); ?></div>
						</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="address"><?php echo lang('location:address');?><span>*</span></label>
						<div class="input"><?php echo form_input('address', $loc_detail->address); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="status"><?php echo lang('location:status');?><span>*</span></label>
						<div class="input"><?php echo form_dropdown('status', array('0'=>"Disable",'1'=>"Enable"), $loc_detail->status); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="theme"><?php echo lang('location:theme');?><span>*</span></label>
						<div class="input"><?php echo form_dropdown('theme', $theme_list, $loc_detail->theme); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="redirect_url"><?php echo lang('location:redirect_url');?></label>
						<div class="input"><?php echo form_input('redirect_url', isset($loc_detail->redirect_url)?$loc_detail->redirect_url:"",'style="width:350px;"'); ?></div>
					</li>
				</ul>
			</div>			
			
			<div class="one_full">
				<ul>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="products_and_services_heading"><?php echo lang('location:products_and_services_heading');?></label>
						<div class="input"><?php echo form_input('products_and_services_heading', isset($loc_detail->products_and_services_heading)?$loc_detail->products_and_services_heading:''); ?></div>
					</li>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="info"><?php echo lang('location:info');?></label>
						<div class="input"><?php echo form_textarea('info', $loc_detail->info,'class="wysiwyg-simple one-half"'); ?></div>
					</li>				
				</ul>
			</div>
		</div>
		<div class="one_full">
			<div class="buttons">
				<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
			</div>
		</div>
	<?php echo form_close(); ?>
</section>