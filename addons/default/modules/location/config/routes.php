<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	www.your-site.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://www.codeigniter.com/user_guide/general/routing.html
*/

// front-end
//$route['sample(/:num)?']			= 'sample/index$1';
//$route['location/admin(/:num)?']       = 'admin_location/index$1';
//$route['location/admin(/:any)?']       = 'admin_location$1';
$route['location/admin/province(/:num)?']       = 'admin_province/index$1';
$route['location/admin/province(/:any)?']       = 'admin_province$1';
$route['location/admin/city(/:num)?']       = 'admin_city/index$1';
$route['location/admin/city(/:any)?']       = 'admin_city$1';
$route['location/admin/fields(/:num)?']       = 'admin_fields/index$1';
$route['location/admin/fields(/:any)?']       = 'admin_fields$1';