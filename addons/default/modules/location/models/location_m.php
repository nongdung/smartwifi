<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Location_m extends MY_Model {
	protected $_table = 'loc_list';
	function __construct()
		{
        // Call the Model constructor
        parent::__construct();
			$this->load->driver('Streams');
    	}
		
	function get_detail($loc_id){
        
		$params = array(
		    'stream'    => 'list',
		    'namespace' => 'location',
		    'where'		=> "`id` = '".$loc_id."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			return $entries['entries'][0];
		}
        return array();
    }
	
	public function get_template_list($template_type, $theme_type, $created_by)
	{
		$params = array(
				'stream'    => 'templates',
    			'namespace' => 'splash',
    			'where'		=> "`default_hotspot_templates`.`created_by` = '".$created_by."' AND `default_hotspot_templates`.`template_type` = '".$template_type."' AND `rel_theme_id`.`theme_type` = '".$theme_type."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		$template_list= array(null => "-----");
		
		if ($entries['total']) {
			foreach ($entries['entries'] as $item) {
				$template_list[$item['id']] = $item['template_name'];				
			}
		}
		
		return $template_list;
	}
	
	function checkLocationOwner($location_id, $owner_id)
	{
		$params = array(
				'stream'    => 'list',
    			'namespace' => 'location',
    			'where'		=> "`id` = '".$location_id."' AND `created_by` = '".$owner_id."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		
		return $entries['total'];
	}
	
	/*
	 * Get list of location forselect in filter
	 * 
	 */
	
	public function get_location_list($merchant_id = null)
	{
		$location_list= array(null => "-- All --");
		$params = array(
				'stream'    => 'list',
    			'namespace' => 'location',
    			'where'		=> "`created_by` = '".$merchant_id."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			foreach ($entries['entries'] as $item) {
				$location_list[$item['id']] = $item['loc_name'];				
			}
		}
		
		return $location_list;
	}
}