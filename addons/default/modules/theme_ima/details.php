<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Theme_ima extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Theme IMA'
			),
			'description' => array(
				'en' => 'Hotspot theme build with IMAges'
			),
			'frontend' => FALSE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',
			'skip_xss' => TRUE	
		);

		if (group_has_role('hotspot', 'admin')) {			
			$info['sections'] ['theme_admin_fields'] = array(
				'name'	=> "Fields",
				'uri'	=> "admin/theme_ima/fields",
				'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'global:add',
							'uri' 	=> 'admin/theme_ima/fields/add',
							'class' => 'add'
							)
					)
			);		
		}							
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		$stream_check = $this->db
								->where('stream_slug','theme_ima')
								->where('stream_namespace', 'theme_ima')
								->limit(1)
								->get('data_streams');
		if ($stream_check->num_rows() > 0) { return FALSE;}
		
		$add_theme_stream = $this->streams->streams->add_stream("Theme IMA", 'theme_ima', 'theme_ima', null, null);
		if ($add_theme_stream) {
			//add theme info to main table
			$entry_data = array(
				'name' => "IMA theme",
				'theme_slug'   => 'theme_ima',
				'stream_slug'   => 'theme_ima',
				'namespace_slug'   => 'theme_ima',
			);
			$result = $this->streams->entries->insert_entry($entry_data, 'themes', 'splash');
			if (! $result) {
				$this->streams->streams->delete_stream('theme_ima', 'theme_ima');
				return FALSE;
			}
			//add fields for this theme
			$fields = array(
	            array(
	                'name' => 'Location_id',
	                'slug' => 'location_id',
	                'namespace' => 'theme_ima',
	                'type' => 'integer',
	                'extra' => array('max_length' => 11),
	                'assign' => 'theme_ima',
	                'locked' => TRUE           
	            )
			);
			$this->streams->fields->add_fields($fields);
			
		} else {
			return FALSE;
		}
		
		//Settings
		$settings = array(
			array(
				'slug' => 'theme_ima_general_fields',
				'title' => 'General Fields',
				'description' => 'Define general fields for input',
				'type' => 'text',
				'default' => "",
				'value' => '',
				'options' => '',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => 'theme_ima',
				'order' => 10,
			),
			array(
				'slug' => 'theme_ima_mobile_fields',
				'title' => 'Mobile Fields',
				'description' => 'Define mobile fields for input',
				'type' => 'text',
				'default' => "",
				'value' => '',
				'options' => '',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => 'theme_ima',
				'order' => 9,
			),
			array(
				'slug' => 'theme_ima_desktop_fields',
				'title' => 'Desktop Fields',
				'description' => 'Define desktop fields for input',
				'type' => 'text',
				'default' => "",
				'value' => '',
				'options' => '',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => 'theme_ima',
				'order' => 8,
			),		
		);

		foreach ($settings as $setting)
		{
			if ( ! $this->db->insert('settings', $setting))
			{
				return false;
			}
		}
				
		return TRUE;		
	}

	public function uninstall()
	{
		$this->load->driver('Streams');
        $this->streams->utilities->remove_namespace('theme_ima');
		$this->db->delete('settings', array('module' => 'theme_ima'));		
		return TRUE;
	}


	public function upgrade($old_version)
	{		
		
		return TRUE;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
    
    
    public function admin_menu(&$menu)
    {	

	}
}
/* End of file details.php */
