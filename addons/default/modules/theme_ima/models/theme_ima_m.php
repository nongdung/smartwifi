<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Theme_ima_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		$module_name = "theme_ima";		
		$dir = ADDONPATH.'modules/';		
		// Check shared addons
	    if (is_dir(SHARED_ADDONPATH.'modules/'.$module_name)) {
	        $dir = SHARED_ADDONPATH.'modules/';
	    } elseif ( ! is_dir($dir.$module_name)) {
	        $core_path = defined('PYROPATH') ? PYROPATH : APPPATH;
	        $dir = $core_path.'modules';
	    }
		
		Asset::add_path($module_name, $dir.$module_name."/");		
	}
	
	public function get($location_id)
	{
		return $this->db
			->select('theme_ima.*')
			->where('theme_ima.location_id', $location_id)
			->limit(1)
			->get('theme_ima')
			->row();
	}
	
	public function set_prelogin_assets($device_type='')
	{
		Asset::add_path('jquery', 'assets/jquery/');
		Asset::add_path('smartwifi', 'assets/smartwifi/');
		
		Asset::js('jquery::jquery-1.11.3.min.js');
		Asset::js('jquery::jquery.backstretch.min.js');
		Asset::js('smartwifi::modernizr-custom.min.js');
		Asset::js('smartwifi::swController.js', FALSE, 'smartwifi');
		Asset::js('smartwifi::prelogin.js', FALSE, 'smartwifi');
	}
	
	public function set_assets($device_type)
	{
		Asset::add_path('bootstrap-3.3.5', 'assets/bootstrap-3.3.5/');
		Asset::add_path('jquery', 'assets/jquery/');
		Asset::add_path('font-awesome', 'assets/font-awesome/');
		Asset::add_path('azm', 'assets/azm-social/');
		
		Asset::css('azm::azm-social.css');
		Asset::css('font-awesome::font-awesome.min.css');
		if ($device_type == "desktop") {
			Asset::css('theme_ima::desktop.css');
		} else {
			Asset::css('theme_ima::mobile.css');
		}
		Asset::js('jquery::readmore.min.js', FALSE, 'smartwifi');
		
	}
	
	public function get_info()
	{
		$data = array(
			'platform' => 'bootstrap-3.3.5',
			'jquery' => TRUE,
			'layout'	=> 'hotspot/bootstrap-3.3.5.html'
		);
		return $data;
	}
	
	public function get_theme_stream()
	{
		$params = array(
		    'stream'    => 'themes',
		    'namespace' => 'splash',
		    'where'		=> "`stream_slug` ='theme_ima' AND `namespace_slug` = 'theme_ima'"
		);		
		return $this->streams->entries->get_entries($params);
	}
	
	public function get_theme_detail($created_by = 0, $location_id = '')
	{
		$params = array(
		    'stream'    => 'theme_ima',
		    'namespace' => 'theme_ima',
		    'where'		=> "`created_by` ='".$created_by."' AND `location_id` = '".$location_id."'"
		);		
		return $this->streams->entries->get_entries($params);
	}
}
