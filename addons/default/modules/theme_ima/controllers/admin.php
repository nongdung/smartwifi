<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('theme_ima');
		$this->load->driver('Streams');
		role_or_die('hotspot', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all billing plans
     */
    public function index($location_id = null)
    {
    	$this->load->model('location/location_m');
    	if (! $this->location_m->checkLocationOwner($location_id,$this->current_user->id)) {
			$this->session->set_flashdata('notice', "You don't have right to edit that location");
			redirect('admin/location');
		}
		
    	$params = array(
		    'stream'    => 'theme_ima',
		    'namespace' => 'theme_ima',
		    'where'		=> "`created_by` ='".$this->current_user->id."' AND `location_id` = '".$location_id."'"
		);
		
		$entries = $this->streams->entries->get_entries($params);
		
		$location_detail = $this->location_m->get_detail($location_id);
    	$this->template
    				->set('title', lang('theme_ima:overview'))
					->set('entries', $entries['entries'])
					->set('location_detail',$location_detail)
    				->build('admin/overview');
    }
	
	function edit($location_id)
    {
    	//$this->load->model('theme_fields_m');
    	//$skip_fields = $this->theme_fields_m->get_skip_fields('general_fields');
		//print_r($skip_fields);exit;
		//Get theme id
		
		$params = array(
		    'stream'    => 'themes',
		    'namespace' => 'splash',
		    'where'		=> "`stream_slug` ='theme_ima' AND `namespace_slug` = 'theme_ima'"
		);		
		$theme = $this->streams->entries->get_entries($params);
		if (! $theme['total']) {
			redirect("admin");
		}

		$skip_fields=array();
    	$params = array(
		    'stream'    => 'theme_ima',
		    'namespace' => 'theme_ima',
		    'where'		=> "`created_by` ='".$this->current_user->id."' AND `location_id` = '".$location_id."'"
		);		
		$entries = $this->streams->entries->get_entries($params);
		//print_r($entries);exit;
		$extra = array(
			    'return'            => 'admin/theme_ima/'.$location_id,
			    'success_message'   => lang('hotspot:submit_success'),
			    'failure_message'   => lang('hotspot:submit_error'),
			    'title'             => 'Edit Fields'
		);
		
		$tabs = array(
		    array(
		        'title'     => "General fields",
		        'id'        => 'general-tab',
		        'fields'    => explode(",","location_id,".Settings::get('theme_ima_general_fields'))
		    ),
		    array(
		        'title'     => "Desktop fields",
		        'id'        => 'desktop-tab',
		        'fields'    => explode(",",Settings::get('theme_ima_desktop_fields'))
		    ),
		    array(
		        'title'     => "Mobile fields",
		        'id'        => 'mobile-tab',
		        'fields'    => explode(",",Settings::get('theme_ima_mobile_fields'))
		    ),
		    array(
		        'title'     => "Custom CSS",
		        'id'        => 'css-tab',
		        'fields'    => array("custom_css")
		    )
		);
		$hiddens = array('location_id');
		$defaults = array('location_id' => $location_id);
		
		if ($entries['total']) {
			//delete cached
			$this->pyrocache->delete_cache("hotspot_theme_m", "getThemeDetail", array($theme['entries'][0]['id'], $location_id));
			//Edit
			$this->streams->cp->entry_form('theme_ima', 'theme_ima', 'edit', $entries['entries'][0]['id'], true, $extra, $skip_fields, $tabs,$hiddens, $defaults);
			
			
		} else {
			//add new
			$this->streams->cp->entry_form('theme_ima', 'theme_ima', 'new', null, true, $extra, $skip_fields, $tabs,$hiddens, $defaults);
		}
		
				
		//$this->template->build('admin/general');
    }  

}