<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<style>
	{{ if exists theme_detail:custom_css and theme_detail:custom_css != "" }}
		<?php 
			$custom_css = html_entity_decode($theme_detail['custom_css']);
			echo $custom_css;
		?>
	{{ endif }}
</style>