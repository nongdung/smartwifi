<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="sw-wrapper">
			<div class="masthead clearfix">
				<div class="container width-800">
					<div class="row">
							<div class="center-block pagination-centered text-center">
								<img class="img-responsive center-block" src="{{ url:site }}files/thumb/{{ theme_detail:desktop_header_image:id }}/800/auto" alt="Header"/>
							</div>
					</div>
				</div>
			</div>
			
			{{ if theme_detail:text_ads_position:val == 'Top' and {url:segments segment="2"} != "success" }}
			<div class="container width-800">
				<div class="row">
					<div class="col-xs-12" style="text-align: left">
						{{ theme_detail:text_ads }}
					</div>		
				</div>	
			</div>
			{{ endif }}
			
