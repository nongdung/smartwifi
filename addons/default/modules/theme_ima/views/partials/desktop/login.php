<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

{{ if showAds }}
	<div class="container width-960" style="padding-top:15px;">
		<div class="row">
				<div class="col-sm-12 text-center">
					<script type='text/javascript'><!--//<![CDATA[
					   var m3_u = (location.protocol=='https:'?'https://ad.smartwifi.vn/www/delivery/ajs.php':'http://ad.smartwifi.vn/www/delivery/ajs.php');
					   var m3_r = Math.floor(Math.random()*99999999999);
					   if (!document.MAX_used) document.MAX_used = ',';
					   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
					   document.write ("?zoneid=5");
					   document.write ('&amp;cb=' + m3_r);
					   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
					   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
					   document.write ("&amp;loc=" + escape(window.location));
					   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
					   if (document.context) document.write ("&context=" + escape(document.context));
					   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
					   document.write ("'><\/scr"+"ipt>");
					//]]>--></script><noscript><a href='http://ad.smartwifi.vn/www/delivery/ck.php?n=a5e76545&amp;cb=<?php echo mt_rand();?>' target='_blank'><img src='http://ad.smartwifi.vn/www/delivery/avw.php?zoneid=5&amp;cb=<?php echo mt_rand();?>&amp;n=a5e76545' border='0' alt='' /></a></noscript>
				</div>
		</div>
	</div>
{{ endif }}

{{ if exists theme_detail:login_message and theme_detail:login_message != "" }}
	<div class="container width-960">
		<div class="row">
				<div class="col-sm-12 text-center">
					<span id="c-name">
						<?php if (!empty($client_info)) echo "<i>Hi ". $client_info['name']."</i>"; ?>
					</span>
					<br/>
					{{ theme_detail:login_message }}
				</div>
		</div>	
	</div>		
{{ endif }}

<div class="container width-960 wifi-login-area" style="padding-top:15px;">
	<div class="row">
		<?php foreach ($wifi_method_position as $method => $value) :?>
			<?php if ($method == 'youtube'):?>
				<div class="col-xs-12 col-sm-12 col-md-12">
				<?php echo $wifi_login_html[$method]['html']; ?>
				</div>
			<?php endif;?>
		<?php endforeach;?>
			<div class="col-xs-2 col-sm-2 col-md-3"></div>
			<div class="col-xs-8 col-sm-8 col-md-6">				
				<?php foreach ($wifi_method_position as $method => $value) :?>
					<?php if ($method == 'modpro'):?>
						<?php echo $wifi_login_html[$method]['html']; ?>
					<?php endif;?>
					
					<?php if ($method == 'facebook_js'):?>
						<a style="background:<?php echo $wifi_login_html[$method]['settings']['button_color'];?>;" href="<?php echo $wifi_login_html[$method]['link']; ?>" class="login-link btn btn-block azm-social azm-btn azm-btn-mobile">
							<i class="fa fa-facebook fa-55 pull-left"></i>
							<?php echo $wifi_login_html[$method]['caption']; ?>
						</a>
					<?php endif;?>
					<?php if ($method == 'google_js'):?>
						<a style="background:<?php echo $wifi_login_html[$method]['settings']['button_color'];?>;" href="<?php echo $wifi_login_html[$method]['link']; ?>" class="login-link btn btn-block azm-social azm-btn azm-btn-mobile">
							<i class="fa fa-google fa-55 pull-left"></i>
							<?php echo $wifi_login_html[$method]['caption']; ?>
						</a>
					<?php endif;?>
					<?php if ($method == 'click'):?>
						<div style="padding-top: 15px; padding-bottom: 5px;">
							<?php if (isset($wifi_login_html[$method]['heading']) && $wifi_login_html[$method]['heading'] != ''):?>
							<div class="login-deading" style="width: 100%;">
								<i class="fa fa-55"></i>
								<i><?php echo $wifi_login_html[$method]['heading'];?></i>
							</div>
							<?php endif;?>
							
							<a style="background:<?php echo $wifi_login_html[$method]['settings']['button_color'];?>;" href="<?php echo $wifi_login_html[$method]['link']; ?>" class="login-link btn btn-block azm-social azm-btn-mobile">
								<i class="fa fa-wifi fa-55 pull-left"></i>
								<?php echo $wifi_login_html[$method]['caption']; ?>
							</a>
						</div>
					<?php endif;?>
					<?php //echo $wifi_login_html[$method]['html'];?>
				<?php endforeach;?>
				
			</div>
			<div class="col-xs-2 col-sm-2 col-md-3"></div>
	</div>
</div>
<?php //print_r($theme_detail);?>
<script>
	$('.wifi-login-link').click(function(e) {
		var my = $(e.target);
		my.html('Redirecting...');
		//$('#sw-status-wait').fadeIn();
	});
	
	var swUser = {};
	$(document).ready(function(){
		$.ajaxSetup({
        	data: {
            	csrf_hash_name: "<?php echo $this->security->get_csrf_hash(); ?>"
        	}
    	});
    	
    	if (!client_info){
    		//Create anonymous profile
			var userdata = {
				'client_mac' : "<?php echo $this->session->userdata('mac')?>",																
			};
								
			$.post( "husers/apis/create_anonymous_profile", userdata, function( response ) {
				swUser = response;
				console.log(JSON.stringify(response));
			});
    	}
    	
    	/*   		
		$.post( "husers/apis/get_client", {client_mac : "<?php echo $this->session->userdata('mac');?>"}, function( jsonp ) {
			if (!jsonp.error) {
				swUser = jsonp;
				if (jsonp.name) {
					//$('#c-name').html("Hi " + jsonp.name + "!<br/>");
				} else {
					//$('#c-name').html("");
				}
				if (!jsonp || !jsonp.profile_id) {
					//Create anonymous profile
					userdata = {
						'client_mac' : "<?php echo $this->session->userdata('mac')?>",																
					};
								
					$.post( "husers/apis/create_anonymous_profile", userdata, function( response ) {
						swUser = response;
						console.log(JSON.stringify(response));
					});
				}
			} else {
				$('#c-name').html();
				$('#error-msg').html("Error:" + jsonp.error.message);
				console.log(JSON.stringify(jsonp));
			}
		});

		*/
		
    });
    
    $('#agreement').bind("change",function(){
    	if (document.getElementById('agreement').checked) {
            $('#error-msg').html("");
        }
    });

</script>
