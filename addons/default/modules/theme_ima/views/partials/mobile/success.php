<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

{{ if showAds }}
	<div class="container" style="padding-top:15px;">
		<div class="row">
				<div class="col-xs-12 text-center">
					<script type='text/javascript'><!--//<![CDATA[
					   var m3_u = (location.protocol=='https:'?'https://ad.smartwifi.vn/www/delivery/ajs.php':'http://ad.smartwifi.vn/www/delivery/ajs.php');
					   var m3_r = Math.floor(Math.random()*99999999999);
					   if (!document.MAX_used) document.MAX_used = ',';
					   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
					   document.write ("?zoneid=5");
					   document.write ('&amp;cb=' + m3_r);
					   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
					   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
					   document.write ("&amp;loc=" + escape(window.location));
					   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
					   if (document.context) document.write ("&context=" + escape(document.context));
					   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
					   document.write ("'><\/scr"+"ipt>");
					//]]>--></script><noscript><a href='http://ad.smartwifi.vn/www/delivery/ck.php?n=a5e76545&amp;cb=<?php echo mt_rand();?>' target='_blank'><img src='http://ad.smartwifi.vn/www/delivery/avw.php?zoneid=5&amp;cb=<?php echo mt_rand();?>&amp;n=a5e76545' border='0' alt='' /></a></noscript>
				</div>
		</div>
	</div>
{{ endif }}

{{ if exists theme_detail:success_message and theme_detail:success_message != "" }}
	<div class="container" style="padding-top:15px;">
		<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-primary">
					  <div class="panel-body">
					  	{{ theme_detail:success_message }}
					  	
					  	{{ if merchant_fb_settings:page_id != "" }}
						  	<div class="text-center center-block">
						  		<br/>
						  		<div class="fb-page" data-width="800" data-href="https://www.facebook.com/<?php echo $merchant_fb_settings['page_id'];?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"></div>
						  	</div>
					  	{{ endif }}
					  </div>
					</div>
				</div>
		</div>
	</div>	
{{ endif }}

{{ if exists location_detail:hasCouponCampaign and location_detail:hasCouponCampaign == "1" }}

<div id="coupon-container" class="container" style="padding-bottom: 15px;padding-top: 15px;display: none;">
	<div class="row">
		<div class="col-xs-12">
					<div id="coupon-content"></div>
					<div id="coupon-error" class="text-danger"></div>
					<div id="coupon-wait" style="display:none; margin:0 auto;" class="clearfix">
						{{ theme:image file="wait.gif" width="30" height="30" alt="Please wait" style="margin: 0 auto;" class="img-rounded center-block" }}
					</div>
		</div>
	</div>
</div>

{{ endif }}
