<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="sw-wrapper">
	<div class="masthead clearfix">
		<div class="container">
			<div class="row">
					<div class="center-block pagination-centered text-center">
						<div id="carousel-generic" class="carousel slide" data-ride="carousel" data-interval="5000">

							<!-- Wrapper for slides -->
							<div class="carousel-inner">
								<div class="active item">			
									<img class="img-responsive center-block" src="{{ url:site }}files/thumb/{{ theme_detail:mobile_header_image:id }}/1024/auto" alt="Header1"/>
								</div>
								
								{{ if exists theme_detail:mobile_header_image_2:id }}
								<div class="item">			
									<img class="img-responsive center-block" src="{{ url:site }}files/thumb/{{ theme_detail:mobile_header_image_2:id }}/1024/auto" alt="Header1"/>
								</div>
								{{ endif }}
								
								{{ if exists theme_detail:mobile_header_image_3:id }}
								<div class="item">			
									<img class="img-responsive center-block" src="{{ url:site }}files/thumb/{{ theme_detail:mobile_header_image_3:id }}/1024/auto" alt="Header1"/>
								</div>
								{{ endif }}
						
							</div>
						
						</div>
					</div>
			</div>
		</div>
	</div>
	
	{{ if theme_detail:text_ads_position:val == 'Top' and {url:segments segment="2"} != "success" }}
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-ads" style="text-align: left">
				{{ theme_detail:text_ads }}
			</div>		
		</div>	
	</div>
	{{ endif }}
