<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

	{{ if exists theme_detail:text_ads and theme_detail:text_ads_position:val == 'Bottom' and {url:segments segment="2"} != "success" }}
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-ads" style="text-align: left">
					{{ theme_detail:text_ads }}
			</div>						
		</div>	
	</div>
	{{ endif }}
	<div class="push"></div>
	
</div>

<div class="mastfoot" id="footer">
	<div class="container">
			<div class="row">
					<div class="center-block pagination-centered text-center">
						<img class="img-responsive center-block" src="{{ url:site }}files/thumb/{{ theme_detail:mobile_footer_image:id }}/1024/auto" alt="Footer"/>
					</div>
			</div>
	</div>
	
</div>
<script>
	$('.text-ads').readmore({
		speed: 500,
		collapsedHeight:95,
		moreLink: '<a href="#" class="readmore" style="text-align:right;padding-right:10px;">... Đọc thêm</a>',
		lessLink: '<a href="#" class="readmore" style="text-align:right;padding-right:10px;">Close</a>'
	});
	
	// Window load event used just in case window height is dependant upon images
	$(window).bind("load", function() { 
	       
	       var footerHeight = 0,
	           footerTop = 0,
	           $footer = $("#footer");
	           
	       positionFooter();
	       
	       function positionFooter() {
	       
	                footerHeight = $footer.height();
	                footerTop = ($(window).scrollTop()+$(window).height()-footerHeight)+"px";
	       
	               if ( ($(document.body).height()+footerHeight) < $(window).height()) {
	                   $footer.css({
	                        position: "absolute"
	                   }).animate({
	                        top: footerTop
	                   })
	               } else {
	                   $footer.css({
	                        position: "static"
	                   })
	               }
	               
	       }
	
	       $(window)
	               .scroll(positionFooter)
	               .resize(positionFooter)
	               
	});
</script>