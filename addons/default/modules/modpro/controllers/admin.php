<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin extends Admin_Controller
{
	protected $section = 'modpro';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('modpro');
		$this->load->driver('Streams');
    }

    /**
     * List all billing plans
     */
    public function index()
    {		
		redirect('admin/splash/methods');
    }
    

	public function settings($loc_id ='')
	{
		//Check loc_id
		$loc_detail = $this->streams->entries->get_entry($loc_id,'list','location');
		if (!$loc_detail && ($loc_detail->created_by_user_id <> $this->current_user->id)) {
			$this->session->set_flashdata('error', "This location is not yours");
			redirect('admin/location');
		}
		
		$validation_rule = array(
            array(
                'field' => 'position',
                'label' => 'Position',
                'rules' => 'trim|max_length[1]|is_natural_no_zero|required'
            ),
            array(
                'field' => 'wait_time',
                'label' => 'Waiting Time',
                'rules' => 'trim|max_length[2]|is_natural_no_zero'
            ),
            array(
                'field' => 'uamsecret',
                'label' => 'UAM Secret',
                'rules' => 'trim|min_length[8]|max_length[16]'
            ),
            array(
                'field' => 'loc_id',
                'label' => 'Location ID',
                'rules' => 'required'
            )
		);
		
		$this->form_validation->set_rules($validation_rule);
		
		if ($this->form_validation->run()){
			if ($this->input->post("id")) {
				//Update existing record
				$entry_data = array(
					'status' => $this->input->post("status"),
				    'position'   => $this->input->post("position"),
				    'caption'   => $this->input->post("caption"),
				    'wait_time'   => $this->input->post("wait_time"),
				    'uamsecret'   => $this->input->post("uamsecret"),
				);
				$this->streams->entries->update_entry($this->input->post("id"),$entry_data, 'settings', 'modpro');
			} else {
				//insert new record
				$entry_data = array(
					'status' => $this->input->post("status"),
				    'position'   => $this->input->post("position"),
					'loc_id'   => $this->input->post("loc_id"),
					'caption'   => $this->input->post("caption"),
					'wait_time'   => $this->input->post("wait_time"),
					'uamsecret'   => $this->input->post("uamsecret"),
				);
				$this->streams->entries->insert_entry($entry_data, 'settings', 'modpro');
			}
			//Delete cache file
			$this->pyrocache->delete_cache("modpro_m", "get_settings", array($this->input->post("loc_id")));
			$this->session->set_flashdata('success', "Updated");
			redirect('admin/splash/methods');
			
		}
		
		$params = array(
		    'stream'    => 'settings',
		    'namespace' => 'modpro',
		    'where'		=> "`loc_id` ='".$loc_id."'"
		);
		
		$entries = $this->streams->entries->get_entries($params);
		
		$this->template
				->title($this->module_details['name'], lang('modpro:settings'))
				->set('entries',$entries)
				->set('loc_id',$loc_id)
				->build('admin/loc_settings');
	}

}