<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	splash Module
 */
class Login extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->lang->load('wificlick');
		$this->load->model('click_m');
		$this->load->model('splash/splash_m');
		$this->load->model('splash/hotspot_theme_m');
		$this->load->model('hotspot/merchant_m');
		$this->load->model('hotspot/service_class_m');
		$this->load->model('location/location_m');
		
	}

	public function index($location_id = 0, $device_type = null)
	{
		$uamsecret = "smartwifi";

		$error_msg = "";
		$settings = $this->pyrocache->model('click_m', 'get_settings', array($location_id), 30);
		$location_detail = $this->pyrocache->model('location_m', 'get_detail', array($location_id), 30);
		
		
		$merchant_info = $this->merchant_m->get_info($location_detail['created_by']['user_id']);		
		if ($merchant_info['end_date'] < time()) {
			//Drop this location to Free class
			$service_class = $this->service_class_m->get_by_name("Free");			
		} else {
			$service_class = $this->service_class_m->get_by_id($merchant_info['class_id']);
		}
		
		//User session data
		preg_match("/iPhone|iPad|iPod|OS X|Captive/", $_SERVER['HTTP_USER_AGENT'], $matches);
		$isApple = !empty($matches);
		if ($isApple && isset($_GET['state']) && $_GET['state'] && $this->config->item('sess_use_redis')) {
			$this->load->library('splash/sw_redis');
			$user_redis_data = $this->sw_redis->get($_GET['state']);
			$this->session->set_userdata($user_redis_data);
		}
		
		$user_data = $this->session->all_userdata();		
		$res = isset($user_data['res'])?$user_data['res']:'notyet';
		$uamip = isset($user_data['uamip'])?$user_data['uamip']:'172.16.0.1';
		$uamport = isset($user_data['uamport'])?$user_data['uamport']:'3990';
		$challenge = isset($user_data['challenge'])?$user_data['challenge']:'0251f2689abd49013ba08abaea9886f6';
		$called = isset($user_data['called'])?$user_data['called']:'';
		$mac = isset($user_data['mac'])?$user_data['mac']:'';
		$ip = isset($user_data['ip'])?$user_data['ip']:'';
		$nasid = isset($user_data['nasid'])?$user_data['nasid']:'';
		$userurl = isset($user_data['userurl'])?$user_data['userurl']:'';
		
		
		
		if (!isset($user_data['res'])) {
			$error_msg .= "You are not in a wifi hotspot";
		}
		//now we log user in to wifi network
		$pappassword = $this->splash_m->get_pappassword($settings['wifi_login_account']['login_password'], $challenge, $uamsecret);		
		$login_link = "http://".$uamip.":".$uamport."/logon?username=".$settings['wifi_login_account']['login_username']."&password=".$pappassword."&userurl=".urlencode($userurl);
		
		
		//Get theme detail
		$theme = $this->hotspot_theme_m->getTheme($location_detail['theme']);
		$theme_detail = $this->hotspot_theme_m->getThemeDetail($location_detail['theme'], $location_detail['id']);
		$model = $theme['theme_slug']."_m";
		$this->load->model($theme['theme_slug']."/".$model);
		$theme_info = $this->$model->get_info();		
		$this->$model->set_assets($device_type);
		
		Asset::add_path('smartwifi', 'assets/smartwifi/');
		Asset::js('smartwifi::basic.js', FALSE, 'smartwifi');
		Asset::js('smartwifi::jquery.countdown.min.js', FALSE, 'smartwifi');
		Asset::js('smartwifi::swController.js', FALSE, 'smartwifi');
		
		$this->template
			->title("Wifi Login")
			->set('theme_slug', $theme['theme_slug'])
			->set('theme_detail', $theme_detail)			
			->set('login_link',$login_link)
			->set('settings', $settings)			
			->set('userurl', $userurl)
			->set('device_type', $device_type)
			->set('location_detail',$location_detail)			
			->set_layout($theme_info['layout'])
			->build('clicklogin');		
	}


}
