<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		NgocDung Nong - SmartWifi VN Dev Team
 * @website		http://nongdung.com
 * @package 	SmartWifi
 * @subpackage 	splash Module
 */
class Uamservice extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();
	}


	public function index()
	{
		$this->load->model('splash/uam_m');
		$this->load->model('modpro_m');
		$loc_id = $this->session->userdata("location_id");
		if ($loc_id) {
			$method_settings  = $this->pyrocache->model("modpro_m",'get_settings', array($loc_id),7200);
			//print_r($method_settings);exit;
			$uamsecret = $method_settings['uamsecret'];
		} else {
			$uamsecret = "smartwifi";
		}

    	$password = trim($_GET["password"]); 
    	$challenge = trim($_GET["challenge"]);
		$pappassword = $this->uam_m->return_new_pwd($password,$challenge,$uamsecret);
		
		header('Content-type: application/javascript');
		print "swJSON.reply({'response':'".$pappassword."'})";
	}	
}
