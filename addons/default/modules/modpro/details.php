<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Modpro extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Pro Wifi Login'
			),
			'description' => array(
				'en' => 'Wifi Login with comercial account'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',		
		);
		
		if (group_has_role('hotspot', 'merchant')) {
			$info['sections']['modpro'] = array(
					'name' 	=> 'modpro:name', // These are translated from your language file
					'uri' 	=> 'admin/modpro'
			);			
		}							
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		
		//add stream
		if ( ! $this->streams->streams->add_stream('Modpro Settings', 'settings', 'modpro', 'modpro_', null)) return false;
		$loc_stream = $this->streams->streams->get_stream('list', 'location');

		$choice = 'Enable
		Disable';
		
		$fields = array(
			array(
                'name' => 'Status',
                'slug' => 'status',
                'namespace' => 'modpro',
                'type' => 'choice',
                'extra'         => array('default_value' => 'Enable','choice_data' => $choice, 'choice_type' => 'dropdown'),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Caption',
                'slug' => 'caption',
                'namespace' => 'modpro',
                'type' => 'text',
                'extra' => array('max_length' => 64,'default_value' => "Login"),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'UAM Secret',
                'slug' => 'uamsecret',
                'namespace' => 'modpro',
                'type' => 'text',
                'extra' => array('max_length' => 16,'default_value' => "smartwifi"),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Position',
                'slug' => 'position',
                'namespace' => 'modpro',
                'type' => 'integer',
                'extra' => array('max_length' => 1,'default_value' => 1),
                'assign' => 'settings',
                'required' => true
            ),
            array(
                'name' => 'Wait Time',
                'slug' => 'wait_time',
                'namespace' => 'modpro',
                'type' => 'integer',
                'extra' => array('max_length' => 2,'default_value' => 5),
                'assign' => 'settings',
            ),
            array(
                'name' => 'Location ID',
                'slug' => 'loc_id',
                'namespace' => 'modpro',
                'type' => 'relationship',
                'extra' => array('choose_stream' => $loc_stream->id),
                'assign' => 'settings',
                'unique' => TRUE,
                'required' => true
            ),

		);

        $this->streams->fields->add_fields($fields);
		
		$entry_data = array(
		    'mod_name' => "Modpro",
		    'mod_slug'   => "modpro",
		    'mod_desc'   => "Wifi Login by commercial account",		    
		);
		$this->streams->entries->insert_entry($entry_data, 'modules', 'login_modules');			
		return TRUE;
	}

	public function uninstall()
	{
		$this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
		$this->streams->utilities->remove_namespace('modpro');
		$this->db->delete('settings', array('module' => 'modpro'));
		
		$params = array(
		    'stream'    => 'modules',
		    'namespace' => 'login_modules',
		    'where' 	=> "mod_slug = 'modpro'"
		);		
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			foreach ($entries['entries'] as $entry) {
				$this->streams->entries->delete_entry($entry["id"], "modules", "login_modules");
			}
		}
		return TRUE;
	}


	public function upgrade($old_version)
	{
		$this->load->driver('Streams');
		$fields = array(
            array(
                'name' => 'UAM Secret',
                'slug' => 'uamsecret',
                'namespace' => 'modpro',
                'type' => 'text',
                'extra' => array('max_length' => 16,'default_value' => "smartwifi"),
                'assign' => 'settings',
                'required' => true
            )
		);

        $this->streams->fields->add_fields($fields);
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
    
    
    public function admin_menu(&$menu)
    {
    	//Get merchant info
		$this->load->model('hotspot/merchant_m');
		$merchant_info = $this->pyrocache->model('merchant_m', 'get_info', $this->current_user->id, 7200);

		if (group_has_role('hotspot', 'merchant') && isset($merchant_info['class_name']) && ($merchant_info['class_name'] == "Pro")) {
			
			$menu['Commercial'] = array(
        		'Vouchers' => 'admin/modpro/vouchers',
            	'Billing Plans' => 'admin/modpro/billings'
        	);
			add_admin_menu_place('Commercial', 2);
        }	
			
	}
}
/* End of file details.php */
