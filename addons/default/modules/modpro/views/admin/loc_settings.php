<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('modpro:settings'); ?></h4>
</section>

<section class="item">
	
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		<div class="one_full">
			<?php 
				if ($entries['total'] > 0){
					echo form_hidden("id",$entries['entries'][0]['id']);	
				}
				echo form_hidden("loc_id",$loc_id);				
			?>
			
		
			<div class="form_inputs">
				<ul>
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="status">Status</label>
						<div class="input">
							<?php
							$options = array('Enable'  => 'Enable', 'Disable'    => 'Disable');							
							echo form_dropdown('status', $options, isset($entries['entries'][0]['status'])?$entries['entries'][0]['status']['val']:'Disable');
							?>
						</div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="position">Position<span>*</span></label>
						<div class="input"><?php echo form_input('position', isset($entries['entries'][0]['position'])?$entries['entries'][0]['position']:1); ?></div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="caption">Button caption<span>*</span></label>
						<div class="input"><?php echo form_input('caption', isset($entries['entries'][0]['caption'])?$entries['entries'][0]['caption']:"Login"); ?></div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="caption">Waiting Time</label>
						<div class="input"><?php echo form_input('wait_time', isset($entries['entries'][0]['wait_time'])?$entries['entries'][0]['wait_time']:"5"); ?></div>
					</li>
					
					<li class="<?php echo alternator('', 'even'); ?>">
						<label for="caption">UAM Secret<span>*</span></label>
						<div class="input"><?php echo form_input('uamsecret', isset($entries['entries'][0]['uamsecret'])?$entries['entries'][0]['uamsecret']:"smartwifi"); ?></div>
					</li>
					
				</ul>
			</div>
		</div>
		<div class="one_full">
			<div class="buttons">
				<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save') )); ?>
				
				<?php echo anchor("admin/splash/methods","Cancel",'class = "btn gray cancel"');?>
			</div>
		</div>
		<?php echo form_close(); ?>
</section>