<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

<div class="pure-form pure-form-stacked wifi-login-form" id="modpro-form">
	<fieldset>
		<legend><h3>Internet/Wifi Login</h3></legend>
		<div class="sw-control-group">
		<label for="modpro-username">Username</label>
        <input id="modpro-username" type="text" placeholder="Your Username">

        <label for="modpro-password">Password</label>
        <input id="modpro-password" type="password" placeholder="Your Password">
		</div>

        <input type="checkbox" id="modpro-remember" name="remember" value="1">Remember me<br />
		
        <button type="submit" class="pure-button pure-button-primary wifi-login-link" onclick="modproLogin();"><?php if (isset($settings['caption']) && $settings['caption']) {echo $settings['caption'];} else {echo "Login";}?></button>
		<div id="modpro-msg"></div>
		<p>Don't have an account? <a href="#">Register here</a> | <a href="#">Forgot password</a></p>
		
	</fieldset>
</div>

<div id="statusPage" class="wifi-status" style="display:none;">			
	<div class="text-center">
		Well done, you are now connected to Internet.
		<div id="originalURL"></div><br/>
		<span id="statusMessage"></span>
		<div class="text-center" style="padding-bottom:5px;">
			<i class="fa fa-shopping-cart fa-lg"></i>				
			<a href="http://1.0.0.0" class="pure-button" style="background: rgb(223, 117, 20);">Wifi Logout</a>
		</div>
	</div>	
</div>
							
<script>
	if (window.location.hostname == "localhost") {
		swController.uamService = window.location.protocol+'//'+window.location.hostname + "/~ngocdungnong/smartwifi/modpro/uamservice";
	} else{
		swController.uamService = window.location.protocol+'//'+window.location.hostname + "/modpro/uamservice";
	};
	
	function modproLogin () {
		
		var username = $("#modpro-username").val();
		var password = $("#modpro-password").val();
		if ($('#modpro-remember').is(":checked")) {
			if (username && password){
				SmartWifi.eraseCookie("sw_modpro_account");
				SmartWifi && SmartWifi.createCookie("sw_modpro_account", username + "-" + password, 168);
			} else {
				$("#modpro-msg").html('<span style="color:red;">Username/Password required!</span>');
			}
		}else{
			SmartWifi.eraseCookie("sw_modpro_account");
		}
		if (username && password ){
			swController.onUpdate = SmartWifi.updateUI;
			swController.onError = SmartWifi.handleError;	
			if (swController.clientState == 0) {
				$("#modpro-msg").html("");
				$(".wifi-login-link").html("Loggingin...");
				if (typeof woopraTracker === 'object') {
					woopra.track("Modpro Login", {
		        			Type: "Attempt"
					});
				}
				
				if (typeof _paq === 'object') {
					_paq.push(['trackEvent', 'Wifi', 'Modpro Attempt', 'Click']);
				}

				setTimeout('swController.logon(\''+ username + '\',\'' + password + '\')', 500);
				setTimeout(function() {
					if (swController.clientState == 1){		
						if (typeof woopraTracker === 'object') {
							woopra.track("Modpro Login", {
	        						Type: "Success"
							});
						}
							
					} else {						
						if (typeof woopraTracker === 'object') {
							woopra.track("Modpro Login", {
	        						Type: "Failed"
							});
						}
					}
					window.scrollTo(0,0);
				},1500);

			} else {
					$("#modpro-msg").html('<span style="color:red;">Wifi is not ready, please wait for a moment or check your connection!</span>');
					return false;
			}
		} else {
					$("#modpro-msg").html('<span style="color:red;">Username/Password required</span>');
					return false;
		}
	}
</script>