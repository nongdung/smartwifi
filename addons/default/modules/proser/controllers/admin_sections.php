<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Admin Page Layouts controller for the Pages module
 *
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Blog\Controllers
 */
class Admin_Sections extends Admin_Controller
{

	/** @var int The current active section */
	protected $section = 'sections';

	/** @var array The validation rules */
	protected $validation_rules = array(
		array(
			'field' => 'title',
			'label' => 'lang:global:title',
			'rules' => 'trim|required|max_length[100]|callback__check_title' //|callback__check_title
		),
		array(
			'field' => 'slug',
			'label' => 'lang:global:slug',
			'rules' => 'trim|required|max_length[100]|callback__check_slug'  //|callback__check_slug
		),
		array(
			'field' => 'id',
			'rules' => 'trim|numeric'
		),
	);

	/**
	 * Every time this controller is called should:
	 * - load the blog_categories model.
	 * - load the categories and blog language files.
	 * - load the form_validation and set the rules for it.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('proser_sections_m');
		$this->lang->load('section');
		$this->lang->load('proser');
		role_or_die('proser', 'merchant');
		// Load the validation library along with the rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->validation_rules);
	}

	/**
	 * Index method, lists all categories
	 */
	public function index()
	{
		$this->pyrocache->delete_all('module_m');

		// Create pagination links
		$total_rows = $this->proser_sections_m->count_all();
		$pagination = create_pagination('admin/proser/sections/index', $total_rows, Settings::get('records_per_page'), 5);

		// Using this data, get the relevant results
		$sections = $this->proser_sections_m
								->where('created_by', $this->current_user->id)
								->order_by('title')
								->limit($pagination['limit'])
								->offset($pagination['offset'])
								->get_all();

		$this->template
			->title($this->module_details['name'], lang('sec:list_title'))
			->set('sections', $sections)
			->set('pagination', $pagination)
			->build('admin/sections/index');
	}

	/**
	 * Create method, creates a new category
	 */
	public function create()
	{
		$section = new stdClass;

		// Validate the data
		if ($this->form_validation->run())
		{
			if ($id = $this->proser_sections_m->insert($this->input->post()))
			{
				// Fire an event. A new blog category has been created.
				//Events::trigger('blog_category_created', $id);

				$this->session->set_flashdata('success', sprintf(lang('sec:add_success'), $this->input->post('title')));
			}
			else
			{
				$this->session->set_flashdata('error', lang('sec:add_error'));
			}

			redirect('admin/proser/sections');
		}

		$section = new stdClass();

		// Loop through each validation rule
		foreach ($this->validation_rules as $rule)
		{
			$section->{$rule['field']} = set_value($rule['field']);
		}

		$this->template
			->title($this->module_details['name'], lang('sec:create_title'))
			->set('section', $section)
			->set('mode', 'create')
			->append_js('module::proser_section_form.js')
			->build('admin/sections/form');
	}

	/**
	 * Edit method, edits an existing category
	 *
	 * @param int $id The ID of the category to edit
	 */
	public function edit($id = 0)
	{
		// Get the category
		$section = $this->proser_sections_m->get($id);
		if ($section->created_by != $this->current_user->id) {
			redirect('admin/proser');
		}
		// ID specified?
		$section or redirect('admin/proser/sections/index');

		$this->form_validation->set_rules('id', 'ID', 'trim|required|numeric');

		// Validate the results
		if ($this->form_validation->run())
		{
			$this->proser_sections_m->update($id, $this->input->post())
				? $this->session->set_flashdata('success', sprintf(lang('sec:edit_success'), $this->input->post('title')))
				: $this->session->set_flashdata('error', lang('sec:edit_error'));

			// Fire an event. A blog category is being updated.
			//Events::trigger('blog_category_updated', $id);

			redirect('admin/proser/sections/index');
		}

		// Loop through each rule
		foreach ($this->validation_rules as $rule)
		{
			if ($this->input->post($rule['field']) !== null)
			{
				$section->{$rule['field']} = $this->input->post($rule['field']);
			}
		}

		$this->template
			->title($this->module_details['name'], sprintf(lang('sec:edit_title'), $section->title))
			->set('section', $section)
			->set('mode', 'edit')
			->append_js('module::proser_section_form.js')
			->build('admin/sections/form');
	}

	/**
	 * Delete method, deletes an existing category (obvious isn't it?)
	 *
	 * @param int $id The ID of the category to edit
	 */
	public function delete($id = 0)
	{
		$id_array = (!empty($id)) ? array($id) : $this->input->post('action_to');

		// Delete multiple
		if (!empty($id_array))
		{
			$deleted = 0;
			$to_delete = 0;
			$deleted_ids = array();
			foreach ($id_array as $id)
			{
				$section = $this->proser_sections_m->get($id);
				if ($section->created_by != $this->current_user->id) {
					redirect('admin/proser');
				}
				if ($this->proser_sections_m->delete($id))
				{
					$deleted++;
					$deleted_ids[] = $id;
				}
				else
				{
					$this->session->set_flashdata('error', sprintf(lang('sec:mass_delete_error'), $id));
				}
				$to_delete++;
			}

			if ($deleted > 0)
			{
				$this->session->set_flashdata('success', sprintf(lang('sec:mass_delete_success'), $deleted, $to_delete));
			}

			// Fire an event. One or more categories have been deleted.
			//Events::trigger('blog_category_deleted', $deleted_ids);
		}
		else
		{
			$this->session->set_flashdata('error', lang('sec:no_select_error'));
		}

		redirect('admin/proser/sections/index');
	}

	/**
	 * Callback method that checks the title of the category
	 *
	 * @param string $title The title to check
	 *
	 * @return bool
	 */
	public function _check_title($title = '')
	{
		if ($this->proser_sections_m->check_title($title, $this->input->post('id'), $this->current_user->id))
		{
			$this->form_validation->set_message('_check_title', sprintf(lang('sec:already_exist_error'), $title));

			return false;
		}

		return true;
	}

	/**
	 * Callback method that checks the slug of the category
	 *
	 * @param string $slug The slug to check
	 *
	 * @return bool
	 */
	public function _check_slug($slug = '')
	{
		if ($this->proser_sections_m->check_slug($slug, $this->input->post('id'), $this->current_user->id))
		{
			$this->form_validation->set_message('_check_slug', sprintf(lang('sec:already_exist_error'), $slug));

			return false;
		}

		return true;
	}

	/**
	 * Create method, creates a new category via ajax
	 */
	public function create_ajax()
	{
		$section = new stdClass();

		// Loop through each validation rule
		foreach ($this->validation_rules as $rule)
		{
			$section->{$rule['field']} = set_value($rule['field']);
		}

		$data = array(
			'mode' => 'create',
			'section' => $section,
		);

		if ($this->form_validation->run())
		{
			$id = $this->proser_sections_m->insert_ajax($this->input->post());

			if ($id > 0)
			{
				$message = sprintf(lang('sec:add_success'), $this->input->post('title', true));
			}
			else
			{
				$message = lang('sec:add_error');
			}

			return $this->template->build_json(array(
				'message' => $message,
				'title' => $this->input->post('title'),
				'section_id' => $id,
				'status' => 'ok'
			));
		}
		else
		{
			// Render the view
			$form = $this->load->view('admin/sections/form', $data, true);

			if ($errors = validation_errors())
			{
				return $this->template->build_json(array(
					'message' => $errors,
					'status' => 'error',
					'form' => $form
				));
			}

			echo $form;
		}
	}
}
