<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Public Blog module controller
 *
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Blog\Controllers
 */
class Proser extends Public_Controller
{
	public $stream;

	/**
	 * Every time this controller is called should:
	 * - load the blog and blog_categories models.
	 * - load the keywords library.
	 * - load the blog language file.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('proser_m');
		$this->load->model('proser_sections_m');
		$this->load->library(array('keywords/keywords'));
		$this->lang->load('proser');

		$this->load->driver('Streams');

		// We are going to get all the categories so we can
		// easily access them later when processing posts.
		$secs = $this->db->get('proser_sections')->result_array();
		$this->sections = array();
	
		foreach ($secs as $sec)
		{
			$this->sections[$sec['id']] = $sec;
		}

		// Get blog stream. We use this to set the template
		// stream throughout the blog module.
		$this->stream = $this->streams_m->get_stream('item', true, 'proser');
	}

	/**
	 * Index
	 *
	 * List out the blog posts.
	 *
	 * URIs such as `blog/page/x` also route here.
	 */
	public function index()
	{
		show_404();
	}

	/**
	 * View a post
	 *
	 * @param string $slug The slug of the blog post.
	 */
	public function view($author_id='', $slug = '')
	{
		// We need a slug to make this work.
		if ( ! $slug || ! $author_id)
		{
			redirect('proser');
		}

		$params = array(
			'stream'		=> 'item',
			'namespace'		=> 'proser',
			'limit'			=> 1,
			'where'			=> "`slug` = '{$slug}' AND `author_id` = '{$author_id}'"
		);
		$data = $this->streams->entries->get_entries($params);
		$item = (isset($data['entries'][0])) ? $data['entries'][0] : null;

		if ( ! $item or ($item['status'] !== 'live' and ! $this->ion_auth->is_admin()))
		{
			redirect('proser');
		}

		$this->_single_view($item);
	}

	/**
	 * Preview a post
	 *
	 * @param string $hash the preview_hash of post
	 */
	public function preview($hash = '')
	{
		if ( ! $hash)
		{
			redirect('proser');
		}

		$params = array(
			'stream'		=> 'item',
			'namespace'		=> 'proser',
			'limit'			=> 1,
			'where'			=> "`preview_hash` = '{$hash}'"
		);
		$data = $this->streams->entries->get_entries($params);
		$item = (isset($data['entries'][0])) ? $data['entries'][0] : null;

		if ( ! $item)
		{
			redirect('proser');
		}

		if ($item['status'] === 'live')
		{
			redirect('proser/'.date('Y/m', $item['created_on']).'/'.$item['slug']);
		}

		// Set index nofollow to attempt to avoid search engine indexing
		$this->template->set_metadata('index', 'nofollow');

		$this->_single_view($item);
	}

	/**
	 * Process Post
	 *
	 * Process data that was not part of the 
	 * initial streams call.
	 *
	 * @return 	void
	 */
	private function _process_post(&$item)
	{
		$this->load->helper('text');

		// Keywords array
		/*
		$keywords = Keywords::get($post['keywords']);
		$formatted_keywords = array();
		$keywords_arr = array();

		foreach ($keywords as $key)
		{
			$formatted_keywords[] 	= array('keyword' => $key->name);
			$keywords_arr[] 		= $key->name;

		}
		$post['keywords'] = $formatted_keywords;
		$post['keywords_arr'] = $keywords_arr;
		*/
		// Full URL for convenience.
		$item['url'] = site_url('proser/'.date('Y/m', $item['created_on']).'/'.$item['slug']);
	
		// What is the preview? If there is a field called intro,
		// we will use that, otherwise we will cut down the blog post itself.
		$item['preview'] = (isset($item['intro'])) ? $item['intro'] : $item['body'];

		// Category
		if ($item['section_id'] > 0 and isset($this->sections[$item['section_id']]))
		{
			$item['section'] = $this->sections[$item['section_id']];
		}
	}

	/**
	 * Posts Metadata
	 *
	 * @param array $posts
	 *
	 * @return array keywords and description
	 */
	private function _posts_metadata(&$posts = array())
	{
		$keywords = array();
		$description = array();

		// Loop through posts and use titles for meta description
		if ( ! empty($posts))
		{
			foreach ($posts as &$post)
			{
				if (isset($post['category']) and ! in_array($post['category']['title'], $keywords))
				{
					$keywords[] = $post['category']['title'];
				}

				$description[] = $post['title'];
			}
		}

		return array(
			'keywords' => implode(', ', $keywords),
			'description' => implode(', ', $description)
		);
	}

	/**
	 * Single View
	 *
	 * Generate a page for viewing a single
	 * blog post.
	 *
	 * @access 	private
	 * @param 	array $post The post to view
	 * @return 	void
	 */
	private function _single_view($item)
	{
		// if it uses markdown then display the parsed version
		if ($item['type'] === 'markdown')
		{
			$item['body'] = $item['parsed'];
		}

		$this->session->set_flashdata(array('referrer' => $this->uri->uri_string()));

		$this->template->set_breadcrumb(lang('proser:items_title'), 'proser');

		if ($item['section_id'] > 0)
		{
			// Get the category. We'll just do it ourselves
			// since we need an array.
			if ($section = $this->db->limit(1)->where('id', $item['section_id'])->get('proser_sections')->row_array())
			{
				$this->template->set_breadcrumb($section['title'], 'proser/section/'.$section['slug']);

				// Set category OG metadata			
				$this->template->set_metadata('article:section', $section['title'], 'og');

				// Add to $post
				$item['section'] = $section;
			}
		}

		$this->_process_post($item);

		// Add in OG keywords
		/*
		foreach ($item['keywords_arr'] as $keyword)
		{
			$this->template->set_metadata('article:tag', $keyword, 'og');
		}
		*/
		// If comments are enabled, go fetch them all
		if (Settings::get('enable_comments'))
		{
			// Load Comments so we can work out what to do with them
			$this->load->library('comments/comments', array(
				'entry_id' => $item['id'],
				'entry_title' => $item['title'],
				'module' => 'proser',
				'singular' => 'proser:item',
				'plural' => 'proser:items',
			));

			// Comments enabled can be 'no', 'always', or a strtotime compatable difference string, so "2 weeks"
			$this->template->set('form_display', (
				$item['comments_enabled'] === 'always' or
					($item['comments_enabled'] !== 'no' and time() < strtotime('+'.$item['comments_enabled'], $item['created_on']))
			));
		}

		$this->template
			->title($item['title'], lang('proser:item_title'))
			->set_metadata('og:type', 'article', 'og')
			->set_metadata('og:url', current_url(), 'og')
			->set_metadata('og:title', $item['title'], 'og')
			->set_metadata('og:site_name', Settings::get('site_name'), 'og')
			->set_metadata('og:description', $item['preview'], 'og')
			->set_metadata('article:published_time', date(DATE_ISO8601, $item['created_on']), 'og')
			->set_metadata('article:modified_time', date(DATE_ISO8601, $item['updated_on']), 'og')
			->set_metadata('description', $item['preview'])
			//->set_metadata('keywords', implode(', ', $item['keywords_arr']))
			->set_breadcrumb($item['title'])
			->set_stream($this->stream->stream_slug, $this->stream->stream_namespace)
			->set('item', array($item))
			->build('view');
	}
}
