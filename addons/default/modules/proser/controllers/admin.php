<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 *
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Blog\Controllers
 */
class Admin extends Admin_Controller
{
	/** @var string The current active section */
	protected $section = 'proser';

	/** @var array The validation rules */
	protected $validation_rules = array(
		'title' => array(
			'field' => 'title',
			'label' => 'lang:global:title',
			'rules' => 'trim|htmlspecialchars|required|max_length[200]|callback__check_title'
		),
		'slug' => array(
			'field' => 'slug',
			'label' => 'lang:global:slug',
			'rules' => 'trim|required|alpha_dot_dash|max_length[200]|callback__check_slug'
		),
		'price' => array(
			'field' => 'price',
			'label' => 'lang:proser:price',
			'rules' => 'trim|natural'
		),
		'currency' => array(
			'field' => 'currency',
			'label' => 'lang:proser:currency',
			'rules' => 'trim|max_length[10]'
		),
		array(
			'field' => 'category_id',
			'label' => 'lang:proser:category_label',
			'rules' => 'trim|numeric'
		),
		array(
			'field' => 'location_id',
			'label' => 'lang:proser:location_label',
			'rules' => 'trim|numeric'
		),
		array(
			'field' => 'keywords',
			'label' => 'lang:global:keywords',
			'rules' => 'trim'
		),
		array(
			'field' => 'body',
			'label' => 'lang:proser:content_label',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'type',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'status',
			'label' => 'lang:proser:status_label',
			'rules' => 'trim|alpha'
		),
		array(
			'field' => 'featured',
			'label' => 'lang:proser:featured_label',
			'rules' => 'trim|alpha'
		),
		array(
			'field' => 'created_on',
			'label' => 'lang:proser:date_label',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'created_on_hour',
			'label' => 'lang:proser:created_hour',
			'rules' => 'trim|numeric|required'
		),
		array(
			'field' => 'created_on_minute',
			'label' => 'lang:proser:created_minute',
			'rules' => 'trim|numeric|required'
		),
		array(
			'field' => 'comments_enabled',
			'label' => 'lang:proser:comments_enabled_label',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'preview_hash',
			'label' => '',
			'rules' => 'trim'
		)
	);

	/**
	 * Every time this controller controller is called should:
	 * - load the blog and blog_categories models
	 * - load the keywords and form validation libraries
	 * - set the hours, minutes and categories template variables.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model(array('proser_m', 'proser_sections_m', 'proser_category_m', 'location/location_m'));
		$this->lang->load(array('proser', 'section'));

		//$this->load->library(array('keywords/keywords', 'form_validation'));
		role_or_die('proser', 'merchant');
		$_sections = array();
		if ($sections = $this->proser_sections_m->where('created_by', $this->current_user->id)->order_by('title')->get_all())
		{
			foreach ($sections as $section)
			{
				$_sections[$section->id] = $section->title;
			}
		}
		
		$_categories = array();
		if ($_categories = $this->proser_category_m->order_by('cat_title')->get_all())
		{
			foreach ($_categories as $category)
			{
				$_categories[$category->id] = $category->cat_title;
			}
		}
		
		$_locations = array();
		if ($_locations = $this->location_m->where('created_by', $this->current_user->id)->order_by('loc_name')->get_all())
		{
			foreach ($_locations as $location)
			{
				$_locations[$location->id] = $location->loc_name;
			}
		}

		// Date ranges for select boxes
		$this->template
			->set('hours', array_combine($hours = range(0, 23), $hours))
			->set('minutes', array_combine($minutes = range(0, 59), $minutes))
			->set('sections', $_sections)
			->set('categories', $_categories)
			->set('locations', $_locations)
			->append_css('module::proser.css');
	}

	/**
	 * Show all created blog posts
	 */
	public function index()
	{
		//set the base/default where clause
		$base_where = array('show_future' => true, 'status' => 'all', 'author_id' => $this->current_user->id);
		
		//add post values to base_where if f_module is posted
		if ($this->input->post('f_section'))
		{
			$base_where['section'] = $this->input->post('f_section');
		}
		
		if ($this->input->post('f_category'))
		{
			$base_where['category'] = $this->input->post('f_category');
		}

		if ($this->input->post('f_status'))
		{
			$base_where['status'] = $this->input->post('f_status');
		}

		if ($this->input->post('f_location'))
		{
			$base_where['location'] = $this->input->post('f_location');
		}
		//print_r($this->proser_m->get_all());exit;
		// Create pagination links
		$total_rows = $this->proser_m->count_by($base_where);
		
		$pagination = create_pagination('admin/proser/index', $total_rows);

		// Using this data, get the relevant results
		$items = $this->proser_m
			->limit($pagination['limit'], $pagination['offset'])
			->get_many_by($base_where);
		
		//do we need to unset the layout because the request is ajax?
		$this->input->is_ajax_request() and $this->template->set_layout(false);
		//print_r($items);exit;
		$this->template
			->title($this->module_details['name'])
			->append_js('admin/filter.js')
			->set_partial('filters', 'admin/partials/filters')
			->set('pagination', $pagination)
			->set('items', $items);

		$this->input->is_ajax_request()
			? $this->template->build('admin/tables/posts')
			: $this->template->build('admin/index');

	}

	/**
	 * Create new post
	 */
	public function create()
	{
		// They are trying to put this live
		if ($this->input->post('status') == 'live')
		{
			role_or_die('proser', 'merchant');

			$hash = "";
		}
		else
		{
			$hash = $this->_preview_hash();
		}

		$item = new stdClass();

		// Get the blog stream.
		$this->load->driver('Streams');
		$stream = $this->streams->streams->get_stream('item', 'proser');
		$stream_fields = $this->streams_m->get_stream_fields($stream->id, $stream->stream_namespace);
		
		// Get the validation for our custom blog fields.
		$item_validation = $this->streams->streams->validation_array($stream->stream_slug, $stream->stream_namespace, 'new');
		
		// Combine our validation rules.
		$rules = array_merge($this->validation_rules, $item_validation);

		// Set our validation rules
		$this->form_validation->set_rules($rules);

		if ($this->input->post('created_on'))
		{
			$created_on = strtotime(sprintf('%s %s:%s', $this->input->post('created_on'), $this->input->post('created_on_hour'), $this->input->post('created_on_minute')));
		}
		else
		{
			$created_on = now();
		}

		if ($this->form_validation->run())
		{
			// Insert a new blog entry.
			// These are the values that we don't pass through streams processing.
			$extra = array(
				'title'            => $this->input->post('title'),
				'slug'             => $this->input->post('slug'),
				'price'             => $this->input->post('price'),
				'section_id'      => $this->input->post('section_id'),
				'category_id'      => $this->input->post('category_id'),
				'location_id'      => $this->input->post('location_id'),
				//'keywords'         => Keywords::process($this->input->post('keywords')),
				'body'             => $this->input->post('body'),
				'status'           => $this->input->post('status'),
				'featured'           => $this->input->post('featured'),
				'created_on'       => $created_on,
				'created'		   => date('Y-m-d H:i:s', $created_on),
				'comments_enabled' => $this->input->post('comments_enabled'),
				'author_id'        => $this->current_user->id,
				'type'             => $this->input->post('type'),
				'parsed'           => ($this->input->post('type') == 'markdown') ? parse_markdown($this->input->post('body')) : '',
				'preview_hash'     => $hash
			);

			if ($id = $this->streams->entries->insert_entry($_POST, 'item', 'proser', array('created'), $extra))
			{
				$this->pyrocache->delete_all('proser_m');
				$this->session->set_flashdata('success', sprintf($this->lang->line('proser:item_add_success'), $this->input->post('title')));

				// Blog article has been updated, may not be anything to do with publishing though
				//Events::trigger('post_created', $id);

				// They are trying to put this live
				if ($this->input->post('status') == 'live')
				{
					// Fire an event, we're posting a new blog!
					//Events::trigger('post_published', $id);
				}
			}
			else
			{
				$this->session->set_flashdata('error', lang('proser:item_add_error'));
			}

			// Redirect back to the form or main page
			($this->input->post('btnAction') == 'save_exit') ? redirect('admin/proser') : redirect('admin/proser/edit/'.$id);
		}
		else
		{
			// Go through all the known fields and get the post values
			$item = new stdClass;
			foreach ($this->validation_rules as $key => $field)
			{
				$item->$field['field'] = set_value($field['field']);
			}
			$item->created_on = $created_on;

			// if it's a fresh new article lets show them the advanced editor
			$item->type or $item->type = 'wysiwyg-advanced';
		}

		// Set Values
		$values = $this->fields->set_values($stream_fields, null, 'new');
		
		// Run stream field events
		$this->fields->run_field_events($stream_fields, array(), $values);
		
		$this->template
			->title($this->module_details['name'], lang('proser:create_title'))
			->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			->append_js('jquery/jquery.tagsinput.js')
			->append_js('module::proser_form.js')
			->append_js('module::proser_section_form.js')
			->append_css('jquery/jquery.tagsinput.css')
			->set('stream_fields', $this->streams->fields->get_stream_fields($stream->stream_slug, $stream->stream_namespace, $values))
			->set('item', $item)
			->build('admin/form');
	}

	/**
	 * Edit blog post
	 *
	 * @param int $id The ID of the blog post to edit
	 */
	public function edit($id = 0)
	{
		$id or redirect('admin/proser');

		$item = $this->proser_m->get($id);
		if ($item->author_id != $this->current_user->id) {
			redirect('admin/proser');
		}
		// They are trying to put this live
		if ($item->status != 'live' and $this->input->post('status') == 'live')
		{
			role_or_die('hotspot', 'merchant');
		}
		
		// If we have keywords before the update, we'll want to remove them from keywords_applied
		//$old_keywords_hash = (trim($item->keywords) != '') ? $post->keywords : null;

		//$post->keywords = Keywords::get_string($post->keywords);

		// If we have a useful date, use it
		if ($this->input->post('created_on'))
		{
			$created_on = strtotime(sprintf('%s %s:%s', $this->input->post('created_on'), $this->input->post('created_on_hour'), $this->input->post('created_on_minute')));
		}
		else
		{
			$created_on = $item->created_on;
		}

		// Load up streams
		$this->load->driver('Streams');
		$stream = $this->streams->streams->get_stream('item', 'proser');
		$stream_fields = $this->streams_m->get_stream_fields($stream->id, $stream->stream_namespace);

		// Get the validation for our custom blog fields.
		$item_validation = $this->streams->streams->validation_array($stream->stream_slug, $stream->stream_namespace, 'new');
		
		$item_validation = array_merge($this->validation_rules, array(
			'title' => array(
				'field' => 'title',
				'label' => 'lang:global:title',
				'rules' => 'trim|htmlspecialchars|required|max_length[100]|callback__check_title['.$id.']'
			),
			'slug' => array(
				'field' => 'slug',
				'label' => 'lang:global:slug',
				'rules' => 'trim|required|alpha_dot_dash|max_length[100]|callback__check_slug['.$id.']'
			),
		));

		// Merge and set our validation rules
		$this->form_validation->set_rules(array_merge($this->validation_rules, $item_validation));

		$hash = $this->input->post('preview_hash');

		if ($this->input->post('status') == 'draft' and $this->input->post('preview_hash') == '')
		{
			$hash = $this->_preview_hash();
		}
		//it is going to be published we don't need the hash
		elseif ($this->input->post('status') == 'live')
		{
			$hash = '';
		}

		if ($this->form_validation->run())
		{
			$author_id = empty($item->display_name) ? $this->current_user->id : $item->author_id;

			$extra = array(
				'title'            => $this->input->post('title'),
				'slug'             => $this->input->post('slug'),
				'price'             => $this->input->post('price'),
				'category_id'      => $this->input->post('category_id'),
				'location_id'      => $this->input->post('location_id'),
				'section_id'      => $this->input->post('section_id'),
				'body'             => $this->input->post('body'),
				'status'           => $this->input->post('status'),
				'featured'           => $this->input->post('featured'),
				'created_on'       => $created_on,
				'updated_on'       => $created_on,
				'created'		   => date('Y-m-d H:i:s', $created_on),
				'updated'		   => date('Y-m-d H:i:s', $created_on),
				'comments_enabled' => $this->input->post('comments_enabled'),
				'author_id'        => $author_id,
				'type'             => $this->input->post('type'),
				'parsed'           => ($this->input->post('type') == 'markdown') ? parse_markdown($this->input->post('body')) : '',
				'preview_hash'     => $hash,
			);

			if ($this->streams->entries->update_entry($id, $_POST, 'item', 'proser', array('updated'), $extra))
			{
				$this->session->set_flashdata(array('success' => sprintf(lang('proser:edit_success'), $this->input->post('title'))));

				// Blog article has been updated, may not be anything to do with publishing though
				//Events::trigger('post_updated', $id);

				// They are trying to put this live
				if ($item->status != 'live' and $this->input->post('status') == 'live')
				{
					// Fire an event, we're posting a new blog!
					//Events::trigger('post_published', $id);
				}
			}
			else
			{
				$this->session->set_flashdata('error', lang('proser:edit_error'));
			}

			// Redirect back to the form or main page
			($this->input->post('btnAction') == 'save_exit') ? redirect('admin/proser') : redirect('admin/proser/edit/'.$id);
		}

		// Go through all the known fields and get the post values
		foreach ($this->validation_rules as $key => $field)
		{
			if (isset($_POST[$field['field']]))
			{
				$item->$field['field'] = set_value($field['field']);
			}
		}

		$item->created_on = $created_on;

		// Set Values
		$values = $this->fields->set_values($stream_fields, $item, 'edit');

		// Run stream field events
		$this->fields->run_field_events($stream_fields, array(), $values);

		$this->template
			->title($this->module_details['name'], sprintf(lang('proser:edit_title'), $item->title))
			->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			->append_js('jquery/jquery.tagsinput.js')
			->append_js('module::proser_form.js')
			->set('stream_fields', $this->streams->fields->get_stream_fields($stream->stream_slug, $stream->stream_namespace, $values, $item->id))
			->append_css('jquery/jquery.tagsinput.css')
			->set('item', $item)
			->build('admin/form');
	}

	/**
	 * Preview blog post
	 *
	 * @param int $id The ID of the blog post to preview
	 */
	public function preview($id = 0)
	{
		$item = $this->proser_m->get($id);

		$this->template
			->set_layout('modal', 'admin')
			->set('item', $item)
			->build('admin/preview');
	}

	/**
	 * Helper method to determine what to do with selected items from form post
	 */
	public function action()
	{
		switch ($this->input->post('btnAction'))
		{
			case 'publish':
				$this->publish();
				break;

			case 'delete':
				$this->delete();
				break;

			default:
				redirect('admin/proser');
				break;
		}
	}

	/**
	 * Publish blog post
	 *
	 * @param int $id the ID of the blog post to make public
	 */
	public function publish($id = 0)
	{
		role_or_die('proser', 'merchant');

		// Publish one
		$ids = ($id) ? array($id) : $this->input->post('action_to');

		if ( ! empty($ids))
		{
			// Go through the array of slugs to publish
			$item_titles = array();
			foreach ($ids as $id)
			{
				// Get the current page so we can grab the id too
				if ($item = $this->proser_m->get($id))
				{
					$this->proser_m->publish($id);

					// Wipe cache for this model, the content has changed
					$this->pyrocache->delete('proser_m');
					$item_titles[] = $item->title;
				}
			}
		}

		// Some posts have been published
		if ( ! empty($item_titles))
		{
			// Only publishing one post
			if (count($item_titles) == 1)
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('proser:publish_success'), $item_titles[0]));
			}
			// Publishing multiple posts
			else
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('proser:mass_publish_success'), implode('", "', $item_titles)));
			}
		}
		// For some reason, none of them were published
		else
		{
			$this->session->set_flashdata('notice', $this->lang->line('proser:publish_error'));
		}

		redirect('admin/proser');
	}

	/**
	 * Delete blog post
	 *
	 * @param int $id The ID of the blog post to delete
	 */
	public function delete($id = 0)
	{
		$this->load->model('comments/comment_m');

		role_or_die('proser', 'merchant');

		// Delete one
		$ids = ($id) ? array($id) : $this->input->post('action_to');

		// Go through the array of slugs to delete
		if ( ! empty($ids))
		{
			$item_titles = array();
			$deleted_ids = array();
			foreach ($ids as $id)
			{
				// Get the current page so we can grab the id too
				if ($item = $this->proser_m->get($id))
				{
					if ($item->author_id != $this->current_user->id) {
						redirect('admin/proser');exit;
					}
					
					if ($this->proser_m->delete($id))
					{
						$this->comment_m->where('module', 'proser')->delete_by('entry_id', $id);

						// Wipe cache for this model, the content has changed
						$this->pyrocache->delete('proser_m');
						$item_titles[] = $item->title;
						$deleted_ids[] = $id;
					}
				}
			}

			// Fire an event. We've deleted one or more blog posts.
			//Events::trigger('post_deleted', $deleted_ids);
		}

		// Some pages have been deleted
		if ( ! empty($item_titles))
		{
			// Only deleting one page
			if (count($item_titles) == 1)
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('proser:delete_success'), $item_titles[0]));
			}
			// Deleting multiple pages
			else
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('proser:mass_delete_success'), implode('", "', $item_titles)));
			}
		}
		// For some reason, none of them were deleted
		else
		{
			$this->session->set_flashdata('notice', lang('proser:delete_error'));
		}

		redirect('admin/proser');
	}

	/**
	 * Callback method that checks the title of an post
	 *
	 * @param string $title The Title to check
	 * @param string $id
	 *
	 * @return bool
	 */
	public function _check_title($title, $id = null)
	{
		$this->form_validation->set_message('_check_title', sprintf(lang('proser:already_exist_error'), lang('global:title')));

		return $this->proser_m->check_exists('title', $title, $id);
	}

	/**
	 * Callback method that checks the slug of an post
	 *
	 * @param string $slug The Slug to check
	 * @param null   $id
	 *
	 * @return bool
	 */
	public function _check_slug($slug, $id = null)
	{
		$this->form_validation->set_message('_check_slug', sprintf(lang('proser:already_exist_error'), lang('global:slug')));

		return $this->proser_m->check_exists('slug', $slug, $id);
	}

	/**
	 * Generate a preview hash
	 *
	 * @return bool
	 */
	private function _preview_hash()
	{
		return md5(microtime() + mt_rand(0, 1000));
	}
}
