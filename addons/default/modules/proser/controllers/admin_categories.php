<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Admin Page Layouts controller for the Pages module
 *
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Blog\Controllers
 */
class Admin_Categories extends Admin_Controller
{

	/** @var int The current active section */
	protected $section = 'categories';

	/** @var array The validation rules */
	

	/**
	 * Every time this controller is called should:
	 * - load the blog_categories model.
	 * - load the categories and blog language files.
	 * - load the form_validation and set the rules for it.
	 */
	public function __construct()
	{
		parent::__construct();
		role_or_die('proser', 'admin');
		$this->load->model('proser_category_m');
		$this->lang->load('categories');
		$this->lang->load('proser');

		// Load the validation library along with the rules
		$this->load->library('form_validation');
		$this->load->driver('Streams');
	}

	/**
	 * Index method, lists all categories
	 */
	public function index()
	{
		$extra = array();
        $extra['title'] = 'Categories';

        $extra['buttons'] = array(
            array(
                'label' => lang('global:edit'),
                'url' => 'admin/proser/categories/edit/-entry_id-'
            ),
            array(
                'label' => lang('global:delete'),
                'url' => 'admin/proser/categories/delete/-entry_id-',
                'confirm' => true
            )
        );

        $this->streams->cp->entries_table('categories', 'proser', 15, 'admin/proser/categories/index', true, $extra);
	}

	/**
	 * Create method, creates a new category
	 */
	public function create()
	{
		$extra = array(
		    'return'            => 'admin/proser/categories',
		    'success_message'   => lang('cat:submit_success'),
		    'failure_message'   => lang('cat:submit_error'),
		    'title'             => 'Add category'
		);

		$this->streams->cp->entry_form('categories', 'proser', 'new', null, true, $extra);
	}

	/**
	 * Edit method, edits an existing category
	 *
	 * @param int $id The ID of the category to edit
	 */
	public function edit($id = 0)
	{
		$extra = array(
		    'return'            => 'admin/proser/categories',
		    'success_message'   => lang('cat:submit_success'),
		    'failure_message'   => lang('cat:submit_error'),
		    'title'             => 'Add category'
		);

		$this->streams->cp->entry_form('categories', 'proser', 'edit', $id, true, $extra);
	}

	/**
	 * Delete method, deletes an existing category (obvious isn't it?)
	 *
	 * @param int $id The ID of the category to edit
	 */
	public function delete($id = 0)
	{
		$id_array = (!empty($id)) ? array($id) : $this->input->post('action_to');

		// Delete multiple
		if (!empty($id_array))
		{
			$deleted = 0;
			$to_delete = 0;
			$deleted_ids = array();
			foreach ($id_array as $id)
			{
				if ($this->blog_categories_m->delete($id))
				{
					$deleted++;
					$deleted_ids[] = $id;
				}
				else
				{
					$this->session->set_flashdata('error', sprintf(lang('cat:mass_delete_error'), $id));
				}
				$to_delete++;
			}

			if ($deleted > 0)
			{
				$this->session->set_flashdata('success', sprintf(lang('cat:mass_delete_success'), $deleted, $to_delete));
			}

			// Fire an event. One or more categories have been deleted.
			Events::trigger('blog_category_deleted', $deleted_ids);
		}
		else
		{
			$this->session->set_flashdata('error', lang('cat:no_select_error'));
		}

		redirect('admin/blog/categories/index');
	}

	/**
	 * Callback method that checks the title of the category
	 *
	 * @param string $title The title to check
	 *
	 * @return bool
	 */
	public function _check_title($title = '')
	{
		if ($this->blog_categories_m->check_title($title, $this->input->post('id')))
		{
			$this->form_validation->set_message('_check_title', sprintf(lang('cat:already_exist_error'), $title));

			return false;
		}

		return true;
	}

	/**
	 * Callback method that checks the slug of the category
	 *
	 * @param string $slug The slug to check
	 *
	 * @return bool
	 */
	public function _check_slug($slug = '')
	{
		if ($this->blog_categories_m->check_slug($slug, $this->input->post('id')))
		{
			$this->form_validation->set_message('_check_slug', sprintf(lang('cat:already_exist_error'), $slug));

			return false;
		}

		return true;
	}

	/**
	 * Create method, creates a new category via ajax
	 */
	public function create_ajax()
	{
		$category = new stdClass();

		// Loop through each validation rule
		foreach ($this->validation_rules as $rule)
		{
			$category->{$rule['field']} = set_value($rule['field']);
		}

		$data = array(
			'mode' => 'create',
			'category' => $category,
		);

		if ($this->form_validation->run())
		{
			$id = $this->blog_categories_m->insert_ajax($this->input->post());

			if ($id > 0)
			{
				$message = sprintf(lang('cat:add_success'), $this->input->post('title', true));
			}
			else
			{
				$message = lang('cat:add_error');
			}

			return $this->template->build_json(array(
				'message' => $message,
				'title' => $this->input->post('title'),
				'category_id' => $id,
				'status' => 'ok'
			));
		}
		else
		{
			// Render the view
			$form = $this->load->view('admin/categories/form', $data, true);

			if ($errors = validation_errors())
			{
				return $this->template->build_json(array(
					'message' => $errors,
					'status' => 'error',
					'form' => $form
				));
			}

			echo $form;
		}
	}
}
