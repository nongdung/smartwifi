<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Blog Permissions
$lang['proser:role_merchant']		= 'Merchant';
$lang['proser:role_manager']	= 'Manager';
$lang['proser:role_admin'] 	= 'Admin';