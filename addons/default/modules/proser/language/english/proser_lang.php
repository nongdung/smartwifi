<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['proser:post']                 = 'Post';
$lang['proser:posts']                   = 'Posts';
$lang['proser:products_n_services']                 = 'Products and Services';
$lang['proser:sections']                   = 'Sections';
$lang['proser:categories']                   = 'Categories';
$lang['proser:price']                   = 'Price';
$lang['proser:currency']                   = 'Currency';
//Fields
$lang['proser:categories_fields']                   = 'Category Fields';

//category
$lang['proser:add_category']                   = 'Add Category';

// labels
$lang['proser:featured_label']                   = 'Featured';
$lang['proser:posted_label']                   = 'Posted';
$lang['proser:posted_label_alt']               = 'Posted at';
$lang['proser:written_by_label']				= 'Written by';
$lang['proser:author_unknown']				= 'Unknown';
$lang['proser:keywords_label']				= 'Keywords';
$lang['proser:tagged_label']					= 'Tagged';
$lang['proser:category_label']                 = 'Category';
$lang['proser:section_label']                 = 'Section';
$lang['proser:location_label']                 = 'Location';
$lang['proser:item_label']                     = 'Item';
$lang['proser:date_label']                     = 'Date';
$lang['proser:date_at']                        = 'at';
$lang['proser:time_label']                     = 'Time';
$lang['proser:status_label']                   = 'Status';
$lang['proser:draft_label']                    = 'Draft';
$lang['proser:live_label']                     = 'Live';
$lang['proser:content_label']                  = 'Content';
$lang['proser:options_label']                  = 'Options';
$lang['proser:intro_label']                    = 'Introduction';
$lang['proser:no_category_select_label']       = '-- None --';
$lang['proser:all_location_select_label']       = '-- All --';
$lang['proser:new_category_label']             = 'Add a category';
$lang['proser:subscripe_to_rss_label']         = 'Subscribe to RSS';
$lang['proser:all_posts_label']                = 'All posts';
$lang['proser:posts_of_category_suffix']       = ' posts';
$lang['proser:rss_name_suffix']                = ' proser';
$lang['proser:rss_category_suffix']            = ' proser';
$lang['proser:author_name_label']              = 'Author name';
$lang['proser:read_more_label']                = 'Read More&nbsp;&raquo;';
$lang['proser:created_hour']                   = 'Created on Hour';
$lang['proser:created_minute']                 = 'Created on Minute';
$lang['proser:comments_enabled_label']         = 'Comments Enabled';
$lang['proser:no_label']                    = 'No';
$lang['proser:yes_label']                     = 'Yes';

//Section
$lang['proser:no_section_select_label']       = '-- None --';
$lang['proser:new_section_label']             = 'Add a section';

// Item
$lang['proser:create_title']                   = 'Add Item';
$lang['proser:edit_title']                     = 'Edit item "%s"';
$lang['proser:archive_title']                 = 'Archive';
$lang['proser:items_title']					= 'Items';
$lang['proser:rss_posts_title']				= 'proser posts for %s';
$lang['proser:proser_title']					= 'Title';
$lang['proser:list_title']					= 'List Posts';

// messages
$lang['proser:disabled_after'] 				= 'Posting comments after %s has been disabled.';
$lang['proser:no_posts']                      = 'There are no posts.';
$lang['proser:subscripe_to_rss_desc']          = 'Get posts straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['proser:currently_no_posts']          = 'There are no posts at the moment.';
$lang['proser:item_add_success']            = 'The item "%s" was added.';
$lang['proser:item_add_error']              = 'An error occured.';
$lang['proser:edit_success']                   = 'The post "%s" was updated.';
$lang['proser:edit_error']                     = 'An error occurred.';
$lang['proser:publish_success']                = 'The post "%s" has been published.';
$lang['proser:mass_publish_success']           = 'The posts "%s" have been published.';
$lang['proser:publish_error']                  = 'No posts were published.';
$lang['proser:delete_success']                 = 'The post "%s" has been deleted.';
$lang['proser:mass_delete_success']            = 'The posts "%s" have been deleted.';
$lang['proser:delete_error']                   = 'No posts were deleted.';
$lang['proser:already_exist_error']            = 'A post with this URL already exists.';

$lang['proser:twitter_posted']                 = 'Posted "%s" %s';
$lang['proser:twitter_error']                  = 'Twitter Error';

// date
$lang['proser:archive_date_format']		= "%B %Y";
