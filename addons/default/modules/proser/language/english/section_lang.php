<?php defined('BASEPATH') OR exit('No direct script access allowed');

// labels
$lang['sec:section_label']         = 'Section';

// titles
$lang['sec:create_title']           = 'Add Section';
$lang['sec:edit_title']             = 'Edit section "%s"';
$lang['sec:list_title']             = 'Sections';

// messages
$lang['sec:no_sections']          = 'There are no sections.';
$lang['sec:add_success']            = 'Your section has been saved.';
$lang['sec:add_error']              = 'An error occurred.';
$lang['sec:edit_success']           = 'The section was saved.';
$lang['sec:edit_error']             = 'An error occurred.';
$lang['sec:mass_delete_error']      = 'Error occurred while trying to delete section "%s".';
$lang['sec:mass_delete_success']    = '%s sections out of %s successfully deleted.';
$lang['sec:no_select_error']        = 'You need to select sections first.';
$lang['sec:already_exist_error']    = 'A section with the name "%s" already exists.';