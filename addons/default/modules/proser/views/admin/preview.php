<h1><?php echo $item->title ?></h1>

<p style="float:left; width: 40%;">
	<?php echo anchor('proser/' .$item->author_id .'/'. $item->slug, null, 'target="_blank"') ?>
</p>

<p style="float:right; width: 40%; text-align: right;">
	<?php echo anchor('admin/proser/edit/'. $item->id, lang('global:edit'), ' target="_parent"') ?>
</p>

<iframe src="<?php echo site_url('proser/' .$item->author_id .'/'. $item->slug) ?>" width="99%" height="400"></iframe>