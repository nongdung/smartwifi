<section class="title">
<?php if ($this->method == 'create'): ?>
	<h4><?php echo lang('proser:create_title') ?></h4>
<?php else: ?>
	<h4><?php echo sprintf(lang('proser:edit_title'), $item->title) ?></h4>
<?php endif ?>
</section>

<section class="item">
<div class="content">

<?php echo form_open_multipart() ?>

<div class="tabs">

	<ul class="tab-menu">
		<li><a href="#proser-content-tab"><span><?php echo lang('proser:content_label') ?></span></a></li>
		<?php if ($stream_fields): ?><li><a href="#proser-custom-fields"><span><?php echo lang('global:custom_fields') ?></span></a></li><?php endif; ?>
		<li><a href="#proser-options-tab"><span><?php echo lang('proser:options_label') ?></span></a></li>
	</ul>

	<!-- Content tab -->
	<div class="form_inputs" id="proser-content-tab">
		<fieldset>
			<ul>
				<li>
					<label for="title"><?php echo lang('global:title') ?> <span>*</span></label>
					<div class="input"><?php echo form_input('title', htmlspecialchars_decode($item->title), 'maxlength="100" id="title"') ?></div>
				</li>
	
				<li>
					<label for="slug"><?php echo lang('global:slug') ?> <span>*</span></label>
					<div class="input"><?php echo form_input('slug', $item->slug, 'maxlength="100" class="width-20"') ?></div>
				</li>
	
				<li>
					<label for="status"><?php echo lang('proser:status_label') ?></label>
					<div class="input"><?php echo form_dropdown('status', array('draft' => lang('proser:draft_label'), 'live' => lang('proser:live_label')), $item->status) ?></div>
				</li>
				
				<li>
					<label for="price"><?php echo lang('proser:price') ?></label>
					<div class="input"><?php echo form_input('price', $item->price, 'maxlength="100" id="price"') ?></div>
				</li>
				
				<li>
					<label for="currency"><?php echo lang('proser:currency') ?></label>
					<div class="input"><?php echo form_dropdown('currency', array('VND' => 'VND', 'USD' => 'USD'), isset($item->currency)?$item->currency:'VND') ?></div>
					
				</li>
				
				<li>
					<label for="location_id"><?php echo lang('proser:location_label') ?></label>
					<div class="input">
					<?php echo form_dropdown('location_id', array(lang('proser:all_location_select_label')) + $locations, @$item->location_id) ?>
					</div>
				</li>
				
				
		
				<li class="editor">
					<label for="body"><?php echo lang('proser:content_label') ?> <span>*</span></label><br>
					<div class="input small-side">
						<?php echo form_dropdown('type', array(
							'html' => 'html',
							'markdown' => 'markdown',
							'wysiwyg-simple' => 'wysiwyg-simple',
							'wysiwyg-advanced' => 'wysiwyg-advanced',
						), $item->type) ?>
					</div>
	
					<div class="edit-content">
						<?php echo form_textarea(array('id' => 'body', 'name' => 'body', 'value' => $item->body, 'rows' => 30, 'class' => $item->type)) ?>
					</div>
				</li>

			</ul>
		<?php echo form_hidden('preview_hash', $item->preview_hash)?>
		</fieldset>
	</div>

	<?php if ($stream_fields): ?>

	<div class="form_inputs" id="proser-custom-fields">
		<fieldset>
			<ul>

				<?php foreach ($stream_fields as $field) echo $this->load->view('admin/partials/streams/form_single_display', array('field' => $field), true) ?>

			</ul>
		</fieldset>
	</div>

	<?php endif; ?>

	<!-- Options tab -->
	<div class="form_inputs" id="proser-options-tab">
		<fieldset>
			<ul>
				<li>
					<label for="featured"><?php echo lang('proser:featured_label') ?></label>
					<div class="input"><?php echo form_dropdown('featured', array('no' => lang('proser:no_label'), 'yes' => lang('proser:yes_label')), $item->featured) ?></div>
				</li>
				
				<li>
					<label for="section_id"><?php echo lang('proser:section_label') ?></label>
					<div class="input">
					<?php echo form_dropdown('section_id', array(lang('proser:no_section_select_label')) + $sections, @$item->section_id) ?>
						[ <?php echo anchor('admin/proser/sections/create', lang('proser:new_section_label'), 'target="_blank"') ?> ]
					</div>
				</li>
				
				<li>
					<label for="category_id"><?php echo lang('proser:category_label') ?></label>
					<div class="input">
					<?php echo form_dropdown('category_id', array(lang('proser:no_category_select_label')) + $categories, @$item->category_id) ?>
					</div>
				</li>
	
				<li class="date-meta">
					<label><?php echo lang('proser:date_label') ?></label>
	
					<div class="input datetime_input">
						<?php echo form_input('created_on', date('Y-m-d', $item->created_on), 'maxlength="10" id="datepicker" class="text width-20"') ?> &nbsp;
						<?php echo form_dropdown('created_on_hour', $hours, date('H', $item->created_on)) ?> :
						<?php echo form_dropdown('created_on_minute', $minutes, date('i', ltrim($item->created_on, '0'))) ?>
					</div>
				</li>
	
				<?php if ( ! module_enabled('comments')): ?>
					<?php echo form_hidden('comments_enabled', 'no'); ?>
				<?php else: ?>
					<li>
						<label for="comments_enabled"><?php echo lang('proser:comments_enabled_label');?></label>
						<div class="input">
							<?php echo form_dropdown('comments_enabled', array(
								'no' => lang('global:no'),
								'1 day' => lang('global:duration:1-day'),
								'1 week' => lang('global:duration:1-week'),
								'2 weeks' => lang('global:duration:2-weeks'),
								'1 month' => lang('global:duration:1-month'),
								'3 months' => lang('global:duration:3-months'),
								'always' => lang('global:duration:always'),
							), $item->comments_enabled ? $item->comments_enabled : '3 months') ?>
						</div>
					</li>
				<?php endif; ?>
			</ul>
		</fieldset>
	</div>

</div>

<input type="hidden" name="row_edit_id" value="<?php if ($this->method != 'create'): echo $item->id; endif; ?>" />

<div class="buttons">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel'))) ?>
</div>

<?php echo form_close() ?>

</div>
</section>