<fieldset id="filters">
	<legend><?php echo lang('global:filters') ?></legend>

	<?php echo form_open('', '', array('f_module' => $module_details['slug'])) ?>
		<ul>
			<li class="">
        		<label for="f_status"><?php echo lang('proser:status_label') ?></label>
        		<?php echo form_dropdown('f_status', array(0 => lang('global:select-all'), 'draft'=>lang('proser:draft_label'), 'live'=>lang('proser:live_label'))) ?>
    		</li>

			<li class="">
        		<label for="f_section"><?php echo lang('proser:section_label') ?></label>
       			<?php echo form_dropdown('f_section', array(0 => lang('global:select-all')) + $sections) ?>
    		</li>

			<li class="">
				<label for="f_category"><?php echo lang('proser:category_label') ?></label>
				<?php echo form_dropdown('f_category', array(0 => lang('global:select-all')) + $categories) ?>
			</li>
			
			<li class="">
				<label for="f_location"><?php echo lang('proser:location_label') ?></label>
				<?php echo form_dropdown('f_location', array(0 => lang('global:select-all')) + $locations) ?>
			</li>

			<li class="">
				<?php echo anchor(current_url() . '#', lang('buttons:cancel'), 'class="button red"') ?>
			</li>
		</ul>
	<?php echo form_close() ?>
</fieldset>
