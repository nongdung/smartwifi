<section class="title">
	<h4><?php echo lang('sec:list_title') ?></h4>
</section>

<section class="item">
	<div class="content">
	
	<?php if ($sections): ?>

		<?php echo form_open('admin/proser/sections/delete') ?>

		<table border="0" class="table-list" cellspacing="0">
			<thead>
			<tr>
				<th width="20"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all')) ?></th>
				<th><?php echo lang('sec:category_label') ?></th>
				<th><?php echo lang('global:slug') ?></th>
				<th width="120"></th>
			</tr>
			</thead>
			<tbody>
				<?php foreach ($sections as $section): ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $section->id) ?></td>
					<td><?php echo $section->title ?></td>
					<td><?php echo $section->slug ?></td>
					<td class="align-center buttons buttons-small">
						<?php echo anchor('admin/proser/sections/edit/'.$section->id, lang('global:edit'), 'class="button edit"') ?>
						<?php echo anchor('admin/proser/sections/delete/'.$section->id, lang('global:delete'), 'class="confirm button delete"') ;?>
					</td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>

		<?php $this->load->view('admin/partials/pagination') ?>

		<div class="table_action_buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )) ?>
		</div>

		<?php echo form_close() ?>

	<?php else: ?>
		<div class="no_data"><?php echo lang('sec:no_sections') ?></div>
	<?php endif ?>
	</div>
</section>