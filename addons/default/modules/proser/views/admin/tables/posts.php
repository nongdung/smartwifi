	<table cellspacing="0">
		<thead>
			<tr>
				<th width="20"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all')) ?></th>
				<th><?php echo lang('proser:item_label') ?></th>
				<th class="collapse"><?php echo lang('proser:section_label') ?></th>
				<th class="collapse"><?php echo lang('proser:category_label') ?></th>
				<th class="collapse"><?php echo lang('proser:location_label') ?></th>
				<th class="collapse"><?php echo lang('proser:price') ?></th>
				<th class="collapse"><?php echo lang('proser:featured_label') ?></th>
				<th class="collapse"><?php echo lang('proser:written_by_label') ?></th>
				<th><?php echo lang('proser:status_label') ?></th>
				<th width="200"><?php echo lang('global:actions') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($items as $item) : ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $item->id) ?></td>
					<td><?php echo $item->title ?></td>
					<td class="collapse"><?php echo $item->section_title ?></td>
					<td class="collapse"><?php echo $item->category_title ?></td>
					<td class="collapse"><?php echo $item->location_title ?></td>
					<td class="collapse"><?php echo $item->currency. " " . number_format($item->price, 0, '.', ','); ?></td>
					<td class="collapse">
						<?php if ($item->featured == 'yes'):?>
						<span class="featured">&#9734;</span>
						<?php endif ?>
					</td>					
					<td class="collapse">
					<?php if (isset($item->display_name)): ?>
						<?php echo anchor('user/'.$item->username, $item->display_name, 'target="_blank"') ?>
					<?php else: ?>
						<?php echo lang('proser:author_unknown') ?>
					<?php endif ?>
					</td>
					<td><?php echo lang('proser:'.$item->status.'_label') ?></td>
					<td style="padding-top:10px;">
                        <?php if($item->status=='live') : ?>
							<a href="<?php echo site_url('proser/'.$item->author_id.'/'.$item->slug) ?>" title="<?php echo lang('global:view')?>" class="button" target="_blank"><?php echo lang('global:view')?></a>
                        <?php else: ?>
							<a href="<?php echo site_url('proser/preview/' . $item->preview_hash) ?>" title="<?php echo lang('global:preview')?>" class="button" target="_blank"><?php echo lang('global:preview')?></a>
                        <?php endif ?>
						<a href="<?php echo site_url('admin/proser/edit/' . $item->id) ?>" title="<?php echo lang('global:edit')?>" class="button"><?php echo lang('global:edit')?></a>
						<a href="<?php echo site_url('admin/proser/delete/' . $item->id) ?>" title="<?php echo lang('global:delete')?>" class="button confirm"><?php echo lang('global:delete')?></a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>

	<?php $this->load->view('admin/partials/pagination') ?>

	<br>

	<div class="table_action_buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete', 'publish'))) ?>
	</div>