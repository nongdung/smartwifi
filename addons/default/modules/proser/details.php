<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Blog module
 *
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Blog
 */
class Module_Proser extends Module
{
	public $version = '1.0.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Proser'
			),
			'description' => array(
				'en' => 'Products and Services.',
				'ar' => 'Sản phẩm & Dịch vụ'
			),
			'frontend' => TRUE,
			'backend' => true,
			'skip_xss' => true,
			'menu' => FALSE,

			'roles' => array(
				'merchant', 'manager', 'admin'
			),

			'sections' => array(
				'proser' => array(
					'name' => 'proser:products_n_services',
					'uri' => 'admin/proser',
					'shortcuts' => array(
						array(
							'name' => 'proser:create_title',
							'uri' => 'admin/proser/create',
							'class' => 'add',
						),
					),
				),
				
				'sections' => array(
					'name' => 'proser:sections',
					'uri' => 'admin/proser/sections',
					'shortcuts' => array(
						array(
							'name' => 'sec:create_title',
							'uri' => 'admin/proser/sections/create',
							'class' => 'add',
						),
					),
				)
			),
		);

		if (function_exists('group_has_role'))
		{
			if(group_has_role('proser', 'admin'))
			{
				$info['sections']['categories'] = array(
					'name' => 'proser:categories',
					'uri' => 'admin/proser/categories',
					'shortcuts' => array(
						array(
							'name' => 'proser:add_category',
							'uri' => 'admin/proser/categories/create',
							'class' => 'add',
						),
					),
				);
				$info['sections']['fields'] = array(
							'name' 	=> 'global:custom_fields',
							'uri' 	=> 'admin/proser/fields',
								'shortcuts' => array(
									'create' => array(
										'name' 	=> 'streams:add_field',
										'uri' 	=> 'admin/proser/fields/create',
										'class' => 'add'
										)
									)
				);
				$info['sections']['categories_fields'] = array(
							'name' 	=> 'proser:categories_fields',
							'uri' 	=> 'admin/proser/categories-fields',
								'shortcuts' => array(
									'create' => array(
										'name' 	=> 'streams:add_field',
										'uri' 	=> 'admin/proser/categories-fields/create',
										'class' => 'add'
										)
									)
				);
			}
		}

		return $info;
	}

	public function install()
	{
		$this->dbforge->drop_table('proser_sections');

		$this->load->driver('Streams');
		$this->streams->utilities->remove_namespace('proser');

		// Just in case.
		$this->dbforge->drop_table('proser_item');

		if ($this->db->table_exists('data_streams'))
		{
			$this->db->where('stream_namespace', 'proser')->delete('data_streams');
		}

		// Create the proser sections table.
		$this->install_tables(array(
			'proser_sections' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false),
				'title' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false),
				'created_by' => array('type' => 'INT', 'constraint' => 11, 'default' => 0, 'key' => true)
			),
		));
		
		$this->streams->streams->add_stream(
			'Proser Category',
			'categories',
			'proser',
			'proser_',
			null
		);
		
		$this->streams->streams->add_stream(
			'Prosers',
			'item',
			'proser',
			'proser_',
			null
		);

		// Add the intro field.
		// This can be later removed by an admin.
		$intro_field = array(
			'name'		=> 'lang:proser:intro_label',
			'slug'		=> 'intro',
			'namespace' => 'proser',
			'type'		=> 'wysiwyg',
			'assign'	=> 'item',
			'extra'		=> array('editor_type' => 'advanced', 'allow_tags' => 'n'),
			'required'	=> true
		);
		$this->streams->fields->add_field($intro_field);
		
		$category_fields = array(
			'name'		=> 'Category Title',
			'slug'		=> 'cat_title',
			'namespace' => 'proser',
			'type'		=> 'text',
			'assign'	=> 'categories',
			'extra' => array('max_length' => 64),
			'title_column'		=> true,
			'required'	=> true
		);
		$this->streams->fields->add_field($category_fields);
		$this->streams->streams->update_stream('categories', 'proser', array(
            'view_options' => array(
                'id',
                'cat_title',
                'created',
                'created_by'
            )
        ));

		// Ad the rest of the item fields the normal way.
		$item_fields = array(
				'title' => array('type' => 'VARCHAR', 'constraint' => 200, 'null' => false, 'unique' => true),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 200, 'null' => false),
				'category_id' => array('type' => 'INT', 'constraint' => 11),
				'section_id' => array('type' => 'INT', 'constraint' => 11),
				'location_id' => array('type' => 'INT', 'constraint' => 11, 'default' => 0, 'key' => true),
				'body' => array('type' => 'TEXT'),
				'price' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
				'currency' => array('type' => 'VARCHAR', 'constraint' => 10, 'default' => 'VND'),
				'parsed' => array('type' => 'TEXT'),
				'author_id' => array('type' => 'INT', 'constraint' => 11, 'default' => 0, 'key' => true),
				'created_on' => array('type' => 'INT', 'constraint' => 11),
				'updated_on' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
				'comments_enabled' => array('type' => 'ENUM', 'constraint' => array('no','1 day','1 week','2 weeks','1 month', '3 months', 'always'), 'default' => '3 months'),
				'status' => array('type' => 'ENUM', 'constraint' => array('draft', 'live'), 'default' => 'draft'),
				'featured' => array('type' => 'ENUM', 'constraint' => array('yes', 'no'), 'default' => 'no'),
				'type' => array('type' => 'SET', 'constraint' => array('html', 'markdown', 'wysiwyg-advanced', 'wysiwyg-simple')),
				'preview_hash' => array('type' => 'CHAR', 'constraint' => 32, 'default' => ''),
		);
		return $this->dbforge->add_column('proser_item', $item_fields);
	}

	public function uninstall()
	{
		// This is a core module, lets keep it around.
		$this->dbforge->drop_table('proser_sections');
		$this->dbforge->drop_table('proser_categories');

		$this->load->driver('Streams');
		$this->streams->utilities->remove_namespace('proser');

		// Just in case.
		$this->dbforge->drop_table('proser_item');

		if ($this->db->table_exists('data_streams'))
		{
			$this->db->where('stream_namespace', 'proser')->delete('data_streams');
		}
		return true;
	}

	public function upgrade($old_version)
	{
		return true;

	}
}
