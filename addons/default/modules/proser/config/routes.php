<?php  defined('BASEPATH') or exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	www.your-site.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://www.codeigniter.com/user_guide/general/routing.html
*/

// public
$route['(proser)/(:num)/(:any)']   = 'proser/view/$2/$3';
$route['(proser)/page(/:num)?']           = 'proser/index$2';
$route['(proser)/rss/all.rss']            = 'rss/index';
$route['(proser)/rss/(:any).rss']         = 'rss/category/$2';

// admin
$route['proser/admin/sections(/:any)?'] = 'admin_sections$1';
$route['proser/admin/categories(/:any)?'] = 'admin_categories$1';
$route['proser/admin/fields(/:any)?']		= 'admin_fields$1';
$route['proser/admin/categories-fields(/:any)?']		= 'admin_categories_fields$1';