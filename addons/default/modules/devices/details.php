<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Devices extends Module {

	public $version = '1.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Devices Management',
				'vi' => 'Quản lý thiết bị Hotspot'
			),
			'description' => array(
				'en' => 'Device Management for Wifi Hotspot.',
				'vi' => 'Quản lý thiết bị'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => FALSE, // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',			
		);
		
		$info['sections']['devices'] = array(
					'name' 	=> 'devices:devices', // These are translated from your language file
					'uri' 	=> 'admin/devices',
					'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'devices:add_devices',
							'uri' 	=> 'admin/devices/add',
							'class' => 'add'
							)
					)
		);
		
		if (group_has_role('hotspot', 'admin')) {			
			$info['sections'] ['admin_fields'] = array(
				'name'	=> "Fields",
				'uri'	=> "admin/devices/fields",
				'shortcuts' => array(
						'add_new' => array(
							'name' 	=> 'global:add',
							'uri' 	=> 'admin/devices/fields/add',
							'class' => 'add'
							)
					)
			);		
		}		
							
		return $info;
	}

	public function install()
	{
		$this->load->driver('Streams');
		$this->db->delete('settings', array('module' => 'devices'));
		$this->streams->utilities->remove_namespace('devices');
		
		//Find Location stream ID
		$streams = $this->db
						->select('id, stream_slug, stream_namespace')
						->where('stream_slug','list')
						->where('stream_namespace', 'location')						
						->get('data_streams')
						->result_array();
		if (!$streams) {
			return false;
		}
		$loc_id = $streams[0]['id'];
		//print_r($streams[0]['id']);exit;
		//add stream
		if ( ! $this->streams->streams->add_stream('Hotspot Devices', 'list', 'devices', 'devices_', null)) return false;		
		
		
		$fields = array(
            //Start of Equipment Stream
            array(
                'name' => 'Device Name',
                'slug' => 'name',
                'namespace' => 'devices',                
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'list',
                'required' => true              
            ),
            array(
                'name' => 'MAC Address',
                'slug' => 'mac',
                'namespace' => 'devices',                
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'list',
                'unique'        => true,
                'required' => true                
            ),
            array(
                'name' => 'Manufacturer',
                'slug' => 'manufacturer',
                'namespace' => 'devices',                
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'list'              
            ),
            array(
                'name' => 'Firmware Version',
                'slug' => 'f_version',
                'namespace' => 'devices',                
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'list',              
            ),
            array(
                'name' => 'Hardware Version',
                'slug' => 'h_version',
                'namespace' => 'devices',                
                'type' => 'text',
                'extra' => array('max_length' => 64),
                'assign' => 'list',             
            ),
            array(
                'name' => 'Settings',
                'slug' => 'settings',
                'namespace' => 'devices',
                'type' => 'textarea',
                'assign' => 'list',                
            ),
			array(
			    'name'          => 'Last Ping',
			    'slug'          => 'last_ping',
			    'namespace'     => 'devices',
			    'type'          => 'datetime',
			    'extra'         => array('storage' => 'unix'),
			    'assign'        => 'list'		
			),
			array(
			    'name'          => 'Last Poll',
			    'slug'          => 'last_poll',
			    'namespace'     => 'devices',
			    'type'          => 'datetime',
			    'extra'         => array('storage' => 'unix'),
			    'assign'        => 'list'		
			),
            array(
                'name' => 'Location ID',
                'slug' => 'loc_id',
                'namespace' => 'devices',
                'type' => 'relationship',
                'assign' => 'list',
                'extra' => array('choose_stream' => $loc_id),
                'required' => true
            ),
            array(
                'name' => 'Information',
                'slug' => 'info',
                'namespace' => 'devices',                
                'type' => 'textarea',
                'assign' => 'list'                
            ),
            //End of Equipment stream
        );

        $this->streams->fields->add_fields($fields);

        $this->streams->streams->update_stream('list', 'devices', array(
            'view_options' => array(
            	'name',
                'mac',
                'manufacturer',               
                'f_version',
                'h_version',
                'loc_id',
                'info'
            )
        ));
		
		return TRUE;
		
	}

	public function uninstall()
	{
		$this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
        // utility in the Streams API Utilties driver.
        $this->streams->utilities->remove_namespace('devices');
		$this->db->delete('settings', array('module' => 'devices'));
		return TRUE;
	}


	public function upgrade($old_version)
	{
		$this->load->driver('Streams');
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
    
    
    public function admin_menu(&$menu)
    {
        //Device menu is defind in Location module
	}
}
/* End of file details.php */
