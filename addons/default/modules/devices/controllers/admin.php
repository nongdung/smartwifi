<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Jerel Unruh - PyroCMS Dev Team
 * @website     http://unruhdesigns.com
 * @package     PyroCMS
 * @subpackage  Sample Module
 */
class Admin extends Admin_Controller
{
	protected $section = 'devices';
    public function __construct()
    {
        parent::__construct();

        // Load all the required classes
		$this->load->language('devices');
		$this->load->driver('Streams');
        //role_or_die('hotspot', 'merchant', 'admin', 'Sorry, you don\'t have right to access this area.');
    }

    /**
     * List all billing plans
     */
    public function index()
    {   	
		$this->load->library('splash/sw_redis');
		$this->sw_redis->setPrefix("device:status:");
		
    	$params = array(
		    'stream'    => 'list',
		    'namespace' => 'devices',
		    'limit'	=> 25,
		    'paginate' => 'yes',
		    'pag_segment' => 3,
			'where' => "`created_by` = '".$this->current_user->id."'"
		);
		
		//Find equipment for this merchant
		if (group_has_role('hotspot', 'admin')) {
			unset($params['where']);
		}
			

		
		//Todo: cache
		$entries = $this->streams->entries->get_entries($params);
		foreach ($entries['entries'] as $key => $device) {
			$device_status = $this->sw_redis->hgetall($device['mac']);
			$entries['entries'][$key]['status'] = $device_status;
			//print_r($device_status);
		}
		//$test = $this->streams->entries->get_entry(1,'list','devices');
		
		
        // Build the view using sample/views/admin/form.php
        $this->template
            ->title($this->module_details['name'], lang('devices:list'))
			->set('entries',$entries)	
            ->build('admin/list'); 
    }

	public function location($loc_id=null)
	{
		$this->load->library('splash/sw_redis');
		$this->sw_redis->setPrefix("device:status:");
		
    	$params = array(
		    'stream'    => 'list',
		    'namespace' => 'devices',
		);
		
		if ($loc_id) {
			$entry = $this->streams->entries->get_entry($loc_id, 'list','location');
			if ($entry && ($entry->created_by <> $this->current_user->id) && !group_has_role("hotspot", "admin")) {
				$this->session->set_flashdata('error', "This location is not yours!");
				redirect('admin/location');
			}
			if (group_has_role("hotspot", "admin")) {
				$params['where'] = "`location_id` ='".$loc_id."'";
			} else {
				$params['where'] = "`location_id` ='".$loc_id."' AND `created_by` = '".$this->current_user->id."'";
			}
			
			
		}else{
			//Find equipment for this merchant
			redirect("admin/location");
			
		}
		
		//Todo: pagination
		$entries = $this->streams->entries->get_entries($params);
		foreach ($entries['entries'] as $key => $device) {
			$device_status = $this->sw_redis->hgetall($device['mac']);
			$entries['entries'][$key]['status'] = $device_status;
			//print_r($device_status);
		}
		//$test = $this->streams->entries->get_entry(1,'list','devices');
		
		
        // Build the view using sample/views/admin/form.php
        $this->template
            ->title($this->module_details['name'], lang('devices:list'))
			->set('entries',$entries)	
            ->build('admin/list'); 
	}
    
    /**
     * Create a billingplan
     */
    public function add($location_id = null)
    {    	
		if (isset($_GET['mac'])) {
			$mac = $_GET['mac'];
		} else 
		{
			 $mac ='';
		}		
		
		$validation_rules = array(
			array(
					'field' => 'name',
					'label' => 'Device Name',
					'rules' => 'trim|required|max_length[64]'
			),
			array(
					'field' => 'mac',
					'label' => 'MAC Address',
					'rules' => 'trim|required|max_length[64]|callback__check_unique_mac'
			),
			array(
					'field' => 'manufacturer',
					'label' => 'Manufacturer',
					'rules' => 'trim|max_length[64]'
			),
			array(
					'field' => 'f_version',
					'label' => 'Firmware Version',
					'rules' => 'trim|max_length[64]'
			),
			array(
					'field' => 'h_version',
					'label' => 'Hardware Version',
					'rules' => 'trim|max_length[64]'
			),
			array(
					'field' => 'location_id',
					'label' => 'Location ID',
					'rules' => 'trim|required|is_natural'
			),
			array(
					'field' => 'info',
					'label' => 'Info',
					'rules' => 'trim|max_length[255]'
			),
		);
		
		
		
		$this->form_validation->set_rules($validation_rules);
		
		if ($this->form_validation->run()){
			$entry_data = array(
			    'name' => $this->input->post('name'),
			    'mac' => $this->input->post('mac'),
			    'manufacturer' => $this->input->post('manufacturer'),
			    'f_version' => $this->input->post('f_version'),
			    'h_version' => $this->input->post('h_version'),			    
			    'location_id' => $this->input->post('location_id'),
			    'info' => $this->input->post('info'),
			    
			);
			if ($this->streams->entries->insert_entry($entry_data, 'list', 'devices')){
				
				//delete cache if exists
				$this->pyrocache->delete_cache("device_m", "get_location", array($this->input->post('mac')));
				
				$this->session->set_flashdata('success', "Success");
				redirect('admin/devices');
			} else {
				$this->session->set_flashdata('error', "Failed !");
				redirect('admin/devices');
			}
		}
		
		//location list
		$location_list= array(null => "-----");
		$params = array(
				'stream'    => 'list',
    			'namespace' => 'location',
    			'where'		=> "`created_by` = '".$this->current_user->id."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		$isValidLoc = false;
		if ($entries['total']) {
			foreach ($entries['entries'] as $item) {
				$location_list[$item['id']] = $item['loc_name'];
				if ($item['id'] == $location_id) { $isValidLoc = TRUE;}
			}
		} else {
			$this->session->set_flashdata('error', "Please add a location before adding devices!");
			redirect('admin/location/add');
		}
		if ($location_id && !$isValidLoc) {
			$this->session->set_flashdata('error', "This is not your location!");
			redirect('admin/location');
		}
		
		
		$this->template
	            ->title($this->module_details['name'], lang('devices:add'))
				->set('mac',$mac)
				->set('location_id',$location_id)
				->set('location_list', $location_list)
	            ->build('admin/add');		  
    }

	public function delete($id = null)
	{
		if (! $id) {
			redirect('admin/devices');
		} else {
			$entry = $this->streams->entries->get_entry($id, 'list','devices');
			
			if (!group_has_role('hotspot', 'admin') && ($entry->created_by <> $this->current_user->id)) {
				$this->session->set_flashdata('error', "This devices is not yours!");
				redirect('admin/devices');
			}
			
			//ok, now we delete
			$this->streams->entries->delete_entry($id, 'list', 'devices');
			
			//Delete cache related
			$this->pyrocache->delete_cache("device_m", "get_location", array($entry->mac));
			
			$this->session->set_flashdata('success', "Deleted!");
			redirect('admin/devices');
		}
		
	}
	
	public function edit($id = null)
	{
		
		if (! $id) {
			redirect('admin/devices');
		}
		
		$validation_rules = array(
			array(
					'field' => 'name',
					'label' => 'Device Name',
					'rules' => 'trim|required|max_length[64]'
			),
			array(
					'field' => 'mac',
					'label' => 'MAC Address',
					'rules' => 'trim|required|max_length[64]'
			),
			array(
					'field' => 'manufacturer',
					'label' => 'Manufacturer',
					'rules' => 'trim|max_length[64]'
			),
			array(
					'field' => 'f_version',
					'label' => 'Firmware Version',
					'rules' => 'trim|max_length[64]'
			),
			array(
					'field' => 'h_version',
					'label' => 'Hardware Version',
					'rules' => 'trim|max_length[64]'
			),
			array(
					'field' => 'location_id',
					'label' => 'Location ID',
					'rules' => 'trim|required|is_natural'
			),
			array(
					'field' => 'info',
					'label' => 'Info',
					'rules' => 'trim|max_length[255]'
			),
		);
		
		
		
		$this->form_validation->set_rules($validation_rules);
		
		if ($this->form_validation->run()){
			$entry_data = array(
			    'name' => $this->input->post('name'),
			    'mac' => $this->input->post('mac'),
			    'manufacturer' => $this->input->post('manufacturer'),
			    'f_version' => $this->input->post('f_version'),
			    'h_version' => $this->input->post('h_version'),			    
			    'location_id' => $this->input->post('location_id'),
			    'info' => $this->input->post('info'),
			    
			);
			if ($this->streams->entries->update_entry($this->input->post('id'),$entry_data, 'list', 'devices')){
				
				//delete cache
				$this->pyrocache->delete_cache("device_m", "get_location", array($this->input->post('mac')));
				
				$this->session->set_flashdata('success', "Success");
				redirect('admin/devices');
			} else {
				$this->session->set_flashdata('error', "Failed !");
				redirect('admin/devices');
			}
		}
		
		//location list
		$location_list= array(null => "-----");
		$params = array(
				'stream'    => 'list',
    			'namespace' => 'location',
    			'where'		=> "`created_by` = '".$this->current_user->id."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			foreach ($entries['entries'] as $item) {
				$location_list[$item['id']] = $item['loc_name'];
			}
		} else {
			$this->session->set_flashdata('error', "Please add a location before adding devices!");
			redirect('admin/location/add');
		}
		
		//Device details
		$device_detail = $this->streams->entries->get_entry($id,'list', 'devices');
		if (!group_has_role('hotspot', 'admin') && ($device_detail->created_by_user_id <> $this->current_user->id)) {
			$this->session->set_flashdata('error', "This is not your devices!");
			redirect('admin/devices');
		}
		
		$this->template
	            ->title($this->module_details['name'], lang('devices:edit'))
				->set('location_list', $location_list)
				->set('device_detail', $device_detail)
	            ->build('admin/edit');	
		
	}

	public function view($id='')
	{
		if (! $id) {
			redirect('admin/devices');
		}
		
		//Device details
		$device_detail = $this->streams->entries->get_entry($id,'list', 'devices');
		if (!group_has_role('hotspot', 'admin') && ($device_detail->created_by_user_id <> $this->current_user->id) && $device_detail ) {
			$this->session->set_flashdata('error', "This is not your devices!");
			redirect('admin/devices');
		}
		
		//Location detail
		$loc_detail = $this->streams->entries->get_entry($device_detail->location_id,'list', 'location');
		
		$this->template
	            ->title($this->module_details['name'], lang('devices:details'))
				->set('device_detail', $device_detail)
				->set('loc_detail', $loc_detail)
	            ->build('admin/view');
	}
	
	public function settings($id = 0)
	{	
		if (! $id) {
			redirect('admin/devices');
		}
		$this->load->model('device_m');
		
		$check = $this->pyrocache->model('device_m', 'check_device_owner', array($this->current_user->id, $id), 7200);
		if (! $check) {
			$this->session->set_flashdata('error', "This is not your devices!");
			redirect('admin/devices');
		}
		
		$validation_rules = array(
			array(
					'field' => 'ssid_2g',
					'label' => 'SSID for 2.4GHz',
					'rules' => 'trim|required|max_length[32]|alpha_dash'
			),
			array(
					'field' => 'ssid_5g',
					'label' => 'SSID for 5GHz',
					'rules' => 'trim|required|max_length[32]|alpha_dash'
			),
			array(
					'field' => 'gateway_ip',
					'label' => 'Gateway IP',
					'rules' => 'trim|required|valid_ip'
			),
			array(
					'field' => 'gateway_network',
					'label' => 'Gateway Network',
					'rules' => 'trim|required|valid_ip'
			),
			array(
					'field' => 'netmask',
					'label' => 'Netmask',
					'rules' => 'trim|required|valid_ip'
			),
			array(
					'field' => 'free_url',
					'label' => 'Free URL',
					'rules' => 'trim|max_length[255]|xss_clean'
			),
			array(
					'field' => 'free_domains',
					'label' => 'Free Domains',
					'rules' => 'trim|max_length[255]|xss_clean'
			),
			array(
					'field' => 'allowed_mac',
					'label' => 'Allowed MAC',
					'rules' => 'trim|max_length[255]|xss_clean'
			),
		);
		$this->form_validation->set_rules($validation_rules);
		if ($this->form_validation->run()){
			unset($_POST['btnAction']);

			$entry_data = array(
		        'settings'    => serialize($_POST)
		    );
			$this->streams->entries->update_entry($this->input->post('id'), $entry_data, 'list', 'devices');
			
			$this->session->set_flashdata('success', "Updated");
			redirect('admin/devices');
		}
		
		//Device details
		$device_detail = $this->device_m->get_device_details($id);
		if (count($device_detail)) {
			$device_detail['settings'] = unserialize($device_detail['settings']);
		}
		
		
		$this->template
	            ->title($this->module_details['name'], lang('devices:settings'))
				->set('device_detail', $device_detail)
				//->set('loc_detail', $loc_detail)
	            ->build('admin/settings');
	}

	public function _check_unique_mac($mac='')
	{
		$params = array(
				'stream'    => 'list',
    			'namespace' => 'devices',
    			'where'		=> "`mac` = '".$mac."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		if (! $entries['total']) {
			return true;
		} else {
			$this->form_validation->set_message('_check_unique_mac', "Error: MAC existed!");
			return false;
		}
	}

}