<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a splash module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	splash Module
 */
class Apis extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('device_m');
	}

	public function index()
	{
		exit('No direct script access allowed');
		
	}
	
	public function ping()
	{
		$secret = "xpHWD7YbSYfTnNWW";
		$this->load->library('splash/sw_redis');
		$this->sw_redis->setPrefix("device:status:");
		$config_version = 0;
		if (isset($_GET['mac']) && ($_GET['mac'] != '')) {
			$mac = $_GET['mac'];
			
			//Get current config_version
			$config_version = $this->sw_redis->hget($mac, 'config_version');
			if (! $config_version) {$config_version = 0;}
			
			$data = array(
				'type' => isset($_GET['type'])?$_GET['type']:"",
				'load' => isset($_GET['load'])?$_GET['load']:0,
				'online' => isset($_GET['online'])?$_GET['online']:0,
				'uptime' => isset($_GET['uptime'])?$_GET['uptime']:"-",
				'localtime' => isset($_GET['localtime'])?$_GET['localtime']:"-",
				'freemem' => isset($_GET['freemem'])?$_GET['freemem']:0,
				'totalmem' => isset($_GET['totalmem'])?$_GET['totalmem']:0,
				'updated' => time()
			);
			$code = isset($_GET['code'])?$_GET['code']:0;
			if (md5($secret.$mac.$data['localtime']) == $code) {
				//if this is valid request
				$this->sw_redis->hmset($mac, $data);
			}
		}
		$this->output
				->set_content_type('text/plain')
				->set_output($config_version);
	}

	public function fetch_config()
	{
		
		if ($this->_is_curl()){
		/*
		[ -n "$HS_2G_SSID"] && {
                uci set wireless.@wifi-iface[0].ssid='$HS_2G_SSID'
                }

                [ -n "$HS_2G_MODE"] && {
                uci set wireless.@wifi-iface[0].mode='$HS_2G_MODE'
                }

                [ -n "$HS_2G_ENCRYPTION"] && {
                uci set wireless.@wifi-iface[0].encryption='$HS_2G_ENCRYPTION'
                }

                [ -n "$HS_2G_NETWORK"] && {
                uci set wireless.@wifi-iface[0].network='$HS_2G_NETWORK'
                }

                [ -n "$HS_2G_KEY"] && [ "$HS_2G_ENCRYPTION" != "none" ] && {
                uci set wireless.@wifi-iface[0].key='$HS_2G_KEY'
                }
		 */
		
		$mac = isset($_GET['mac'])?$_GET['mac']:'0';
		$state = isset($_GET['state'])?$_GET['state']:'0';
		
		$config = "#/bin/sh";
		$config .= "\n#";
		$config .= "\nHS_STATE=".$state;
		$config .= "\nHS_MAC=".$mac;
		$config .= "\nHS_NETWORK=172.18.0.0";
		$config .= "\nHS_NETMASK=255.0.0.0";
		$config .= "\necho \"ok run\"";
		$this->output
				->set_content_type('text/plain')
				->set_output($config);
		} else {exit("success");}
	}

	public function enable_module($mac_address='')
	{
		if (! $this->_is_curl() || ($mac_address == '') || ($mac_address == null) ){
			exit();
		}
		$module_string = '';
		//$enable_modules = $this->device_m->get_enable_module($mac_address);
		$enable_modules = $this->pyrocache->model('device_m', 'get_enable_module', array($mac_address),600);
		foreach ($enable_modules as $module) {
			$module_string .= $module."\n";
		}
		
		$this->output
				->set_content_type('text/plain')
				->set_output($module_string);
	}

	function _is_curl()
	{
		if (isset($_SERVER['HTTP_USER_AGENT']))
		{
			$user_agent = trim($_SERVER['HTTP_USER_AGENT']);
		} else {
			return FALSE;
		}
		if (preg_match("/curl/", $user_agent)){
			return TRUE;
		} else {
			return false;
		}
	}

}
