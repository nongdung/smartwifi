<?php defined('BASEPATH') or exit('No direct script access allowed');?>
	<table>
		<thead>
				<tr>
					<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
	    			<th style="width:5%;">#</th>
	    			<th style="width:10%;"><?php echo lang('devices:name'); ?></th>
	    			<th style="width:20%;"><?php echo lang('devices:mac'); ?></th>
	    			<th style="width:10%;"><?php echo lang('cp:location'); ?></th>
	    			<th style="width:10%;"><?php echo lang('cp:status'); ?></th>
	    			<th style="width:10%;"><?php echo lang('devices:last_ping'); ?></th>
	    			<th style="width:40%;"></th>
				</tr>
		</thead>
		<tfoot>
				<tr>
					<td colspan="10">
						<div class="inner"><?php if (isset($entries) && $entries['total']) echo $entries['pagination']; ?></div>
					</td>
				</tr>
		</tfoot>
		<tbody>
			    <?php $i=$this->uri->segment(3)+1; ?>
				<?php foreach( $entries['entries'] as $entry ): ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $entry['id']); ?></td>
					<td><?php echo $i++; ?></td>
					<td><?php echo $entry['name']; ?></td>
					<td><?php echo $entry['mac']; ?></td>					
					<td><?php echo $entry['location_id']['loc_name'];?></td>
					<td>
						<?php
						$current_time = time();
						if (isset($entry['status']) && count($entry['status'])) {
							if ($entry['status']['updated'] + 300 > $current_time) {
								echo '<span style="color:blue;">Online</span>';
							} else {
								echo '<span style="color:red;">Offline</span>';
							}
							
						} else {
							echo "-";
						}
						
						?>
					</td>
					<td>
						<?php
						if (isset($entry['status']) && count($entry['status'])) {
							echo date("Y-m-d H:i:s", $entry['status']['updated']);
						} else {
							echo "-";
						}
						
						?>
					</td>
					<td class="actions">
						<?php echo
						anchor('admin/devices/settings/'.$entry['id'], lang('devices:settings'), 'class="button"').' '.
						anchor('admin/devices/view/'.$entry['id'], lang('global:view'), 'class="button"').' '.						
						anchor('admin/devices/edit/'.$entry['id'], lang('global:edit'), 'class="button"').' '.
						anchor('admin/devices/delete/'.$entry['id'], 	lang('global:delete'), array('class'=>'button confirm')); ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
	</table>
