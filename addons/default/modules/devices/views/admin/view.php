<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('devices:details');?></h4>
</section>

<section class="item">
	<table class="table-list" border="0" cellspacing="0">
		<?php if (group_has_role("hotspot", "admin")): ?>
		<tr>
			<td style="width:30%;">ID</td>
			<td style="width:70%;"><?php echo $device_detail->id;?></td>
		</tr>
		<?php endif; ?>
		<tr>
			<td style="width:30%;"><?php echo lang('devices:name');?></td>
			<td style="width:70%;"><?php echo $device_detail->name;?></td>
		</tr>
		<tr>
			<td style="width:30%;"><?php echo lang('devices:mac');?></td>
			<td style="width:70%;"><?php echo $device_detail->mac;?></td>
		</tr>
		<tr>
			<td style="width:30%;"><?php echo lang('devices:manufacturer');?></td>
			<td style="width:70%;"><?php echo $device_detail->manufacturer;?></td>
		</tr>		
		<tr>
			<td style="width:30%;"><?php echo lang('devices:h_version');?></td>
			<td style="width:70%;"><?php echo $device_detail->h_version;?></td>
		</tr>
		<tr>
			<td style="width:30%;"><?php echo lang('devices:f_version');?></td>
			<td style="width:70%;"><?php echo $device_detail->f_version;?></td>
		</tr>
		<tr>
			<td style="width:30%;"><?php echo lang('devices:last_ping');?></td>
			<td style="width:70%;"><?php echo $device_detail->last_ping;?></td>
		</tr>
		<tr>
			<td style="width:30%;"><?php echo lang('devices:last_poll');?></td>
			<td style="width:70%;"><?php echo $device_detail->last_poll;?></td>
		</tr>
		<tr>
			<td style="width:30%;"><?php echo lang('cp:location');?></td>
			<td style="width:70%;"><?php if (isset($device_detail->location_id)) {echo $device_detail->location_id;}?> <?php if (isset($loc_detail->loc_name)) {echo " - <i>(".$loc_detail->loc_name."</i>)";}?></td>
		</tr>
		<tr>
			<td style="width:30%;"><?php echo lang('devices:info');?></td>
			<td style="width:70%;"><?php echo $device_detail->info;?></td>
		</tr>
	</table>
</section>
