<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('devices:list'); ?></h4>
</section>

<section class="item">
	
	<?php if ($entries['total'] > 0): ?>
	<?php echo form_open('admin/devices/delete');?>	
	    <div id="filter-stage">
			<?php echo $this->load->view('admin/ajax_list') ?>
		</div>		
		<div class="table_action_buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
		</div>
	<?php echo form_close(); ?>
	
	<?php else: ?>
		<div class="no_data"><?php echo lang('devices:no_items'); ?></div>
	<?php endif;?>
</section>