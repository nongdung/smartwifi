<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('devices:edit_devices');?></h4>
</section>

<section class="item">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		<?php echo form_hidden('id',$device_detail->id);?>
		<div class="form_inputs">
			<ul>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="name"><?php echo lang('devices:name');?><span>*</span></label>
					<div class="input"><?php echo form_input('name', $device_detail->name); ?></div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="mac"><?php echo lang('devices:mac');?><span>*</span></label>
					<div class="input"><?php echo form_input('mac', $device_detail->mac); ?></div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="manufacturer"><?php echo lang('devices:manufacturer');?></label>
					<div class="input"><?php echo form_input('manufacturer', $device_detail->manufacturer); ?></div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="h_version"><?php echo lang('devices:h_version');?></label>
					<div class="input"><?php echo form_input('h_version', $device_detail->h_version); ?></div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="f_version"><?php echo lang('devices:f_version');?></label>
					<div class="input"><?php echo form_input('f_version', $device_detail->f_version); ?></div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="location_id"><?php echo lang('cp:location');?><span>*</span></label>
					<div class="input"><?php echo form_dropdown('location_id', $location_list,isset($device_detail->location_id)?$device_detail->location_id:""); ?></div>
				</li>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="info"><?php echo lang('devices:info');?></label>
					<div class="input"><?php echo form_textarea('info', $device_detail->info); ?></div>
				</li>				
			</ul>
		</div>
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
		</div>
	<?php echo form_close(); ?>
</section>