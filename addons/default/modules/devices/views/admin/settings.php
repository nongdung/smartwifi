<?php defined('BASEPATH') or exit('No direct script access allowed');?>
<section class="title">
    <h4><?php echo lang('devices:settings');?></h4>
</section>

<section class="item">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		<div class="form_inputs">
			<?php echo form_hidden('id', $device_detail['id'])?>
			<ul>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="ssid"><?php echo lang('devices:ssid_2g');?> <span>*</span></label>
					<div class="input"><?php echo form_input('ssid_2g', isset($device_detail['settings']['ssid_2g'])?$device_detail['settings']['ssid_2g']:'Free-Wifi'); ?></div>
				</li>
				
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="ssid"><?php echo lang('devices:ssid_5g');?> <span>*</span></label>
					<div class="input"><?php echo form_input('ssid_5g', isset($device_detail['settings']['ssid_5g'])?$device_detail['settings']['ssid_5g']:'Free-Wifi-5G'); ?></div>
				</li>
				
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="gateway_ip"><?php echo lang('devices:gateway_ip');?> <span>*</span></label>
					<div class="input"><?php echo form_input('gateway_ip', isset($device_detail['settings']['gateway_ip'])?$device_detail['settings']['gateway_ip']:'172.16.0.1'); ?></div>
				</li>
				
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="gateway_network"><?php echo lang('devices:gateway_network');?> <span>*</span></label>
					<div class="input"><?php echo form_input('gateway_network', isset($device_detail['settings']['gateway_network'])?$device_detail['settings']['gateway_network']:'172.16.0.0'); ?></div>
				</li>
				
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="netmask"><?php echo lang('devices:netmask');?> <span>*</span></label>
					<div class="input"><?php echo form_input('netmask', isset($device_detail['settings']['netmask'])?$device_detail['settings']['netmask']:'255.255.255.0'); ?></div>
				</li>
				
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="free_url"><?php echo lang('devices:free_url');?></label>
					<div class="input"><?php echo form_input('free_url', isset($device_detail['settings']['free_url'])?$device_detail['settings']['free_url']:''); ?></div>
					<i> (URLs separated by comma)</i>
				</li>
				
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="free_domains"><?php echo lang('devices:free_domains');?></label>
					<div class="input"><?php echo form_input('free_domains', isset($device_detail['settings']['free_domains'])?$device_detail['settings']['free_domains']:''); ?></div>
					<i> (Subdomain start with *, eg: *.smartwifi.vn | multi domains separated by comma)</i>
				</li>
				
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="allowed_mac"><?php echo lang('devices:allowed_mac');?></label>
					<div class="input"><?php echo form_input('allowed_mac', isset($device_detail['settings']['allowed_mac'])?$device_detail['settings']['allowed_mac']:''); ?></div>
					<i> (in format XX-XX-XX-XX-XX-XX)</i>
				</li>
						
			</ul>
		</div>
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
		</div>
	<?php echo form_close(); ?>
</section>