<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Device_m extends MY_Model {
	function __construct()
		{
        // Call the Model constructor
        parent::__construct();
			$this->load->driver('Streams');
    	}
		
	function get_info($mac){
        $info = array();
		$params = array(
		    'stream'    => 'list',
		    'namespace' => 'devices',
		    'where'		=> "`mac` = '".$mac."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			$info['created_by'] = $entries['entries'][0]['created_by']['user_id'];
			$info['loc_id'] = $entries['entries'][0]['loc_id']['id'];
			$info['long_lat'] = $entries['entries'][0]['loc_id']['long_lat'];
			$info['province'] = $entries['entries'][0]['loc_id']['province'];
			$info['city'] = $entries['entries'][0]['loc_id']['city'];
			$info['address'] = $entries['entries'][0]['loc_id']['address'];
			$info['template_id'] = $entries['entries'][0]['loc_id']['template_id'];
		}
        return $info;
    }

	//create a new item
	public function get_location($mac)
	{
		$params = array(
		    'stream'    => 'list',
		    'namespace' => 'devices',
		    'where'		=> "mac = '".$mac."'"
		);
		
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			return $entries['entries'][0];
		} else {
			return array();
		}
	}
	
	public function get_from_loc($loc_id)
	{
		$params = array(
		    'stream'    => 'list',
		    'namespace' => 'devices',
		    'where'		=> "`loc_id` = '".$loc_id."'"
		);
		
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			return $entries['entries'][0];
		} else {
			return array();
		}
	}
	
	function is_valid_mac($mac)
	{
	  // 01:23:45:67:89:ab
	  if (preg_match('/^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$/', $mac))
	    return true;
	  // 01-23-45-67-89-ab
	  if (preg_match('/^([a-fA-F0-9]{2}\-){5}[a-fA-F0-9]{2}$/', $mac))
	    return true;	  
	  else
	    return false;
	}

	/*
	 * Get device list for select in Filters
	 */
	public function get_device_list($merchant_id = null)
	{
		$device_list= array(null => "-- All --");
		$params = array(
				'stream'    => 'list',
    			'namespace' => 'devices',
    			'where'		=> "`created_by` = '".$merchant_id."'"
		);
		$entries = $this->streams->entries->get_entries($params);
		if ($entries['total']) {
			foreach ($entries['entries'] as $item) {
				$device_list[$item['mac']] = $item['name'];				
			}
		}
		
		return $device_list;
	}

	public function get_device_details($device_id = null)
	{
		if (! $device_id) return array();
		$entry = $this->streams->entries->get_entry($device_id, 'list', 'devices');
		$data = json_decode(json_encode($entry),TRUE); 
		return $data;
	}
	
	public function check_device_owner($merchant_id='', $device_id)
	{
		if (!$merchant_id || !$device_id) return false;
		$entry = $this->streams->entries->get_entry($device_id, 'list', 'devices');
		if (isset($entry) && ($merchant_id == $entry->created_by)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	/*
	 * Get enable modules for a device based on its mac address
	 * 
	 */
	
	public function get_enable_module($mac_address='')
	{
		$enable_modules = array();
		if (($mac_address == '') || ($mac_address == null) ){
			return $enable_modules;
		}
		
		$this->load->model('location/location_m');
		$this->load->model('hotspot/service_class_m');
		$this->load->model('hotspot/merchant_m');
		
		$device = $this->device_m->get_location($mac_address);
		
		if (isset($device['location_id']['id'])) {
			$loc_id = $device['location_id']['id'];
			$merchant_id = $device['created_by']['user_id'];
		} else {
			$loc_id = 1;
			$merchant_id = 1;
			//$router = $this->pyrocache->model('device_m', 'get_from_loc', array($loc_id), 7200);
		}

		$merchant_info = $this->merchant_m->get_info($merchant_id);
		
		if (empty($merchant_info) || ($merchant_info['class_name'] == "Free") || (isset($merchant_info['end_date']) && $merchant_info['end_date'] < time())) {
			$service_class = $this->pyrocache->model('service_class_m', 'get_by_name', array("Free"), 7200);			
		} else {
			$service_class = $this->pyrocache->model('service_class_m', 'get_by_id', array($merchant_info['class_id']), 7200);
		}
		
		$available_modules = $this->pyrocache->model('service_class_m', 'get_login_modules', array($service_class['class_id']), 7200);
		
		foreach ($available_modules as $module) {
			$this->load->model($module."/".$module."_m");
			$module_model = $module."_m";
			$module_settings = $this->$module_model->get_settings($loc_id);
			
			if (isset($module_settings['status']['val']) && ($module_settings['status']['val'] == 'Enable')) {
				array_push($enable_modules, $module);
			}
			
		}
		
		return $enable_modules;
	}
}