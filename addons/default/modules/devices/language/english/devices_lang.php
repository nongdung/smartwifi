<?php
$lang['devices:devices']			=	'Devices';
$lang['devices:name']			=	'Device Name';
$lang['devices:list']			=	'Devices List';
$lang['devices:no_items']			=	'No items';
$lang['devices:add_devices']			=	'Add device';
$lang['devices:edit_devices']			=	'Edit device';

$lang['devices:mac']			=	'MAC Address';
$lang['devices:manufacturer']			=	'Manufacturer';
$lang['devices:h_version']			=	'Hardware Version';
$lang['devices:f_version']			=	'Firmware Version';
$lang['devices:location_id']			=	'Location ID';
$lang['devices:info']			=	'Info';

$lang['devices:settings']			=	'Settings';
$lang['devices:details']			=	'Device Details';
$lang['devices:ssid']			=	'SSID';
$lang['devices:ssid_2g']			=	'SSID 2G';
$lang['devices:ssid_5g']			=	'SSID 5G';
$lang['devices:gateway_ip']			=	'Gateway IP';
$lang['devices:gateway_network']			=	'Gateway Network';
$lang['devices:netmask']			=	'Netmask';
$lang['devices:free_url']			=	'Free URL';
$lang['devices:free_domains']			=	'Free Domains';
$lang['devices:allowed_mac']			=	'Allowed MAC';

$lang['devices:last_ping']			=	'Last Ping';
$lang['devices:last_poll']			=	'Last Poll';
?>