<?php
$lang['devices:devices']			=	'Thiết bị';
$lang['devices:name']			=	'Tên thiết bị';
$lang['devices:list']			=	'Danh sách thiết bị';
$lang['devices:no_items']			=	'No items';
$lang['devices:add_devices']			=	'Thêm thiết bị';
$lang['devices:edit_devices']			=	'Sửa thiết bị';

$lang['devices:mac']			=	'Địa chỉ MAC';
$lang['devices:manufacturer']			=	'Nhà sản xuất';
$lang['devices:h_version']			=	'Phiên bản phần cứng';
$lang['devices:f_version']			=	'Phiên bản phần mềm';
$lang['devices:location_id']			=	'ID địa điểm';
$lang['devices:info']			=	'Thông tin';

$lang['devices:settings']			=	'Cài đặt';
$lang['devices:details']			=	'Chi tiết thiết bị';
$lang['devices:ssid']			=	'SSID';
$lang['devices:ssid_2g']			=	'SSID 2G';
$lang['devices:ssid_5g']			=	'SSID 5G';
$lang['devices:gateway_ip']			=	'Gateway IP';
$lang['devices:gateway_network']			=	'Gateway Network';
$lang['devices:netmask']			=	'Netmask';
$lang['devices:free_url']			=	'URL truy cập miễn phí';
$lang['devices:free_domains']			=	'Tên miền truy cập miễn phí';
$lang['devices:allowed_mac']			=	'Allowed MAC';

$lang['devices:last_ping']			=	'Thời điểm Ping cuối';
$lang['devices:last_poll']			=	'Last Poll';
?>