<?php
	if ( session_status() == PHP_SESSION_NONE ) {
	  session_start();
	}
  require_once (APPPATH . 'libraries/google/autoload.php'); // or wherever autoload.php is located

  class Google {
  	var $client;
	var $ci;
	var $plus;
	
	public function __construct() {
	  $this->ci =& get_instance();
	  $this->client = new Google_Client();
	  //$this->client->setApplicationName("SmartWifi");
	  $this->client->setClientId($this->ci->config->item('client_id', 'google'));
	  $this->client->setClientSecret($this->ci->config->item('client_secret', 'google'));
	  $this->client->setRedirectUri($this->ci->config->item('redirect_uri', 'google'));
	  $this->client->setDeveloperKey($this->ci->config->item('api_key', 'google'));
	  $this->client->addScope('email');
	  $this->client->addScope('profile');
	  //$this->client->addScope('https://www.googleapis.com/auth/plus.login'); //profile
		
	}
	
	public function getToken($code)
	{
		$this->client->authenticate($code);
		try {	
			
			$jsonTokens = $this->client->getAccessToken();
		}
		catch (Google_IO_Exception $e) {
			$jsonTokens = '';
		}
		catch (Google_Auth_Exception $e) {
			$jsonTokens = '';
		}
		return $jsonTokens;
		
	}
	
	public function getAuthUrl($state = null, $scope = null)
	{
		
		$this->client->addScope($scope);
		$this->client->setState($state);
		return $this->client->createAuthUrl();
	}
	
	public function getUserInfo($token)
	{
		$this->client->setAccessToken($token);
		
		$this->plus = new Google_Service_Plus($this->client);
		try {
			$userProfile = $this->plus->people->get('me');
		}
		catch (Google_IO_Exception $e) {
			$userProfile = '';
		}
		catch (Google_Auth_Exception $e) {
			$userProfile = '';
		}
		return $userProfile;//->getEmails();
	}

  
  }