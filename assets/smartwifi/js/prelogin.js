function preUpdate(resp){
    console.log("RESP: " + JSON.stringify(resp));
    if (swController.clientState == 1) {
    	var e = document.getElementById('location_id');
    	if((e != null) && (e.value != null)){
    		window.location.assign("login/" + e.value);
    	}
    } else if (swController.clientState == 0){
    	swController.logon('prelogin','prelogin');
    }
}
    	
window.onload = function(){
			
    		if (swController.clientState == 0) {
				swController.onUpdate = preUpdate;
				swController.logon('prelogin','prelogin');
			} else {
				swController.onUpdate = function(){
					if (swController.clientState == 0) {
						swController.onUpdate = preUpdate;
						swController.logon('prelogin','prelogin');
					} else if (swController.clientState == 1) {
						swController.onUpdate = preUpdate;
						preUpdate();
					}
				};	
			}
			
			setTimeout(function() {
				var node = document.getElementById('location_id');
				if (node.firstChild) {
				    node = node.firstChild;
				    node.nodeValue = "Wifi unavailable!";
				} else {
				    node.innerHTML = "Wifi unavailable!";
				}
			},5000);
};