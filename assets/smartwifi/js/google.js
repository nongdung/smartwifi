GoogleClientLoad =	function () {
	gapi.client.setApiKey(swGoogle.apiKey);
	window.setTimeout(swGoogle.checkAuth,100);
};

var swGoogle = {};

swGoogle.checkAuth = function checkAuth() {
	gapi.auth.authorize({client_id: swGoogle.clientId, scope: swGoogle.scopes, immediate: true}, swGoogle.handleAuthResult);
};

swGoogle.handleAuthResult =	function(authResult) {
        if (authResult && !authResult.error) {
			swGoogle.makeApiCall();
        } else {
			$('#content').html('<div class="text-danger">Google login failed! Click <a href="http://1.0.0.0">here</a> to retry!</div>');
        }
};

swGoogle.makeApiCall = function() {
		gapi.client.load('plus', 'v1', function() {
			var request = gapi.client.plus.people.get({
				'userId': 'me'
			});
		request.execute(function(resp) {
				var primaryEmail;
				var location_id = document.getElementById('location_id');
				var client_mac = document.getElementById('client_mac');	
				
				for (var i=0; i < resp.emails.length; i++) {
					if (resp.emails[i].type === 'account') primaryEmail = resp.emails[i].value;
				}
				console.log(JSON.stringify(resp));
				if (resp.id) {
					if (swUser.profile_type == "google") {
						if (swUser.social_network_id != resp.id) {
							//Found new user => Try to created new profile and update current_profile table
							userdata = {
								client_mac			: client_mac.value,
								social_network_id	: resp.id,
								profile_type		: "google",
								name				: resp.displayName,
								email				: primaryEmail,
								first_name			: resp.name.givenName,
								last_name			: resp.familyName,
								language			: resp.language,
								pic_url 			: resp.image.url,																	
							};
							if (resp.gender == "male") {
								userdata.gender = 0;
							} else if (resp.gender == "female") {
								userdata.gender = 1;
							} else {
								userdata.gender = 2;
							}
										
							$.post( "husers/apis/create_profile", userdata, function( response ) {
								swUser = response;
								console.log(JSON.stringify(response));
							}); 
						};
					} else {
						//Try to create new facebook profile AND replace current_profile table with new created
						userdata = {
								client_mac			: client_mac.value,
								social_network_id	: resp.id,
								profile_type		: "google",
								name				: resp.displayName,
								email				: primaryEmail,
								first_name			: resp.name.givenName,
								last_name			: resp.familyName,
								language			: resp.language,
								pic_url 			: resp.image.url,																	
						};
						if (resp.gender == "male") {
								userdata.gender = 0;
						} else if (resp.gender == "female") {
								userdata.gender = 1;
						} else {
								userdata.gender = 2;
						}
						
						if ((swUser.social_network_id == null)
							&& (swUser.profile_type == null)
							&& (swUser.email == null)
							&& (swUser.mobile == null)
							&& (swUser.name == null)
							&& (swUser.profile_id)
							&& (swUser.client_mac)
						) {
							//This is anonymous profile
							userdata.anonymous = "1";
							userdata.profile_id = swUser.profile_id;
						};		
						
										
						$.post( "husers/apis/create_profile", userdata, function( response ) {
								swUser = response;
								console.log(JSON.stringify(response));
						});
					};					
				}
				
				if (typeof woopraTracker === 'object') {
					woopra.identify({
						ggid : resp.id,
						name : resp.displayName,
						email : primaryEmail,
						gender : resp.gender,						
						profile : resp.url,
					}).push();
				}

				var heading = document.createElement('h4');
				var image = document.createElement('img');
				image.src = resp.image.url;
				heading.appendChild(document.createTextNode('Hi! ' + resp.displayName));
				document.getElementById('content').appendChild(heading);
				
				var wait_time = document.getElementById('wait_time');
				var val = 1000 * parseInt(wait_time.value);
    			selectedDate = new Date().valueOf() + val;
    			swGoogle.clock.countdown(selectedDate.toString());
			});
		});
};

swGoogle.ggConnect = function(){
	swController.onUpdate = SmartWifi.updateUI;	
		if ( swController.clientState == 0 ) {
			var username;
			var password;	
			var u = document.getElementById('gg-username');
			var p = document.getElementById('gg-password');
			if (u != null) {username = u.value;} else {username ='';}
			if (p != null) {password = p.value;} else {password ='';}
			
			if (typeof woopraTracker === 'object') {
				woopra.track("Google Login", {
	        			Type: "Attempt"
				});
			}
			
			//Piwik tracking
			if (typeof _paq === 'object') {
				_paq.push(['trackEvent', 'Wifi', 'Login Attempt', 'Google']);
			}
	
			if (username != '' && password != '') {		 
				setTimeout('swController.logon(\''+ username + '\',\'' + password + '\')', 500);
				setTimeout(function() {
					var gg_result = {action: 'google' };
					if (swController.clientState == 1){
						gg_result.action_result = 1;		
						if (typeof woopraTracker === 'object') {
							woopra.track("Google Login", {
	        						Type: "Success"
							});
						}
	
					} else {
						gg_result.action_result = 0;
						if (typeof woopraTracker === 'object') {
							woopra.track("Google Login", {
	        						Type: "Failed"
							});
						}
					}
					
					SmartWifi.raw_log(gg_result);
					
					window.scrollTo(0,0);
				},1500);
			} else {
				$('#content').html('<div class="text-danger">Error! Hotspot settings incorect!</div>');
				return false;
			}
		} else {
			$('#content').html('<div class="text-danger">Error! Wifi is not ready!</div>');
			return false;
		}
};