$(document).ready(function(){
		var obj = {};
		obj[swAjaxSetup.csrf_token_name] = swAjaxSetup.csrf_hash;
		$.ajaxSetup({data: obj});
	    console.log('start ready function');	
	    if (swLocation.hasCouponCampaign == '1') {
	    	console.log('start check coupon');
			$.post( "coupon/apis/check", {profile_id : client_info.profile_id}, function( resp ) {
					if (resp && !resp.error) {
						console.log(JSON.stringify(resp));
						var email = "";
						if (resp.email) email = resp.email;
						if (resp.campaign_id > 0) {
							client_info.coupon_campaign_id = resp.campaign_id;
							var html = '<div class="form-inline">'
							  +'<div class="form-group" style="margin-right:5px;">'
							  +'<label class="sr-only" for="coupon-email">Email address</label>'
							  +'<input type="email" class="form-control" id="coupon-email" value="' + email + '">'
							  +'</div>'
							  +'<button type="submit" class="btn btn-default btn-primary" id="coupon-submit">Nhận Coupon</button>'
							+'</div>';
							$('#coupon-content').html( resp.campaign_text + "<br/>" + html);
							$('#coupon-container').show();
						}
					} else {
						console.log("Error: " + JSON.stringify(resp));
					}
			});
		}

}); //end of document ready

$(document).on('click', '#coupon-submit', function() {
    //console.log($('#coupon-email').val());
    //console.log(client_info.coupon_campaign_id);
    if( !validateEmail($('#coupon-email').val())) {
    	$('#coupon-error').html("Invalid email!");
    	return false;
    } else {
    	$('#coupon-error').html("");
    }
    
    if (swLocation.hasCouponCampaign == '1') {
    	$('#coupon-submit').prop('disabled', true);
    	$('#coupon-email').prop('disabled', true);
    	$('#coupon-wait').show();
    	var ajaxdata = {email : $('#coupon-email').val(), campaign_id : client_info.coupon_campaign_id, profile_id: client_info.profile_id};
    	$.post( "coupon/apis/getCoupon", ajaxdata, function( resp ) {
	    	//console.log("json rerurn");
	    	console.log(JSON.stringify(resp));
	    	
	    	if (resp.status == 'success') {
	    		$('#coupon-content').html("Please check your email to get your Coupon. Thank you!");
	    		$('#coupon-email').prop('disabled', false);
	    		$('#coupon-wait').hide();
	    	} else{
	    		$('#coupon-wait').hide();
	    		$('#coupon-error').html('Failed to send. Please check your email and retry!');
	    		$('#coupon-submit').prop('disabled', false);
	    		$('#coupon-email').prop('disabled', false);
	    	};
    	
    	});
    }
});

function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,6})?$/;
  return emailReg.test( $email );
}