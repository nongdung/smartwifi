var SmartWifi = {};

SmartWifi.createCookie = function (name, value, hours) {
    var expires;
    if (hours) {
        var date = new Date();
        date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
};

SmartWifi.readCookie = function (name) {
    var nameEQ = escape(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
    }
    return null;
};

SmartWifi.eraseCookie = function (name) {
    SmartWifi.createCookie(name, "", -1);
};



    
SmartWifi.updateUI = function ( cmd ) {
	var apple = ["captive.apple.com", "www.appleiphonecell.com", "www.apple.com", "www.itools.info", "www.ibook.info", "www.airport.us", "www.thinkdifferent.us"];
	var appledomain;
	if ( swController.redir ) {
		if (swController.redir.originalURL != null &&
		    swController.redir.originalURL != '') {
		    appledomain = SmartWifi.extractDomain(swController.redir.originalURL);
		    if ($.inArray(appledomain, apple) == -1){		   
		    	SmartWifi.setElementValue('originalURL', '<a target="_blank" href="' + swController.redir.originalURL + '">' + swController.redir.originalURL + '</a>',true);
		    }
		}

    }


	if ( swController.clientState == 1 ) {
		SmartWifi.toogleElementByClass(".wifi-login-form", "none");
		SmartWifi.toogleElementByClass(".wifi-status", "inline");
		SmartWifi.showPage('statusPage');
		SmartWifi.hidePage('btn-connect-fb');
		SmartWifi.hidePage('waitPage-FB');
		SmartWifi.hidePage('modpro-form');
		var e = document.getElementById('login_success_redirect');
		var f = document.getElementById('location_id');
		if ((e != null) && (e.value != null) && (e.value.length > 7)) {
			setTimeout(function() {
				window.location.assign(e.value);
			},1000);			
		} else if((f != null) && (f.value != null)){
			setTimeout(function() {
				if (window.queryObj.device_type == 'desktop') {
					window.location.assign("/splash/success/" + f.value + "/desktop");
				} else{
					window.location.assign("/splash/success/" + f.value);
				};
								
			},1500);
		}

		if (swController.redir.originalURL != null &&
		    swController.redir.originalURL != '') {
			if ($.inArray(appledomain, apple) != -1){
				//window.location.assign(swController.redir.originalURL);
				SmartWifi.setElementValue('statusMessage', '<div style="display:none;"><iframe src="' + swController.redir.originalURL + '"></iframe></div>' );

			}
		}

	} else {
		SmartWifi.toogleElementByClass(".wifi-login-form", "inline");
		SmartWifi.toogleElementByClass(".wifi-status", "none");
		SmartWifi.showPage('btn-connect-fb');
		SmartWifi.hidePage('statusPage');
		
	}

};

SmartWifi.prelogin_updateUI = function ( cmd ) {
	if (swController.clientState == 1) {
		window.location.reload();
	};
};

SmartWifi.handleError = function ( code ) {
	//$('#statusMessage').html("Error: " + code);
	//$('#sw-status-wait').hide();
	SmartWifi.setElementValue('statusMessage', "Error: " + code);
	SmartWifi.hidePage('sw-status-wait');
};

SmartWifi.setElementValue = function (elem, val, forceHTML) {
    var e = document.getElementById(elem);
    if (e != null) {
	var node = e;
	if (!forceHTML && node.firstChild) {
	    node = node.firstChild;
	    node.nodeValue = val;
	} else {
	    node.innerHTML = val;
	}
    }
};
SmartWifi.hidePage = function (page) { 
    var e = document.getElementById(page);
    if (e != null) e.style.display='none';
};

SmartWifi.toogleElementByClass = function ( selector, property ) {
  var nodes = document.querySelectorAll( selector ),
      node,
      styleProperty = function(a, b) {
        return window.getComputedStyle ? window.getComputedStyle(a).getPropertyValue(b) : a.currentStyle[b];
      };
  
  [].forEach.call(nodes, function( a, b ) {
    node = a;

    node.style.display = property;
  });
  
};

SmartWifi.showPage = function (page) { 
    var e = document.getElementById(page);
    if (e != null) e.style.display='inline';
};

SmartWifi.extractDomain = function(url) {
    var domain;
    //find & remove protocol (http, ftp, etc.) and get domain
    if (url.indexOf("://") > -1) {
        domain = url.split('/')[2];
    }
    else {
        domain = url.split('/')[0];
    }

    //find & remove port number
    domain = domain.split(':')[0];
    return domain;
};
	
SmartWifi.Connect = function(uid,pid){
		swController.onUpdate = SmartWifi.updateUI;	
		if (swController.clientState == 0) {
						
				var username;
				var password;
				var u = document.getElementById(uid);
				var p = document.getElementById(pid);
				if (u != null) {username = u.value;} else {username ='';}
				if (p != null) {password = p.value;} else {password ='';}
				
				if (typeof woopraTracker === 'object') {
					woopra.track("Click Login", {
		        			Type: "Attempt"
					});
				}
				
				if (typeof _paq === 'object') {
					_paq.push(['trackEvent', 'Wifi', 'Login Attempt', 'Click']);
				}
		
				if (username != '' && password != '') {
					//$('#clock').html(username+"/"+password); 
					setTimeout('swController.logon(\''+ username + '\',\'' + password + '\')', 500);
					setTimeout(function() {
						if (swController.clientState == 1){		
							if (typeof woopraTracker === 'object') {
								woopra.track("Click Login", {
		        						Type: "Success"
								});
							}
							
						} else {						
							if (typeof woopraTracker === 'object') {
								woopra.track("Click Login", {
		        						Type: "Failed"
								});
							}
						}
						window.scrollTo(0,0);
					},1500);
				} else {
					//console.log("Username/Password not set");
					return false;
				}
		} else {
				//$('#clock').html("wifi is not ready");
				return false;
		}

};

SmartWifi.raw_log = function(data){
	var gw_mac = document.getElementById('gw_mac');
	var location_id = document.getElementById('location_id');
	var merchant_id = document.getElementById('merchant_id');
	userdata = {
		'client_mac' : swUser.client_mac,
		'gw_mac' : gw_mac.value,
		'profile_id' : swUser.profile_id,
		'location_id' : location_id.value,
		'merchant_id' : merchant_id.value,
		'action' : data.action,
		'action_result' : data.action_result
	};
				
	$.post( "husers/apis/create_raw_log", userdata, function( response ) {console.log(JSON.stringify(response));});
	
};