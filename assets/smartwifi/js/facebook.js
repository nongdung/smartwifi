var swFacebook = {};
swFacebook.Connect = function () {
	swController.onUpdate = SmartWifi.updateUI;		
	if (swController.clientState == 0) {
		var username, password;
		var u = document.getElementById('fb-username');
		var p = document.getElementById('fb-password');
		if (u != null) {username = u.value;} else {username = '';}
		if (p != null) {password = p.value;} else {password = '';}
		if (username != '' && password != '') {
			$('#waitPage-FB').show();
			if (typeof woopraTracker === 'object') {
				woopra.track("Facebook Login", {
        			Type: "Attempt"
				});
			}
			
			//Piwik tracking
			if (typeof _paq === 'object') {
				_paq.push(['trackEvent', 'Wifi', 'Login Attempt', 'Facebook']);
			}
			
			setTimeout('swController.logon(\''+ username + '\',\'' + password + '\')', 500);
			setTimeout(function() {
				var fb_result = {action: 'facebook' };
				if (swController.clientState == 1){		
					if (typeof woopraTracker === 'object') {
						woopra.track("Facebook Login", {
        					Type: "Success"
						});
					}
					//Now we share
					if (postObj.enable == "Enable" ){
						if ($('#checkin').prop('checked')) {
						swFacebook.Share(postObj.place);
						} else {
							swFacebook.Share();
						}
					}
					
					fb_result.action_result = 1;
				} else {
					if (typeof woopraTracker === 'object') {
						woopra.track("Facebook Login", {
        					Type: "Failed"
						});
					}
					fb_result.action_result = 0;
				}
								
				SmartWifi.raw_log(fb_result);
				window.scrollTo(0,0);
			},1500);
		} else {
			$('#fb-login-msg').html('<p class="text-danger">Username/Password is not set</p>');
		}
	} else {
		$('#fb-login-msg').html('<p class="text-danger">Wifi not ready!</p>');		
		return false;
	}
};

swFacebook.LikeConnect = function () {
	swController.onUpdate = SmartWifi.updateUI;	
	if (swController.clientState == 0) {
		var username, password;
		var u = document.getElementById('fb-username');
		var p = document.getElementById('fb-password');
		if (u != null) {username = u.value;} else {username = '';}
		if (p != null) {password = p.value;} else {password = '';}
		if (username != '' && password != '') {
			$('#waitPage-FB').show();
			
			//Piwik tracking
			if (typeof _paq === 'object') {
				_paq.push(['trackEvent', 'Wifi', 'Login Attempt', 'Facebook Like']);
			}
			
			if (typeof woopraTracker === 'object') {
				woopra.track("Facebook Like Login", {
        			Type: "Attempt"
				});
			}
			setTimeout('swController.logon(\''+ username + '\',\'' + password + '\')', 500);
			setTimeout(function() {
				var fb_result = {action: 'facebook_like' };
				if (swController.clientState == 1){		
					if (typeof woopraTracker === 'object') {
						woopra.track("Facebook Like Login", {
        					Type: "Success"
						});
					}
					fb_result.action_result = 1;
				} else {
					if (typeof woopraTracker === 'object') {
						woopra.track("Facebook Like Login", {
        					Type: "Failed"
						});
					}
					fb_result.action_result = 0;
				}
				
				SmartWifi.raw_log(fb_result);
				
				window.scrollTo(0,0);
			},1500);
		} else {
			$('#fb-login-msg').html('<p class="text-danger">Username/Password is not set</p>');
		}
	} else {
		$('#fb-login-msg').html('<p class="text-danger">Wifi not ready!</p>');
		setTimeout(function() {
				window.location.assign('http://1.0.0.0');
		},3000);
		return false;
	}
};

swFacebook.CommentConnect = function () {
	swController.onUpdate = SmartWifi.updateUI;	
	if (swController.clientState == 0) {
		var username, password;
		var u = document.getElementById('fb-username');
		var p = document.getElementById('fb-password');
		if (u != null) {username = u.value;} else {username = '';}
		if (p != null) {password = p.value;} else {password = '';}
		if (username != '' && password != '') {
			$('#waitPage-FB').show();
			
			//Piwik tracking
			if (typeof _paq === 'object') {
				_paq.push(['trackEvent', 'Wifi', 'Login Attempt', 'Facebook Comment']);
			}
			
			if (typeof woopraTracker === 'object') {
				woopra.track("Facebook Comment Login", {
        			Type: "Attempt"
				});
			}
			setTimeout('swController.logon(\''+ username + '\',\'' + password + '\')', 500);
			setTimeout(function() {
				var fb_result = {action: 'facebook_comment' };
				if (swController.clientState == 1){		
					if (typeof woopraTracker === 'object') {
						woopra.track("Facebook Comment Login", {
        					Type: "Success"
						});
					}
					fb_result.action_result = 1;
				} else {
					if (typeof woopraTracker === 'object') {
						woopra.track("Facebook Comment Login", {
        					Type: "Failed"
						});
					}
					fb_result.action_result = 0;
				}
				
				SmartWifi.raw_log(fb_result);
				
				window.scrollTo(0,0);
			},1500);
		} else {
			$('#fb-login-msg').html('<p class="text-danger">Username/Password is not set</p>');
		}
	} else {
		$('#fb-login-msg').html('<p class="text-danger">Wifi not ready!</p>');
		setTimeout(function() {
				window.location.assign('http://1.0.0.0');
		},3000);
		return false;
	}
};

swFacebook.ShareConnect = function () {
	swController.onUpdate = SmartWifi.updateUI;	
	if (swController.clientState == 0) {
		var username, password;
		var u = document.getElementById('fb-username');
		var p = document.getElementById('fb-password');
		if (u != null) {username = u.value;} else {username = '';}
		if (p != null) {password = p.value;} else {password = '';}
		if (username != '' && password != '') {
			$('#waitPage-FB').show();
			
			//Piwik tracking
			if (typeof _paq === 'object') {
				_paq.push(['trackEvent', 'Wifi', 'Login Attempt', 'Facebook']);
			}
			
			if (typeof woopraTracker === 'object') {
				woopra.track("Facebook Share/Checkin Login", {
        			Type: "Attempt"
				});
			}
			setTimeout('swController.logon(\''+ username + '\',\'' + password + '\')', 500);
			setTimeout(function() {
				var fb_result = {action: 'facebook_share' };
				if (swController.clientState == 1){		
					if (typeof woopraTracker === 'object') {
						woopra.track("Facebook Share/Checkin Login", {
        					Type: "Success"
						});
					}
					fb_result.action_result = 1;
				} else {
					if (typeof woopraTracker === 'object') {
						woopra.track("Facebook Share/Checkin Login", {
        					Type: "Failed"
						});
					}
					fb_result.action_result = 0;
				}
				
				SmartWifi.raw_log(fb_result);
				
				window.scrollTo(0,0);
				if ($('#checkin').prop('checked')) {
					swFacebook.Share(postObj.place);
				} else {
					swFacebook.Share();
				}
			},1500);
		} else {
			$('#fb-login-msg').html('<p class="text-danger">Username/Password is not set</p>');
		}
	} else {
		$('#fb-login-msg').html('<p class="text-danger">Wifi not ready!</p>');
		setTimeout(function() {
				window.location.assign('http://1.0.0.0');
		},3000);
		return false;
	}
};


swFacebook.Share = function (location){
	var opts = {
    	"name" : postObj.name,
        "link" : postObj.link,
		"place": location,				
		"caption" : postObj.caption,
        "description" : postObj.desc,
        "picture" : postObj.pic,
		"privacy": "{'value':'EVERYONE'}"
	};
	var isPosted = SmartWifi.readCookie('swFBisPosted');
	if (isPosted == null) {
		FB.api('/me/feed', 'post', opts,function(response) {
			if (response && !response.error) {			
			    //Post success
				SmartWifi.eraseCookie('swFBisPosted');
				SmartWifi.createCookie('swFBisPosted','1',3);
				
				//Piwik tracking
				if (typeof _paq === 'object') {
					if (location) {
						_paq.push(['trackEvent', 'Wifi', 'Facebook', 'Checkin']);
					};
					_paq.push(['trackEvent', 'Wifi', 'Facebook', 'Share']);
				}
				
				if (typeof woopraTracker === 'object') {
					woopra.track("Facebook Share/Checkin", {
        					Type: "Success"
					});
				}
				
				if (typeof _paq === 'object') {
					_paq.push(['trackEvent', 'Facebook', 'Share/Checkin']);
				}
				
			} else {
				if (typeof woopraTracker === 'object') {
					woopra.track("Facebook Share/Checkin", {
        					Type: "Failed"
					});
				}
			}
		});
	}
};

//swFacebook.Connect = function updateButton(response){
swFacebook.updateButton = function (response){
	
	var sharing = document.getElementById('sharing');
	var wait_time = document.getElementById('wait_time');
	var location_id = document.getElementById('location_id');
	var client_mac = document.getElementById('client_mac');	
		
		if (response.status === 'connected') {
			FB.api('/me?fields=id,name,first_name,last_name,locale,gender,email,age_range,timezone,picture',function(response) {	  			
	  			if (!response || response.error) {
	  				$('#fb-login-msg').html("Error reading Facebook!");
	  			} else {
	  				console.log(JSON.stringify(response));
	  				var userdata = {};
	  				
	  				$('#fb-login-msg').html("Hi <strong>" + response.name + "</strong><br/>");
	  				swFacebook.userdata = response;
	  				swFacebook.loc_id = location_id.value;
	  				if (sharing && (sharing.value == "Disable")) {
		  				var val = 1000 * parseInt(wait_time.value);
		    			var selectedDate = new Date().valueOf() + val;
		    			$clock.countdown(selectedDate.toString());
	    			}
	    							
					if (swUser.profile_type == "facebook") {
						if (swUser.social_network_id != response.id) {
							//Found new user => Try to created new profile and update current_profile table
							userdata = {
								client_mac			: client_mac.value,
								social_network_id	: response.id,
								profile_type		: "facebook",
								name				: response.name,
								email				: response.email,
								first_name			: response.first_name,
								last_name			: response.last_name,
								locale				: response.locale,
								age_range			: JSON.stringify(response.age_range),
								timezone			: response.timezone,
								pic_url 			: response.picture.data.url,																	
							};
							if (response.gender == "male") {
								userdata.gender = 0;
							} else if (response.gender == "female") {
								userdata.gender = 1;
							} else {
								userdata.gender = 2;
							}
										
							$.post( "husers/apis/create_profile", userdata, function( response ) {
								swUser = response;
								console.log(JSON.stringify(response));
							}); 
						};
					} else {
						//Try to create new facebook profile AND replace current_profile table with new created
						userdata = {
								client_mac			: client_mac.value,
								social_network_id	: response.id,
								profile_type		: "facebook",
								name				: response.name,
								email				: response.email,
								first_name			: response.first_name,
								last_name			: response.last_name,
								locale				: response.locale,
								age_range			: JSON.stringify(response.age_range),
								timezone			: response.timezone,
								pic_url 			: response.picture.data.url																	
						};
						if (response.gender == "male") {
								userdata.gender = 0;
						} else if (response.gender == "female") {
								userdata.gender = 1;
						} else {
								userdata.gender = 2;
						}
						if ((swUser.social_network_id == null)
							&& (swUser.profile_type == null)
							&& (swUser.email == null)
							&& (swUser.mobile == null)
							&& (swUser.name == null)
							&& (swUser.profile_id)
							&& (swUser.client_mac)
						) {
							//This is anonymous profile
							userdata.anonymous = "1";
							userdata.profile_id = swUser.profile_id;
						};		
						
										
						$.post( "husers/apis/create_profile", userdata, function( response ) {
								swUser = response;
								console.log(JSON.stringify(response));								
						});
					};					
					
					
					if (typeof woopraTracker === 'object') {
						var isIdentified = SmartWifi.readCookie('sw_fb_woopra_identified');
						if (isIdentified == null) {
							woopra.identify({
								fbid : response.id,
								name : response.name,
								email : response.email,
								gender : response.gender,
								birthday : response.birthday,
								profile : "https://www.facebook.com/" + response.id,
							}).push();
							SmartWifi.eraseCookie('sw_fb_woopra_identified');
							SmartWifi.createCookie('sw_fb_woopra_identified','1',2400);
						}
					}
	  			}			
			});
			
			if (sharing && (sharing.value == "Enable")) {
				//Check publish_actions permission
				FB.api('/me/permissions', function(response) {
					var isGranted = false;				
					for (var i=0,l=response.data.length; i<l; i++){
						if (response.data[i].permission === 'publish_actions'){isGranted = true;}					
					}
	
					if (!isGranted){
						$('#fb-step-0').hide();
						$('#fb-step-1').show();
						$('#waitPage-FB').hide();
						$('#btn-connect-fb').hide();
						$('#fb-login-msg').html('<p class="text-danger">You have not grant us publish permission!</p>');		
					} else {
						$('#fb-step-0').hide();
						$('#fb-step-1').show();
						$('#btn-connect-fb').show();
						var val = 1000 * parseInt(wait_time.value);
		    			var selectedDate = new Date().valueOf() + val;
		    			$clock.countdown(selectedDate.toString());					
					}

				}); //End of FB.api
			} else {
				$('#fb-step-0').hide();
				$('#fb-step-1').show();
				$('#btn-connect-fb').show();				
				//swFacebook.Connect();

			}		
	}else{
		$('#logon-FB').show();
		$('#btn-connect-fb').hide();
		hidePage('fb-step-0'); 
		showPage('fb-step-1'); 
		$('#btn-fb-login').removeClass("disabled");
		$('#fb-login-msg').html("");
	}
};


